#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_module_response_construct (void)
{
  g_autoptr (ApexModuleResponse) module_response = NULL;

  module_response = apex_module_response_new ();

  g_assert_nonnull (module_response);
}

static void
test_module_response_deserialize (void)
{
  g_autoptr (ApexModuleResponse) module_response = NULL;

  module_response = apex_module_response_new ();

  g_assert_nonnull (module_response);
}

static void
test_module_response_serialize (void)
{
  g_autoptr (ApexModuleResponse) module_response = NULL;

  module_response = apex_module_response_new ();

  g_assert_nonnull (module_response);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/ModuleResponse/construct", test_module_response_construct);
  g_test_add_func ("/ModuleResponse/deserialize", test_module_response_deserialize);
  g_test_add_func ("/ModuleResponse/serialize", test_module_response_serialize);

  return g_test_run ();
}
