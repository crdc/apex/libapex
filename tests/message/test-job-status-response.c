#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_job_status_response_construct (void)
{
  g_autoptr (ApexJobStatusResponse) job_status_response = NULL;

  job_status_response = apex_job_status_response_new ();

  g_assert_nonnull (job_status_response);
}

static void
test_job_status_response_deserialize (void)
{
  g_autoptr (ApexJobStatusResponse) job_status_response = NULL;

  job_status_response = apex_job_status_response_new ();

  g_assert_nonnull (job_status_response);
}

static void
test_job_status_response_serialize (void)
{
  g_autoptr (ApexJobStatusResponse) job_status_response = NULL;

  job_status_response = apex_job_status_response_new ();

  g_assert_nonnull (job_status_response);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/JobStatusResponse/construct", test_job_status_response_construct);
  g_test_add_func ("/JobStatusResponse/deserialize", test_job_status_response_deserialize);
  g_test_add_func ("/JobStatusResponse/serialize", test_job_status_response_serialize);

  return g_test_run ();
}
