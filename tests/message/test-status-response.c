#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_status_response_construct (void)
{
  g_autoptr (ApexStatusResponse) status_response = NULL;

  status_response = apex_status_response_new ();

  g_assert_nonnull (status_response);
}

static void
test_status_response_deserialize (void)
{
  g_autoptr (ApexStatusResponse) status_response = NULL;

  status_response = apex_status_response_new ();

  g_assert_nonnull (status_response);
}

static void
test_status_response_serialize (void)
{
  g_autoptr (ApexStatusResponse) status_response = NULL;

  status_response = apex_status_response_new ();

  g_assert_nonnull (status_response);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/StatusResponse/construct", test_status_response_construct);
  g_test_add_func ("/StatusResponse/deserialize", test_status_response_deserialize);
  g_test_add_func ("/StatusResponse/serialize", test_status_response_serialize);

  return g_test_run ();
}
