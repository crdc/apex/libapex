#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_settings_response_construct (void)
{
  g_autoptr (ApexSettingsResponse) object = NULL;

  object = apex_settings_response_new ();

  g_assert_nonnull (object);
}

static const gchar *json = "{ \
  \"id\" : \"0xDEADBEEF\", \
  \"settings\" : { \
    \"a\" : \"b\" \
  } \
}";

static void
test_settings_response_json_serialize (void)
{
  g_autoptr (ApexSettingsResponse) object = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  object = apex_settings_response_new ();
  g_assert_nonnull (object);

  apex_settings_response_set_id (object, "0xDEADBEEF");
  apex_settings_response_add_setting (object, "a", "b");

  g_assert_cmpstr (apex_settings_response_get_id (object), ==, "0xDEADBEEF");
  g_assert_cmpstr (apex_settings_response_get_setting (object, "a"), ==, "b");

  data = apex_settings_response_serialize (object);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_settings_response_json_deserialize (void)
{
  g_autoptr (ApexSettingsResponse) object = NULL;
  g_autofree gchar *id = NULL;
  g_autofree gchar *setting = NULL;

  object = apex_settings_response_new ();

  g_assert_nonnull (object);

  apex_settings_response_deserialize (object, json);

  id = g_strdup (apex_settings_response_get_id (object));
  setting = g_strdup (apex_settings_response_get_setting (object, "a"));

  g_assert_cmpstr (id, ==, "0xDEADBEEF");
  /*g_assert_true (apex_settings_response_has_setting (object, "a"));*/
  g_assert_cmpstr (setting, ==, "b");

  if (g_test_verbose ())
    g_print ("\n%s\n", apex_settings_response_serialize (object));
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/SettingsResponse/construct", test_settings_response_construct);
  g_test_add_func ("/SettingsResponse/json_serialize", test_settings_response_json_serialize);
  g_test_add_func ("/SettingsResponse/json_deserialize", test_settings_response_json_deserialize);

  return g_test_run ();
}
