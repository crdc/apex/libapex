#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_channel_request_construct (void)
{
  g_autoptr (ApexChannelRequest) channel_request = NULL;

  channel_request = apex_channel_request_new ();

  g_assert_nonnull (channel_request);
}

static void
test_channel_request_deserialize (void)
{
  g_autoptr (ApexChannelRequest) channel_request = NULL;

  channel_request = apex_channel_request_new ();

  g_assert_nonnull (channel_request);
}

static void
test_channel_request_serialize (void)
{
  g_autoptr (ApexChannelRequest) channel_request = NULL;

  channel_request = apex_channel_request_new ();

  g_assert_nonnull (channel_request);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/ChannelRequest/construct", test_channel_request_construct);
  g_test_add_func ("/ChannelRequest/deserialize", test_channel_request_deserialize);
  g_test_add_func ("/ChannelRequest/serialize", test_channel_request_serialize);

  return g_test_run ();
}
