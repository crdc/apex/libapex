#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_property_request_construct (void)
{
  g_autoptr (ApexPropertyRequest) object = NULL;

  object = apex_property_request_new ("service-id", "prop-key", "prop-value");

  g_assert_nonnull (object);
  g_assert_cmpstr (apex_property_request_get_id (object), ==, "service-id");
  g_assert_cmpstr (apex_property_request_get_key (object), ==, "prop-key");
  g_assert_cmpstr (apex_property_request_get_value (object), ==, "prop-value");

  apex_property_request_set_id (object, "new-id");
  apex_property_request_set_key (object, "new-key");
  apex_property_request_set_value (object, "new-value");

  g_assert_cmpstr (apex_property_request_get_id (object), ==, "new-id");
  g_assert_cmpstr (apex_property_request_get_key (object), ==, "new-key");
  g_assert_cmpstr (apex_property_request_get_value (object), ==, "new-value");
}

static const gchar *json = "{" \
  "\"id\": \"service-id\"," \
  "\"key\": \"prop-key\"," \
  "\"value\": \"prop-value\"" \
"}";

static void
test_property_request_json_serialize (void)
{
  g_autoptr (ApexPropertyRequest) request = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  request = apex_property_request_new ("service-id", "prop-key", "prop-value");

  data = apex_property_request_serialize (request);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_property_request_json_deserialize (void)
{
  g_autoptr (ApexPropertyRequest) request = NULL;

  request = apex_property_request_new (NULL, NULL, NULL);

  apex_property_request_deserialize (request, json);

  g_assert_cmpstr (apex_property_request_get_id (request), ==, "service-id");
  g_assert_cmpstr (apex_property_request_get_key (request), ==, "prop-key");
  g_assert_cmpstr (apex_property_request_get_value (request), ==, "prop-value");
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/PropertyRequest/construct", test_property_request_construct);
  g_test_add_func ("/PropertyRequest/json-serialize", test_property_request_json_serialize);
  g_test_add_func ("/PropertyRequest/json-deserialize", test_property_request_json_deserialize);

  return g_test_run ();
}
