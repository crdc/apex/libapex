#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_job_response_construct (void)
{
  g_autoptr (ApexJobResponse) job_response = NULL;

  job_response = apex_job_response_new ();

  g_assert_nonnull (job_response);
}

static void
test_job_response_deserialize (void)
{
  g_autoptr (ApexJobResponse) job_response = NULL;

  job_response = apex_job_response_new ();

  g_assert_nonnull (job_response);
}

static void
test_job_response_serialize (void)
{
  g_autoptr (ApexJobResponse) job_response = NULL;

  job_response = apex_job_response_new ();

  g_assert_nonnull (job_response);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/JobResponse/construct", test_job_response_construct);
  g_test_add_func ("/JobResponse/deserialize", test_job_response_deserialize);
  g_test_add_func ("/JobResponse/serialize", test_job_response_serialize);

  return g_test_run ();
}
