#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_module_job_request_construct (void)
{
  g_autoptr (ApexModuleJobRequest) object = NULL;
  g_autoptr (ApexProperty) prop1 = NULL;
  g_autoptr (ApexProperty) prop2 = NULL;

  object = apex_module_job_request_new ();
  prop1 = apex_property_new ("prop-key1", "prop-val1");
  prop2 = apex_property_new ("prop-key2", "prop-val2");

  g_assert_nonnull (object);

  apex_module_job_request_set_id (object, "req-id");
  apex_module_job_request_set_job_id (object, "job-id");
  apex_module_job_request_set_job_value (object, "job-value");

  g_assert_cmpstr (apex_module_job_request_get_id (object), ==, "req-id");
  g_assert_cmpstr (apex_module_job_request_get_job_id (object), ==, "job-id");
  g_assert_cmpstr (apex_module_job_request_get_job_value (object), ==, "job-value");

  apex_module_job_request_set_id (object, "new-id");
  apex_module_job_request_set_job_id (object, "new-job-id");
  apex_module_job_request_set_job_value (object, "new-value");

  g_assert_cmpstr (apex_module_job_request_get_id (object), ==, "new-id");
  g_assert_cmpstr (apex_module_job_request_get_job_id (object), ==, "new-job-id");
  g_assert_cmpstr (apex_module_job_request_get_job_value (object), ==, "new-value");

  g_assert_nonnull (prop1);
  g_assert_nonnull (prop2);

  apex_module_job_request_add (object, prop1);
  apex_module_job_request_add (object, prop2);

  g_assert_true (apex_module_job_request_contains (object, "prop-key1"));
  g_assert_true (apex_module_job_request_contains (object, "prop-key2"));

  apex_module_job_request_remove (object, "prop-key2");

  g_assert_true (!apex_module_job_request_contains (object, "prop-key2"));

  {
    g_autoptr (ApexProperty) prop = NULL;

    prop = apex_module_job_request_get (object, "prop-key1");

    g_assert_cmpstr (apex_property_get_key (prop), ==, "prop-key1");
    g_assert_cmpstr (apex_property_get_value (prop), ==, "prop-val1");
  }

  if (g_test_verbose ())
    g_print ("\n%s\n", apex_module_job_request_serialize (object));
}

static const gchar *json = "{ \
  \"id\": \"0xDEADBEEF\", \
  \"jobId\": \"foo\", \
  \"jobValue\": \"foo\", \
  \"jobProperties\": [ \
    { \"key\": \"baz\", \"value\": \"meh\" }, \
    { \"key\": \"foo\", \"value\": \"bar\" } \
  ] \
}";

static void
test_module_job_request_serialize (void)
{
  g_autoptr (ApexModuleJobRequest) object = NULL;
  g_autoptr (ApexProperty) prop1 = NULL;
  g_autoptr (ApexProperty) prop2 = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  object = apex_module_job_request_new ();
  g_assert_nonnull (object);

  apex_module_job_request_set_id (object, "0xDEADBEEF");
  apex_module_job_request_set_job_id (object, "foo");
  apex_module_job_request_set_job_value (object, "foo");
  prop1 = apex_property_new ("foo", "bar");
  prop2 = apex_property_new ("baz", "meh");
  apex_module_job_request_add (object, prop1);
  apex_module_job_request_add (object, prop2);

  g_assert_cmpstr (apex_module_job_request_get_id (object), == , "0xDEADBEEF");
  g_assert_cmpstr (apex_module_job_request_get_job_id (object), == , "foo");
  g_assert_cmpstr (apex_module_job_request_get_job_value (object), == , "foo");

  data = apex_module_job_request_serialize (object);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_module_job_request_deserialize (void)
{
  g_autoptr (ApexModuleJobRequest) object = NULL;

  object = apex_module_job_request_new ();
  g_assert_nonnull (object);
  apex_module_job_request_deserialize (object, json);

  g_assert_cmpstr (apex_module_job_request_get_id (object), ==, "0xDEADBEEF");
  g_assert_cmpstr (apex_module_job_request_get_job_id (object), ==, "foo");
  g_assert_cmpstr (apex_module_job_request_get_job_value (object), ==, "foo");
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/ModuleJobRequest/construct", test_module_job_request_construct);
  g_test_add_func ("/ModuleJobRequest/serialize", test_module_job_request_serialize);
  g_test_add_func ("/ModuleJobRequest/deserialize", test_module_job_request_deserialize);

  return g_test_run ();
}
