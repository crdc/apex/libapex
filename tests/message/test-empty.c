#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_empty_construct (void)
{
  g_autoptr (ApexEmpty) empty = NULL;

  empty = apex_empty_new ();

  g_assert_nonnull (empty);
}

static void
test_empty_deserialize (void)
{
  g_autoptr (ApexEmpty) empty = NULL;

  empty = apex_empty_new ();

  g_assert_nonnull (empty);
}

static void
test_empty_serialize (void)
{
  g_autoptr (ApexEmpty) empty = NULL;

  empty = apex_empty_new ();

  g_assert_nonnull (empty);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/Empty/construct", test_empty_construct);
  g_test_add_func ("/Empty/deserialize", test_empty_deserialize);
  g_test_add_func ("/Empty/serialize", test_empty_serialize);

  return g_test_run ();
}
