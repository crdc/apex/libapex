#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_property_response_construct (void)
{
  g_autoptr (ApexPropertyResponse) object = NULL;
  g_autoptr (ApexProperty) prop = NULL;

  object = apex_property_response_new ();
  prop = apex_property_new ("prop-key", "prop-val");

  g_assert_nonnull (object);
  g_assert_nonnull (prop);

  apex_property_response_set (object, prop);

  {
    g_autoptr (ApexProperty) p = NULL;

    p = apex_property_response_get (object);

    g_assert_nonnull (p);
    g_assert_cmpstr (apex_property_get_key (p), ==, "prop-key");
    g_assert_cmpstr (apex_property_get_value (p), ==, "prop-val");
  }
}

static const gchar *json = "{" \
  "\"property\": { \"key\": \"prop-key\", \"value\": \"prop-val\" }" \
"}";

static void
test_property_response_json_serialize (void)
{
  {
    g_autoptr (ApexPropertyResponse) response = NULL;
    g_autoptr (ApexProperty) prop = NULL;
    g_autofree gchar *data = NULL;
    g_autofree gchar *data1 = NULL;
    g_autofree gchar *data2 = NULL;

    response = apex_property_response_new ();
    prop = apex_property_new ("prop-key", "prop-val");

    apex_property_response_set (response, prop);

    data = apex_property_response_serialize (response);
    data1 = remove_whitespace (json);
    data2 = remove_whitespace (data);

    g_assert_cmpstr (data1, ==, data2);
  }

  {
    g_autoptr (ApexPropertyResponse) response = NULL;
    g_autoptr (ApexMessageError) error = NULL;
    g_autofree gchar *data = NULL;

    response = apex_property_response_new ();
    error = apex_message_error_new ();
    apex_message_error_set_code (error, 404);
    apex_message_error_set_message (error, "not found");
    apex_response_set_error (APEX_RESPONSE (response), error);
    data = apex_property_response_serialize (response);
    if (g_test_verbose ())
      g_print ("\n%s\n", data);
  }
}

static void
test_property_response_json_deserialize (void)
{
  g_autoptr (ApexPropertyResponse) response = NULL;
  g_autoptr (ApexProperty) prop = NULL;

  response = apex_property_response_new ();
  apex_property_response_deserialize (response, json);

  prop = apex_property_response_get (response);

  g_assert_nonnull (prop);

  g_assert_cmpstr (apex_property_get_key (prop), ==, "prop-key");
  g_assert_cmpstr (apex_property_get_value (prop), ==, "prop-val");
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/PropertyResponse/construct", test_property_response_construct);
  g_test_add_func ("/PropertyResponse/json-serialize", test_property_response_json_serialize);
  g_test_add_func ("/PropertyResponse/json-deserialize", test_property_response_json_deserialize);

  return g_test_run ();
}
