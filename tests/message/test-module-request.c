#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_module_request_construct (void)
{
  g_autoptr (ApexModuleRequest) module_request = NULL;

  module_request = apex_module_request_new ();

  g_assert_nonnull (module_request);
}

static void
test_module_request_deserialize (void)
{
  g_autoptr (ApexModuleRequest) module_request = NULL;

  module_request = apex_module_request_new ();

  g_assert_nonnull (module_request);
}

static void
test_module_request_serialize (void)
{
  g_autoptr (ApexModuleRequest) module_request = NULL;

  module_request = apex_module_request_new ();

  g_assert_nonnull (module_request);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/ModuleRequest/construct", test_module_request_construct);
  g_test_add_func ("/ModuleRequest/deserialize", test_module_request_deserialize);
  g_test_add_func ("/ModuleRequest/serialize", test_module_request_serialize);

  return g_test_run ();
}
