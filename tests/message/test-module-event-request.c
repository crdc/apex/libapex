#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_module_event_request_construct (void)
{
  g_autoptr (ApexModuleEventRequest) module_event_request = NULL;

  module_event_request = apex_module_event_request_new ();

  g_assert_nonnull (module_event_request);
}

static void
test_module_event_request_deserialize (void)
{
  g_autoptr (ApexModuleEventRequest) module_event_request = NULL;

  module_event_request = apex_module_event_request_new ();

  g_assert_nonnull (module_event_request);
}

static void
test_module_event_request_serialize (void)
{
  g_autoptr (ApexModuleEventRequest) module_event_request = NULL;

  module_event_request = apex_module_event_request_new ();

  g_assert_nonnull (module_event_request);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/ModuleEventRequest/construct", test_module_event_request_construct);
  g_test_add_func ("/ModuleEventRequest/deserialize", test_module_event_request_deserialize);
  g_test_add_func ("/ModuleEventRequest/serialize", test_module_event_request_serialize);

  return g_test_run ();
}
