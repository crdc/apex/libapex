#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_response_construct (void)
{
  g_autoptr (ApexResponse) response = NULL;

  response = apex_response_new ();

  g_assert_nonnull (response);
}

static void
test_response_serialize (void)
{
  g_autoptr (ApexResponse) response = NULL;
  g_autoptr (ApexMessageError) error = NULL;
  g_autofree gchar *data = NULL;

  response = apex_response_new ();
  error = apex_message_error_new ();

  g_assert_nonnull (response);
  g_assert_nonnull (error);

  apex_message_error_set_code (error, 200);
  apex_message_error_set_message (error, "things are borked");
  apex_response_set_error (response, error);

  data = apex_response_serialize (response);

  if (g_test_verbose ())
    g_print ("%s\n", data);

  {
    g_autoptr (ApexMessageError) err = NULL;

    err = apex_response_get_error (response);
    g_assert_cmpstr (apex_message_error_get_message (err), ==, "things are borked");
    g_assert_cmpint (apex_message_error_get_code (err), ==, 200);
  }
}

static void
test_response_deserialize (void)
{
  g_autoptr (ApexResponse) response = NULL;

  response = apex_response_new ();

  g_assert_nonnull (response);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/Response/construct", test_response_construct);
  g_test_add_func ("/Response/serialize", test_response_serialize);
  g_test_add_func ("/Response/deserialize", test_response_deserialize);

  return g_test_run ();
}
