#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_configurations_response_construct (void)
{
  g_autoptr (ApexConfigurationsResponse) configurations_response = NULL;

  configurations_response = apex_configurations_response_new ();

  g_assert_nonnull (configurations_response);
}

static void
test_configurations_response_deserialize (void)
{
  g_autoptr (ApexConfigurationsResponse) configurations_response = NULL;

  configurations_response = apex_configurations_response_new ();

  g_assert_nonnull (configurations_response);
}

static void
test_configurations_response_serialize (void)
{
  g_autoptr (ApexConfigurationsResponse) configurations_response = NULL;

  configurations_response = apex_configurations_response_new ();

  g_assert_nonnull (configurations_response);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/ConfigurationsResponse/construct", test_configurations_response_construct);
  g_test_add_func ("/ConfigurationsResponse/deserialize", test_configurations_response_deserialize);
  g_test_add_func ("/ConfigurationsResponse/serialize", test_configurations_response_serialize);

  return g_test_run ();
}
