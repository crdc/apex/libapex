#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_properties_response_construct (void)
{
  g_autoptr (ApexPropertiesResponse) object = NULL;
  g_autoptr (ApexProperty) prop1 = NULL;
  g_autoptr (ApexProperty) prop2 = NULL;

  object = apex_properties_response_new ();
  prop1 = apex_property_new ("prop-key1", "prop-val1");
  prop2 = apex_property_new ("prop-key2", "prop-val2");

  g_assert_nonnull (object);
  g_assert_nonnull (prop1);
  g_assert_nonnull (prop2);

  apex_properties_response_add (object, prop1);
  apex_properties_response_add (object, prop2);

  g_assert_true (apex_properties_response_contains (object, "prop-key1"));
  g_assert_true (apex_properties_response_contains (object, "prop-key2"));

  apex_properties_response_remove (object, "prop-key2");

  g_assert_true (!apex_properties_response_contains (object, "prop-key2"));

  {
    g_autoptr (ApexProperty) prop = NULL;

    prop = apex_properties_response_get (object, "prop-key1");

    g_assert_cmpstr (apex_property_get_key (prop), ==, "prop-key1");
    g_assert_cmpstr (apex_property_get_value (prop), ==, "prop-val1");
  }
}

static const gchar *json = "{" \
  "\"error\":{\"message\":\"thingsareborked\",\"code\":200}," \
  "\"properties\": [" \
    "{ \"key\": \"prop-key2\", \"value\": \"prop-val2\" }," \
    "{ \"key\": \"prop-key1\", \"value\": \"prop-val1\" }" \
  "]" \
"}";


static void
test_properties_response_json_serialize (void)
{
  g_autoptr (ApexPropertiesResponse) response = NULL;
  g_autoptr (ApexProperty) prop1 = NULL;
  g_autoptr (ApexProperty) prop2 = NULL;
  g_autoptr (ApexMessageError) error = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  response = apex_properties_response_new ();
  prop1 = apex_property_new ("prop-key1", "prop-val1");
  prop2 = apex_property_new ("prop-key2", "prop-val2");
  error = apex_message_error_new ();

  g_assert_nonnull (error);

  apex_properties_response_add (response, prop1);
  apex_properties_response_add (response, prop2);

  apex_message_error_set_code (error, 200);
  apex_message_error_set_message (error, "things are borked");
  apex_response_set_error (APEX_RESPONSE (response), error);

  data = apex_properties_response_serialize (response);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  if (g_test_verbose ())
    g_print ("\n%s\n", data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_properties_response_json_deserialize (void)
{
  g_autoptr (ApexPropertiesResponse) response = NULL;
  g_autoptr (ApexProperty) prop1 = NULL;
  g_autoptr (ApexProperty) prop2 = NULL;

  response = apex_properties_response_new ();
  apex_properties_response_deserialize (response, json);

  prop1 = apex_properties_response_get (response, "prop-key1");
  prop2 = apex_properties_response_get (response, "prop-key2");

  g_assert_nonnull (prop1);
  g_assert_nonnull (prop2);

  g_assert_cmpstr (apex_property_get_key (prop1), ==, "prop-key1");
  g_assert_cmpstr (apex_property_get_value (prop1), ==, "prop-val1");
  g_assert_cmpstr (apex_property_get_key (prop2), ==, "prop-key2");
  g_assert_cmpstr (apex_property_get_value (prop2), ==, "prop-val2");
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/PropertiesResponse/construct", test_properties_response_construct);
  g_test_add_func ("/PropertiesResponse/json-serialize", test_properties_response_json_serialize);
  g_test_add_func ("/PropertiesResponse/json-deserialize", test_properties_response_json_deserialize);

  return g_test_run ();
}
