#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_modules_response_construct (void)
{
  g_autoptr (ApexModulesResponse) modules_response = NULL;

  modules_response = apex_modules_response_new ();

  g_assert_nonnull (modules_response);
}

static void
test_modules_response_deserialize (void)
{
  g_autoptr (ApexModulesResponse) modules_response = NULL;

  modules_response = apex_modules_response_new ();

  g_assert_nonnull (modules_response);
}

static void
test_modules_response_serialize (void)
{
  g_autoptr (ApexModulesResponse) modules_response = NULL;

  modules_response = apex_modules_response_new ();

  g_assert_nonnull (modules_response);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/ModulesResponse/construct", test_modules_response_construct);
  g_test_add_func ("/ModulesResponse/deserialize", test_modules_response_deserialize);
  g_test_add_func ("/ModulesResponse/serialize", test_modules_response_serialize);

  return g_test_run ();
}
