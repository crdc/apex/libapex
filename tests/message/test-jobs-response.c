#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_jobs_response_construct (void)
{
  g_autoptr (ApexJobsResponse) jobs_response = NULL;

  jobs_response = apex_jobs_response_new ();

  g_assert_nonnull (jobs_response);
}

static void
test_jobs_response_deserialize (void)
{
  g_autoptr (ApexJobsResponse) jobs_response = NULL;

  jobs_response = apex_jobs_response_new ();

  g_assert_nonnull (jobs_response);
}

static void
test_jobs_response_serialize (void)
{
  g_autoptr (ApexJobsResponse) jobs_response = NULL;

  jobs_response = apex_jobs_response_new ();

  g_assert_nonnull (jobs_response);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/JobsResponse/construct", test_jobs_response_construct);
  g_test_add_func ("/JobsResponse/deserialize", test_jobs_response_deserialize);
  g_test_add_func ("/JobsResponse/serialize", test_jobs_response_serialize);

  return g_test_run ();
}
