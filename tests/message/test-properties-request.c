#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_properties_request_construct (void)
{
  g_autoptr (ApexPropertiesRequest) object = NULL;
  g_autoptr (ApexProperty) prop1 = NULL;
  g_autoptr (ApexProperty) prop2 = NULL;

  object = apex_properties_request_new ("service-id");
  prop1 = apex_property_new ("prop-key1", "prop-val1");
  prop2 = apex_property_new ("prop-key2", "prop-val2");

  g_assert_nonnull (object);

  g_assert_cmpstr (apex_properties_request_get_id (object), ==, "service-id");

  apex_properties_request_set_id (object, "new-id");

  g_assert_cmpstr (apex_properties_request_get_id (object), ==, "new-id");

  g_assert_nonnull (prop1);
  g_assert_nonnull (prop2);

  apex_properties_request_add (object, prop1);
  apex_properties_request_add (object, prop2);

  g_assert_true (apex_properties_request_contains (object, "prop-key1"));
  g_assert_true (apex_properties_request_contains (object, "prop-key2"));

  apex_properties_request_remove (object, "prop-key2");

  g_assert_true (!apex_properties_request_contains (object, "prop-key2"));

  {
    g_autoptr (ApexProperty) prop;

    prop = apex_properties_request_get (object, "prop-key1");

    g_assert_cmpstr (apex_property_get_key (prop), ==, "prop-key1");
    g_assert_cmpstr (apex_property_get_value (prop), ==, "prop-val1");
  }
}

static const gchar *json = "{" \
  "\"id\": \"service-id\"," \
  "\"properties\": [" \
    "{ \"key\": \"prop-key2\", \"value\": \"prop-val2\" }," \
    "{ \"key\": \"prop-key1\", \"value\": \"prop-val1\" }" \
  "]" \
"}";

static void
test_properties_request_json_serialize (void)
{
  g_autoptr (ApexPropertiesRequest) request = NULL;
  g_autoptr (ApexProperty) prop1 = NULL;
  g_autoptr (ApexProperty) prop2 = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  request = apex_properties_request_new ("service-id");
  prop1 = apex_property_new ("prop-key1", "prop-val1");
  prop2 = apex_property_new ("prop-key2", "prop-val2");

  apex_properties_request_add (request, prop1);
  apex_properties_request_add (request, prop2);

  data = apex_properties_request_serialize (request);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_properties_request_json_deserialize (void)
{
  g_autoptr (ApexPropertiesRequest) request = NULL;
  g_autoptr (ApexProperty) prop1 = NULL;
  g_autoptr (ApexProperty) prop2 = NULL;

  request = apex_properties_request_new (NULL);

  apex_properties_request_deserialize (request, json);

  g_assert_cmpstr (apex_properties_request_get_id (request), ==, "service-id");

  prop1 = apex_properties_request_get (request, "prop-key1");
  prop2 = apex_properties_request_get (request, "prop-key2");

  g_assert_nonnull (prop1);
  g_assert_nonnull (prop2);

  g_assert_cmpstr (apex_property_get_key (prop1), ==, "prop-key1");
  g_assert_cmpstr (apex_property_get_value (prop1), ==, "prop-val1");
  g_assert_cmpstr (apex_property_get_key (prop2), ==, "prop-key2");
  g_assert_cmpstr (apex_property_get_value (prop2), ==, "prop-val2");
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/PropertiesRequest/construct", test_properties_request_construct);
  g_test_add_func ("/PropertiesRequest/json-serialize", test_properties_request_json_serialize);
  g_test_add_func ("/PropertiesRequest/json-deserialize", test_properties_request_json_deserialize);

  return g_test_run ();
}
