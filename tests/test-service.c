#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_service_construct (void)
{
  g_autoptr (ApexService) object = NULL;

  object = apex_service_new ("org.apex.Test");
  g_assert_nonnull (object);

/*
 *  g_autoptr (ApexStatus) status = NULL;
 *  g_autoptr (ApexState) state = NULL;
 *
 *  status = apex_status_new ();
 *  state = apex_state_new ();
 *
 *  apex_service_set_name (object, "test-service");
 *  apex_service_set_description (object, "A test service");
 *  apex_service_set_configuration_id (object,
 *                                     "41ea2a26-e968-4960-accd-5ff5baa1e219");
 *  apex_service_set_status (object, status);
 *  apex_service_set_state (object, state);
 *
 *  g_assert_cmpstr (apex_service_get_name (object), ==,
 *                   "test-service");
 *  g_assert_cmpstr (apex_service_get_description (object), ==,
 *                   "A test service");
 *  g_assert_cmpstr (apex_service_get_configuration_id (object),
 *                   ==, "41ea2a26-e968-4960-accd-5ff5baa1e219");
 */
}

static const gchar *json = "{ \
  \"name\": \"test-service\", \
  \"description\": \"A test service\", \
  \"configuration-id\": \"41ea2a26-e968-4960-accd-5ff5baa1e219\" \
}";

static void
test_service_json_serialize (void)
{
  g_autoptr (ApexService) object = NULL;
  g_autoptr (ApexStatus) status = NULL;
  g_autoptr (ApexState) state = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  object = apex_service_new ("org.apex.Test");
  g_assert_nonnull (object);

/*
 *  apex_service_set_name (object, "test-service");
 *  apex_service_set_description (object, "A test service");
 *  apex_service_set_configuration_id (object,
 *                                     "41ea2a26-e968-4960-accd-5ff5baa1e219");
 *  apex_service_set_status (object, status);
 *  apex_service_set_state (object, state);
 *
 *  data = apex_service_serialize (object);
 *  data1 = remove_whitespace (json);
 *  data2 = remove_whitespace (data);
 *
 *  g_assert_cmpstr (data1, ==, data2);
 */
}

static void
test_service_json_deserialize (void)
{
  g_autoptr (ApexService) object = NULL;

  object = apex_service_new ("org.apex.Test");
  g_assert_nonnull (object);

/*
 *  apex_service_deserialize (object, json);
 *
 *  g_assert_cmpstr (apex_service_get_name (object), ==,
 *                   "test-service");
 *  g_assert_cmpstr (apex_service_get_description (object), ==,
 *                   "A test service");
 *  g_assert_cmpstr (apex_service_get_configuration_id (object),
 *                   ==, "41ea2a26-e968-4960-accd-5ff5baa1e219");
 */
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/service/construct", test_service_construct);
  g_test_add_func ("/service/json-serialize", test_service_json_serialize);
  g_test_add_func ("/service/json-deserialize", test_service_json_deserialize);

  return g_test_run ();
}
