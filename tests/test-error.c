#include <errno.h>
#include <apex/apex.h>

static void
test_error_errno (void)
{
  ApexErrorEnum error;

  error = apex_error_from_errno (ENOTCONN);
  g_assert_cmpint (error, ==, APEX_ERROR_NOT_CONNECTED);

  error = apex_error_from_errno (ENOTSUP);
  g_assert_cmpint (error, ==, APEX_ERROR_FAILED);
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/error/errno", test_error_errno);

  return g_test_run ();
}
