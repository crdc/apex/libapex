#include <apex/apex.h>

static void
test_state_machine_construct (void)
{
  g_autoptr (ApexStateMachine) object = NULL;

  object = apex_state_machine_new ();

  g_assert_nonnull (object);
}

static void
test_state_machine_running (void)
{
  g_autoptr (ApexStateMachine) fsm = NULL;
  g_autoptr (ApexTransitionTable) table = NULL;

  table = apex_transition_table_new ();

  // Add transitions to table
  // #1
  {
    g_autoptr (ApexTransition) transition = NULL;
    g_autoptr (ApexState) start = NULL;
    g_autoptr (ApexState) end = NULL;

    transition = apex_transition_new (0);
    start = apex_state_new_full (0, "start", "transition 1 start state");
    end = apex_state_new_full (1, "end", "transition 1 end state");

    g_assert_nonnull (transition);
    g_assert_nonnull (start);
    g_assert_nonnull (end);

    apex_transition_set_start_state (transition, g_steal_pointer (&start));
    apex_transition_set_end_state (transition, g_steal_pointer (&end));

    apex_transition_table_add (table, g_steal_pointer (&transition));

    g_assert_cmpint (apex_transition_table_length (table), ==, 1);
  }
  // #2
  {
    g_autoptr (ApexTransition) transition = NULL;
    g_autoptr (ApexState) start = NULL;
    g_autoptr (ApexState) end = NULL;

    transition = apex_transition_new (1);
    start = apex_state_new_full (1, "start", "transition 2 start state");
    end = apex_state_new_full (2, "end", "transition 2 end state");

    g_assert_nonnull (transition);
    g_assert_nonnull (start);
    g_assert_nonnull (end);

    apex_transition_set_start_state (transition, g_steal_pointer (&start));
    apex_transition_set_end_state (transition, g_steal_pointer (&end));

    apex_transition_table_add (table, g_steal_pointer (&transition));

    g_assert_cmpint (apex_transition_table_length (table), ==, 2);
  }
  // #3
  {
    g_autoptr (ApexTransition) transition = NULL;
    g_autoptr (ApexState) start = NULL;
    g_autoptr (ApexState) end = NULL;

    transition = apex_transition_new (2);
    start = apex_state_new_full (2, "start", "transition 3 start state");
    end = apex_state_new_full (0, "end", "transition 3 end state");

    g_assert_nonnull (transition);
    g_assert_nonnull (start);
    g_assert_nonnull (end);

    apex_transition_set_start_state (transition, g_steal_pointer (&start));
    apex_transition_set_end_state (transition, g_steal_pointer (&end));

    apex_transition_table_add (table, g_steal_pointer (&transition));

    g_assert_cmpint (apex_transition_table_length (table), ==, 3);
  }

  fsm = apex_state_machine_new ();

  g_assert_nonnull (fsm);

  apex_state_machine_set_transition_table (fsm, table);
  apex_state_machine_start (fsm);
  g_usleep (100000);
  g_assert_true (apex_state_machine_running (fsm));

  // Test transitions
  {
    g_autoptr (ApexEvent) event = NULL;

    event = apex_event_new_full (1, "one", "event 1");
    apex_state_machine_submit_event (fsm, event);
    //g_assert_true (apex_state_equal (apex_state_machine_current_state (fsm),
    //                                 apex_transition_table_get (table, 1));
  }
  {
    g_autoptr (ApexEvent) event = NULL;

    event = apex_event_new_full (2, "two", "event 2");
    apex_state_machine_submit_event (fsm, event);
    //g_assert_true (apex_state_equal (apex_state_machine_current_state (fsm),
    //                                 apex_transition_table_get (table, 2));
  }
  {
    g_autoptr (ApexEvent) event = NULL;

    event = apex_event_new_full (0, "zero", "event 0");
    apex_state_machine_submit_event (fsm, event);
    //g_assert_true (apex_state_equal (apex_state_machine_current_state (fsm),
    //                                 apex_transition_table_get (table, 1));
  }

  apex_state_machine_stop (fsm);
  g_assert_true (!apex_state_machine_running (fsm));
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/state-machine/construct", test_state_machine_construct);
  g_test_add_func ("/state-machine/running", test_state_machine_running);

  return g_test_run ();
}
