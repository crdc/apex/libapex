#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_object_construct (void)
{
  g_autoptr (ApexObject) object = NULL;

  object = apex_object_new ("obj0");

  g_assert_nonnull (object);
  g_assert_cmpstr ("obj0", ==, apex_object_get_id (object));
}

static const gchar *json = "{ \
  \"id\": \"obj0\", \
  \"name\": \"TestObject\", \
  \"properties\": [ \
    { \"key\": \"key_b\", \"value\": \"value_b\" }, \
    { \"key\": \"key_a\", \"value\": \"value_a\" } \
  ], \
  \"objects\": [ \
    { \
      \"id\": \"obj1\", \
      \"name\": \"TestChildObject\", \
      \"properties\": [ \
        { \"key\": \"key_c\", \"value\": \"value_c\" } \
      ], \
      \"objects\": [ \
      ] \
    } \
  ] \
}";

static void
test_object_json_serialize (void)
{
  g_autoptr (ApexObject) object = NULL;
  g_autoptr (ApexObject) child = NULL;
  g_autoptr (ApexProperty) prop_a = NULL;
  g_autoptr (ApexProperty) prop_b = NULL;
  g_autoptr (ApexProperty) prop_c = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  object = apex_object_new ("obj0");
  g_assert_nonnull (object);
  apex_object_set_name (object, "TestObject");

  prop_a = apex_property_new ("key_a", NULL);
  prop_b = apex_property_new ("key_b", NULL);
  prop_c = apex_property_new ("key_c", NULL);
  g_assert_nonnull (prop_a);
  g_assert_nonnull (prop_b);
  g_assert_nonnull (prop_c);

  apex_property_set_value (prop_a, "value_a");
  apex_property_set_value (prop_b, "value_b");
  apex_property_set_value (prop_c, "value_c");

  apex_object_add_property (object, g_object_ref (prop_a));
  apex_object_add_property (object, g_object_ref (prop_b));

  child = apex_object_new ("obj1");
  g_assert_nonnull (child);
  apex_object_set_name (child, "TestChildObject");
  apex_object_add_property (child, g_object_ref (prop_c));

  apex_object_add_object (object, g_object_ref (child));

  data = apex_object_serialize (object);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_object_json_deserialize (void)
{
  g_autoptr (ApexObject) object = NULL;
  g_autofree gchar *id = NULL;
  g_autofree gchar *name = NULL;

  object = apex_object_new ("test");
  g_assert_nonnull (object);
  apex_object_deserialize (object, json);

  /*object = apex_gobject_from_data (json);*/

  id = g_strdup (apex_object_get_id (object));
  name = g_strdup (apex_object_get_name (object));

  g_assert_cmpstr (id, ==, "obj0");
  g_assert_cmpstr (name, ==, "TestObject");
  g_assert_true (apex_object_has_property (object, "key_a"));
  g_assert_true (apex_object_has_property (object, "key_b"));
  g_assert_true (apex_object_has_object (object, "obj1"));
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/object/construct", test_object_construct);
  g_test_add_func ("/object/json-serialize", test_object_json_serialize);
  g_test_add_func ("/object/json-deserialize", test_object_json_deserialize);

  return g_test_run ();
}
