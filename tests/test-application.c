#include <apex/apex.h>

/**
 * TestApplication
 *
 * An #ApexApplication implementation for testing.
 */

#define TEST_TYPE_APPLICATION test_application_get_type ()

typedef ApexApplication TestApplication;
typedef ApexApplicationClass TestApplicationClass;

G_DEFINE_TYPE (TestApplication, test_application, APEX_TYPE_APPLICATION)

ApexApplication *test_application_new (void);

static void
test_application_finalize (GObject *object)
{
  G_OBJECT_CLASS (test_application_parent_class)->finalize (object);
}

static ApexConfigurationResponse *
test_application_get_configuration (ApexApplication  *self,
                                    GError          **error)
{
  g_autoptr (ApexConfiguration) configuration = NULL;
  ApexConfigurationResponse *response = NULL;
  g_autofree gchar *uuid = NULL;

  uuid = g_uuid_string_random ();
  configuration = apex_configuration_new ();
  apex_configuration_set_id (configuration, uuid);
  apex_configuration_set_namespace (configuration, APEX_CONFIGURATION_NAMESPACE_ACQUIRE);

  response = apex_configuration_response_new ();
  apex_configuration_response_set_configuration (response, configuration);

  return response;
}

static void
test_application_class_init (TestApplicationClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = test_application_finalize;
  APEX_APPLICATION_CLASS (klass)->get_configuration = test_application_get_configuration;

  if (g_test_verbose ())
    g_print ("setting environment");

  /* Run the application in standalone mode */
  g_setenv ("PLANTD_MODULE_STANDALONE", "true", TRUE);
  g_setenv ("PLANTD_MODULE_ENDPOINT", "tcp://localhost:19999", TRUE);
  g_setenv ("PLANTD_MODULE_BROKER", "tcp://*:19999", TRUE);

  g_assert_true (APEX_APPLICATION_CLASS (klass)->get_configuration != NULL);
}

static void
test_application_init (TestApplication *self)
{
}

static gboolean
test_application_quit (gpointer user_data)
{
  GApplication *app;

  app = G_APPLICATION (user_data);

  g_application_quit (app);

  return FALSE;
}

ApexApplication *
test_application_new (void)
{
  return g_object_new (TEST_TYPE_APPLICATION, NULL);
}

/**
 * TestSink
 *
 * An #ApexSink implementation for testing.
 */

#define TEST_TYPE_SINK test_sink_get_type ()

typedef ApexSink TestSink;
typedef ApexSinkClass TestSinkClass;

G_DEFINE_TYPE (TestSink, test_sink, APEX_TYPE_SINK);

static guint message_count = 0;
static guint message_type = 0;

static void
test_sink_handle_message (ApexSink    *self,
                          const gchar *msg)
{
  if (g_test_verbose ())
    g_print ("saw a message: %s\n", msg);

  if (message_type == 0)
    {
      g_autoptr (ApexEvent) event = NULL;

      event = apex_event_new ();
      apex_event_deserialize (event, msg);
    }
  else
    {
      g_autoptr (ApexMetric) metric = NULL;

      metric = apex_metric_new ();
      apex_metric_deserialize (metric, msg);
    }

  message_count++;
}

static void
test_sink_finalize (GObject *object)
{
  G_OBJECT_CLASS (test_sink_parent_class)->finalize (object);
}

static void
test_sink_class_init (TestSinkClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = test_sink_finalize;
  APEX_SINK_CLASS (klass)->handle_message = test_sink_handle_message;
}

static void
test_sink_init (TestSink *self)
{
  message_count = 0;
}

ApexSink *
test_sink_new (const gchar *endpoint,
               const gchar *filter)
{
  return g_object_new (TEST_TYPE_SINK,
                       "endpoint", endpoint,
                       "filter", filter,
                       NULL);
}

/* Test application construction and get/set methods */
static void
test_application_construct (void)
{
  g_autoptr (ApexApplication) app = NULL;
  g_autoptr (ApexProperty) prop1 = NULL;
  g_autoptr (ApexProperty) prop2 = NULL;

  app = test_application_new ();
  apex_application_set_id (app, "org.apex.TestApplication");
  apex_application_set_endpoint (app, "tcp://localhost:19999");
  apex_application_set_service (app, "test-app");

  g_assert_nonnull (app);
  g_assert_cmpstr (apex_application_get_id (app), ==, "org.apex.TestApplication");
  g_assert_cmpstr (apex_application_get_endpoint (app), ==, "tcp://localhost:19999");
  g_assert_cmpstr (apex_application_get_service (app), ==, "test-app");

  prop1 = apex_property_new ("key1", "prop1");
  prop2 = apex_property_new ("key2", "prop2");

  apex_application_add_property (app, prop1);
  apex_application_add_property (app, prop2);

  g_assert_true (apex_application_has_property (app, "key1"));
  g_assert_true (apex_application_has_property (app, "key2"));

  apex_application_remove_property (app, "key2");

  g_assert_true (!apex_application_has_property (app, "key2"));

  {
    ApexProperty *prop;

    g_assert_true (apex_application_update_property (app, "key1", "foo"));

    prop = apex_application_lookup_property (app, "key1");

    g_assert_nonnull (prop);
    g_assert_cmpstr (apex_property_get_value (prop), ==, "foo");
  }

  {
    for (gint i = 0; i < 100; i++)
      {
        g_autoptr (ApexProperty) p = NULL;
        g_autofree gchar *key = NULL;
        g_autofree gchar *value = NULL;
        key = g_strdup_printf ("key-list%d", i);
        value = g_strdup_printf ("value-list%d", i);
        p = apex_property_new (key, value);
        apex_application_add_property (app, p);
        g_assert_true (apex_application_has_property (app, key));
      }
  }
}

/* Tests for various message handling calls. */

static void
test_message_get_configuration (ApexClient *client)
{
  g_autoptr (ApexConfigurationRequest) req = NULL;
  g_autoptr (ApexConfigurationResponse) resp = NULL;
  g_autofree const gchar *msg = NULL;
  g_autofree const gchar *data = NULL;
  g_autofree gchar *id = NULL;
  ApexConfiguration *config;

  req = apex_configuration_request_new ();
  apex_configuration_request_set_id (req, "test-app");
  msg = apex_configuration_request_serialize (req);
  apex_client_send_request (client,
                            "test-app",
                            "get-configuration",
                            msg);
  data = apex_client_recv_response (client);
  g_assert_nonnull (data);

  resp = apex_configuration_response_new ();
  apex_configuration_response_deserialize (resp, data);

  config = apex_configuration_response_get_configuration (resp);
  id = g_strdup (apex_configuration_get_id (config));

  if (g_test_verbose ())
    g_print ("%s\n", data);

  g_assert_nonnull (id);
  g_assert_cmpint (apex_configuration_get_namespace (config),
                   ==,
                   APEX_CONFIGURATION_NAMESPACE_ACQUIRE);
}

static void
test_message_get_property (ApexClient *client)
{
  g_autoptr (ApexPropertyRequest) req = NULL;
  g_autoptr (ApexPropertyResponse) resp = NULL;
  g_autofree const gchar *msg = NULL;
  g_autofree const gchar *data = NULL;

  req = apex_property_request_new ("test-app", "key1", NULL);
  msg = apex_property_request_serialize (req);
  apex_client_send_request (client,
                            "test-app",
                            "get-property",
                            msg);
  data = apex_client_recv_response (client);
  g_assert_nonnull (data);

  resp = apex_property_response_new ();
  apex_property_response_deserialize (resp, data);

  // TODO: make this work
  if (g_test_verbose ())
    g_print ("%s\n", data);

  // TODO: ... assert things
}

/*
 *static void
 *test_message_set_property (ApexClient *client)
 *{
 *  g_autoptr (ApexPropertyRequest) req = NULL;
 *  g_autoptr (ApexPropertyResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  apex_client_send_request (client,
 *                            "test-app",
 *                            "set-property",
 *                            msg);
 *  data = apex_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_get_properties (ApexClient *client)
 *{
 *  g_autoptr (ApexPropertiesRequest) req = NULL;
 *  g_autoptr (ApexPropertiesResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  apex_client_send_request (client,
 *                            "test-app",
 *                            "get-properties",
 *                            msg);
 *  data = apex_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_set_properties (ApexClient *client)
 *{
 *  g_autoptr (ApexPropertiesRequest) req = NULL;
 *  g_autoptr (ApexPropertiesResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  apex_client_send_request (client,
 *                            "test-app",
 *                            "set-properties",
 *                            msg);
 *  data = apex_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_get_status (ApexClient *client)
 *{
 *  g_autoptr (ApexStatusRequest) req = NULL;
 *  g_autoptr (ApexStatusResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  apex_client_send_request (client,
 *                            "test-app",
 *                            "get-status",
 *                            msg);
 *  data = apex_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_get_settings (ApexClient *client)
 *{
 *  g_autoptr (ApexSettingsRequest) req = NULL;
 *  g_autoptr (ApexSettingsResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  apex_client_send_request (client,
 *                            "test-app",
 *                            "get-settings",
 *                            msg);
 *  data = apex_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_get_job (ApexClient *client)
 *{
 *  g_autoptr (ApexModuleJobRequest) req = NULL;
 *  g_autoptr (ApexJobResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  apex_client_send_request (client,
 *                            "test-app",
 *                            "get-job",
 *                            msg);
 *  data = apex_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_get_jobs (ApexClient *client)
 *{
 *  g_autoptr (ApexEmpty) req = NULL;
 *  g_autoptr (ApexJobsResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  apex_client_send_request (client,
 *                            "test-app",
 *                            "get-jobs",
 *                            msg);
 *  data = apex_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_get_active_job (ApexClient *client)
 *{
 *  g_autoptr (ApexEmpty) req = NULL;
 *  g_autoptr (ApexJobResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  apex_client_send_request (client,
 *                            "test-app",
 *                            "get-active-job",
 *                            msg);
 *  data = apex_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_cancel_job (ApexClient *client)
 *{
 *  g_autoptr (ApexJobRequest) req = NULL;
 *  g_autoptr (ApexJobResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  apex_client_send_request (client,
 *                            "test-app",
 *                            "cancel-job",
 *                            msg);
 *  data = apex_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_submit_job (ApexClient *client)
 *{
 *  g_autoptr (ApexModuleJobRequest) req = NULL;
 *  g_autoptr (ApexJobResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  apex_client_send_request (client,
 *                            "test-app",
 *                            "submit-job",
 *                            msg);
 *  data = apex_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_submit_event (ApexClient *client)
 *{
 *  g_autoptr (ApexEventRequest) req = NULL;
 *  g_autoptr (ApexEventResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  apex_client_send_request (client,
 *                            "test-app",
 *                            "submit-event",
 *                            msg);
 *  data = apex_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_available_events (ApexClient *client)
 *{
 *  g_autoptr (ApexEmpty) req = NULL;
 *  g_autoptr (ApexEventResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  apex_client_send_request (client,
 *                            "test-app",
 *                            "available-events",
 *                            msg);
 *  data = apex_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

static void
test_message_shutdown (ApexClient *client)
{
  apex_client_send_request (client,
                            "test-app",
                            "shutdown",
                            "{}");
}

static gboolean
test_messages_cb (gpointer data)
{
  ApexClient *client;

  client = APEX_CLIENT (data);

  test_message_get_configuration (client);
  test_message_get_property (client);
  /*test_message_set_property (client);*/
  /*test_message_get_properties (client);*/
  /*test_message_set_properties (client);*/
  /*test_message_get_status (client);*/
  /*test_message_get_settings (client);*/
  /*test_message_get_job (client);*/
  /*test_message_get_jobs (client);*/
  /*test_message_get_active_job (client);*/
  /*test_message_cancel_job (client);*/
  /*test_message_submit_job (client);*/
  /*test_message_submit_event (client);*/
  /*test_message_available_events (client);*/
  test_message_shutdown (client);

  return FALSE;
}

static void
test_application_messages (void)
{
  gchar *binpath = g_test_build_filename (G_TEST_BUILT, "unimportant", NULL);
  gchar *argv[] = { binpath, NULL };

  g_autoptr (ApexApplication) app = NULL;
  g_autoptr (ApexProperty) prop = NULL;
  g_autoptr (ApexClient) client = NULL;

  /*apex_log_init (TRUE, NULL);*/

  app = test_application_new ();
  apex_application_set_id (app, "org.apex.TestApplication");
  apex_application_set_service (app, "test-app");

  prop = apex_property_new ("key1", "prop1");
  apex_application_add_property (app, prop);

  client = apex_client_new ("tcp://localhost:19999");

  g_timeout_add (1, test_messages_cb, client);

  g_application_run (G_APPLICATION (app), 1, argv);

  /*apex_log_shutdown ();*/

  g_free (binpath);
}

static gboolean
test_events_cb (gpointer data)
{
  g_autoptr (ApexClient) client = NULL;
  ApexApplication *app;

  if (g_test_verbose ())
    g_print ("/test/events\n");

  app = APEX_APPLICATION (data);

  g_usleep (200000);

  for (gint i = 0; i < 100; i++)
    {
      g_autoptr (ApexEvent) event = NULL;

      event = apex_event_new_full (i, "test", "test-event");
      apex_application_send_event (app, event, NULL);
      g_usleep (100);
    }

  g_usleep (200000);
  g_assert_cmpint (message_count, ==, 100);

  client = apex_client_new ("tcp://localhost:19999");
  g_usleep (100000);
  test_message_shutdown (client);

  return FALSE;
}

static void
test_events_activate_cb (GApplication *application)
{
  ApexApplication *app;
  ApexSource *source;
  ApexSink *sink;

  if (g_test_verbose ())
    g_print ("/test/events/activate\n");

  app = APEX_APPLICATION (application);

  source = apex_application_get_source (app, "event");
  apex_source_start (source);

  sink = apex_application_get_sink (app, "event");
  apex_sink_start (sink);
}

static void
test_application_events (void)
{
  gchar *binpath = g_test_build_filename (G_TEST_BUILT, "unimportant", NULL);
  gchar *argv[] = { binpath, NULL };

  g_autoptr (ApexApplication) app = NULL;
  g_autoptr (ApexSource) source = NULL;
  g_autoptr (ApexSink) sink = NULL;
  g_autoptr (ApexHub) hub = NULL;

  if (g_test_verbose ())
    g_print ("/test/application/events\n");

  /*apex_log_init (TRUE, NULL);*/

  app = test_application_new ();
  apex_application_set_id (app, "org.apex.TestApplication");
  apex_application_set_service (app, "test-app");

  message_type = 0;

  source = apex_source_new ("tcp://localhost:20998", "");
  apex_application_add_source (APEX_APPLICATION (app), "event", source);
  g_assert_true (apex_application_has_source (APEX_APPLICATION (app), "event"));

  sink = test_sink_new ("tcp://localhost:20999", "");
  apex_application_add_sink (APEX_APPLICATION (app), "event", sink);

  hub = apex_hub_new ("tcp://*:20998", "tcp://*:20999");
  apex_hub_start (hub);

  g_timeout_add (1, test_events_cb, app);
  g_signal_connect (app, "activate", G_CALLBACK (test_events_activate_cb), NULL);

  g_application_run (G_APPLICATION (app), 1, argv);

  apex_hub_stop (hub);

  /*apex_log_shutdown ();*/

  g_free (binpath);
}

static gboolean
test_metrics_cb (gpointer data)
{
  g_autoptr (ApexMetric) metric = NULL;
  g_autoptr (ApexMetricTable) table = NULL;
  g_autoptr (ApexMetricTableHeader) header = NULL;
  g_autoptr (ApexClient) client = NULL;
  ApexApplication *app;

  g_usleep (200000);

  app = APEX_APPLICATION (data);

  table = apex_metric_table_new ();
  header = apex_metric_table_header_new ("calibration");

  apex_metric_table_header_add_column (header, "column_1", 1.0);
  apex_metric_table_header_add_column (header, "column_2", 0.0);
  apex_metric_table_header_add_column (header, "column_3", 1.0);
  apex_metric_table_header_add_column (header, "column_4", 0.0);
  apex_metric_table_header_add_column (header, "column_5", 1.0);
  apex_metric_table_header_add_column (header, "column_6", 0.0);

  apex_metric_table_set_name (table, "baz");
  apex_metric_table_set_timestamp (table, "2019-09-30 18:30:0.00-00");
  apex_metric_table_set_header (table, header);
  apex_metric_table_add_entry (table, "value_1", 1.0);
  apex_metric_table_add_entry (table, "value_2", 2.0);
  apex_metric_table_add_entry (table, "value_3", 3.0);

  metric = apex_metric_new_full ("foo", "bar", table);
  apex_application_send_metric (app, metric, NULL);

  g_usleep (200000);
  g_assert_cmpint (message_count, ==, 1);

  client = apex_client_new ("tcp://localhost:19999");
  g_usleep (100000);
  test_message_shutdown (client);

  return FALSE;
}

static void
test_metrics_activate_cb (GApplication *application)
{
  ApexApplication *app;
  ApexSource *source;
  ApexSink *sink;

  app = APEX_APPLICATION (application);

  source = apex_application_get_source (app, "metric");
  apex_source_start (source);

  sink = apex_application_get_sink (app, "metric");
  apex_sink_start (sink);
}

static void
test_application_metrics (void)
{
  gchar *binpath = g_test_build_filename (G_TEST_BUILT, "unimportant", NULL);
  gchar *argv[] = { binpath, NULL };

  g_autoptr (ApexApplication) app = NULL;
  g_autoptr (ApexSource) source = NULL;
  g_autoptr (ApexSink) sink = NULL;
  g_autoptr (ApexHub) hub = NULL;

  /*apex_log_init (TRUE, NULL);*/

  app = test_application_new ();
  apex_application_set_id (app, "org.apex.TestApplication");
  apex_application_set_service (app, "test-app");

  message_type = 1;

  source = apex_source_new ("tcp://localhost:21998", "");
  apex_application_add_source (APEX_APPLICATION (app), "metric", source);
  g_assert_true (apex_application_has_source (APEX_APPLICATION (app), "metric"));

  sink = test_sink_new ("tcp://localhost:21999", "");
  apex_application_add_sink (APEX_APPLICATION (app), "metric", sink);

  hub = apex_hub_new ("tcp://*:21998", "tcp://*:21999");
  apex_hub_start (hub);

  g_timeout_add (1, test_metrics_cb, app);
  g_signal_connect (app, "activate", G_CALLBACK (test_metrics_activate_cb), NULL);

  g_application_run (G_APPLICATION (app), 1, argv);

  apex_hub_stop (hub);

  /*apex_log_shutdown ();*/

  g_free (binpath);
}

static void
test_application_jobs (void)
{
  g_autoptr (ApexApplication) app = NULL;

  app = test_application_new ();
  apex_application_set_id (app, "org.apex.TestApplication");
  apex_application_set_service (app, "test-app");
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/application/construct", test_application_construct);
  g_test_add_func ("/application/messages", test_application_messages);
  g_test_add_func ("/application/events", test_application_events);
  g_test_add_func ("/application/metrics", test_application_metrics);
  g_test_add_func ("/application/jobs", test_application_jobs);

  return g_test_run ();
}
