#include <apex/apex.h>

static void
test_sink_construct (void)
{
  g_autoptr (ApexSink) object = NULL;

  object = apex_sink_new ("tcp://localhost:5555", "");

  g_assert_nonnull (object);
}

/*
 *static const gchar *json = "{ \
 *  \"\": \"\", \
 *}";
 */

static void
test_sink_serialize (void)
{
  g_autoptr (ApexSink) object = NULL;
}

static void
test_sink_deserialize (void)
{
  g_autoptr (ApexSink) object = NULL;
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/sink/construct", test_sink_construct);
  g_test_add_func ("/sink/serialize", test_sink_serialize);
  g_test_add_func ("/sink/deserialize", test_sink_deserialize);

  return g_test_run ();
}
