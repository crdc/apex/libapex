#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_state_construct (void)
{
  g_autoptr (ApexState) object = NULL;

  object = apex_state_new ();

  g_assert_nonnull (object);

  apex_state_set_id (object, 99);
  apex_state_set_name (object, "test-state");
  apex_state_set_description (object, "A test state");

  g_assert_cmpint (apex_state_get_id (object), ==, 99);
  g_assert_cmpstr (apex_state_get_name (object), ==, "test-state");
  g_assert_cmpstr (apex_state_get_description (object), ==, "A test state");
}

static void
test_state_construct_full (void)
{
  g_autoptr (ApexState) object = NULL;

  object = apex_state_new_full (1, "test-state", "A test state");
  g_assert_nonnull (object);

  g_assert_cmpint (apex_state_get_id (object), ==, 1);
  g_assert_cmpstr (apex_state_get_name (object), ==, "test-state");
  g_assert_cmpstr (apex_state_get_description (object), ==, "A test state");
}

static const gchar *json = "{ \
  \"id\": 99, \
  \"name\": \"test-state\", \
  \"description\": \"A test state\" \
}";

static void
test_state_json_serialize (void)
{
  g_autoptr (ApexState) object = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  object = apex_state_new ();
  g_assert_nonnull (object);

  apex_state_set_id (object, 99);
  apex_state_set_name (object, "test-state");
  apex_state_set_description (object, "A test state");

  data = apex_state_serialize (object);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_state_json_deserialize (void)
{
  g_autoptr (ApexState) object = NULL;

  object = apex_state_new ();
  g_assert_nonnull (object);

  apex_state_deserialize (object, json);

  g_assert_cmpint (apex_state_get_id (object), ==, 99);
  g_assert_cmpstr (apex_state_get_name (object), ==, "test-state");
  g_assert_cmpstr (apex_state_get_description (object), ==, "A test state");
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/state/construct", test_state_construct);
  g_test_add_func ("/state/construct-full", test_state_construct_full);
  g_test_add_func ("/state/json-serialize", test_state_json_serialize);
  g_test_add_func ("/state/json-deserialize", test_state_json_deserialize);

  return g_test_run ();
}
