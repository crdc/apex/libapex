#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_metric_construct (void)
{
  {
    g_autoptr (ApexMetric) object = NULL;
    g_autoptr (ApexMetricTable) table = NULL;

    object = apex_metric_new ();
    table = apex_metric_table_new ();
    g_assert_nonnull (object);
    g_assert_nonnull (table);
    apex_metric_set_id (object, "foo");
    apex_metric_set_name (object, "bar");
    apex_metric_set_data (object, table);
    g_assert_cmpstr (apex_metric_get_id (object), ==, "foo");
    g_assert_cmpstr (apex_metric_get_name (object), ==, "bar");
    g_assert_nonnull (apex_metric_get_data (object));
    g_object_unref (table);
  }

  {
    g_autoptr (ApexMetric) object = NULL;
    g_autoptr (ApexMetricTable) table = NULL;

    table = apex_metric_table_new ();
    g_assert_nonnull (table);
    object = apex_metric_new_full ("foo", "bar", table);
    g_assert_nonnull (object);
    g_object_unref (table);

    {
      g_autofree gchar *id = NULL;
      g_autofree gchar *name = NULL;

      id = apex_metric_dup_id (object);
      name = apex_metric_dup_name (object);
      g_assert_cmpstr (id, ==, "foo");
      g_assert_cmpstr (name, ==, "bar");
      g_assert_nonnull (apex_metric_get_data (object));
    }

    {
      g_autofree gchar *id = NULL;
      g_autofree gchar *name = NULL;

      apex_metric_set_id (object, "baz");
      apex_metric_set_name (object, "meh");
      id = apex_metric_dup_id (object);
      name = apex_metric_dup_name (object);
      g_assert_cmpstr (id, ==, "baz");
      g_assert_cmpstr (name, ==, "meh");
    }
  }
}

static const gchar *json = "{ \
  \"id\": \"foo\", \
  \"name\": \"bar\", \
  \"data\": { \
    \"name\": \"baz\", \
    \"timestamp\": \"2019-04-09 14:00:0.00-00\", \
    \"header\": { \
      \"name\": \"calibration\", \
      \"columns\": { \
        \"column_3\": 1.0, \
        \"column_5\": 1.0, \
        \"column_2\": 0.0, \
        \"column_4\": 0.0, \
        \"column_6\": 0.0, \
        \"column_1\": 1.0 \
      } \
    }, \
    \"entries\": { \
      \"value_2\": 2.0, \
      \"value_1\": 1.0, \
      \"value_3\": 3.0 \
    } \
  } \
}";

static void
test_metric_serialize (void)
{
  g_autoptr (ApexMetric) metric = NULL;
  g_autoptr (ApexMetricTable) table = NULL;
  g_autoptr (ApexMetricTableHeader) header = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  table = apex_metric_table_new ();
  header = apex_metric_table_header_new ("calibration");

  g_assert_nonnull (table);
  g_assert_nonnull (header);

  apex_metric_table_header_add_column (header, "column_1", 1.0);
  apex_metric_table_header_add_column (header, "column_2", 0.0);
  apex_metric_table_header_add_column (header, "column_3", 1.0);
  apex_metric_table_header_add_column (header, "column_4", 0.0);
  apex_metric_table_header_add_column (header, "column_5", 1.0);
  apex_metric_table_header_add_column (header, "column_6", 0.0);

  apex_metric_table_set_name (table, "baz");
  apex_metric_table_set_timestamp (table, "2019-04-09 14:00:0.00-00");
  apex_metric_table_set_header (table, header);
  apex_metric_table_add_entry (table, "value_1", 1.0);
  apex_metric_table_add_entry (table, "value_2", 2.0);
  apex_metric_table_add_entry (table, "value_3", 3.0);

  metric = apex_metric_new_full ("foo", "bar", table);
  g_assert_nonnull (metric);

  data = apex_metric_serialize (metric);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_metric_deserialize (void)
{
  g_autoptr (ApexMetric) metric = NULL;
  g_autofree gchar *id = NULL;
  g_autofree gchar *name = NULL;
  ApexMetricTable *table;

  metric = apex_metric_new ();

  g_assert_nonnull (metric);

  apex_metric_deserialize (metric, json);

  id = apex_metric_dup_id (metric);
  name = apex_metric_dup_name (metric);
  g_assert_cmpstr (id, ==, "foo");
  g_assert_cmpstr (name, ==, "bar");

  table = apex_metric_ref_data (metric);
  g_assert_nonnull (table);

  g_object_unref (table);
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/metric/construct", test_metric_construct);
  g_test_add_func ("/metric/serialize", test_metric_serialize);
  g_test_add_func ("/metric/deserialize", test_metric_deserialize);

  return g_test_run ();
}
