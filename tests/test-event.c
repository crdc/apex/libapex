#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_event_construct (void)
{
  g_autoptr (ApexEvent) object = NULL;

  object = apex_event_new ();
  g_assert_nonnull (object);

  apex_event_set_id (object, 1);
  apex_event_set_name (object, "Test");
  apex_event_set_description (object, "A test event");

  g_assert_cmpint (apex_event_get_id (object), ==, 1);
  g_assert_cmpstr (apex_event_get_name (object), ==, "Test");
  g_assert_cmpstr (apex_event_get_description (object), ==, "A test event");
}

static void
test_event_construct_full (void)
{
  g_autoptr (ApexEvent) object = NULL;

  object = apex_event_new_full (1, "Test", "A test event");
  g_assert_nonnull (object);

  g_assert_cmpint (apex_event_get_id (object), ==, 1);
  g_assert_cmpstr (apex_event_get_name (object), ==, "Test");
  g_assert_cmpstr (apex_event_get_description (object), ==, "A test event");
}

static const gchar *json = "{ \
  \"id\": 99, \
  \"name\": \"test-event\", \
  \"description\": \"A test event\" \
}";

static void
test_event_json_serialize (void)
{
  g_autoptr (ApexEvent) object = NULL;

  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  object = apex_event_new ();
  g_assert_nonnull (object);

  apex_event_set_id (object, 99);
  apex_event_set_name (object, "test-event");
  apex_event_set_description (object, "A test event");

  data = apex_event_serialize (object);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_event_json_deserialize (void)
{
  g_autoptr (ApexEvent) object = NULL;

  object = apex_event_new ();
  g_assert_nonnull (object);

  apex_event_deserialize (object, json);

  g_assert_cmpint (apex_event_get_id (object), ==, 99);
  g_assert_cmpstr (apex_event_get_name (object), ==, "test-event");
  g_assert_cmpstr (apex_event_get_description (object), ==, "A test event");
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/event/construct", test_event_construct);
  g_test_add_func ("/event/construct-full", test_event_construct_full);
  g_test_add_func ("/event/json-serialize", test_event_json_serialize);
  g_test_add_func ("/event/json-deserialize", test_event_json_deserialize);

  return g_test_run ();
}
