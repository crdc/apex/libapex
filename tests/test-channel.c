#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_channel_construct (void)
{
  g_autoptr (ApexChannel) object = NULL;

  object = apex_channel_new ();

  g_assert_nonnull (object);

  apex_channel_set_endpoint (object, "ipc://*:9999");
  apex_channel_set_envelope (object, "channel");

  g_assert_cmpstr (apex_channel_get_endpoint (object), ==, "ipc://*:9999");
  g_assert_cmpstr (apex_channel_get_envelope (object), ==, "channel");
}

// XXX: type is the wrong word here, too much glib conflict
static void
test_channel_type (void)
{
  g_autoptr (ApexChannel) object = NULL;
}

static const gchar *json = "{ \
  \"endpoint\": \"ipc://*:9999\", \
  \"envelope\": \"channel\" \
}";

static void
test_channel_json_serialize (void)
{
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  g_autoptr (ApexChannel) object = NULL;

  object = apex_channel_new ();
  g_assert_nonnull (object);

  apex_channel_set_endpoint (object, "ipc://*:9999");
  apex_channel_set_envelope (object, "channel");

  data = apex_channel_serialize (object);

  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_channel_json_deserialize (void)
{
  g_autoptr (ApexChannel) object = NULL;

  object = apex_channel_new ();
  g_assert_nonnull (object);

  apex_channel_deserialize (object, json);

  g_assert_cmpstr (apex_channel_get_endpoint (object), ==, "ipc://*:9999");
  g_assert_cmpstr (apex_channel_get_envelope (object), ==, "channel");
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/channel/construct", test_channel_construct);
  g_test_add_func ("/channel/type", test_channel_type);
  g_test_add_func ("/channel/json-serialize", test_channel_json_serialize);
  g_test_add_func ("/channel/json_deserialize", test_channel_json_deserialize);

  return g_test_run ();
}
