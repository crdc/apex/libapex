#include <apex/apex.h>

static void
test_transition_table_construct (void)
{
  g_autoptr (ApexTransitionTable) object = NULL;

  object = apex_transition_table_new ();

  g_assert_nonnull (object);
}

static void
test_transition_table_operations (void)
{
  g_autoptr (ApexTransitionTable) table = NULL;
  g_autoptr (ApexTransition) a = NULL;
  g_autoptr (ApexTransition) b = NULL;
  g_autoptr (ApexTransition) c = NULL;

  table = apex_transition_table_new ();
  a = apex_transition_new (1);
  b = apex_transition_new (2);
  c = apex_transition_new (3);

  g_assert_nonnull (table);
  g_assert_nonnull (a);
  g_assert_nonnull (b);
  g_assert_nonnull (c);

  g_assert_true (apex_transition_table_add (table, a));
  g_assert_true (apex_transition_table_add (table, b));
  g_assert_true (apex_transition_table_add (table, c));

  g_assert_true (apex_transition_table_contains (table, a));
  g_assert_true (apex_transition_table_contains (table, b));
  g_assert_true (apex_transition_table_contains (table, c));

  g_assert_cmpint (apex_transition_table_length (table), ==, 3);

  g_assert_true (!apex_transition_table_remove (table, 4));
  g_assert_true (apex_transition_table_remove (table, 3));

  g_assert_cmpint (apex_transition_table_length (table), ==, 2);

  g_assert_false (apex_transition_table_contains (table, c));

  for (gint i = 0; i < apex_transition_table_length (table); i++)
    {
      ApexTransition *transition;

      transition = apex_transition_table_get (table, i);
      g_assert_cmpint (apex_transition_get_id (transition), ==, i + 1);
    }
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/transition-table/construct", test_transition_table_construct);
  g_test_add_func ("/transition-table/operations", test_transition_table_operations);

  return g_test_run ();
}
