# -*- Mode: Python -*-

from __future__ import absolute_import

import gi
import unittest
import pytest

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex

#pylint: enable=wrong-import-position

class TestState(unittest.TestCase):
    pass


class TestStateMachine(unittest.TestCase):
    pass


class TestTransition(unittest.TestCase):
    pass


class TestTransitionTable(unittest.TestCase):
    pass
