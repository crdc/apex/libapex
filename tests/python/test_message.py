# -*- Mode: Python -*-

from __future__ import absolute_import

import gi
import unittest
import pytest

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex

#pylint: enable=wrong-import-position

class TestChannelRequest(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestChannelResponse(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestChannelsResponse(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestConfigurationRequest(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestConfigurationResponse(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestConfigurationsResponse(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestEmpty(unittest.TestCase):

    json = "{}"

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestEventRequest(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestEventResponse(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestJobRequest(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestJobResponse(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestJobStatusResponse(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestJobsResponse(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestModuleEventRequest(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestModuleJobRequest(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestModuleRequest(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestModuleResponse(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestModulesResponse(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestPropertiesRequest(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestPropertiesResponse(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestPropertyRequest(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestPropertyResponse(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestResponse(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestSettingsRequest(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestSettingsResponse(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestStatusRequest(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass


class TestStatusResponse(unittest.TestCase):

    json = """{
    }"""

    def test_construct(self):
        pass

    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass
