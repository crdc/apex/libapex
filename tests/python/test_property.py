# -*- Mode: Python -*-

from __future__ import absolute_import

import gi
import unittest
import pytest

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex

#pylint: enable=wrong-import-position

class TestProperty(unittest.TestCase):

    json = """{
        \"key\": \"foo\",
        \"value\": \"bar\"
    }"""

    def test_construct(self):
        prop = Apex.Property.new("foo", "bar")
        self.assertEqual(prop.get_key(), "foo")
        self.assertEqual(prop.get_value(), "bar")
        prop.set_key("baz")
        prop.set_value("meh")
        self.assertEqual(prop.get_key(), "baz")
        self.assertEqual(prop.get_value(), "meh")

    def test_serialize(self):
        prop = Apex.Property.new("foo", "bar")
        data = prop.serialize()
        data = data.replace("\n", "")
        data = data.replace(" ", "")
        json_cmp = self.json.replace("\n", "")
        json_cmp = json_cmp.replace(" ", "")
        self.assertEqual(data, json_cmp)

    def test_deserialize(self):
        prop = Apex.Property.new("", "")
        prop.deserialize(self.json)
        self.assertEqual(prop.get_key(), "foo")
        self.assertEqual(prop.get_value(), "bar")
