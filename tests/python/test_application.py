# -*- Mode: Python -*-

from __future__ import absolute_import

import os
import sys
import time
import unittest
import gi

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex
from gi.repository import GLib

#pylint: enable=wrong-import-position

class TestApp(Apex.Application):
    __gtype_name__ = "TestApp"

    def __init__(self, *args, **kwargs):
        super().__init__(*args,
                         application_id="org.apex.TestApp",
                         flags=0,
                         **kwargs)
        # Run the test in standalone mode
        os.environ["PLANTD_MODULE_STANDALONE"] = "true"
        os.environ["PLANTD_MODULE_ENDPOINT"] = "tcp://localhost:17999"
        os.environ["PLANTD_MODULE_BROKER"] = "tcp://*:17999"
        # Named service for messaging
        self.set_service("test-app")
        self.set_inactivity_timeout(10000)
        # Client for message tests
        self.client = Apex.Client.new("tcp://localhost:17999")

    def request_shutdown(self):
        self.client.send_request("test-app", "shutdown", "{}")


class TestSink(Apex.Sink):
    __gtype_name__ = "TestSink"

    def __init__(self, *args, **kwargs):
        super().__init__(*args,
                         endpoint="tcp://localhost:18999",
                         filter="",
                         **kwargs)
        self.message_count = 0
        self.message_type = 0

    def do_handle_message(self, msg):
        if self.message_type == 0:
            event = Apex.Event.new()
            event.deserialize(msg)
        else:
            metric = Apex.Metric.new()
            metric.deserialize(msg)
        self.message_count = self.message_count + 1


class TestApplication(unittest.TestCase):

    def test_events(self):
        self.app = TestApp()
        # Setup the event bus
        source = Apex.Source.new("tcp://localhost:18998", "")
        self.app.add_source("event", source)
        self.sink = TestSink()
        self.app.add_sink("event", self.sink)
        hub = Apex.Hub.new("tcp://*:18998", "tcp://*:18999")
        hub.start()
        source.start()
        self.sink.start()
        GLib.timeout_add_seconds(1, self.on_test_events_cb, None)
        # Launch the test application
        self.app.run(sys.argv)

    def on_test_events_cb(self, user_data):
        time.sleep(.2)
        for i in range(0, 100):
            event = Apex.Event.new_full(i, "test", "test-event")
            self.app.send_event(event)
            time.sleep(.0001)
        time.sleep(.2)
        self.assertEqual(self.sink.message_count, 100)
        time.sleep(.1)
        self.app.request_shutdown()
        return False
