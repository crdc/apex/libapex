#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_configuration_construct (void)
{
  g_autoptr (ApexConfiguration) object = NULL;
  g_autoptr (ApexObject) child = NULL;
  g_autoptr (ApexProperty) prop1 = NULL;
  g_autoptr (ApexProperty) prop2 = NULL;

  object = apex_configuration_new ();

  g_assert_nonnull (object);

  apex_configuration_set_id (object, "41ea2a26-e968-4960-accd-5ff5baa1e219");
  apex_configuration_set_namespace (object, APEX_CONFIGURATION_NAMESPACE_ACQUIRE);

  g_assert_cmpstr (apex_configuration_get_id (object), ==,
                   "41ea2a26-e968-4960-accd-5ff5baa1e219");
  g_assert_cmpint (apex_configuration_get_namespace (object), ==,
                   APEX_CONFIGURATION_NAMESPACE_ACQUIRE);

  /* XXX: currently just for testing finalize order */
  prop1 = apex_property_new ("foo", "bar");
  apex_configuration_add_property (object, g_object_ref (prop1));
  prop2 = apex_property_new ("key", "value");
  child = apex_object_new ("obj1");
  apex_object_set_name (child, "TestChildObject");
  apex_object_add_property (child, g_object_ref (prop2));

  apex_configuration_add_object (object, g_object_ref (child));
}

static const gchar *json1 = "{ \
  \"id\": \"41ea2a26-e968-4960-accd-5ff5baa1e219\", \
  \"namespace\": \"ACQUIRE\", \
  \"properties\": [ \
    { \"key\": \"foo\", \"value\": \"bar\" } \
  ], \
  \"objects\": [ \
  ] \
}";

static const gchar *json2 = "{ \
  \"id\": \"41ea2a26-e968-4960-accd-5ff5baa1e219\", \
  \"namespace\": \"ACQUIRE\", \
  \"properties\": [ \
    { \"key\": \"foo\", \"value\": \"bar\" } \
  ], \
  \"objects\": [ \
    { \
      \"id\": \"obj1\", \
      \"name\": \"TestChildObject\", \
      \"properties\": [ \
        { \"key\": \"key\", \"value\": \"value\" } \
      ], \
      \"objects\": [ \
      ] \
    } \
  ] \
}";

static void
test_configuration_json_serialize (void)
{
  g_autoptr (ApexConfiguration) object = NULL;
  g_autoptr (ApexObject) child = NULL;
  g_autoptr (ApexProperty) prop1 = NULL;
  g_autoptr (ApexProperty) prop2 = NULL;

  gchar *data;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;
  g_autofree gchar *data3 = NULL;
  g_autofree gchar *data4 = NULL;

  object = apex_configuration_new ();
  g_assert_nonnull (object);

  apex_configuration_set_id (object, "41ea2a26-e968-4960-accd-5ff5baa1e219");
  apex_configuration_set_namespace (object, APEX_CONFIGURATION_NAMESPACE_ACQUIRE);

  g_assert_cmpint (apex_configuration_get_namespace (object), ==,
                   APEX_CONFIGURATION_NAMESPACE_ACQUIRE);

  prop1 = apex_property_new ("foo", "bar");
  apex_configuration_add_property (object, g_object_ref (prop1));

  data = apex_configuration_serialize (object);
  data1 = remove_whitespace (json1);
  data2 = remove_whitespace (data);
  g_free (data);

  g_assert_cmpstr (data1, ==, data2);

  prop2 = apex_property_new ("key", NULL);
  g_assert_nonnull (prop2);

  apex_property_set_value (prop2, "value");

  child = apex_object_new ("obj1");
  g_assert_nonnull (child);
  apex_object_set_name (child, "TestChildObject");
  apex_object_add_property (child, g_object_ref (prop2));

  apex_configuration_add_object (object, g_object_ref (child));

  data = apex_configuration_serialize (object);
  data3 = remove_whitespace (json2);
  data4 = remove_whitespace (data);
  g_free (data);

  g_assert_cmpstr (data3, ==, data4);
}

static void
test_configuration_json_deserialize (void)
{
  g_autoptr (ApexConfiguration) object = NULL;
  g_autofree gchar *id = NULL;

  object = apex_configuration_new ();
  g_assert_nonnull (object);

  apex_configuration_deserialize (object, json1);

  id = g_strdup (apex_configuration_get_id (object));

  g_assert_cmpstr (id, ==, "41ea2a26-e968-4960-accd-5ff5baa1e219");
  g_assert_cmpint (apex_configuration_get_namespace (object), ==,
                   APEX_CONFIGURATION_NAMESPACE_ACQUIRE);
}

static void
test_configuration_load (void)
{
  g_autoptr (ApexConfiguration) config = NULL;
  /*g_autoptr (ApexProperty) prop = NULL;*/
  g_autoptr (GError) error = NULL;
  g_autofree gchar *id = NULL;
  g_autofree gchar *path = NULL;

  gchar *data;

  config = apex_configuration_new ();

  path = g_test_build_filename (G_TEST_BUILT, "test.json", NULL);
  apex_configuration_load (config, path, &error);

  id = g_strdup (apex_configuration_get_id (config));

  g_assert_no_error (error);
  g_assert_cmpstr (id, ==, "41ea2a26-e968-4960-accd-5ff5baa1e219");
  g_assert_cmpint (apex_configuration_get_namespace (config), ==,
                   APEX_CONFIGURATION_NAMESPACE_ACQUIRE);

  g_assert_false (apex_configuration_has_object (config, "foo"));
  g_assert_true (apex_configuration_has_property (config, "app"));

  /*prop = apex_configuration_lookup_property (config, "app");*/

  data = apex_configuration_serialize (config);
  g_print ("%s\n", data);
  g_free (data);

  apex_configuration_save (config, "/tmp/test2.json", &error);
}

static void
test_configuration_save (void)
{
}

static void
test_configuration_objects (void)
{
  g_autoptr (ApexConfiguration) config = NULL;
  g_autoptr (GHashTable) objects = NULL;
  GHashTableIter iter;
  gpointer key, val;

  config = apex_configuration_new ();
  g_assert_nonnull (config);

  apex_configuration_deserialize (config, json2);

  objects = apex_configuration_get_objects (config);
  g_assert_nonnull (objects);

  g_hash_table_iter_init (&iter, objects);
  while (g_hash_table_iter_next (&iter, &key, &val))
    g_assert_cmpstr (key, ==, apex_object_get_id (APEX_OBJECT (val)));
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/configuration/construct", test_configuration_construct);
  g_test_add_func ("/configuration/json-serialize", test_configuration_json_serialize);
  g_test_add_func ("/configuration/json-deserialize", test_configuration_json_deserialize);
  g_test_add_func ("/configuration/load", test_configuration_load);
  g_test_add_func ("/configuration/save", test_configuration_save);
  g_test_add_func ("/configuration/objects", test_configuration_objects);

  return g_test_run ();
}
