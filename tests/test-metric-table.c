#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_metric_table_construct (void)
{
  g_autoptr (ApexMetricTable) object = NULL;

  object = apex_metric_table_new ();
  g_assert_nonnull (object);

  apex_metric_table_set_name (object, "foo");
  g_assert_cmpstr (apex_metric_table_get_name (object), ==, "foo");
}

static const gchar *json = "{ \
  \"name\": \"foo\", \
  \"timestamp\": \"2019-04-09 14:00:0.00-00\", \
  \"header\": { \
    \"name\": \"bar\", \
    \"columns\": { \
      \"column_3\": 1.0, \
      \"column_5\": 1.0, \
      \"column_2\": 0.0, \
      \"column_4\": 0.0, \
      \"column_6\": 0.0, \
      \"column_1\": 1.0 \
    } \
  }, \
  \"entries\": { \
    \"value_2\": 2.0, \
    \"value_1\": 1.0, \
    \"value_3\": 3.0 \
  } \
}";

static void
test_metric_table_serialize (void)
{
  g_autoptr (ApexMetricTable) table = NULL;
  g_autoptr (ApexMetricTableHeader) header = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  table = apex_metric_table_new ();

  g_assert_nonnull (table);

  header = apex_metric_table_header_new ("bar");
  apex_metric_table_header_add_column (header, "column_1", 1.0);
  apex_metric_table_header_add_column (header, "column_2", 0.0);
  apex_metric_table_header_add_column (header, "column_3", 1.0);
  apex_metric_table_header_add_column (header, "column_4", 0.0);
  apex_metric_table_header_add_column (header, "column_5", 1.0);
  apex_metric_table_header_add_column (header, "column_6", 0.0);

  apex_metric_table_set_name (table, "foo");
  apex_metric_table_set_timestamp (table, "2019-04-09 14:00:0.00-00");
  apex_metric_table_set_header (table, header);
  apex_metric_table_add_entry (table, "value_1", 1.0);
  apex_metric_table_add_entry (table, "value_2", 2.0);
  apex_metric_table_add_entry (table, "value_3", 3.0);

  data = apex_metric_table_serialize (table);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_metric_table_deserialize (void)
{
  g_autoptr (ApexMetricTable) table = NULL;
  g_autoptr (ApexMetricTableHeader) header = NULL;
  g_autofree gchar *name = NULL;
  g_autofree gchar *timestamp = NULL;

  table = apex_metric_table_new ();

  g_assert_nonnull (table);

  apex_metric_table_deserialize (table, json);

  name = g_strdup (apex_metric_table_get_name (table));
  timestamp = g_strdup (apex_metric_table_get_timestamp (table));
  header = apex_metric_table_get_header (table);

  g_assert_nonnull (header);
  g_assert_cmpstr (name, ==, "foo");
  g_assert_cmpstr (timestamp, ==, "2019-04-09 14:00:0.00-00");
  g_assert_true (apex_metric_table_has_entry (table, "value_1"));
  g_assert_true (apex_metric_table_has_entry (table, "value_2"));
  g_assert_true (apex_metric_table_has_entry (table, "value_3"));
  g_assert_cmpfloat (apex_metric_table_get_entry (table, "value_1"), ==, 1.0);
  g_assert_cmpfloat (apex_metric_table_get_entry (table, "value_2"), ==, 2.0);
  g_assert_cmpfloat (apex_metric_table_get_entry (table, "value_3"), ==, 3.0);
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/metric-table/construct", test_metric_table_construct);
  g_test_add_func ("/metric-table/serialize", test_metric_table_serialize);
  g_test_add_func ("/metric-table/deserialize", test_metric_table_deserialize);

  return g_test_run ();
}
