#include <json-glib/json-glib.h>
#include <apex/apex.h>

#include "apex/apex-utils.h"
#include "apex/core/apex-table.h"

static void
test_table_double_value (void)
{
  g_autoptr (ApexTable) table = NULL;

  GValue v1 = G_VALUE_INIT;
  GValue v2 = G_VALUE_INIT;
  GValue v3 = G_VALUE_INIT;

  table = apex_table_new (G_TYPE_DOUBLE);
  g_assert_nonnull (table);

  g_value_init (&v1, G_TYPE_DOUBLE);
  g_value_init (&v2, G_TYPE_DOUBLE);
  g_value_init (&v3, G_TYPE_DOUBLE);

  g_value_set_double (&v1, 1.0);
  g_value_set_double (&v2, 2.0);
  g_value_set_double (&v3, 3.0);

  apex_table_add (table, "value_1", &v1);
  apex_table_add (table, "value_2", &v2);
  apex_table_add (table, "value_3", &v3);

  g_assert_true (apex_table_has (table, "value_1"));
  g_assert_true (apex_table_has (table, "value_2"));
  g_assert_true (apex_table_has (table, "value_3"));
  g_assert_cmpfloat (g_value_get_double (apex_table_get (table, "value_1")), ==, 1.0);
  g_assert_cmpfloat (g_value_get_double (apex_table_get (table, "value_2")), ==, 2.0);
  g_assert_cmpfloat (g_value_get_double (apex_table_get (table, "value_3")), ==, 3.0);

  apex_table_remove (table, "value_1");
  apex_table_remove (table, "value_2");
  apex_table_remove (table, "value_3");
  g_assert_false (apex_table_has (table, "value_1"));
  g_assert_false (apex_table_has (table, "value_2"));
  g_assert_false (apex_table_has (table, "value_3"));
}

static void
test_table_string_value (void)
{
  g_autoptr (ApexTable) table = NULL;

  GValue v1 = G_VALUE_INIT;
  GValue v2 = G_VALUE_INIT;
  GValue v3 = G_VALUE_INIT;

  table = apex_table_new (G_TYPE_STRING);
  g_assert_nonnull (table);

  g_value_init (&v1, G_TYPE_STRING);
  g_value_init (&v2, G_TYPE_STRING);
  g_value_init (&v3, G_TYPE_STRING);

  g_value_set_static_string (&v1, "a");
  g_value_set_static_string (&v2, "b");
  g_value_set_static_string (&v3, "c");

  apex_table_add (table, "value_1", &v1);
  apex_table_add (table, "value_2", &v2);
  apex_table_add (table, "value_3", &v3);

  g_assert_true (apex_table_has (table, "value_1"));
  g_assert_true (apex_table_has (table, "value_2"));
  g_assert_true (apex_table_has (table, "value_3"));
  g_assert_cmpstr (g_value_get_string (apex_table_get (table, "value_1")), ==, "a");
  g_assert_cmpstr (g_value_get_string (apex_table_get (table, "value_2")), ==, "b");
  g_assert_cmpstr (g_value_get_string (apex_table_get (table, "value_3")), ==, "c");

  apex_table_remove (table, "value_1");
  apex_table_remove (table, "value_2");
  apex_table_remove (table, "value_3");
  g_assert_false (apex_table_has (table, "value_1"));
  g_assert_false (apex_table_has (table, "value_2"));
  g_assert_false (apex_table_has (table, "value_3"));
}

static void
test_table_object_value (void)
{
  g_autoptr (ApexTable) table = NULL;
  g_autoptr (ApexObject) obj1 = NULL;
  g_autoptr (ApexObject) obj2 = NULL;
  g_autoptr (ApexObject) obj3 = NULL;

  GValue v1 = G_VALUE_INIT;
  GValue v2 = G_VALUE_INIT;
  GValue v3 = G_VALUE_INIT;

  obj1 = apex_object_new ("obj1");
  obj2 = apex_object_new ("obj2");
  obj3 = apex_object_new ("obj3");

  table = apex_table_new (G_TYPE_OBJECT);
  g_assert_nonnull (table);

  g_value_init (&v1, G_TYPE_OBJECT);
  g_value_init (&v2, G_TYPE_OBJECT);
  g_value_init (&v3, G_TYPE_OBJECT);

  g_value_set_object (&v1, obj1);
  g_value_set_object (&v2, obj2);
  g_value_set_object (&v3, obj3);

  apex_table_add (table, "value_1", &v1);
  apex_table_add (table, "value_2", &v2);
  apex_table_add (table, "value_3", &v3);

  g_value_unset (&v1);
  g_value_unset (&v2);
  g_value_unset (&v3);

  g_assert_true (apex_table_has (table, "value_1"));
  g_assert_true (apex_table_has (table, "value_2"));
  g_assert_true (apex_table_has (table, "value_3"));

  {
    g_autofree gchar *id1 = NULL;
    g_autofree gchar *id2 = NULL;
    g_autofree gchar *id3 = NULL;

    id1 = g_strdup (apex_object_get_id (g_value_get_object (apex_table_get (table, "value_1"))));
    id2 = g_strdup (apex_object_get_id (g_value_get_object (apex_table_get (table, "value_2"))));
    id3 = g_strdup (apex_object_get_id (g_value_get_object (apex_table_get (table, "value_3"))));

    g_assert_cmpstr (id1, ==, "obj1");
    g_assert_cmpstr (id2, ==, "obj2");
    g_assert_cmpstr (id3, ==, "obj3");
  }

  apex_table_remove (table, "value_1");
  apex_table_remove (table, "value_2");
  apex_table_remove (table, "value_3");
  g_assert_false (apex_table_has (table, "value_1"));
  g_assert_false (apex_table_has (table, "value_2"));
  g_assert_false (apex_table_has (table, "value_3"));
}

static const gchar *double_json = "{ \
  \"value_2\": 2.0, \
  \"value_1\": 1.0, \
  \"value_3\": 3.0 \
}";

static const gchar *string_json = "{ \
  \"value_2\": \"b\", \
  \"value_1\": \"a\", \
  \"value_3\": \"c\" \
}";

static const gchar *object_json = "[ \
  { \"key\": \"b\", \"value\": \"value_b\" }, \
  { \"key\": \"a\", \"value\": \"value_a\" }, \
  { \"key\": \"c\", \"value\": \"value_c\" } \
]";

static void
test_table_double_serialize (void)
{
  g_autoptr (ApexTable) table = NULL;
  g_autoptr (JsonGenerator) generator = NULL;
  g_autoptr (JsonNode) node = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  GValue v1 = G_VALUE_INIT;
  GValue v2 = G_VALUE_INIT;
  GValue v3 = G_VALUE_INIT;

  table = apex_table_new (G_TYPE_DOUBLE);

  g_assert_nonnull (table);

  g_value_init (&v1, G_TYPE_DOUBLE);
  g_value_init (&v2, G_TYPE_DOUBLE);
  g_value_init (&v3, G_TYPE_DOUBLE);

  g_value_set_double (&v1, 1.0);
  g_value_set_double (&v2, 2.0);
  g_value_set_double (&v3, 3.0);

  apex_table_add (table, "value_1", &v1);
  apex_table_add (table, "value_2", &v2);
  apex_table_add (table, "value_3", &v3);

  generator = json_generator_new ();
  node = apex_table_serialize (table, JSON_TYPE_OBJECT);
  json_generator_set_root (generator, node);
  data = json_generator_to_data (generator, NULL);

  data1 = remove_whitespace (double_json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_table_string_serialize (void)
{
  g_autoptr (ApexTable) table = NULL;
  g_autoptr (JsonGenerator) generator = NULL;
  g_autoptr (JsonNode) node = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  GValue v1 = G_VALUE_INIT;
  GValue v2 = G_VALUE_INIT;
  GValue v3 = G_VALUE_INIT;

  table = apex_table_new (G_TYPE_STRING);

  g_assert_nonnull (table);

  g_value_init (&v1, G_TYPE_STRING);
  g_value_init (&v2, G_TYPE_STRING);
  g_value_init (&v3, G_TYPE_STRING);

  g_value_set_static_string (&v1, "a");
  g_value_set_static_string (&v2, "b");
  g_value_set_static_string (&v3, "c");

  apex_table_add (table, "value_1", &v1);
  apex_table_add (table, "value_2", &v2);
  apex_table_add (table, "value_3", &v3);

  generator = json_generator_new ();
  node = apex_table_serialize (table, JSON_TYPE_OBJECT);
  json_generator_set_root (generator, node);
  data = json_generator_to_data (generator, NULL);

  data1 = remove_whitespace (string_json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_table_object_serialize (void)
{
  g_autoptr (ApexTable) table = NULL;
  g_autoptr (ApexProperty) prop1 = NULL;
  g_autoptr (ApexProperty) prop2 = NULL;
  g_autoptr (ApexProperty) prop3 = NULL;
  g_autoptr (JsonGenerator) generator = NULL;
  g_autoptr (JsonNode) node = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  GValue v1 = G_VALUE_INIT;
  GValue v2 = G_VALUE_INIT;
  GValue v3 = G_VALUE_INIT;

  table = apex_table_new (G_TYPE_OBJECT);

  g_assert_nonnull (table);

  prop1 = apex_property_new ("a", "value_a");
  prop2 = apex_property_new ("b", "value_b");
  prop3 = apex_property_new ("c", "value_c");

  g_value_init (&v1, G_TYPE_OBJECT);
  g_value_init (&v2, G_TYPE_OBJECT);
  g_value_init (&v3, G_TYPE_OBJECT);

  g_value_set_object (&v1, prop1);
  g_value_set_object (&v2, prop2);
  g_value_set_object (&v3, prop3);

  apex_table_add (table, "value_1", &v1);
  apex_table_add (table, "value_2", &v2);
  apex_table_add (table, "value_3", &v3);

  g_value_unset (&v1);
  g_value_unset (&v2);
  g_value_unset (&v3);

  generator = json_generator_new ();
  node = apex_table_serialize (table, JSON_TYPE_ARRAY);
  json_generator_set_root (generator, node);
  data = json_generator_to_data (generator, NULL);

  data1 = remove_whitespace (object_json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_table_double_deserialize (void)
{
  g_autoptr (ApexTable) table = NULL;
  g_autoptr (JsonNode) node = NULL;

  table = apex_table_new (G_TYPE_DOUBLE);

  g_assert_nonnull (table);

  node = json_from_string (double_json, NULL);
  apex_table_deserialize (table, node);

  g_assert_true (apex_table_has (table, "value_1"));
  g_assert_true (apex_table_has (table, "value_2"));
  g_assert_true (apex_table_has (table, "value_3"));
  g_assert_cmpfloat (g_value_get_double (apex_table_get (table, "value_1")), ==, 1.0);
  g_assert_cmpfloat (g_value_get_double (apex_table_get (table, "value_2")), ==, 2.0);
  g_assert_cmpfloat (g_value_get_double (apex_table_get (table, "value_3")), ==, 3.0);
}

static void
test_table_string_deserialize (void)
{
  g_autoptr (ApexTable) table = NULL;
  g_autoptr (JsonNode) node = NULL;

  table = apex_table_new (G_TYPE_STRING);

  g_assert_nonnull (table);

  node = json_from_string (string_json, NULL);
  apex_table_deserialize (table, node);

  g_assert_true (apex_table_has (table, "value_1"));
  g_assert_true (apex_table_has (table, "value_2"));
  g_assert_true (apex_table_has (table, "value_3"));
  g_assert_cmpstr (g_value_get_string (apex_table_get (table, "value_1")), ==, "a");
  g_assert_cmpstr (g_value_get_string (apex_table_get (table, "value_2")), ==, "b");
  g_assert_cmpstr (g_value_get_string (apex_table_get (table, "value_3")), ==, "c");
}

static void
test_table_object_deserialize (void)
{
  g_autoptr (ApexTable) table = NULL;
  g_autoptr (JsonNode) node = NULL;

  table = apex_table_new (APEX_TYPE_PROPERTY);

  g_assert_nonnull (table);

  node = json_from_string (object_json, NULL);
  apex_table_deserialize (table, node);

  g_assert_true (apex_table_has (table, "a"));
  g_assert_true (apex_table_has (table, "b"));
  g_assert_true (apex_table_has (table, "c"));

  {
    g_autoptr (ApexProperty) prop1 = NULL;
    g_autofree gchar *key1 = NULL;
    g_autofree gchar *key2 = NULL;
    g_autofree gchar *key3 = NULL;

    key1 = g_strdup (apex_property_get_key (g_value_get_object (apex_table_get (table, "a"))));
    key2 = g_strdup (apex_property_get_key (g_value_get_object (apex_table_get (table, "b"))));
    key3 = g_strdup (apex_property_get_key (g_value_get_object (apex_table_get (table, "c"))));

    g_assert_cmpstr (key1, ==, "a");
    g_assert_cmpstr (key2, ==, "b");
    g_assert_cmpstr (key3, ==, "c");
  }
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/table/double-value", test_table_double_value);
  g_test_add_func ("/table/string-value", test_table_string_value);
  g_test_add_func ("/table/object-value", test_table_object_value);
  g_test_add_func ("/table/double-serialize", test_table_double_serialize);
  g_test_add_func ("/table/string-serialize", test_table_string_serialize);
  g_test_add_func ("/table/object-serialize", test_table_object_serialize);
  g_test_add_func ("/table/double-deserialize", test_table_double_deserialize);
  g_test_add_func ("/table/string-deserialize", test_table_string_deserialize);
  g_test_add_func ("/table/object-deserialize", test_table_object_deserialize);

  return g_test_run ();
}
