#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_job_construct (void)
{
  g_autoptr (ApexJob) object = NULL;

  object = apex_job_new ();

  g_assert_nonnull (object);

  apex_job_set_id (object, "41ea2a26-e968-4960-accd-5ff5baa1e219");
  apex_job_set_priority (object, 1);

  g_assert_cmpstr (apex_job_get_id (object), ==,
                   "41ea2a26-e968-4960-accd-5ff5baa1e219");
  g_assert_cmpint (apex_job_get_priority (object), ==, 1);
}

static const gchar *json = "{ \
  \"id\": \"41ea2a26-e968-4960-accd-5ff5baa1e219\", \
  \"priority\": 1 \
}";

static void
test_job_json_serialize (void)
{
  g_autoptr (ApexJob) object = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  object = apex_job_new ();
  g_assert_nonnull (object);

  apex_job_set_id (object, "41ea2a26-e968-4960-accd-5ff5baa1e219");
  apex_job_set_priority (object, 1);

  g_assert_cmpint (apex_job_get_priority (object), ==, 1);

  data = apex_job_serialize (object);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_job_json_deserialize (void)
{
  g_autoptr (ApexJob) object = NULL;

  object = apex_job_new ();
  g_assert_nonnull (object);

  apex_job_deserialize (object, json);

  g_assert_cmpstr (apex_job_get_id (object), ==,
                   "41ea2a26-e968-4960-accd-5ff5baa1e219");
  g_assert_cmpint (apex_job_get_priority (object), ==, 1);
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/job/construct", test_job_construct);
  g_test_add_func ("/job/json-serialize", test_job_json_serialize);
  g_test_add_func ("/job/json-deserialize", test_job_json_deserialize);

  return g_test_run ();
}
