#!/bin/bash

#
# This requires a build of glib that enables systemtap.
#
# For more info:
# - https://blogs.gnome.org/alexl/2010/01/04/tracing-glib/
# - https://tecnocode.co.uk/2010/07/13/reference-count-debugging-with-systemtap/

# XXX: just here to remind me how to run it

stap tests/refs.stp --ldd \
  -d _build/apex/libapex.so \
  -c "$@"
