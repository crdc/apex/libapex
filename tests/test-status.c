#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_status_construct (void)
{
  g_autoptr (ApexStatus) object = NULL;

  object = apex_status_new ();

  g_assert_nonnull (object);
}

static const gchar *json = "{ \
  \"enabled\" : true, \
  \"loaded\" : true, \
  \"active\" : false, \
  \"details\" : { \
    \"a\" : \"b\", \
    \"c\" : \"d\" \
  } \
}";

static void
test_status_json_serialize (void)
{
  g_autoptr (ApexStatus) object = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  object = apex_status_new ();
  g_assert_nonnull (object);

  apex_status_set_enabled (object, true);
  apex_status_set_loaded (object, true);
  apex_status_set_active (object, false);

  apex_status_add_detail (object, "a", "b");
  apex_status_add_detail (object, "c", "d");

  g_assert_true (apex_status_get_enabled (object));
  g_assert_true (apex_status_get_loaded (object));
  g_assert_false (apex_status_get_active (object));
  g_assert_cmpstr (apex_status_get_detail (object, "a"), ==, "b");
  g_assert_cmpstr (apex_status_get_detail (object, "c"), ==, "d");

  data = apex_status_serialize (object);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_status_json_deserialize (void)
{
  g_autoptr (ApexStatus) object = NULL;

  object = apex_status_new ();
  g_assert_nonnull (object);

  apex_status_deserialize (object, json);

  g_assert_true (apex_status_get_enabled (object));
  g_assert_true (apex_status_get_loaded (object));
  g_assert_false (apex_status_get_active (object));

  {
    g_autofree gchar *a = NULL;
    g_autofree gchar *b = NULL;

    a = g_strdup (apex_status_get_detail (object, "a"));
    b = g_strdup (apex_status_get_detail (object, "c"));

    g_assert_cmpstr (a, ==, "b");
    g_assert_cmpstr (b, ==, "d");
  }
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/status/construct", test_status_construct);
  g_test_add_func ("/status/json-serialize", test_status_json_serialize);
  g_test_add_func ("/status/json-deserialize", test_status_json_deserialize);

  return g_test_run ();
}
