#!/bin/bash

#
# Before this is run the command to run unit tests should be run:
#
#   G_SLICE=always-malloc G_DEBUG=gc-friendly meson test \
#     --wrap='valgrind --tool=memcheck --leak-check=full \
#       --show-leak-kinds=definite --errors-for-leak-kinds=definite \
#       --leak-resolution=high --num-callers=20 \
#       --suppressions=../tests/glib.supp' \
#       --suppressions=../tests/zeromq.supp' \
#     -C _build
#
# Then execute this script from the project root with:
#
#   ./tests/gen-leak-report.sh _build/meson-logs/testlog-valgrind.json
#

deflost=0

divider=======================================
divider=$divider$divider

header="\n %-24s %16s %8s %16s %8s\n"
format=" %-24s %16d %8d %16d %8d\n"
width=77

printf "$header" "Name" "Definite (Bytes)" "(Blocks)" "Indirect (Bytes)" "(Blocks)"
printf "%$width.${width}s\n" "$divider"

while IFS= read -r line
do
  name=$(echo $line | jq -r '.name')

  # definitely lost
  defbytes=$(echo $line | jq -r '.stderr' | \
             egrep "^==.*==\s*definitely lost" | \
             awk '{print $4}' | \
             sed 's/,//')
  deflost=$(($deflost + $defbytes))
  defblocks=$(echo $line | jq -r '.stderr' | \
              egrep "^==.*==\s*definitely lost" | \
              awk '{print $7}' | \
             sed 's/,//')

  # indirectly lost
  indbytes=$(echo $line | jq -r '.stderr' | \
             egrep "^==.*==\s*indirectly lost" | \
             awk '{print $4}' | \
             sed 's/,//')
  indlost=$(($indlost + $indbytes))
  indblocks=$(echo $line | jq -r '.stderr' | \
              egrep "^==.*==\s*indirectly lost" | \
              awk '{print $7}' | \
              sed 's/,//')

  printf "$format" $name $defbytes $defblocks $indbytes $indblocks
done < $1

printf "\nDefinitely lost %d bytes" $deflost
printf "\nIndirectly lost %d bytes\n" $indlost
