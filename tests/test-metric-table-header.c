#include <apex/apex.h>

#include "apex/apex-utils.h"

static void
test_metric_table_header_construct (void)
{
  g_autoptr (ApexMetricTableHeader) object = NULL;

  object = apex_metric_table_header_new (NULL);
  g_assert_nonnull (object);

  apex_metric_table_header_set_name (object, "foo");
  g_assert_cmpstr (apex_metric_table_header_get_name (object), ==, "foo");
}

static const gchar *json = "{ \
  \"name\": \"foo\", \
  \"columns\": { \
    \"column_2\": 0.0, \
    \"column_1\": 1.0 \
  } \
}";

static void
test_metric_table_header_serialize (void)
{
  g_autoptr (ApexMetricTableHeader) header = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  header = apex_metric_table_header_new ("foo");

  g_assert_nonnull (header);

  apex_metric_table_header_add_column (header, "column_1", 1.0);
  apex_metric_table_header_add_column (header, "column_2", 0.0);

  data = apex_metric_table_header_serialize (header);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_metric_table_header_deserialize (void)
{
  g_autoptr (ApexMetricTableHeader) header = NULL;
  g_autofree gchar *name = NULL;

  header = apex_metric_table_header_new ("bar");

  g_assert_nonnull (header);

  apex_metric_table_header_deserialize (header, json);

  name = g_strdup (apex_metric_table_header_get_name (header));

  g_assert_cmpstr (name, ==, "foo");
  g_assert_cmpfloat (apex_metric_table_header_get_column (header, "column_1"), ==, 1.0);
  g_assert_cmpfloat (apex_metric_table_header_get_column (header, "column_2"), ==, 0.0);
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/metric-table-header/construct", test_metric_table_header_construct);
  g_test_add_func ("/metric-table-header/serialize", test_metric_table_header_serialize);
  g_test_add_func ("/metric-table-header/deserialize", test_metric_table_header_deserialize);

  return g_test_run ();
}
