#include <apex/apex.h>

static void
test_transition_construct (void)
{
  g_autoptr (ApexTransition) object = NULL;

  object = apex_transition_new (1);

  g_assert_nonnull (object);
}

static void
test_transition_properties (void)
{
  g_autoptr (ApexTransition) transition = NULL;
  g_autoptr (ApexState) start = NULL;
  g_autoptr (ApexState) end = NULL;

  transition = apex_transition_new (1);

  g_assert_nonnull (transition);
  g_assert_cmpint (apex_transition_get_id (transition), ==, 1);

  start = apex_state_new_full (1, "start", "start state");
  end = apex_state_new_full (2, "end", "end state");

  g_assert_nonnull (start);
  g_assert_nonnull (end);

  apex_transition_set_id (transition, 99);
  apex_transition_set_start_state (transition, start);
  apex_transition_set_end_state (transition, end);

  g_assert_cmpint (apex_transition_get_id (transition), ==, 99);

  {
    g_autoptr (ApexState) s = NULL;
    g_autoptr (ApexState) e = NULL;

    s = apex_transition_ref_start_state (transition);
    e = apex_transition_ref_end_state (transition);

    g_assert_nonnull (s);
    g_assert_nonnull (e);
    g_assert_cmpint (apex_state_get_id (s), ==, 1);
    g_assert_cmpint (apex_state_get_id (e), ==, 2);
  }
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/transition/construct", test_transition_construct);
  g_test_add_func ("/transition/properties", test_transition_properties);

  return g_test_run ();
}
