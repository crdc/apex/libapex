# Contributing

## Licensing

Your work is considered a derivative work of the libapex codebase and therefore must be licensed as LGPLv2.1+.

## Testing

When working on a new object or tool, try to write unit tests to prove the implementation.
Not everything we have in the code base has tests, and ideally that will improve, not get worse.

## Code Style

We follow the GObject and Gtk coding style.

```c
static gboolean
this_is_a_function (GObject      *param,
                    const gchar  *another_param,
                    guint         third_param,
                    GError      **error)
{
  g_return_val_if_fail (G_IS_OBJECT (param), FALSE);
  g_return_val_if_fail (third_param > 10, FALSE);

  if (another_param != NULL)
    {
      if (!do_some_more_work ())
        {
          g_set_error (error,
                       G_IO_ERROR,
                       G_IO_ERROR_FAILED,
                       "Something failed");
          return FALSE;
        }
    }

goto_labels_here:

  return TRUE;
}
```

```c
void      do_something_one   (GObject *object);
void      do_something_two   (GObject *object,
                              GError  **error);
gchar    *do_something_three (GObject *object);
gboolean  do_something_four  (GObject *object);
```

 * Notice that we use 2-space indention.
 * We indent new blocks {} with 2 spaces, and braces on their own line. We understand that this is confusing at first, but it is rather nice once it becomes familiar.
 * No tabs, spaces only.
 * Always compare against `NULL` rather than implicit comparisons. This eases ports to other languages and readability.
 * Use #define for constants. Try to avoid "magic constants".
 * goto labels are fully unindented.
 * Align function parameters.
 * Align blocks of function declarations in the header. This vastly improves readability and scanning to find what you want.

If in doubt, look for examples elsewhere in the codebase.
