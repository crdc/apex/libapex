# Apex GLib Library

[WIP] Pre-alpha, doesn't do anything yet.

## Dependencies

* `ninja`
* `meson`
* `glib-2.0`
* `libczmq`
* `protobuf-c`
* `gobject-introspection`

## Setup

### Ubuntu (16.04 to 18.04)

```sh
sudo apt install meson ninja-build libglib2.0-dev libczmq-dev \
  protobuf-c-compiler gobject-introspection libjson-glib-dev
```

### Fedora (27/28/29)

```sh
sudo dnf install meson ninja-build glib2-devel czmq-devel \
  protobuf-c-devel gobject-introspection-devel json-glib-devel
```

## Build

```sh
meson _build
ninja -C _build
```

Compiling examples is done by setting the `meson` flag and recompiling.

```sh
meson configure -Denable-examples=true _build
ninja -C _build
```

## Coverage

To generate code coverage one of `lcov`/`genhtml`/`gcovr` is required to use
with `ninja`.

```sh
meson configure -Db_coverage=true _build
ninja -C _build coverage-html
```

The output will be in `_build/meson-logs/`. This unfortunately includes all
project files, and won't easily allow for exclusions using `meson`. A better
report can be generated using:

```sh
cd _build
gcovr -r .. -e ../tests/ -e ../examples/ --html -o coverage.html
```

## Documentation

Documentation is generated using `gtkdoc` during installation provided that the
`enable-gtk-doc` build flag has been set to `true`. Vala documentation can also
be created using `valadoc`, but currently this must be done manually.

```sh
valadoc --force -o _build/doc/valadoc/ \
  --deps --doclet=html \
  --package-name=Apex \
  --package-version=0.2.1 \
  --pkg=libapex-1.0 \
  --pkg=glib-2.0 \
  --pkg=libczmq \
  --vapidir=data/vapi/
```

### Testing

Running unit tests is done using `meson`.

```sh
meson test -C _build
```

Individual test suites exist for each C type and an entire one for Python, to
an individual suite can be run by passing the name to the execution.

```sh
# eg. for python tests
meson test -C _build python-test-suite
# or for just the application ones
meson test -C _build application
```

#### Memory Leaks

The `valgrind` tool can be used to generate a memory leak report.

```sh
G_SLICE=always-malloc G_DEBUG=gc-friendly meson test \
  --wrap='valgrind --tool=memcheck --leak-check=full \
  --show-leak-kinds=definite --errors-for-leak-kinds=definite \
  --leak-resolution=high --num-callers=20 \
  --suppressions=../tests/glib.supp \
  --suppressions=../tests/zeromq.supp' \
  --suppressions=../tests/python/valgrind.supp' \
  -C _build
```

A report is generated by `meson` at `_build/meson-logs/testlog-valgrind.json`
that can be used to generate an easier to read report about memory that has
definitely been lost.

```sh
./tests/gen-leak-report.sh _build/meson-logs/testlog-valgrind.json
```

## API

_This section is out of date_

Most of what's here is a C implementation of the API defined [here][proto].

### Basic Types

* [ ] Channel
* [ ] Configuration
* [x] Empty
* [x] Event
* [ ] Job
* [ ] Module
* [x] Object
* [x] Property
* [ ] Service
* [x] State
* [ ] Status
* [ ] System

### Request/Response Envelopes

* [x] Channel Request
* [x] Channel Response
* [x] Channels Response
* [x] Configuration Request
* [ ] Configuration Response
* [ ] Configurations Response
* [x] Event Request
* [ ] Event Response
* [x] Job Request
* [x] Job Response
* [x] Job Status Response
* [x] Jobs Response
* [x] Module Event Request
* [x] Module Job Request
* [x] Module Request
* [x] Module Response
* [ ] Modules Response
* [ ] Settings Request
* [ ] Settings Response
* [ ] Status Request
* [ ] Status Response

[proto]: https://gitlab.com/crdc/apex/apex-proto
