FROM debian:buster

RUN apt-get update -qq && apt-get install --no-install-recommends -qq -y \
    clang \
    clang-tools-6.0 \
    findutils \
    gcc \
    g++ \
    gettext \
    git \
    libc6-dev \
    gtk-doc-tools \
    itstool \
    gcovr \
    libglib2.0-dev \
    libgirepository1.0-dev \
    libjson-glib-dev \
    libprotobuf-c-dev \
    libzmq3-dev \
    libczmq-dev \
    libxml2-utils \
    libxslt1-dev \
    libz3-dev \
    locales \
    ninja-build \
    python3 \
    python3-pip \
    python3-setuptools \
    python3-wheel \
    valac \
    xsltproc \
    xz-utils \
    zlib1g-dev \
 && rm -rf /usr/share/doc/* /usr/share/man/*

# Locale for our build
RUN locale-gen C.UTF-8 && /usr/sbin/update-locale LANG=C.UTF-8

ENV LANG=C.UTF-8 LANGUAGE=C.UTF-8 LC_ALL=C.UTF-8

RUN pip3 install meson==0.48.0

ARG HOST_USER_ID=5555
ENV HOST_USER_ID ${HOST_USER_ID}
RUN useradd -u $HOST_USER_ID -ms /bin/bash user

USER user
WORKDIR /home/user

ENV LANG=C.UTF-8 LANGUAGE=C.UTF-8 LC_ALL=C.UTF-8
