FROM fedora:29

RUN dnf -y install \
    clang \
    clang-analyzer \
    gcc \
    gcc-c++ \
    gettext \
    git \
    glib2-devel \
    json-glib-devel \
    protobuf-c-devel \
    czmq-devel \
    zeromq-devel \
    gobject-introspection-devel \
    gtk-doc \
    itstool \
    gcovr \
    libxslt \
    ncurses-compat-libs \
    ninja-build \
    pcre-devel \
    python3 \
    python3-pip \
    python3-wheel \
    vala \
    xz \
    zlib-devel \
 && dnf clean all

RUN pip3 install meson==0.48.0

ARG HOST_USER_ID=5555
ENV HOST_USER_ID ${HOST_USER_ID}
RUN useradd -u $HOST_USER_ID -ms /bin/bash user

USER user
WORKDIR /home/user

ENV LANG C.UTF-8
