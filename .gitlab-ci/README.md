# Docker Registry

This assumes that the user being used has already connected to the GitLab
registry using `docker login registry.gitlab.com` and has a valid set of
credentials.

## Build

```sh
.gitlab-ci/run-docker.sh build --base=debian --base-version=1
.gitlab-ci/run-docker.sh build --base=fedora --base-version=1
```

## Push

```sh
.gitlab-ci/run-docker.sh push --base=debian --base-version=1
.gitlab-ci/run-docker.sh push --base=fedora --base-version=1
```
