#!/bin/bash

if [[ "$(pwd | sed 's/.*libapex/x/')" != "x" ]]; then
  echo "Run from top level of project"
  exit 1
fi

for file in $(ls data/defs/message/*.json); do
  test_file=$(echo $file | sed 's/^.*apex\(.*\)\.json$/test\1.c/')
  if [[ -e tests/message/$test_file ]]; then
    echo $test_file already exists
  else
    cat $file | gob > tests/message/$test_file
  fi
done
