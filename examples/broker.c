#include <glib.h>
#include <gio/gio.h>

#include "apex/apex.h"

#define G_LOG_DOMAIN "broker"

static void
activate_cb (GApplication *application)
{
  g_print ("activated\n");
}

static gpointer
thread_cb (gpointer data)
{
  gint count = 30;
  while (count-- > 0)
    {
      g_debug ("still going");
      g_usleep (1000000);
    }

  return NULL;
}

gint
main (gint argc, gchar *argv[])
{
  gint status;
  GThread *thread;
  g_autoptr (ApexBroker) broker = NULL;
  g_autoptr (GApplication) app = NULL;

  const gchar *endpoint = "tcp://*:5555";

  apex_log_init (true, NULL);
  apex_log_increase_verbosity ();
  apex_log_increase_verbosity ();
  apex_log_increase_verbosity ();
  apex_log_increase_verbosity ();

  app = g_application_new ("org.plantd.example.Broker", 0);
  g_signal_connect (app, "activate", G_CALLBACK (activate_cb), NULL);
  g_application_set_inactivity_timeout (app, 1000000);

  status = g_application_run (app, argc, argv);

  thread = g_thread_create (thread_cb, NULL, TRUE, NULL);
  broker = apex_broker_new (endpoint);
  apex_broker_bind (broker);
  apex_broker_run (broker);

  g_thread_join (thread);

  apex_log_shutdown ();

  return status;
}
