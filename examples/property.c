#include <glib.h>

#include "apex/apex.h"

gint
main (gint argc, gchar *argv[])
{
  g_autoptr (ApexProperty) prop = NULL;
  g_autoptr (ApexProperty) deser = NULL;
  g_autofree gchar *json = NULL;

  prop = apex_property_new ("key", "value");

  json = apex_property_serialize (prop);

  g_message ("%s", json);

  deser = apex_property_new ("deserialized", NULL);
  apex_property_deserialize (deser, json);

  return 0;
}
