#include <glib.h>
#include <gio/gio.h>

#include "apex/apex.h"

static gint count = 0;

#define METRIC_TYPE_SINK metric_sink_get_type ()
/*G_DECLARE_FINAL_TYPE (MetricSink, metric_sink, METRIC, SINK, ApexSink)*/

typedef ApexSink MetricSink;
typedef ApexSinkClass MetricSinkClass;

G_DEFINE_TYPE (MetricSink, metric_sink, APEX_TYPE_SINK)

static void
metric_sink_handle_message (ApexSink    *self,
                            const gchar *msg)
{
  g_autoptr (ApexMetric) metric = NULL;
  g_autoptr (GDateTime) dt = NULL;

  ApexMetricTable *table;

  metric = apex_metric_new ();
  apex_metric_deserialize (metric, msg);

  if (!APEX_IS_METRIC (metric))
    {
      g_print ("Received a data not for a metric\n");
      return;
    }

  table = apex_metric_get_data (metric);

  g_print ("%s: %.6f, %.6f, %.6f\n",
           apex_metric_table_get_timestamp (table),
           apex_metric_table_get_entry (table, "value_1"),
           apex_metric_table_get_entry (table, "value_2"),
           apex_metric_table_get_entry (table, "value_3"));
  count++;
}

static void
metric_sink_finalize (GObject *object)
{
  G_OBJECT_CLASS (metric_sink_parent_class)->finalize (object);
}

static void
metric_sink_class_init (MetricSinkClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = metric_sink_finalize;
  APEX_SINK_CLASS (klass)->handle_message = metric_sink_handle_message;
}

static void
metric_sink_init (MetricSink *self)
{
}

ApexSink *
metric_sink_new (const gchar *endpoint,
                 const gchar *filter)
{
  return g_object_new (METRIC_TYPE_SINK,
                       "endpoint", endpoint,
                       "filter", filter,
                       NULL);
}

static void
activate (GApplication *application)
{
  g_debug ("activated");
}

gint
main (gint argc, gchar *argv[])
{
  gint status, n, duration;
  g_autoptr (GApplication) app = NULL;
  g_autoptr (ApexSink) sink = NULL;

  g_autofree gchar *endpoint = NULL;
  g_autofree gchar *filter = NULL;

  const gchar *context_string = "- metric sink for testing";
  const gchar *context_summary = "Summary:\n"
                                 "Just a simple Apex metric consumer.";
  const gchar *context_description = "Description:\n"
                                     "This is meant to be used with a unit service "
                                     "and one or more metric producers.\n";

  duration = 60;

  GOptionEntry entries[] = {
    { "endpoint", 'e', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &endpoint, NULL, NULL },
    { "filter", 'f', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &filter, NULL, NULL },
    { "duration", 'd', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &duration, NULL, NULL },
    { NULL }
  };

  app = g_application_new ("org.apex.SubExample", 0);

  g_application_add_main_option_entries (app, entries);

  g_application_set_option_context_parameter_string (app, context_string);
  g_application_set_option_context_summary (app, context_summary);
  g_application_set_option_context_description (app, context_description);

  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  g_application_set_inactivity_timeout (app, 10000);

  status = g_application_run (app, argc, argv);

  /* Set defaults */
  if (!endpoint)
    endpoint = g_strdup ("tcp://localhost:14000");
  if (!filter)
    filter = g_strdup ("");

  g_info ("Subscribing to messages on '%s'", endpoint);
  g_info ("Subscribing to messages with filter '%s'", filter);
  g_info ("Subscribing for %d seconds", duration);

  sink = metric_sink_new (endpoint, filter);
  apex_sink_start (sink);

  n = 0;
  while (n < duration)
    {
      g_print ("Received %d metrics\n", count);
      g_usleep (1000000);
      n++;
    }

  apex_sink_stop (sink);

  return status;
}
