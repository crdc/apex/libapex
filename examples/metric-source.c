#include <glib.h>
#include <gio/gio.h>

#include "apex/apex.h"

static void
activate (GApplication *application)
{
  g_debug ("activated");
}

gint
main (gint argc, gchar *argv[])
{
  gint status, n, count, delay;
  g_autoptr (GApplication) app = NULL;
  g_autoptr (ApexSource) source = NULL;
  g_autoptr (ApexMetric) metric = NULL;
  g_autoptr (ApexMetricTable) table = NULL;
  g_autoptr (ApexMetricTableHeader) header = NULL;
  g_autoptr (GRand) rand = NULL;

  g_autofree gchar *endpoint = NULL;
  g_autofree gchar *envelope = NULL;

  count = 100;
  delay = 100000;

  GOptionEntry entries[] = {
    { "endpoint", 'e', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &endpoint, NULL, NULL },
    { "envelope", 'p', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &envelope, NULL, NULL },
    { "count", 'n', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &count, NULL, NULL },
    { "delay", 'd', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &delay, NULL, NULL },
    { NULL }
  };

  app = g_application_new ("org.apex.PubExample", 0);

  g_application_add_main_option_entries (app, entries);

  g_application_set_option_context_parameter_string (app, "- metric source for testing");
  g_application_set_option_context_summary (app,
                                            "Summary:\n"
                                            "Just a simple Apex metric producer.");
  g_application_set_option_context_description (app,
                                                "Description:\n"
                                                "This is meant to be used with a unit service "
                                                "and one or more test metric sinks.\n");

  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  g_application_set_inactivity_timeout (app, 10000);

  status = g_application_run (app, argc, argv);

  if (!endpoint)
    endpoint = g_strdup ("tcp://*:14000");
  if (!envelope)
    envelope = g_strdup ("");

  g_info ("Publishing messages on '%s'", endpoint);
  g_info ("Publishing messages with envelope '%s'", envelope);
  g_info ("Publishing %d messages", count);

  source = apex_source_new (endpoint, envelope);
  apex_source_start (source);

  rand = g_rand_new ();
  table = apex_metric_table_new ();
  header = apex_metric_table_header_new ("calibration");

  apex_metric_table_header_add_column (header, "column_1", 1.0);
  apex_metric_table_header_add_column (header, "column_2", 0.0);
  apex_metric_table_header_add_column (header, "column_3", 1.0);
  apex_metric_table_header_add_column (header, "column_4", 0.0);
  apex_metric_table_header_add_column (header, "column_5", 1.0);
  apex_metric_table_header_add_column (header, "column_6", 0.0);

  apex_metric_table_set_name (table, "test");
  apex_metric_table_set_timestamp (table, "2019-04-09 14:00:0.00-00");
  apex_metric_table_set_header (table, header);
  apex_metric_table_add_entry (table, "value_1", 0.0);
  apex_metric_table_add_entry (table, "value_2", 0.0);
  apex_metric_table_add_entry (table, "value_3", 0.0);

  metric = apex_metric_new_full (g_uuid_string_random (), "test-metric", table);

  n = 0;
  while (n < count)
    {
      g_autoptr (GDateTime) dt = NULL;
      g_autofree gchar *msg;
      g_autofree gchar *ts;

      dt = g_date_time_new_now_local ();
      ts = g_strdup_printf ("%d-%02d-%02d %02d:%02d:%d.%06d",
                            g_date_time_get_year (dt),
                            g_date_time_get_month (dt),
                            g_date_time_get_day_of_month (dt),
                            g_date_time_get_hour (dt),
                            g_date_time_get_minute (dt),
                            g_date_time_get_second (dt),
                            g_date_time_get_microsecond (dt));

      // add_entry replaces if it already exists
      apex_metric_table_set_timestamp (table, ts);
      apex_metric_table_add_entry (table, "value_1", g_rand_double (rand));
      apex_metric_table_add_entry (table, "value_2", g_rand_double (rand));
      apex_metric_table_add_entry (table, "value_3", g_rand_double (rand));

      msg = apex_metric_serialize (metric);
      apex_source_queue_message (source, msg);

      g_usleep (delay);
      n++;
    }

  apex_source_stop (source);

  return status;
}
