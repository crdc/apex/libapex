#! /usr/bin/env python

"""
To run:

    meson configure -Dshared-lib=true _build
    ninja -C _build
    export GI_TYPELIB_PATH=./_build/apex/:$GI_TYPELIB_PATH
    export LD_LIBRARY_PATH=./_build/apex/:$LD_LIBRARY_PATH
    python examples/module.py
"""

import gi
import sys
import signal

gi.require_version('Apex', '1.0')

from gi.repository import Apex
from gi.repository import Gio
from gi.repository import GLib


class ModuleExampleJob(Apex.Job):
    __gtype_name__ = "ModuleExampleJob"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    # apex_job_task override
    def do_task(self):
        Apex.message("task function")
        for i in range(20):
            Apex.message("task loop - %d" % i)
            GLib.usleep(500000)


class ModuleExample(Apex.Application):
    __gtype_name__ = "ModuleExample"

    def __init__(self, *args, **kwargs):
        Apex.info("init")
        super().__init__(*args,
                         application_id="org.apex.ModuleExample",
                         flags=0,
                         **kwargs)

    # apex_application_get_configuration override
    def do_get_configuration(self):
        Apex.info("get-configuration")
        configuration = Apex.Configuration.new()
        configuration.set_id(GLib.uuid_string_random())
        configuration.set_namespace(Apex.ConfigurationNamespace.ACQUIRE)
        response = Apex.ConfigurationResponse.new()
        response.set_configuration(configuration)
        Apex.message("%s" % configuration.serialize())
        Apex.message("%s" % response.serialize())
        return response

    # apex_application_submit_job override
    def do_submit_job(self, job_name):
        job = ModuleExampleJob()
        response = Apex.JobResponse.new()
        response.set_job(job)
        return response


if __name__ == "__main__":
    Apex.log_init(True, "module.log")

    app = ModuleExample()
    signal.signal(signal.SIGTERM, signal.SIG_DFL)

    app.set_endpoint("tcp://localhost:7202")
    app.set_service("analyze-cant")
    app.set_inactivity_timeout(10000)
    app.run(sys.argv)

    Apex.log_shutdown()
