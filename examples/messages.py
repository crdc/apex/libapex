#! /usr/bin/env python

import gi
import sys

gi.require_version('Apex', '1.0')

from gi.repository import Apex
from gi.repository import GLib


def _log():
    Apex.critical("Critical level")
    Apex.debug("Debug level")
    Apex.info("Info level")
    Apex.message("Message level")
    Apex.warning("Warning level")


def _object():
    obj = Apex.Object.new("obj0")
    Apex.message("ID: %s" % obj.get_id())


def _job():
    job = Apex.Job.new()
    job.set_id(GLib.uuid_string_random())
    Apex.message("ApexJob:\n%s" % job.serialize())


def _module_event_request():
    msg = Apex.ModuleEventRequest.new()
    msg.set_event_id(1234)
    Apex.message("ApexModuleEventRequest:\nID: %s\nEvent ID: %d" % (msg.get_id(), msg.get_event_id()))


def main(argv):
    _log()
    # types
    _object()
    _job()
    # messages
    _module_event_request()


if __name__ == '__main__':
    SystemExit(main(sys.argv))
