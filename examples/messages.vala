using Apex;

public static int main (string[] args) {
    Apex.ModuleJobRequest req;

    Apex.log_init (true, null);
    Apex.log_increase_verbosity ();

    // FIXME: fails if id/job-id/job-value unset
    req = new Apex.ModuleJobRequest ();
    var prop = new Apex.Property ("foo", "bar");
    req.set_id ("foo");
    req.set_job_id ("bar");
    req.set_job_value ("baz");
    req.add (prop);

    var msg = req.serialize ();
    message (msg);

    Apex.log_shutdown ();

    return 0;
}
