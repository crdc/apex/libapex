#include <glib.h>
#include <gio/gio.h>

#include "apex/apex.h"

static void
activate (GApplication *application)
{
  g_debug ("activated");
}

gint
main (gint argc, gchar *argv[])
{
  gint status, n, duration;
  g_autoptr (GApplication) app = NULL;
  g_autoptr (ApexSink) sink = NULL;

  gchar *endpoint = NULL;
  gchar *filter = NULL;

  duration = 60;

  GOptionEntry entries[] = {
    { "endpoint", 'e', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &endpoint, NULL, NULL },
    { "filter", 'f', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &filter, NULL, NULL },
    { "duration", 'd', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &duration, NULL, NULL },
    { NULL }
  };

  app = g_application_new ("org.apex.SubExample", 0);

  g_application_add_main_option_entries (app, entries);

  g_application_set_option_context_parameter_string (app, "- subscriber for testing");
  g_application_set_option_context_summary (app,
                                            "Summary:\n"
                                            "Just a simple ZeroMQ subscriber.");
  g_application_set_option_context_description (app,
                                                "Description:\n"
                                                "This is meant to be used with a unit service "
                                                "and one or more test subscribers.\n");

  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  g_application_set_inactivity_timeout (app, 10000);

  status = g_application_run (app, argc, argv);

  /* Set defaults */
  if (!endpoint)
    endpoint = g_strdup ("tcp://localhost:9201");
  if (!filter)
    filter = g_strdup ("test");

  g_info ("Subscribing to messages on '%s'", endpoint);
  g_info ("Subscribing to messages with filter '%s'", filter);
  g_info ("Subscribing for %d seconds", duration);

  sink = apex_sink_new (endpoint, filter);
  apex_sink_start (sink);

  n = 0;
  while (n < duration)
    {
      g_usleep (1000000);
      n++;
    }

  apex_sink_stop (sink);

  return status;
}
