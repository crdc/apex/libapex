#! /usr/bin/env python

import gi
import sys

gi.require_version('Apex', '1.0')

from gi.repository import Apex
from gi.repository import GLib


class ConfigurationExample:

    def __init__(self, *args, **kwargs):
        Apex.message("configuration example")
        self.configuration = Apex.Configuration()
        self.configuration.set_id("0xDEADBEEF")
        self.configuration.set_namespace(Apex.ConfigurationNamespace.ACQUIRE)
        self.configuration.add_property(Apex.Property.new("foo", "bar"))
        obj = Apex.Object.new("baz")
        obj.add_property(Apex.Property.new("baz-foo", "baz-bar"))
        self.configuration.add_object(obj)

    def dump(self):
        Apex.message(self.configuration.serialize())

    def test(self):
        objs = self.configuration.get_objects()
        props = objs["baz"].get_properties()
        val = props["baz-foo"].get_value()
        Apex.message(val)


if __name__ == "__main__":
    Apex.log_init(True, "configuration.log")
    app = ConfigurationExample()
    app.dump()
    app.test()
    app.test()
    Apex.log_shutdown()
