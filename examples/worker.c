#include <glib.h>
#include <gio/gio.h>

#include "apex/apex.h"

#define G_LOG_DOMAIN "client"

static void
activate_cb (GApplication *application)
{
  g_print ("activated\n");
}

gint
main (gint argc, gchar *argv[])
{
  gint status;
  g_autoptr (ApexWorker) worker = NULL;
  g_autoptr (GApplication) app = NULL;

  const gchar *endpoint = "tcp://localhost:5555";
  const gchar *service = "echo";

  apex_log_init (true, NULL);
  apex_log_increase_verbosity ();
  apex_log_increase_verbosity ();
  apex_log_increase_verbosity ();
  apex_log_increase_verbosity ();

  app = g_application_new ("org.plantd.example.Worker", 0);
  g_signal_connect (app, "activate", G_CALLBACK (activate_cb), NULL);
  g_application_set_inactivity_timeout (app, 10000);

  status = g_application_run (app, argc, argv);

  worker = apex_worker_new (endpoint, service);
  apex_worker_connect (worker);
  g_info ("`%s' service worker created for: %s", service, endpoint);

  while (true)
    {
      zframe_t *reply = NULL;
      zmsg_t *request = NULL;

      request = apex_worker_recv (worker, &reply);

      if (request == NULL)
        break; // Worker was interrupted

      // Echo received message
      apex_worker_send (worker, &request, reply);
      zframe_destroy (&reply);
    }

  apex_log_shutdown ();

  return status;
}
