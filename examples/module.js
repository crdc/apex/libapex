const System = imports.system;

const Apex = imports.gi.Apex;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const GObject = imports.gi.GObject;

var ModuleExample = GObject.registerClass({
  GTypeName: 'ModuleExample',
}, class ModuleExample extends Apex.Application {
  _init() {
    super._init({
      application_id: 'org.apex.AcquirePLC',
      flags: Gio.ApplicationFlags.FLAGS_NONE,
    });
  }

  // XXX: this dies
  vfunc_get_configuration() {
    let configuration = new Apex.Configuration();
    let response = new Apex.ConfigurationResponse();
    configuration.set_id(GLib.uuid_string_random());
    configuration.set_namespace(Apex.ConfigurationNamespace.ACQUIRE);
    response.set_configuration(configuration);
    return response;
  }

  vfunc_activate() {
    super.vfunc_activate();

    this.hold();

    this.set_endpoint("tcp://localhost:7201");
    this.set_service("acquire-plc");
    this.job_monitor();
    this.message_handler();
  }
});

let app = new ModuleExample();
app.run([System.programInvocationName].concat(ARGV));
