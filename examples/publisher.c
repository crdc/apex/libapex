#include <glib.h>
#include <gio/gio.h>

#include "apex/apex.h"

static void
activate (GApplication *application)
{
  g_debug ("activated");
}

gint
main (gint argc, gchar *argv[])
{
  gint status, n, count, delay;
  g_autoptr (GApplication) app = NULL;
  g_autoptr (ApexSource) source = NULL;

  gchar *endpoint = NULL;
  gchar *envelope = NULL;

  count = 100;
  delay = 100000;

  GOptionEntry entries[] = {
    { "endpoint", 'e', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &endpoint, NULL, NULL },
    { "envelope", 'p', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &envelope, NULL, NULL },
    { "count", 'n', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &count, NULL, NULL },
    { "delay", 'd', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &delay, NULL, NULL },
    { NULL }
  };

  app = g_application_new ("org.apex.PubExample", 0);

  g_application_add_main_option_entries (app, entries);

  g_application_set_option_context_parameter_string (app, "- publisher for testing");
  g_application_set_option_context_summary (app,
                                            "Summary:\n"
                                            "Just a simple ZeroMQ publisher.");
  g_application_set_option_context_description (app,
                                                "Description:\n"
                                                "This is meant to be used with a unit service "
                                                "and one or more test subscribers.\n");

  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  g_application_set_inactivity_timeout (app, 10000);

  status = g_application_run (app, argc, argv);

  if (!endpoint)
    endpoint = g_strdup ("tcp://*:9200");
  if (!envelope)
    envelope = g_strdup ("test");

  g_info ("Publishing messages on '%s'", endpoint);
  g_info ("Publishing messages with envelope '%s'", envelope);
  g_info ("Publishing %d messages", count);

  source = apex_source_new (endpoint, envelope);
  apex_source_start (source);

  n = 0;
  while (n < count)
    {
      g_autofree gchar *msg;
      msg = g_strdup_printf ("hello-%d", n);
      apex_source_queue_message (source, msg);
      g_usleep (delay);
      n++;
    }

  apex_source_stop (source);

  return status;
}
