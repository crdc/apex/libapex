using Apex;

public static int main (string[] args) {
    Apex.log_init (true, null);

    Apex.log_increase_verbosity ();

    critical ("critical");
    warning ("warning");
    message ("message");
    info ("info");
    debug ("debug");

    Apex.log_shutdown ();

    return 0;
}
