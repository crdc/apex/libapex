#include <glib-unix.h>

#include "apex/apex.h"

#include "module.h"

gint
main (gint argc, gchar *argv[])
{
  gint status;
  g_autoptr (ApexApplication) app = NULL;
  g_autoptr (ApexConfiguration) configuration = NULL;
  g_autoptr (GPtrArray) properties = NULL;

  apex_log_init (true, NULL);

  app = apex_module_example_new ();
  apex_application_set_id (app, "org.apex.ModuleExample");
  apex_application_set_endpoint (app, "tcp://localhost:5555");
  apex_application_set_service (app, "example");

  g_application_set_inactivity_timeout (G_APPLICATION (app), 10000);

/*
 *  apex_application_load_config (app, "/tmp/test.json", NULL);
 *  configuration = apex_application_get_configuration (app);
 *  properties = apex_configuration_get_properties (configuration);
 *
 *  for (gint i = 0; i < properties->len; i++)
 *    {
 *      g_autoptr (ApexProperty) property = NULL;
 *
 *      property = g_ptr_array_index (properties, i);
 *      if (!property)
 *        {
 *          g_error ("null property");
 *          break;
 *        }
 *
 *      g_message ("%s: %s", apex_property_get_key (property), apex_property_get_value (property));
 *    }
 */

  status = g_application_run (G_APPLICATION (app), argc, argv);

  apex_log_shutdown ();

  return status;
}
