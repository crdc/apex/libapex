/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include "module.h"
#include "job.h"

typedef ApexApplication ApexModuleExample;
typedef ApexApplicationClass ApexModuleExampleClass;

G_DEFINE_TYPE (ApexModuleExample, apex_module_example, APEX_TYPE_APPLICATION)

static ApexConfigurationResponse *
apex_module_example_get_configuration (ApexApplication  *self,
                                       GError          **error)
{
  g_autofree const gchar *data = NULL;
  g_autoptr (ApexConfiguration) configuration = NULL;
  ApexConfigurationResponse *response = NULL;

  g_message ("execute get-configuration in module example");

  configuration = apex_configuration_new ();
  apex_configuration_set_id (configuration, g_uuid_string_random ());
  apex_configuration_set_namespace (configuration, APEX_CONFIGURATION_NAMESPACE_ACQUIRE);

  response = apex_configuration_response_new ();
  apex_configuration_response_set_configuration (response, configuration);

  // Use response
  g_assert (response != NULL);
  data = apex_configuration_response_serialize (response);
  g_debug ("response: %s", data);

  return response;
}

static ApexSettingsResponse *
apex_module_example_get_settings (ApexApplication  *self,
                                  GError          **error)
{
  g_autoptr (ApexSettingsResponse) response = NULL;

  g_message ("execute get-settings in module example");
  response = apex_settings_response_new ();

  return response;
}

static ApexJobResponse *
apex_module_example_submit_job (ApexApplication  *self,
                                const gchar      *job_id,
                                GError          **error)
{
  g_autoptr (ApexJob) job = NULL;
  /*g_autoptr (ApexJobResponse) response = NULL;*/
  ApexJobResponse *response;

  g_message ("The %s job was requested", job_id);

  job = apex_module_example_job_new ();
  apex_job_set_id (APEX_JOB (job), g_uuid_string_random ());
  response = apex_job_response_new ();

  apex_job_response_set_job (response, APEX_JOB (job));

  return response;
}

static void
apex_module_example_finalize (GObject *object)
{
  G_OBJECT_CLASS (apex_module_example_parent_class)->finalize (object);
}

static void
apex_module_example_class_init (ApexModuleExampleClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = apex_module_example_finalize;
  APEX_APPLICATION_CLASS (klass)->get_configuration = apex_module_example_get_configuration;
  APEX_APPLICATION_CLASS (klass)->get_settings = apex_module_example_get_settings;
  APEX_APPLICATION_CLASS (klass)->submit_job = apex_module_example_submit_job;

  g_debug ("module example class init");
  g_assert (APEX_APPLICATION_CLASS (klass)->get_configuration != NULL);
}

static void
apex_module_example_init (ApexModuleExample *self)
{
  g_debug ("module example init");
}

ApexApplication *
apex_module_example_new (void)
{
  return g_object_new (APEX_TYPE_MODULE_EXAMPLE, NULL);
}
