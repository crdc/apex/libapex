/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <glib.h>
#include <gio/gio.h>

#include "apex/apex.h"

G_BEGIN_DECLS

#define APEX_TYPE_MODULE_EXAMPLE apex_module_example_get_type ()
//G_DECLARE_FINAL_TYPE (ApexModuleExample, apex_module_example, APEX, MODULE_EXAMPLE, ApexApplication)

ApexApplication *apex_module_example_new (void);

G_END_DECLS
