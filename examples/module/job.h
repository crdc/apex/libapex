/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <glib.h>

#include "apex/apex.h"

G_BEGIN_DECLS

#define APEX_TYPE_MODULE_EXAMPLE_JOB apex_module_example_job_get_type ()
//G_DECLARE_FINAL_TYPE (ApexModuleExampleJob, apex_module_example_job, APEX, MODULE_EXAMPLE_JOB, ApexJob)

ApexJob *apex_module_example_job_new (void);

G_END_DECLS
