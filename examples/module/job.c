/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include "job.h"

typedef ApexJob ApexModuleExampleJob;
typedef ApexJobClass ApexModuleExampleJobClass;

G_DEFINE_TYPE (ApexModuleExampleJob, apex_module_example_job, APEX_TYPE_JOB)

static void
apex_module_example_job_task (ApexJob *self)
{
  g_message ("Running task");
  for (gint i = 0; i < 20; i++)
    {
      g_message ("running...");
      g_usleep (500000);
    }
}

static void
apex_module_example_job_finalize (GObject *object)
{
  G_OBJECT_CLASS (apex_module_example_job_parent_class)->finalize (object);
}

static void
apex_module_example_job_class_init (ApexModuleExampleJobClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = apex_module_example_job_finalize;
  APEX_JOB_CLASS (klass)->task = apex_module_example_job_task;
}

static void
apex_module_example_job_init (ApexModuleExampleJob *self)
{
}

ApexJob *
apex_module_example_job_new (void)
{
  return g_object_new (APEX_TYPE_MODULE_EXAMPLE_JOB, NULL);
}
