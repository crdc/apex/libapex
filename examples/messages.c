#include <glib.h>

#include "apex/apex.h"

/**
 * Message initialization.
 *
 * TODO: refactor base type initialization into `init_message`
 * TODO: refactor envelope type initialization into `init_message_envelope`
 */

static void
init_channel (ApexChannel *message)
{
  apex_channel_set_endpoint (message, "ipc://*:9999");
  apex_channel_set_envelope (message, "example");
}

static void
init_configuration (ApexConfiguration *message)
{
  apex_configuration_set_id (message, g_uuid_string_random ());
  apex_configuration_set_namespace (message, APEX_CONFIGURATION_NAMESPACE_ACQUIRE);
}

static void
init_event (ApexEvent *message)
{
  apex_event_set_id (message, 1);
  apex_event_set_name (message, "example");
  apex_event_set_description (message, "Example event");
}

static void
init_job (ApexJob *message)
{
  apex_job_set_id (message, g_uuid_string_random ());
}

static void
init_channel_request (ApexChannelRequest *message)
{
  apex_channel_request_set_id (message, g_uuid_string_random ());
}

static void
init_channel_response (ApexChannelResponse *message,
                       ApexChannel         *channel)
{
  apex_channel_response_set_channel (message, channel);
}

static void
init_channels_response (ApexChannelsResponse *message,
                        ApexChannel          *channel)
{
  apex_channels_response_add_channel (message, channel);
}

static void
init_configuration_request (ApexConfigurationRequest *message)
{
  apex_configuration_request_set_id (message, g_uuid_string_random ());
  apex_configuration_request_set_namespace (message, APEX_CONFIGURATION_NAMESPACE_ACQUIRE);
}

static void
init_configuration_response (ApexConfigurationResponse *message,
                             ApexConfiguration         *configuration)
{
  apex_configuration_response_set_configuration (message, configuration);
}

static void
init_configurations_response (ApexConfigurationsResponse *message,
                              ApexConfiguration          *configuration)
{
  apex_configurations_response_add_configuration (message, configuration);
}

static void
init_event_request (ApexEventRequest *message,
                    ApexEvent        *event)
{
  apex_event_request_set_event (message, event);
}

// XXX: need to change ApexEventResponse -> ApexEventsResponse
/*
 *static void
 *init_events_response (ApexEventsResponse *message,
 *                      ApexEvent          *event)
 *{
 *}
 */

static void
init_job_response (ApexJobResponse *message,
                   ApexJob         *job)
{
  apex_job_response_set_job (message, job);
}

static void
init_job_status_response (ApexJobStatusResponse *message,
                          ApexJob               *job)
{
  apex_job_status_response_set_job (message, job);
}

static void
init_jobs_response (ApexJobsResponse *message,
                    ApexJob          *job)
{
  apex_jobs_response_add_job (message, job);
}

static void
init_module_event_request (ApexModuleEventRequest *message)
{
  apex_module_event_request_set_id (message, g_uuid_string_random ());
  apex_module_event_request_set_event_id (message, 12);
}

static void
init_module_job_request (ApexModuleJobRequest *message)
{
}

static void
init_module_request (ApexModuleRequest *message)
{
  apex_module_request_set_id (message, g_uuid_string_random ());
}

static void
init_module_response (ApexModuleResponse *message,
                      ApexModule         *module)
{
  // apex_module_status_response_set_module (message, module);
}

static void
init_modules_response (ApexModulesResponse *message)
{
}

static void
init_settings_request (ApexSettingsRequest *message)
{
}

static void
init_settings_response (ApexSettingsResponse *message)
{
}

static void
init_status_request (ApexStatusRequest *message)
{
}

static void
init_status_response (ApexStatusResponse *message)
{
}

/**
 * JSON message section.
 */

static void
json_print_message (GObject *message)
{
  g_autofree gchar *json = NULL;
  g_autofree gchar *type = NULL;

  g_assert_nonnull (message);

  /* Message body types */
  if (APEX_IS_CHANNEL (message))
    {
      json = apex_channel_serialize (APEX_CHANNEL (message));
      type = g_strdup (g_type_name (APEX_TYPE_CHANNEL));
    }
  else if (APEX_IS_CONFIGURATION (message))
    {
      json = apex_configuration_serialize (APEX_CONFIGURATION (message));
      type = g_strdup (g_type_name (APEX_TYPE_CONFIGURATION));
    }
  else if (APEX_IS_EVENT (message))
    {
      json = apex_event_serialize (APEX_EVENT (message));
      type = g_strdup (g_type_name (APEX_TYPE_EVENT));
    }
  else if (APEX_IS_JOB (message))
    {
      json = apex_job_serialize (APEX_JOB (message));
      type = g_strdup (g_type_name (APEX_TYPE_JOB));
    }
  else if (APEX_IS_MODULE (message))
    {
      json = g_strdup ("{\"msg\":\"not implemented\"}");
      /*json = apex_module_serialize (APEX_MODULE (message));*/
      type = g_strdup (g_type_name (APEX_TYPE_MODULE));
    }
  else if (APEX_IS_OBJECT (message))
    {
      json = apex_object_serialize (APEX_OBJECT (message));
      type = g_strdup (g_type_name (APEX_TYPE_OBJECT));
    }
  else if (APEX_IS_PROPERTY (message))
    {
      json = apex_property_serialize (APEX_PROPERTY (message));
      type = g_strdup (g_type_name (APEX_TYPE_PROPERTY));
    }
  /*
   *else if (APEX_IS_SERVICE (message))
   *  {
   *    json = apex_service_serialize (APEX_SERVICE (message));
   *    type = g_strdup (g_type_name (APEX_TYPE_SERVICE));
   *  }
   */
  else if (APEX_IS_STATE (message))
    {
      json = apex_state_serialize (APEX_STATE (message));
      type = g_strdup (g_type_name (APEX_TYPE_STATE));
    }
  else if (APEX_IS_STATUS (message))
    {
      json = g_strdup ("{\"msg\":\"not implemented\"}");
      /*json = apex_status_serialize (APEX_STATUS (message));*/
      type = g_strdup (g_type_name (APEX_TYPE_STATUS));
    }
  else if (APEX_IS_SYSTEM (message))
    {
      json = apex_system_serialize (APEX_SYSTEM (message));
      type = g_strdup (g_type_name (APEX_TYPE_SYSTEM));
    }
  /* Request and Response message envelopes */
  else if (APEX_IS_CHANNEL_REQUEST (message))
    {
      json = apex_channel_request_serialize (APEX_CHANNEL_REQUEST (message));
      type = g_strdup (g_type_name (APEX_TYPE_CHANNEL_REQUEST));
    }
  else if (APEX_IS_CHANNEL_RESPONSE (message))
    {
      json = apex_channel_response_serialize (APEX_CHANNEL_RESPONSE (message));
      type = g_strdup (g_type_name (APEX_TYPE_CHANNEL_RESPONSE));
    }
  else if (APEX_IS_CHANNELS_RESPONSE (message))
    {
      json = apex_channels_response_serialize (APEX_CHANNELS_RESPONSE (message));
      type = g_strdup (g_type_name (APEX_TYPE_CHANNELS_RESPONSE));
    }
  else if (APEX_IS_CONFIGURATION_REQUEST (message))
    {
      json = apex_configuration_request_serialize (APEX_CONFIGURATION_REQUEST (message));
      type = g_strdup (g_type_name (APEX_TYPE_CONFIGURATION_REQUEST));
    }
  else if (APEX_IS_CONFIGURATION_RESPONSE (message))
    {
      json = apex_configuration_response_serialize (APEX_CONFIGURATION_RESPONSE (message));
      type = g_strdup (g_type_name (APEX_TYPE_CONFIGURATION_RESPONSE));
    }
  else if (APEX_IS_CONFIGURATIONS_RESPONSE (message))
    {
      json = apex_configurations_response_serialize (APEX_CONFIGURATIONS_RESPONSE (message));
      type = g_strdup (g_type_name (APEX_TYPE_CONFIGURATIONS_RESPONSE));
    }
  else if (APEX_IS_JOB_RESPONSE (message))
    {
      json = apex_job_response_serialize (APEX_JOB_RESPONSE (message));
      type = g_strdup (g_type_name (APEX_TYPE_JOB_RESPONSE));
    }
  else if (APEX_IS_JOB_STATUS_RESPONSE (message))
    {
      json = apex_job_status_response_serialize (APEX_JOB_STATUS_RESPONSE (message));
      type = g_strdup (g_type_name (APEX_TYPE_JOB_STATUS_RESPONSE));
    }
  else if (APEX_IS_JOBS_RESPONSE (message))
    {
      json = apex_jobs_response_serialize (APEX_JOBS_RESPONSE (message));
      type = g_strdup (g_type_name (APEX_TYPE_JOBS_RESPONSE));
    }
  else if (APEX_IS_MODULE_EVENT_REQUEST (message))
    {
      json = apex_module_event_request_serialize (APEX_MODULE_EVENT_REQUEST (message));
      type = g_strdup (g_type_name (APEX_TYPE_MODULE_EVENT_REQUEST));
    }
  else if (APEX_IS_MODULE_REQUEST (message))
    {
      json = apex_module_request_serialize (APEX_MODULE_REQUEST (message));
      type = g_strdup (g_type_name (APEX_TYPE_MODULE_REQUEST));
    }
  // else if (APEX_IS_MODULE_RESPONSE (message))
  //   {
  //     json = apex_module_response_serialize (APEX_MODULE_RESPONSE (message));
  //     type = g_strdup (g_type_name (APEX_TYPE_MODULE_RESPONSE));
  //   }
  /*
   *else if (APEX_IS_ (message))
   *  {
   *    json = apex__serialize (APEX_ (message));
   *    type = g_strdup (g_type_name (APEX_TYPE_));
   *  }
   */
  else
    {
      return;
    }

  g_message ("\n%s:\n%s\n", type, json);
}

/**
 * Protobuf message section.
 *
 * TODO: create a `pb_print_message` similar to JSON one
 */

static void
status_pb (ApexStatus *message)
{
  g_autofree guint8 *data = NULL;
  g_autoptr (GString) str_data = NULL;
  g_autoptr (ApexStatus) unpacked = NULL;

  apex_status_set_enabled (message, true);
  apex_status_set_loaded (message, true);
  apex_status_set_active (message, true);

/*
 *  g_message ("enabled: %d", apex_status_get_enabled (message));
 *
 *  str_data = g_string_new (NULL);
 *  data = apex_status_to_data (message);
 *  for (int i = 0; i < sizeof (data); i++)
 *    {
 *      g_string_append_c (str_data, data[i]);
 *    }
 *  g_message ("Status: %s", str_data->str);
 *
 *  unpacked = apex_status_new_from_data (data);
 */

  g_message ("Enabled: %d", apex_status_get_enabled (unpacked));
  g_message ("Loaded:  %d", apex_status_get_loaded (unpacked));
  g_message ("Active:  %d", apex_status_get_active (unpacked));
}

static void
status_request_pb (ApexStatusRequest *message)
{
  g_autofree guint8 *data = NULL;
  g_autoptr (GString) str_data = NULL;

  str_data = g_string_new (NULL);
  data = apex_status_request_to_data (message);
  for (int i = 0; i < sizeof (data); i++)
    {
      g_string_append_c (str_data, data[i]);
    }
  g_message ("StatusRequest: %s", str_data->str);
}

/**
 * Application section.
 */

gint
main (gint argc, gchar *argv[])
{
  /* TODO: use GOptions for these */
  gboolean json = true;
  gboolean protobuf = false;

  apex_log_init (TRUE, NULL);
  apex_log_increase_verbosity ();
  apex_log_increase_verbosity ();
  apex_log_increase_verbosity ();

  /* Core types */
  g_autoptr (ApexChannel) channel             = NULL;
  g_autoptr (ApexConfiguration) configuration = NULL;
  g_autoptr (ApexEvent) event                 = NULL;
  g_autoptr (ApexJob) job                     = NULL;
  /*g_autoptr (ApexModule) module               = NULL;*/
  g_autoptr (ApexObject) object               = NULL;
  g_autoptr (ApexProperty) property           = NULL;
  g_autoptr (ApexService) service             = NULL;
  g_autoptr (ApexState) state                 = NULL;
  g_autoptr (ApexStatus) status               = NULL;
  g_autoptr (ApexSystem) system               = NULL;

  /* Request/response message envelopes */
  g_autoptr (ApexChannelRequest) channel_req                 = NULL;
  g_autoptr (ApexChannelResponse) channel_resp               = NULL;
  g_autoptr (ApexChannelsResponse) channels_resp             = NULL;
  g_autoptr (ApexConfigurationRequest) configuration_req     = NULL;
  g_autoptr (ApexConfigurationResponse) configuration_resp   = NULL;
  g_autoptr (ApexConfigurationsResponse) configurations_resp = NULL;
  g_autoptr (ApexEmpty) empty                                = NULL;
  g_autoptr (ApexEventRequest) event_req                     = NULL;
  /*g_autoptr (ApexEventsResponse) events_resp                 = NULL;*/
  g_autoptr (ApexJobRequest) job_req                         = NULL;
  g_autoptr (ApexJobResponse) job_resp                       = NULL;
  g_autoptr (ApexJobStatusResponse) job_status_resp          = NULL;
  g_autoptr (ApexJobsResponse) jobs_resp                     = NULL;
  g_autoptr (ApexModuleEventRequest) module_event_req        = NULL;
  g_autoptr (ApexModuleJobRequest) module_job_req            = NULL;
  g_autoptr (ApexModuleRequest) module_req                   = NULL;
  // g_autoptr (ApexModuleResponse) module_resp                 = NULL;
  g_autoptr (ApexModulesResponse) modules_resp               = NULL;
  g_autoptr (ApexSettingsRequest) settings_req               = NULL;
  g_autoptr (ApexSettingsResponse) settings_resp             = NULL;
  g_autoptr (ApexStatusRequest) status_req                   = NULL;
  g_autoptr (ApexStatusResponse) status_resp                 = NULL;

  if (json || protobuf)
    {
      channel = apex_channel_new ();
      configuration = apex_configuration_new ();
      event = apex_event_new ();
      job = apex_job_new ();
      /*module = apex_module_new ();*/
      object = apex_object_new (g_uuid_string_random ());
      property = apex_property_new ("example-key", "example-value");
      service = apex_service_new ("org.apex.example.Messages");
      state = apex_state_new ();
      status = apex_status_new ();
      system = apex_system_new ();

      channel_req = apex_channel_request_new ();
      channel_resp = apex_channel_response_new ();
      channels_resp = apex_channels_response_new ();
      configuration_req = apex_configuration_request_new ();
      configuration_resp = apex_configuration_response_new ();
      configurations_resp = apex_configurations_response_new ();
      empty = apex_empty_new ();
      event_req = apex_event_request_new ();
      /*events_resp = apex_events_response_new ();*/
      job_req = apex_job_request_new (g_uuid_string_random ());
      job_resp = apex_job_response_new ();
      job_status_resp = apex_job_status_response_new ();
      jobs_resp = apex_jobs_response_new ();
      module_event_req = apex_module_event_request_new ();
      module_job_req = apex_module_job_request_new ();
      module_req = apex_module_request_new ();
      // module_resp = apex_module_response_new ();
      modules_resp = apex_modules_response_new ();
      settings_req = apex_settings_request_new ();
      settings_resp = apex_settings_response_new ();
      status_req = apex_status_request_new ();
      status_resp = apex_status_response_new ();

      init_channel (channel);
      init_configuration (configuration);
      init_event (event);
      init_job (job);
      /*init_service (service);*/
      /*init_state (state);*/
      /*init_status (status);*/
      /*init_system (system);*/

      init_channel_request (channel_req);
      init_channel_response (channel_resp, channel);
      init_channels_response (channels_resp, channel);
      init_configuration_request (configuration_req);
      init_configuration_response (configuration_resp, configuration);
      init_configurations_response (configurations_resp, configuration);
      init_event_request (event_req, event);
      /*init_events_response (events_resp);*/
      init_job_response (job_resp, job);
      init_job_status_response (job_status_resp, job);
      init_jobs_response (jobs_resp, job);
      init_module_event_request (module_event_req);
      init_module_job_request (module_job_req);
      init_module_request (module_req);
      // init_module_response (module_resp, module);
      init_modules_response (modules_resp);
      init_settings_request (settings_req);
      init_settings_response (settings_resp);
      init_status_request (status_req);
      init_status_response (status_resp);
    }

  if (json)
    {
      g_info ("JSON serialization");

      json_print_message (G_OBJECT (channel));
      json_print_message (G_OBJECT (configuration));
      json_print_message (G_OBJECT (event));
      json_print_message (G_OBJECT (job));
      /*json_print_message (G_OBJECT (module));*/
      json_print_message (G_OBJECT (object));
      json_print_message (G_OBJECT (property));
      json_print_message (G_OBJECT (service));
      json_print_message (G_OBJECT (state));
      json_print_message (G_OBJECT (status));
      json_print_message (G_OBJECT (system));

      json_print_message (G_OBJECT (channel_req));
      json_print_message (G_OBJECT (channel_resp));
      json_print_message (G_OBJECT (channels_resp));
      json_print_message (G_OBJECT (configuration_req));
      json_print_message (G_OBJECT (configuration_resp));
      json_print_message (G_OBJECT (configurations_resp));
      json_print_message (G_OBJECT (empty));
      json_print_message (G_OBJECT (event_req));
      /*json_print_message (G_OBJECT (events_resp));*/
      json_print_message (G_OBJECT (job_req));
      json_print_message (G_OBJECT (job_resp));
      json_print_message (G_OBJECT (job_status_resp));
      json_print_message (G_OBJECT (jobs_resp));
      json_print_message (G_OBJECT (module_event_req));
      json_print_message (G_OBJECT (module_job_req));
      json_print_message (G_OBJECT (module_req));
      // json_print_message (G_OBJECT (module_resp));
      json_print_message (G_OBJECT (modules_resp));
      json_print_message (G_OBJECT (settings_req));
      json_print_message (G_OBJECT (settings_resp));
      json_print_message (G_OBJECT (status_req));
      json_print_message (G_OBJECT (status_resp));
    }

  if (protobuf)
    {
      g_info ("Protobuf serialization");

      status_pb (status);
      status_request_pb (status_req);
    }

  apex_log_shutdown ();

  return 0;
}
