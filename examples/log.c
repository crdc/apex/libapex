#include <glib.h>

#include "apex/apex.h"

#define G_LOG_DOMAIN "log"

static gboolean
verbose_cb (const gchar  *option_name,
            const gchar  *value,
            gpointer      data,
            GError      **error)
{
  apex_log_increase_verbosity ();
  return TRUE;
}

static void
param_check (gint    *argc,
             gchar ***argv)
{
  g_autoptr(GOptionContext) context = NULL;
  GOptionEntry entries[] = {
    { "verbose", 'v', G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_CALLBACK, verbose_cb },
    { NULL }
  };

  context = g_option_context_new (NULL);
  g_option_context_set_ignore_unknown_options (context, TRUE);
  g_option_context_set_help_enabled (context, FALSE);
  g_option_context_add_main_entries (context, entries, NULL);
  g_option_context_parse (context, argc, argv, NULL);
}

gint
main (gint argc, gchar *argv[])
{
  apex_log_init (true, NULL);

  param_check (&argc, &argv);

  g_critical ("critical");
  g_warning ("warning");
  g_message ("message");
  g_info ("info");
  g_debug ("debug");
  /*g_error ("error");*/

  // enable with: `meson configure -Denable-tracing=true _build`
  APEX_TRACE_MSG ("trace");

  apex_log_shutdown ();

  return 0;
}
