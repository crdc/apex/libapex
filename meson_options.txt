option('enable-tracing', type: 'boolean', value: false)
option('enable-profiling', type: 'boolean', value: false)

# Support for multiple languages
option('with-introspection', type: 'boolean', value: true)
option('with-vapi', type: 'boolean', value: true)

# Extra build things
option('enable-examples',
         type: 'boolean',
        value: false,
  description: 'Whether to compile examples'
)

option('enable-tests',
         type: 'boolean',
        value: true,
  description: 'Whether to compile unit tests'
)

option('enable-gtk-doc',
         type: 'boolean',
        value: false,
  description: 'Whether to generate the API reference for Apex'
)

option('shared-lib',
         type: 'boolean',
        value: false,
  description: 'Whether to build Apex as a shared library'
)

option('python-libprefix', type: 'string')
