#include <errno.h>
#include "apex-error.h"

/**
 * SECTION:apex-error
 * @short_description: Error helper functions
 * @include: apex/apex.h
 *
 * Contains helper functions for reporting errors to the user.
 **/

/**
 * apex_error_quark:
 *
 * Gets the Apex Error Quark.
 *
 * Returns: a #GQuark.
 **/
G_DEFINE_QUARK (apex-error-quark, apex_error)

/**
 * apex_error_from_errno:
 * @err_no: Error number as defined in errno.h.
 *
 * Converts errno.h error codes into Apex error codes.
 *
 * Returns: #ApexErrorEnum value for the given errno.h error number.
 **/
ApexErrorEnum
apex_error_from_errno (gint err_no)
{
  switch (err_no)
    {
#ifdef ENOTCONN
    case ENOTCONN:
      return APEX_ERROR_NOT_CONNECTED;
#endif

    default:
      return APEX_ERROR_FAILED;
    }
}
