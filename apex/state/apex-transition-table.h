/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_TRANSITION_TABLE apex_transition_table_get_type ()
G_DECLARE_FINAL_TYPE (ApexTransitionTable, apex_transition_table, APEX, TRANSITION_TABLE, GObject)

ApexTransitionTable *apex_transition_table_new      (void);

gboolean             apex_transition_table_add      (ApexTransitionTable *self,
                                                     ApexTransition      *transition);
gboolean             apex_transition_table_remove   (ApexTransitionTable *self,
                                                     gint                 row);
gint                 apex_transition_table_length   (ApexTransitionTable *self);
ApexTransition      *apex_transition_table_get      (ApexTransitionTable *self,
                                                     gint                 row);
gboolean             apex_transition_table_contains (ApexTransitionTable *self,
                                                     ApexTransition      *transition);

G_END_DECLS
