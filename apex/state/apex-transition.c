/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include "apex-transition.h"
#include "apex-state.h"
#include "core/apex-event.h"

/**
 * SECTION:apex-transition
 * @short_description: Transition base class
 *
 * An #ApexTransition is used by applications that connect to message buses.
 */

/**
 * ApexTransition:
 *
 * #ApexTransition is used with #ApexStateMachine instances and meant to
 * control program flow.
 */

/**
 * ApexTransitionClass:
 * @evaluate: invoked to determine if the transition should occur.
 * @execute: invoked by the state machine when the transition takes place.
 *
 * Virtual function table for #ApexTransition.
 */

typedef struct
{
  /*< public >*/
  gint       id;
  ApexState *start;
  ApexState *end;
} ApexTransitionPrivate;

enum {
  PROP_0,
  PROP_ID,
  PROP_START,
  PROP_END,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE_WITH_PRIVATE (ApexTransition, apex_transition, G_TYPE_OBJECT)

static void
apex_transition_finalize (GObject *object)
{
  ApexTransitionPrivate *priv;

  priv = apex_transition_get_instance_private (APEX_TRANSITION (object));

  g_clear_object (&priv->start);
  g_clear_object (&priv->end);

  G_OBJECT_CLASS (apex_transition_parent_class)->finalize (object);
}

static void
apex_transition_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  ApexTransition *self;
  G_GNUC_UNUSED ApexTransitionPrivate *priv;

  self = APEX_TRANSITION (object);
  priv = apex_transition_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_int (value, apex_transition_get_id (self));
      break;

    case PROP_START:
      g_value_take_object (value, apex_transition_ref_start_state (self));
      break;

    case PROP_END:
      g_value_take_object (value, apex_transition_ref_end_state (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_transition_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  ApexTransition *self;

  self = APEX_TRANSITION (object);

  switch (prop_id)
    {
    case PROP_ID:
      apex_transition_set_id (self, g_value_get_int (value));
      break;

    case PROP_START:
      apex_transition_set_start_state (self, g_value_get_object (value));
      break;

    case PROP_END:
      apex_transition_set_end_state (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static gboolean
apex_transition_real_evaluate (ApexTransition *self,
                               ApexEvent      *event)
{
  ApexTransitionPrivate *priv;

  g_return_val_if_fail (APEX_IS_TRANSITION (self), FALSE);

  priv = apex_transition_get_instance_private (self);

  if (priv->id == apex_event_get_id (event))
    return TRUE;

  return FALSE;
}

static void
apex_transition_real_execute (ApexTransition *self,
                              gpointer        data)
{
  ApexTransitionPrivate *priv;

  g_return_if_fail (APEX_IS_TRANSITION (self));

  priv = apex_transition_get_instance_private (self);

  g_info ("Transition ID: %d\n", priv->id);
}

static void
apex_transition_class_init (ApexTransitionClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_transition_finalize;

  klass->evaluate = apex_transition_real_evaluate;
  klass->execute = apex_transition_real_execute;

  object_class->get_property = apex_transition_get_property;
  object_class->set_property = apex_transition_set_property;

  properties [PROP_ID] =
    g_param_spec_int ("id",
                      "ID",
                      "The ID of the transition.",
                      G_MININT,
                      G_MAXINT,
                      -1,
                      G_PARAM_READWRITE);

  properties [PROP_START] =
    g_param_spec_object ("start-state",
                         "Start state",
                         "The starting state of the transition.",
                         APEX_TYPE_STATE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_END] =
    g_param_spec_object ("end-state",
                         "End state",
                         "The ending state of the transition.",
                         APEX_TYPE_STATE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_transition_init (ApexTransition *self)
{
}

ApexTransition *
apex_transition_new (gint id)
{
  return g_object_new (APEX_TYPE_TRANSITION,
                       "id", id,
                       NULL);
}

gboolean
apex_transition_equal (ApexTransition *a,
                       ApexTransition *b)
{
  g_return_val_if_fail (APEX_IS_TRANSITION (a), FALSE);
  g_return_val_if_fail (APEX_IS_TRANSITION (b), FALSE);

  return (apex_transition_get_id (a) == apex_transition_get_id (b));
}

gboolean
apex_transition_evaluate (ApexTransition *self,
                          ApexEvent      *event)
{
  ApexTransitionClass *klass;

  g_return_val_if_fail (APEX_IS_TRANSITION (self), FALSE);
  g_return_val_if_fail (APEX_IS_EVENT (event), FALSE);

  klass = APEX_TRANSITION_GET_CLASS (self);
  g_return_val_if_fail (klass->evaluate != NULL, FALSE);

  return klass->evaluate (self, event);
}

void
apex_transition_execute (ApexTransition *self,
                         gpointer        data)
{
  ApexTransitionClass *klass;

  g_return_if_fail (APEX_IS_TRANSITION (self));

  klass = APEX_TRANSITION_GET_CLASS (self);
  g_return_if_fail (klass->execute != NULL);

  return klass->execute (self, data);
}

gint
apex_transition_get_id (ApexTransition *self)
{
  ApexTransitionPrivate *priv;

  g_return_val_if_fail (APEX_IS_TRANSITION (self), G_MAXINT);

  priv = apex_transition_get_instance_private (self);

  return priv->id;
}

void
apex_transition_set_id (ApexTransition *self,
                        gint            id)
{
  ApexTransitionPrivate *priv;

  g_return_if_fail (APEX_IS_TRANSITION (self));

  priv = apex_transition_get_instance_private (self);

  priv->id = id;
}

/**
 * apex_transition_get_start_state:
 * @self: an #ApexTransition
 *
 * Retrieve the #ApexState held as the start state.
 *
 * Returns: (transfer full): an #ApexState if one is set as the start state.
 */
ApexState *
apex_transition_get_start_state (ApexTransition *self)
{
  ApexState *state;

  g_return_val_if_fail (APEX_IS_TRANSITION (self), NULL);

  g_object_get (self, "start-state", &state, NULL);

  return state;
}

/**
 * apex_transition_ref_start_state:
 * @self: an #ApexTransition
 *
 * Gets the start state for the transition, and returns a new reference
 * to the #ApexState.
 *
 * Returns: (transfer full) (nullable): a #ApexState or %NULL
 */
ApexState *
apex_transition_ref_start_state (ApexTransition *self)
{
  ApexTransitionPrivate *priv;
  ApexState *ret = NULL;

  g_return_val_if_fail (APEX_IS_TRANSITION (self), NULL);

  priv = apex_transition_get_instance_private (self);

  /*apex_object_lock (G_OBJECT (self));*/
  g_set_object (&ret, priv->start);
  /*apex_object_unlock (G_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
apex_transition_set_start_state (ApexTransition *self,
                                 ApexState      *start)
{
  ApexTransitionPrivate *priv;

  g_return_if_fail (APEX_IS_TRANSITION (self));
  g_return_if_fail (!start || APEX_IS_STATE (start));

  priv = apex_transition_get_instance_private (self);

  if (g_set_object (&priv->start, start))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_START]);
}

/**
 * apex_transition_get_end_state:
 * @self: an #ApexTransition
 *
 * Retrieve the #ApexState held as the end state.
 *
 * Returns: (transfer full): an #ApexState if one is set as the end state.
 */
ApexState *
apex_transition_get_end_state (ApexTransition *self)
{
  ApexState *state;

  g_return_val_if_fail (APEX_IS_TRANSITION (self), NULL);

  g_object_get (self, "end-state", &state, NULL);

  return state;
}

/**
 * apex_transition_ref_end_state:
 * @self: an #ApexTransition
 *
 * Gets the start state for the transition, and returns a new reference
 * to the #ApexState.
 *
 * Returns: (transfer full) (nullable): a #ApexState or %NULL
 */
ApexState *
apex_transition_ref_end_state (ApexTransition *self)
{
  ApexTransitionPrivate *priv;
  ApexState *ret = NULL;

  g_return_val_if_fail (APEX_IS_TRANSITION (self), NULL);

  priv = apex_transition_get_instance_private (self);

  /*apex_object_lock (G_OBJECT (self));*/
  g_set_object (&ret, priv->end);
  /*apex_object_unlock (G_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
apex_transition_set_end_state (ApexTransition *self,
                               ApexState      *end)
{
  ApexTransitionPrivate *priv;

  g_return_if_fail (APEX_IS_TRANSITION (self));
  g_return_if_fail (!end || APEX_IS_STATE (end));

  priv = apex_transition_get_instance_private (self);

  if (g_set_object (&priv->end, end))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_END]);
}
