/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_TRANSITION apex_transition_get_type ()
G_DECLARE_DERIVABLE_TYPE (ApexTransition, apex_transition, APEX, TRANSITION, GObject)

struct _ApexTransitionClass
{
  /*< private >*/
  GObjectClass parent_class;

  /* vfuncs */

  gboolean (*evaluate) (ApexTransition *self,
                        ApexEvent      *event);
  void     (*execute)  (ApexTransition *self,
                        gpointer        data);

  /*< private >*/
  gpointer padding[12];
};

ApexTransition *apex_transition_new             (gint            id);

gboolean        apex_transition_equal           (ApexTransition *a,
                                                 ApexTransition *b);

gboolean        apex_transition_evaluate        (ApexTransition *self,
                                                 ApexEvent      *event);
void            apex_transition_execute         (ApexTransition *self,
                                                 gpointer        data);

gint            apex_transition_get_id          (ApexTransition *self);
void            apex_transition_set_id          (ApexTransition *self,
                                                 gint            id);

ApexState      *apex_transition_get_start_state (ApexTransition *self);
ApexState      *apex_transition_ref_start_state (ApexTransition *self);
void            apex_transition_set_start_state (ApexTransition *self,
                                                 ApexState      *start);

ApexState      *apex_transition_get_end_state   (ApexTransition *self);
ApexState      *apex_transition_ref_end_state   (ApexTransition *self);
void            apex_transition_set_end_state   (ApexTransition *self,
                                                 ApexState      *end);

G_END_DECLS
