/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include "apex-transition-table.h"

#include "apex/apex.h"

/*
 * ApexTransitionTable:
 *
 * Represents a ...
 */
struct _ApexTransitionTable
{
  GObject    parent;
  GPtrArray *transitions;
};

G_DEFINE_TYPE (ApexTransitionTable, apex_transition_table, G_TYPE_OBJECT)

static void
apex_transition_table_finalize (GObject *object)
{
  ApexTransitionTable *self = (ApexTransitionTable *)object;

  if (self->transitions)
    g_ptr_array_free (self->transitions, TRUE);

  G_OBJECT_CLASS (apex_transition_table_parent_class)->finalize (object);
}

static void
apex_transition_table_class_init (ApexTransitionTableClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_transition_table_finalize;
}

static void
apex_transition_table_init (ApexTransitionTable *self)
{
}

ApexTransitionTable *
apex_transition_table_new (void)
{
  return g_object_new (APEX_TYPE_TRANSITION_TABLE, NULL);
}

/*
 *const gchar *
 *apex_transition_table_to_string (ApexTransitionTable *self)
 *{
 *  g_autoptr (GString) str = NULL;
 *
 *  g_return_val_if_fail (APEX_IS_TRANSITION_TABLE (self), NULL);
 *
 *  str = g_string_new (" # | start |  end |");
 *
 *  for (gint i = 0; i < self->transitions->len; i++)
 *    {
 *      ApexTransition *el;
 *      ApexState *start;
 *      ApexState *end;
 *
 *      el = g_ptr_array_index (self->transitions, i);
 *
 *      g_return_val_if_fail (APEX_IS_TRANSITION (el), NULL);
 *
 *      g_string_append_printf (str, "", );
 *
 *    }
 *
 *  return g_strdup (str->str);
 *}
 */

gboolean
apex_transition_table_add (ApexTransitionTable *self,
                           ApexTransition      *transition)
{
  g_return_val_if_fail (APEX_IS_TRANSITION_TABLE (self), FALSE);
  g_return_val_if_fail (APEX_IS_TRANSITION (transition), FALSE);

  if (self->transitions == NULL)
    self->transitions = g_ptr_array_new ();

  g_ptr_array_add (self->transitions, transition);

  return TRUE;
}

/**
 * apex_transition_table_remove:
 * @self: an #ApexTransitionTable
 * @row: the row number to remove, not the row index
 *
 * returns: TRUE if the row was removed, FALSE otherwise
 */
gboolean
apex_transition_table_remove (ApexTransitionTable *self,
                              gint                 row)
{
  g_return_val_if_fail (APEX_IS_TRANSITION_TABLE (self), FALSE);
  g_return_val_if_fail (self->transitions != NULL, FALSE);

  // Array is not zero terminated so there's no extra element to -1
  if (row <= self->transitions->len)
    {
      g_ptr_array_remove_index (self->transitions, row - 1);
      return TRUE;
    }

  return FALSE;
}

gint
apex_transition_table_length (ApexTransitionTable *self)
{
  g_return_val_if_fail (APEX_IS_TRANSITION_TABLE (self), 0);
  g_return_val_if_fail (self->transitions != NULL, 0);

  return self->transitions->len;
}

/**
 * apex_transition_table_get:
 * @self: an #ApexTransitionTable
 *
 * Retrieve the #ApexTransition located at the given row index.
 *
 * Returns: (transfer none): an #ApexTransition if one is set at the index given.
 */
ApexTransition *
apex_transition_table_get (ApexTransitionTable *self,
                           gint                 row)
{
  g_return_val_if_fail (APEX_IS_TRANSITION_TABLE (self), NULL);
  g_return_val_if_fail (self->transitions != NULL, NULL);

  return g_ptr_array_index (self->transitions, row);
}

gboolean
apex_transition_table_contains (ApexTransitionTable *self,
                                ApexTransition      *transition)
{
  g_return_val_if_fail (APEX_IS_TRANSITION_TABLE (self), FALSE);
  g_return_val_if_fail (APEX_IS_TRANSITION (transition), FALSE);

  for (gint i = 0; i < self->transitions->len; i++)
    {
      ApexTransition *el;

      el = g_ptr_array_index (self->transitions, i);

      g_return_val_if_fail (APEX_IS_TRANSITION (el), FALSE);

      if (apex_transition_equal (transition, el))
        return TRUE;
    }

  return FALSE;
}
