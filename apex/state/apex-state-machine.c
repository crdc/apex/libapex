/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <gio/gio.h>

#include "apex-state-machine.h"
#include "apex-state.h"
#include "apex-transition.h"
#include "apex-transition-table.h"
#include "core/apex-event.h"

// TODO: implement add_sub_machine for forks

/**
 * SECTION:apex-state-machine
 * @short_description: State machine class
 *
 * An #ApexStateMachine is used by applications that control program flow by
 * altering state using events.
 */

/*
 * ApexStateMachine:
 *
 * #ApexStateMachine is an implementation of a finite state machine.
 */

typedef struct
{
  /*< private >*/
  ApexTransitionTable *table;
  gboolean             running;
  ApexState           *initial_state;
  ApexState           *current_state;
  GAsyncQueue         *queue;
} ApexStateMachinePrivate;

enum {
  SIGNAL_STATE_CHANGED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

G_DEFINE_TYPE_WITH_PRIVATE (ApexStateMachine, apex_state_machine, G_TYPE_OBJECT)

static void
apex_state_machine_finalize (GObject *object)
{
  ApexStateMachine *self = (ApexStateMachine *)object;
  ApexStateMachinePrivate *priv;

  priv = apex_state_machine_get_instance_private (self);

  apex_state_machine_stop (self);

  g_clear_object (&priv->table);
  g_clear_object (&priv->initial_state);
  g_clear_object (&priv->current_state);
  g_clear_pointer (&priv->queue, g_async_queue_unref);

  G_OBJECT_CLASS (apex_state_machine_parent_class)->finalize (object);
}

static void
apex_state_machine_class_init (ApexStateMachineClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_state_machine_finalize;

  /**
   * ApexStateMachine::state_changed:
   * @self: the state machine
   *
   * The ::state_changed signal is emitted on the primary instance
   * after it has transitioned into a new state.
   */
  signals [SIGNAL_STATE_CHANGED] =
    g_signal_new ("state-changed", APEX_TYPE_STATE_MACHINE, G_SIGNAL_RUN_FIRST,
                  G_STRUCT_OFFSET (ApexStateMachineClass, state_changed),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 1, APEX_TYPE_STATE);
}

static void
apex_state_machine_init (ApexStateMachine *self)
{
  ApexStateMachinePrivate *priv;

  priv = apex_state_machine_get_instance_private (self);

  priv->running = FALSE;
  priv->queue = g_async_queue_new ();
}

ApexStateMachine *
apex_state_machine_new (void)
{
  return g_object_new (APEX_TYPE_STATE_MACHINE, NULL);
}

static void
_update_state (ApexStateMachine *self,
               ApexTransition   *transition)
{
  ApexStateMachinePrivate *priv;
  ApexState *new_state;

  priv = apex_state_machine_get_instance_private (self);

  new_state = apex_transition_get_end_state (transition);
  g_set_object (&priv->current_state, new_state);

  // XXX: not totally positive that passing the state machine down is wise
  apex_transition_execute (transition, self);
}

static gboolean
_process_event (ApexStateMachine *self,
                ApexEvent        *event)
{
  ApexStateMachinePrivate *priv;
  g_autoptr (ApexState) frozen = NULL;

  priv = apex_state_machine_get_instance_private (self);

  frozen = apex_state_copy (priv->current_state);

  for (gint i = 0; i < apex_transition_table_length (priv->table); i++)
    {
      ApexTransition *transition;
      ApexState *start;
      ApexState *end;

      transition = apex_transition_table_get (priv->table, i);
      start = apex_transition_get_start_state (transition);
      end = apex_transition_get_end_state (transition);

      g_debug (" * i:%2d/%2d | t:%5d | f:%5d | s:%5d | e:%5d",
               i, apex_transition_table_length (priv->table),
               apex_transition_get_id (transition),
               apex_state_get_id (frozen),
               apex_state_get_id (start),
               apex_state_get_id (end));

      if (apex_state_equal (start, frozen))
        {
          g_debug (" > i:%2d/%2d | t:%5d | f:%5d = s:%5d | e:%5d | evt:%5d",
                   i, apex_transition_table_length (priv->table),
                   apex_transition_get_id (transition),
                   apex_state_get_id (frozen),
                   apex_state_get_id (start),
                   apex_state_get_id (end),
                   apex_event_get_id (event));

          if (apex_transition_evaluate (transition, event))
            {
              _update_state (self, transition);
              g_signal_emit (self, signals [SIGNAL_STATE_CHANGED], 0, priv->current_state);

              return TRUE;
            }
        }
    }

  return FALSE;
}

static void
_run_state_machine_cb (GObject      *source_object,
                       GAsyncResult *result,
                       gpointer      user_data)
{
  g_debug ("state machine finished");
}

static void
_run_state_machine_cancel (ApexStateMachine *self,
                           gpointer          data)
{
  ApexStateMachinePrivate *priv;
  GCancellable *cancellable G_GNUC_UNUSED;

  priv = apex_state_machine_get_instance_private (self);

  /*cancellable = G_CANCELLABLE (cancellable);*/

  priv->running = FALSE;

  g_debug ("state machine cancelled");
}

static void
_run_state_machine_thread (GTask        *task,
                           gpointer      source_object,
                           gpointer      task_data,
                           GCancellable *cancellable)
{
  ApexStateMachinePrivate *priv;

  g_assert (source_object == g_task_get_source_object (task));
  g_assert (cancellable == g_task_get_cancellable (task));

  priv = apex_state_machine_get_instance_private (source_object);

  g_debug ("starting state machine");

  if (priv->table == NULL)
    g_task_return_boolean (task, FALSE);

  if (apex_transition_table_length (priv->table) == 0)
    g_task_return_boolean (task, FALSE);

  /*
   *if (priv->initial_state != NULL)
   *  priv->current_state = priv->initial_state;
   *else
   *  priv->current_state = apex_transition_get_start_state (
   *      apex_transition_table_get (priv->table, 0));
   */

  priv->current_state = apex_transition_get_start_state (
      apex_transition_table_get (priv->table, 0));

  /*
   *g_debug ("starting at %s (%d): %s",
   *         apex_state_get_id (priv->current_state),
   *         apex_state_get_name (priv->current_state),
   *         apex_state_get_description (priv->current_state));
   */

  priv->running = TRUE;

  while (priv->running)
    {
      g_autoptr (ApexEvent) event = NULL;

      if (g_cancellable_is_cancelled (cancellable))
        {
          break;
        }

      g_async_queue_lock (priv->queue);

      // block and wait for next event
      event = g_async_queue_pop_unlocked (priv->queue);

      if (!_process_event (APEX_STATE_MACHINE (source_object), APEX_EVENT (event)))
        g_debug ("skip state %d in %d",
                 apex_event_get_id (event),
                 apex_state_get_id (priv->current_state));

      g_async_queue_unlock (priv->queue);
    }

  priv->running = FALSE;

  g_task_return_boolean (task, TRUE);
}

static void
_run_state_machine_async (ApexStateMachine *self)
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (GCancellable) cancellable = NULL;

  g_return_if_fail (APEX_IS_STATE_MACHINE (self));

  cancellable = g_cancellable_new ();
  task = g_task_new (self, cancellable, _run_state_machine_cb, NULL);

  g_task_run_in_thread (task, _run_state_machine_thread);
}

void
apex_state_machine_start (ApexStateMachine *self)
{
  g_return_if_fail (APEX_IS_STATE_MACHINE (self));

  _run_state_machine_async (self);
}

void
apex_state_machine_stop (ApexStateMachine *self)
{
  g_return_if_fail (APEX_IS_STATE_MACHINE (self));

  _run_state_machine_cancel (self, NULL);
}

gboolean
apex_state_machine_running (ApexStateMachine *self)
{
  ApexStateMachinePrivate *priv;

  g_return_val_if_fail (APEX_IS_STATE_MACHINE (self), FALSE);

  priv = apex_state_machine_get_instance_private (self);

  return priv->running;
}

void
apex_state_machine_submit_event (ApexStateMachine *self,
                                 ApexEvent        *event)
{
  ApexStateMachinePrivate *priv;
  ApexEvent *copy;

  g_return_if_fail (APEX_IS_STATE_MACHINE (self));
  g_return_if_fail (APEX_IS_EVENT (event));

  priv = apex_state_machine_get_instance_private (self);

  copy = apex_event_new_full (apex_event_get_id (event),
                              apex_event_get_name (event),
                              apex_event_get_description (event));

  g_async_queue_lock (priv->queue);
  g_async_queue_push_unlocked (priv->queue, copy);
  g_async_queue_unlock (priv->queue);
}

void
apex_state_machine_set_transition_table (ApexStateMachine    *self,
                                         ApexTransitionTable *table)
{
  ApexStateMachinePrivate *priv;

  g_return_if_fail (APEX_IS_STATE_MACHINE (self));
  g_return_if_fail (APEX_IS_TRANSITION_TABLE (table));

  priv = apex_state_machine_get_instance_private (self);

  g_set_object (&priv->table, table);
}

/**
 * apex_state_machine_get_current_state:
 * @self: an #ApexStateMachine
 *
 * Retrieve the #ApexState held as the current state.
 *
 * Returns: (transfer none): an #ApexState if one is set as the current state.
 */
ApexState *
apex_state_machine_get_current_state (ApexStateMachine *self)
{
  ApexStateMachinePrivate *priv;

  g_return_val_if_fail (APEX_IS_STATE_MACHINE (self), NULL);

  priv = apex_state_machine_get_instance_private (self);

  return priv->current_state;
}

gboolean
apex_state_machine_set_initial_state (ApexStateMachine *self,
                                      ApexState        *state)
{
  ApexStateMachinePrivate *priv;

  g_return_val_if_fail (APEX_IS_STATE_MACHINE (self), FALSE);

  priv = apex_state_machine_get_instance_private (self);

  if (priv->running)
    return FALSE;

  if (apex_state_equal (priv->initial_state, state))
    return TRUE;

  if (g_set_object (&priv->initial_state, state))
    return TRUE;

  return FALSE;
}
