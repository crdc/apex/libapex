/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-state.h"

struct _ApexState
{
  GObject  parent;
  gint     id;
  gchar   *name;
  gchar   *description;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_NAME,
  PROP_DESCRIPTION,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexState, apex_state, G_TYPE_OBJECT)

static void
apex_state_finalize (GObject *object)
{
  ApexState *self = (ApexState *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->description, g_free);

  G_OBJECT_CLASS (apex_state_parent_class)->finalize (object);
}

static void
apex_state_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  ApexState *self = APEX_STATE (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_int (value, apex_state_get_id (self));
      break;

    case PROP_NAME:
      g_value_take_string (value, apex_state_dup_name (self));
      break;

    case PROP_DESCRIPTION:
      g_value_take_string (value, apex_state_dup_description (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_state_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  ApexState *self = APEX_STATE (object);

  switch (prop_id)
    {
    case PROP_ID:
      apex_state_set_id (self, g_value_get_int (value));
      break;

    case PROP_NAME:
      apex_state_set_name (self, g_value_get_string (value));
      break;

    case PROP_DESCRIPTION:
      apex_state_set_description (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_state_class_init (ApexStateClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_state_finalize;
  object_class->get_property = apex_state_get_property;
  object_class->set_property = apex_state_set_property;

  properties [PROP_ID] =
    g_param_spec_int ("id",
                      "ID",
                      "The ID of the state.",
                      G_MININT,
                      G_MAXINT,
                      0,
                      (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the state.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_DESCRIPTION] =
    g_param_spec_string ("description",
                         "Description",
                         "The state description",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_state_init (ApexState *self)
{
}

ApexState *
apex_state_new (void)
{
  return g_object_new (APEX_TYPE_STATE, NULL);
}

ApexState *
apex_state_new_full (gint         id,
                     const gchar *name,
                     const gchar *description)
{
  return g_object_new (APEX_TYPE_STATE,
                       "id", id,
                       "name", name,
                       "description", description,
                       NULL);
}

ApexState *
apex_state_copy (ApexState *self)
{
  g_return_val_if_fail (APEX_IS_STATE (self), NULL);

  return g_object_new (APEX_TYPE_STATE,
                       "id", apex_state_get_id (self),
                       "name", apex_state_get_name (self),
                       "description", apex_state_get_description (self),
                       NULL);
}

gboolean
apex_state_equal (ApexState *a,
                  ApexState *b)
{
  g_return_val_if_fail (APEX_IS_STATE (a), FALSE);
  g_return_val_if_fail (APEX_IS_STATE (b), FALSE);

  return (apex_state_get_id (a) == apex_state_get_id (b));
}

/**
 * apex_state_serialize:
 * @self: an #ApexState
 *
 * Returns the serialized json data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_state_serialize (ApexState *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_STATE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
apex_state_deserialize (ApexState   *self,
                        const gchar *data)
{
  g_autoptr (GObject) object = NULL;
  g_autofree gchar *name = NULL;
  g_autofree gchar *description = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (APEX_TYPE_STATE,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_STATE (object));

  name = apex_state_dup_name (APEX_STATE (object));
  description = apex_state_dup_description (APEX_STATE (object));

  apex_state_set_id (self, apex_state_get_id (APEX_STATE (object)));
  apex_state_set_name (self, name);
  apex_state_set_description (self, description);

  g_clear_object (&object);
}

gint
apex_state_get_id (ApexState *self)
{
  // TODO: define a no-state state for this
  g_return_val_if_fail (APEX_IS_STATE (self), -1);

  return self->id;
}

void
apex_state_set_id (ApexState *self,
                   gint       id)
{
  g_return_if_fail (APEX_IS_STATE (self));

  self->id = id;
}

const gchar *
apex_state_get_name (ApexState *self)
{
  g_return_val_if_fail (APEX_IS_STATE (self), NULL);

  return self->name;
}

/**
 * apex_state_dup_name:
 *
 * Copies the name of the state and returns it to the caller (after
 * locking the object). A copy is used to avoid thread-races.
 *
 * Returns: (transfer full): a copy of the state name.
 */
gchar *
apex_state_dup_name (ApexState *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_STATE (self), NULL);

  /*apex_object_lock (APEX_OBJECT (self));*/
  ret = g_strdup (self->name);
  /*apex_object_unlock (APEX_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
apex_state_set_name (ApexState   *self,
                     const gchar *name)
{
  g_return_if_fail (APEX_IS_STATE (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

const gchar *
apex_state_get_description (ApexState *self)
{
  g_return_val_if_fail (APEX_IS_STATE (self), NULL);

  return self->description;
}

/**
 * apex_state_dup_description:
 *
 * Copies the description of the state and returns it to the caller (after
 * locking the object). A copy is used to avoid thread-races.
 *
 * Returns: (transfer full): a copy of the state description.
 */
gchar *
apex_state_dup_description (ApexState *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_STATE (self), NULL);

  /*apex_object_lock (APEX_OBJECT (self));*/
  ret = g_strdup (self->description);
  /*apex_object_unlock (APEX_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
apex_state_set_description (ApexState   *self,
                            const gchar *description)
{
  g_return_if_fail (APEX_IS_STATE (self));

  if (g_strcmp0 (description, self->description) != 0)
    {
      g_free (self->description);
      self->description = g_strdup (description);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DESCRIPTION]);
    }
}
