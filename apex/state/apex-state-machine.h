/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_STATE_MACHINE apex_state_machine_get_type ()
G_DECLARE_DERIVABLE_TYPE (ApexStateMachine, apex_state_machine, APEX, STATE_MACHINE, GObject)

/* TODO: remove if private class isn't needed for signal function */
struct _ApexStateMachineClass
{
  /*< private >*/
  GObjectClass parent_class;

  /*< public >*/
  /* signals */
  void (*state_changed) (ApexApplication *self,
                         ApexState       *state);

  /*< private >*/
  gpointer padding[12];
};

ApexStateMachine *apex_state_machine_new                  (void);

void              apex_state_machine_start                (ApexStateMachine    *self);
void              apex_state_machine_stop                 (ApexStateMachine    *self);
gboolean          apex_state_machine_running              (ApexStateMachine    *self);
void              apex_state_machine_submit_event         (ApexStateMachine    *self,
                                                           ApexEvent           *event);
void              apex_state_machine_set_transition_table (ApexStateMachine    *self,
                                                           ApexTransitionTable *table);
ApexState        *apex_state_machine_get_current_state    (ApexStateMachine    *self);
gboolean          apex_state_machine_set_initial_state    (ApexStateMachine    *self,
                                                           ApexState           *state);

G_END_DECLS
