/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_STATE apex_state_get_type ()
G_DECLARE_FINAL_TYPE (ApexState, apex_state, APEX, STATE, GObject)

ApexState   *apex_state_new             (void);
ApexState   *apex_state_new_full        (gint         id,
                                         const gchar *name,
                                         const gchar *description);

ApexState   *apex_state_copy            (ApexState   *self);
void         apex_state_free            (ApexState   *self);
gboolean     apex_state_equal           (ApexState   *a,
                                         ApexState   *b);

gchar       *apex_state_serialize       (ApexState   *self);
void         apex_state_deserialize     (ApexState   *self,
                                         const gchar *data);

gint         apex_state_get_id          (ApexState   *self);
void         apex_state_set_id          (ApexState   *self,
                                         gint         id);

const gchar *apex_state_get_name        (ApexState   *self);
gchar       *apex_state_dup_name        (ApexState   *self);
void         apex_state_set_name        (ApexState   *self,
                                         const gchar *name);

const gchar *apex_state_get_description (ApexState   *self);
gchar       *apex_state_dup_description (ApexState   *self);
void         apex_state_set_description (ApexState   *self,
                                         const gchar *description);

G_END_DECLS
