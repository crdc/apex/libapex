/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "apex-job-request"

#include <json-glib/json-glib.h>

#include <apex/apex.h>

/*
 * ApexJobRequest:
 *
 * Represents a ...
 */
struct _ApexJobRequest
{
  GObject     parent;
  gchar      *id;
  gchar      *job_id;
  gchar      *job_value;
  GHashTable *job_properties;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_JOB_ID,
  PROP_JOB_VALUE,
  PROP_JOB_PROPERTIES,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (ApexJobRequest, apex_job_request, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
apex_job_request_serialize_property (JsonSerializable *serializable,
                                     const gchar      *name,
                                     const GValue     *value,
                                     GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "job-id") == 0 || g_strcmp0 (name, "jobId") == 0)
    {
      retval = json_node_new (JSON_NODE_VALUE);
      json_node_set_string (retval, g_value_get_string (value));
    }
  else if (g_strcmp0 (name, "job-value") == 0 || g_strcmp0 (name, "jobValue") == 0)
    {
      retval = json_node_new (JSON_NODE_VALUE);
      json_node_set_string (retval, g_value_get_string (value));
    }
  else if (g_strcmp0 (name, "job-properties") == 0 || g_strcmp0 (name, "jobProperties") == 0)
    {
      JsonArray *arr;
      GHashTable *job_properties;
      GHashTableIter iter;
      gpointer key, val;

      retval = json_node_new (JSON_NODE_ARRAY);

      g_return_val_if_fail (value != NULL, retval);
      g_return_val_if_fail (G_VALUE_HOLDS_POINTER (value), retval);

      job_properties = g_value_get_pointer (value);

      if (job_properties == NULL)
        return retval;

      arr = json_array_new ();

      if (job_properties != NULL)
        {
          g_hash_table_iter_init (&iter, job_properties);
          while(g_hash_table_iter_next (&iter, &key, &val))
            {
              g_autoptr (ApexProperty) property = NULL;
              JsonNode *node;
              JsonObject *obj;

              property = apex_property_new (key, val);
              node = json_gobject_serialize (G_OBJECT (property));

              if (JSON_NODE_HOLDS_OBJECT (node))
                {
                  obj = json_node_dup_object (node);
                  json_array_add_object_element (arr, obj);
                }

              json_node_free (node);
            }
        }

      json_node_take_array (retval, arr);
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
apex_job_request_deserialize_property (JsonSerializable *serializable,
                                       const gchar      *name,
                                       GValue           *value,
                                       GParamSpec       *pspec,
                                       JsonNode         *property_node)
{
  gboolean retval = FALSE;

  if (g_strcmp0 (name, "job-id") == 0 || g_strcmp0 (name, "jobId") == 0)
    {
      g_value_set_string (value, json_node_get_string (property_node));
      retval = TRUE;
    }
  else if (g_strcmp0 (name, "job-value") == 0 || g_strcmp0 (name, "jobValue") == 0)
    {
      g_value_set_string (value, json_node_get_string (property_node));
      retval = TRUE;
    }
  else if (g_strcmp0 (name, "job-properties") == 0 || g_strcmp0 (name, "jobProperties") == 0)
    {
      g_autoptr (GHashTable) job_properties = NULL;
      g_autoptr (JsonArray) arr = NULL;
      gint len;

      arr = json_node_dup_array (property_node);
      job_properties = g_hash_table_new_full (g_str_hash,
                                              g_str_equal,
                                              g_free,
                                              g_free);

      if (arr == NULL)
        {
          len = 0;
          json_array_ref (arr);
        }
      else
        {
          len = json_array_get_length (arr);
        }

      for (gint i = 0; i < len; i++)
        {
          g_autoptr (ApexProperty) property = NULL;
          JsonNode *node = NULL;

          node = json_array_dup_element (arr, i);

          property = APEX_PROPERTY (json_gobject_deserialize (APEX_TYPE_PROPERTY, node));
          g_return_val_if_fail (APEX_IS_PROPERTY (property), FALSE);
          g_hash_table_insert (job_properties,
                               g_strdup (apex_property_get_key (property)),
                               g_strdup (apex_property_get_value (property)));
          json_node_free (node);
        }

      g_value_set_pointer (value, g_hash_table_ref (job_properties));

      retval = TRUE;
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = apex_job_request_serialize_property;
  iface->deserialize_property = apex_job_request_deserialize_property;
}

static void
apex_job_request_finalize (GObject *object)
{
  ApexJobRequest *self = (ApexJobRequest *)object;

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->job_id, g_free);
  g_clear_pointer (&self->job_value, g_free);
  g_clear_pointer (&self->job_properties, g_hash_table_unref);

  G_OBJECT_CLASS (apex_job_request_parent_class)->finalize (object);
}

static void
apex_job_request_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  ApexJobRequest *self = APEX_JOB_REQUEST (object);

  switch (prop_id)
  {
    case PROP_ID:
      g_value_set_string (value, apex_job_request_get_id (self));
      break;

    case PROP_JOB_ID:
      g_value_set_string (value, apex_job_request_get_job_id (self));
      break;

    case PROP_JOB_VALUE:
      g_value_set_string (value, apex_job_request_get_job_value (self));
      break;

    case PROP_JOB_PROPERTIES:
      g_value_set_pointer (value, self->job_properties);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_job_request_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  ApexJobRequest *self = APEX_JOB_REQUEST (object);

  switch (prop_id)
  {
    case PROP_ID:
      apex_job_request_set_id (self, g_value_get_string (value));
      break;

    case PROP_JOB_ID:
      apex_job_request_set_job_id (self, g_value_get_string (value));
      break;

    case PROP_JOB_VALUE:
      apex_job_request_set_job_value (self, g_value_get_string (value));
      break;

    case PROP_JOB_PROPERTIES:
      apex_job_request_set_list (self, g_value_get_pointer (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_job_request_class_init (ApexJobRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_job_request_finalize;
  object_class->get_property = apex_job_request_get_property;
  object_class->set_property = apex_job_request_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The ID of the job being requested.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_JOB_ID] =
    g_param_spec_string ("jobId",
                         "Job ID",
                         "The ID of the job being requested.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_JOB_VALUE] =
    g_param_spec_string ("jobValue",
                         "Job value",
                         "The Value of the job being requested.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_JOB_PROPERTIES] =
    g_param_spec_pointer ("jobProperties",
                          "Job properties",
                          "The job properties list",
                          (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_job_request_init (ApexJobRequest *self)
{
}

ApexJobRequest *
apex_job_request_new (const gchar *id)
{
  return g_object_new (APEX_TYPE_JOB_REQUEST,
                       "id", id,
                       NULL);
}

/**
 * apex_job_request_serialize:
 * @self: an #ApexJobRequest
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_job_request_serialize (ApexJobRequest *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_JOB_REQUEST (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
apex_job_request_deserialize (ApexJobRequest *self,
                               const gchar     *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (GHashTable) job_properties = NULL;
  g_autofree gchar *id = NULL;
  g_autofree gchar *job_id = NULL;
  g_autofree gchar *job_value = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (APEX_TYPE_JOB_REQUEST,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_JOB_REQUEST (object));

  id = g_strdup (apex_job_request_get_id (APEX_JOB_REQUEST (object)));
  job_id = g_strdup (apex_job_request_get_job_id (APEX_JOB_REQUEST (object)));
  job_value = g_strdup (apex_job_request_get_job_value (APEX_JOB_REQUEST (object)));
  job_properties = apex_job_request_get_list (APEX_JOB_REQUEST (object));

  apex_job_request_set_id (self, id);
  apex_job_request_set_job_id (self, job_id);
  apex_job_request_set_job_value (self, job_value);
  apex_job_request_set_list (self, job_properties);

  g_clear_object (&object);
}

void
apex_job_request_add (ApexJobRequest *self,
                      ApexProperty   *property)
{
  const gchar *key;
  const gchar *value;

  g_return_if_fail (APEX_IS_JOB_REQUEST (self));
  g_return_if_fail (APEX_IS_PROPERTY (property));

  key = apex_property_get_key (property);
  value = apex_property_get_value (property);

  if (self->job_properties == NULL)
    self->job_properties = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);

  if (g_hash_table_contains (self->job_properties, key))
    g_hash_table_replace (self->job_properties, g_strdup (key), g_strdup (value));
  else
    g_hash_table_insert (self->job_properties, g_strdup (key), g_strdup (value));
}

void
apex_job_request_remove (ApexJobRequest *self,
                         const gchar    *key)
{
  g_return_if_fail (APEX_IS_JOB_REQUEST (self));
  g_return_if_fail (self->job_properties != NULL);

  if (g_hash_table_contains (self->job_properties, key))
    g_hash_table_remove (self->job_properties, key);
}

ApexProperty *
apex_job_request_get (ApexJobRequest *self,
                      const gchar    *key)
{
  g_return_val_if_fail (APEX_IS_JOB_REQUEST (self), NULL);
  g_return_val_if_fail (self->job_properties != NULL, NULL);

  if (g_hash_table_contains (self->job_properties, key))
    return apex_property_new (key, g_hash_table_lookup (self->job_properties, key));

  return NULL;
}

gboolean
apex_job_request_contains (ApexJobRequest *self,
                           const gchar    *key)
{
  gboolean ret;

  g_return_val_if_fail (APEX_IS_JOB_REQUEST (self), FALSE);
  g_return_val_if_fail (self->job_properties != NULL, FALSE);
  g_return_val_if_fail (key != NULL, FALSE);

  ret = g_hash_table_contains (self->job_properties, key);

  return ret;
}

GHashTable *
apex_job_request_get_list (ApexJobRequest *self)
{
  GHashTable *job_properties;

  g_return_val_if_fail (APEX_IS_JOB_REQUEST (self), NULL);

  g_object_get (self, "jobProperties", &job_properties, NULL);

  return job_properties;
}

void
apex_job_request_set_list (ApexJobRequest *self,
                           GHashTable     *job_properties)
{
  g_return_if_fail (APEX_IS_JOB_REQUEST (self));

  if (self->job_properties == job_properties)
    return;

  if (self->job_properties)
    g_hash_table_unref (self->job_properties);

  self->job_properties = job_properties;

  if (job_properties)
    g_hash_table_ref (job_properties);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_JOB_PROPERTIES]);
}

const gchar *
apex_job_request_get_id (ApexJobRequest *self)
{
  g_return_val_if_fail (APEX_IS_JOB_REQUEST (self), NULL);

  return self->id;
}

void
apex_job_request_set_id (ApexJobRequest *self,
                         const gchar    *id)
{
  g_return_if_fail (APEX_IS_JOB_REQUEST (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}

const gchar *
apex_job_request_get_job_id (ApexJobRequest *self)
{
  g_return_val_if_fail (APEX_IS_JOB_REQUEST (self), NULL);

  return self->job_id;
}

void
apex_job_request_set_job_id (ApexJobRequest *self,
                             const gchar    *job_id)
{
  g_return_if_fail (APEX_IS_JOB_REQUEST (self));

  if (g_strcmp0 (job_id, self->job_id) != 0)
    {
      g_free (self->job_id);
      self->job_id = g_strdup (job_id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_JOB_ID]);
    }
}

const gchar *
apex_job_request_get_job_value (ApexJobRequest *self)
{
  g_return_val_if_fail (APEX_IS_JOB_REQUEST (self), NULL);

  return self->job_value;
}

void
apex_job_request_set_job_value (ApexJobRequest *self,
                                const gchar    *job_value)
{
  g_return_if_fail (APEX_IS_JOB_REQUEST (self));

  if (g_strcmp0 (job_value, self->job_value) != 0)
    {
      g_free (self->job_value);
      self->job_value = g_strdup (job_value);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_JOB_VALUE]);
    }
}
