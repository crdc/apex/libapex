/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_STATUS_RESPONSE apex_status_response_get_type ()
G_DECLARE_FINAL_TYPE (ApexStatusResponse, apex_status_response, APEX, STATUS_RESPONSE, GObject)

ApexStatusResponse *apex_status_response_new           (void);
ApexStatusResponse *apex_status_response_new_from_data (const guint8       *data);

guint8             *apex_status_response_to_data       (ApexStatusResponse *self);

gchar              *apex_status_response_serialize     (ApexStatusResponse *self);
void                apex_status_response_deserialize   (ApexStatusResponse *self,
                                                        const gchar        *data);

ApexStatus         *apex_status_response_get_status    (ApexStatusResponse *self);
void                apex_status_response_set_status    (ApexStatusResponse *self,
                                                        ApexStatus         *status);

G_END_DECLS
