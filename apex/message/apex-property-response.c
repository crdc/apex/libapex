/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-property-response.h"
#include "apex-response.h"
#include "core/apex-property.h"

/*
 * ApexPropertyResponse:
 *
 * Represents a ...
 */
struct _ApexPropertyResponse
{
  ApexResponse  parent;
  ApexProperty *field;
};

enum {
  PROP_0,
  PROP_FIELD,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexPropertyResponse, apex_property_response, APEX_TYPE_RESPONSE)

static void
apex_property_response_finalize (GObject *object)
{
  ApexPropertyResponse *self = (ApexPropertyResponse *)object;

  g_clear_object (&self->field);

  G_OBJECT_CLASS (apex_property_response_parent_class)->finalize (object);
}

static void
apex_property_response_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
  ApexPropertyResponse *self = APEX_PROPERTY_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_FIELD:
      g_value_take_object (value, apex_property_response_ref (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_property_response_set_property (GObject      *object,
                                     guint         prop_id,
                                     const GValue *value,
                                     GParamSpec   *pspec)
{
  ApexPropertyResponse *self = APEX_PROPERTY_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_FIELD:
      apex_property_response_set (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_property_response_class_init (ApexPropertyResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_property_response_finalize;
  object_class->get_property = apex_property_response_get_property;
  object_class->set_property = apex_property_response_set_property;

  properties [PROP_FIELD] =
    g_param_spec_object ("property",
                         "Property",
                         "The property field of the response",
                         APEX_TYPE_PROPERTY,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_property_response_init (ApexPropertyResponse *self)
{
}

ApexPropertyResponse *
apex_property_response_new (void)
{
  return g_object_new (APEX_TYPE_PROPERTY_RESPONSE, NULL);
}

/**
 * apex_property_response_serialize:
 * @self: an #ApexPropertyResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_property_response_serialize (ApexPropertyResponse *self)
{
  gchar *ret = NULL;

  g_return_val_if_fail (APEX_IS_PROPERTY_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
apex_property_response_deserialize (ApexPropertyResponse *self,
                                    const gchar          *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (ApexProperty) field = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (APEX_TYPE_PROPERTY_RESPONSE,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_PROPERTY_RESPONSE (object));

  field = apex_property_response_ref (APEX_PROPERTY_RESPONSE (object));
  apex_property_response_set (self, field);

  g_clear_object (&object);
}

/**
 * apex_property_response_get:
 * @self: an #ApexPropertyResponse
 *
 * Retrieve the #ApexProperty contained by the response message.
 *
 * Returns: (transfer none): an #ApexProperty if one is set.
 */
ApexProperty *
apex_property_response_get (ApexPropertyResponse *self)
{
  ApexProperty *field;

  g_return_val_if_fail (APEX_IS_PROPERTY_RESPONSE (self), NULL);

  g_object_get (self, "property", &field, NULL);

  return field;
}

/**
 * apex_property_ref:
 *
 * Gets the property field for the response, and returns a new reference
 * to the #ApexProperty.
 *
 * Returns: (transfer full) (nullable): a #ApexProperty or %NULL
 */
ApexProperty *
apex_property_response_ref (ApexPropertyResponse *self)
{
  ApexProperty *ret = NULL;

  g_return_val_if_fail (APEX_IS_PROPERTY_RESPONSE (self), NULL);

  g_set_object (&ret, self->field);

  return g_steal_pointer (&ret);
}

void
apex_property_response_set (ApexPropertyResponse *self,
                            ApexProperty         *field)
{
  g_return_if_fail (APEX_IS_PROPERTY_RESPONSE (self));
  g_return_if_fail (APEX_IS_PROPERTY (field));

  if (self->field)
    g_object_unref (self->field);

  if (field)
    g_object_ref (field);

  self->field = field;
}
