/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "apex-job-status-response"

#include <json-glib/json-glib.h>

#include <apex/apex.h>

/*
 * ApexJobStatusResponse:
 *
 * Represents a ...
 */
struct _ApexJobStatusResponse
{
  GObject  parent;
  ApexJob *job;
};

enum {
  PROP_0,
  PROP_JOB,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexJobStatusResponse, apex_job_status_response, G_TYPE_OBJECT)

static void
apex_job_status_response_finalize (GObject *object)
{
  ApexJobStatusResponse *self = (ApexJobStatusResponse *)object;

  // TODO: really need to determine if things like ApexJob should be a boxed type with
  // a _ref and _unref, see DzlRing as example
  g_object_unref (self->job);

  G_OBJECT_CLASS (apex_job_status_response_parent_class)->finalize (object);
}

static void
apex_job_status_response_get_property (GObject    *object,
                                       guint       prop_id,
                                       GValue     *value,
                                       GParamSpec *pspec)
{
  ApexJobStatusResponse *self = APEX_JOB_STATUS_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_JOB:
      /*g_value_set_object (value, apex_job_status_response_get_job (self));*/
      g_value_set_object (value, self->job);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_job_status_response_set_property (GObject      *object,
                                       guint         prop_id,
                                       const GValue *value,
                                       GParamSpec   *pspec)
{
  ApexJobStatusResponse *self = APEX_JOB_STATUS_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_JOB:
      apex_job_status_response_set_job (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_job_status_response_class_init (ApexJobStatusResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_job_status_response_finalize;
  object_class->get_property = apex_job_status_response_get_property;
  object_class->set_property = apex_job_status_response_set_property;

  properties [PROP_JOB] =
    g_param_spec_object ("job",
                         "Job",
                         "The job to use in the response.",
                         APEX_TYPE_JOB,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_job_status_response_init (ApexJobStatusResponse *self)
{
  self->job = apex_job_new ();
}

ApexJobStatusResponse *
apex_job_status_response_new (void)
{
  return g_object_new (APEX_TYPE_JOB_STATUS_RESPONSE, NULL);
}

/**
 * apex_job_status_response_serialize:
 * @self: an #ApexJobStatusResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_job_status_response_serialize (ApexJobStatusResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_JOB_STATUS_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
apex_job_status_response_deserialize (ApexJobStatusResponse *self,
                                      const gchar           *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_JOB_STATUS_RESPONSE,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_JOB_STATUS_RESPONSE (object));

  apex_job_status_response_set_job (
    self,
    apex_job_status_response_get_job (APEX_JOB_STATUS_RESPONSE (object)));

  g_object_unref (object);
}

/**
 * apex_job_status_response_get_job:
 * @self: #ApexJobStatusResponse instance
 *
 * Returns: (transfer full): The #ApexJob
 */
ApexJob *
apex_job_status_response_get_job (ApexJobStatusResponse *self)
{
  ApexJob *job;

  g_return_val_if_fail (APEX_IS_JOB_STATUS_RESPONSE (self), NULL);

  g_object_get (self, "job", &job, NULL);

  return job;
}

void
apex_job_status_response_set_job (ApexJobStatusResponse *self,
                                  ApexJob               *job)
{
  g_return_if_fail (APEX_IS_JOB_STATUS_RESPONSE (self));
  g_return_if_fail (APEX_IS_JOB (job));

  if (self->job)
    g_object_unref (self->job);

  if (job)
    g_object_ref (job);

  self->job = job;
}
