/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "apex-jobs-response"

#include <json-glib/json-glib.h>

#include <apex/apex.h>

/*
 * ApexJobsResponse:
 *
 * Represents a ...
 */
struct _ApexJobsResponse
{
  GObject    parent;
  GPtrArray *jobs;
};

enum {
  PROP_0,
  PROP_JOBS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

// G_DEFINE_TYPE (ApexJobsResponse, apex_jobs_response, G_TYPE_OBJECT)

G_DEFINE_TYPE_WITH_CODE (ApexJobsResponse, apex_jobs_response, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
apex_jobs_response_serialize_property (JsonSerializable *serializable,
                                       const gchar      *name,
                                       const GValue     *value,
                                       GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "jobs") == 0)
    {
      g_autoptr (GPtrArray) jobs = NULL;
      JsonArray *arr = NULL;

      retval = json_node_new (JSON_NODE_ARRAY);
      arr = json_array_new ();

      jobs = g_value_get_boxed (value);

      for (int i = 0; i < jobs->len; i++)
        {
          JsonNode *node = NULL;
          JsonObject *obj = NULL;

          node = json_gobject_serialize (jobs->pdata[i]);

          if (JSON_NODE_HOLDS_OBJECT (node))
            {
              obj = json_node_get_object (node);
              json_array_add_object_element (arr, obj);
            }
        }

      json_node_take_array (retval, arr);
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
apex_jobs_response_deserialize_property (JsonSerializable *serializable,
                                         const gchar      *name,
                                         GValue           *value,
                                         GParamSpec       *pspec,
                                         JsonNode         *property_node)
{
  gboolean retval = FALSE;
  JsonArray *arr = NULL;

  if (g_strcmp0 (name, "jobs") == 0)
    {
      g_autoptr (GPtrArray) jobs = NULL;
      arr = json_node_get_array (property_node);
      jobs = g_ptr_array_new ();

      for (int i = 0; i < json_array_get_length (arr); i++)
        {
          g_autoptr (ApexJob) job = NULL;
          JsonNode *node = NULL;

          node = json_array_get_element (arr, i);

          job = APEX_JOB (json_gobject_deserialize (APEX_TYPE_JOB, node));
          g_return_val_if_fail (APEX_IS_JOB (job), FALSE);
          g_ptr_array_add (jobs, job);
        }

      g_value_set_boxed (value, jobs);

      retval = TRUE;
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = apex_jobs_response_serialize_property;
  iface->deserialize_property = apex_jobs_response_deserialize_property;
}

static void
apex_jobs_response_finalize (GObject *object)
{
  G_GNUC_UNUSED ApexJobsResponse *self = (ApexJobsResponse *)object;

  // XXX: not sure what to do with this
  /*g_clear_object (self->jobs, g_object_unref);*/

  G_OBJECT_CLASS (apex_jobs_response_parent_class)->finalize (object);
}

static void
apex_jobs_response_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  ApexJobsResponse *self = APEX_JOBS_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_JOBS:
      g_value_set_boxed (value, apex_jobs_response_get_jobs (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_jobs_response_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  ApexJobsResponse *self = APEX_JOBS_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_JOBS:
      apex_jobs_response_set_jobs (self, g_value_get_boxed (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_jobs_response_class_init (ApexJobsResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_jobs_response_finalize;
  object_class->get_property = apex_jobs_response_get_property;
  object_class->set_property = apex_jobs_response_set_property;

  properties [PROP_JOBS] =
    g_param_spec_boxed ("jobs",
                        "Jobs",
                        "The list of jobs to provide in the response.",
                        G_TYPE_PTR_ARRAY,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_jobs_response_init (ApexJobsResponse *self)
{
}

ApexJobsResponse *
apex_jobs_response_new (void)
{
  return g_object_new (APEX_TYPE_JOBS_RESPONSE, NULL);
}

/**
 * apex_jobs_response_serialize:
 * @self: an #ApexJobsResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_jobs_response_serialize (ApexJobsResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_JOBS_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
apex_jobs_response_deserialize (ApexJobsResponse *self,
                                const gchar      *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_JOBS_RESPONSE,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
  {
    g_critical ("%s", err->message);
    g_error_free (err);
  }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_JOBS_RESPONSE (object));

  apex_jobs_response_set_jobs (self,
                               apex_jobs_response_get_jobs (
                                 APEX_JOBS_RESPONSE (object)));

  g_object_unref (object);
}

void
apex_jobs_response_add_job (ApexJobsResponse *self,
                            ApexJob          *job)
{
  g_return_if_fail (APEX_IS_JOBS_RESPONSE (self));
  g_return_if_fail (APEX_IS_JOB (job));

  g_ptr_array_add (self->jobs, job);
}

void
apex_jobs_response_remove_job (ApexJobsResponse *self,
                               ApexJob          *job)
{
  g_return_if_fail (APEX_IS_JOBS_RESPONSE (self));
  g_return_if_fail (APEX_IS_JOB (job));

  for (int i = 0; i < self->jobs->len; i++)
    {
      // TODO: add ID to job and compare here, endpoint is wrong
      if (g_strcmp0 (apex_job_get_id (job),
                     apex_job_get_id (self->jobs->pdata[i])) == 0)
        self->jobs = g_ptr_array_remove_index (self->jobs, i);
    }
}

/**
 * apex_jobs_response_get_jobs:
 * @self: a #ApexJobsResponse
 *
 * Returns: (element-type Apex.Job) (transfer full): an array of
 *          #ApexJob objects, free the array with g_ptr_array_free when
 *          done.
 */
GPtrArray *
apex_jobs_response_get_jobs (ApexJobsResponse *self)
{
  g_return_val_if_fail (APEX_IS_JOBS_RESPONSE (self), NULL);

  return self->jobs;
}

/**
 * apex_jobs_response_set_jobs:
 * @self: a #ApexJobsResponse
 * @jobs: (element-type Apex.Job): an array of #ApexJob objects
 *            to set.
 */
void
apex_jobs_response_set_jobs (ApexJobsResponse *self,
                             GPtrArray        *jobs)
{
  g_return_if_fail (APEX_IS_JOBS_RESPONSE (self));

  if (self->jobs == jobs)
    return;

  if (self->jobs != NULL)
    g_ptr_array_unref (self->jobs);

  self->jobs = jobs;

  if (self->jobs != NULL)
    g_ptr_array_ref (self->jobs);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_JOBS]);
}
