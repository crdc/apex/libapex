/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include "apex-modules-response.h"

/*
 * ApexModulesResponse:
 *
 * Represents a ...
 */
struct _ApexModulesResponse
{
  GObject parent;
};

G_DEFINE_TYPE (ApexModulesResponse, apex_modules_response, G_TYPE_OBJECT)

static void
apex_modules_response_finalize (GObject *object)
{
  G_GNUC_UNUSED ApexModulesResponse *self = (ApexModulesResponse *)object;

  G_OBJECT_CLASS (apex_modules_response_parent_class)->finalize (object);
}

static void
apex_modules_response_class_init (ApexModulesResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_modules_response_finalize;
}

static void
apex_modules_response_init (ApexModulesResponse *self)
{
}

ApexModulesResponse *
apex_modules_response_new (void)
{
  return g_object_new (APEX_TYPE_MODULES_RESPONSE, NULL);
}
