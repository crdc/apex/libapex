/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_JOB_STATUS_RESPONSE apex_job_status_response_get_type ()
G_DECLARE_FINAL_TYPE (ApexJobStatusResponse, apex_job_status_response, APEX, JOB_STATUS_RESPONSE, GObject)

ApexJobStatusResponse *apex_job_status_response_new         (void);

gchar                 *apex_job_status_response_serialize   (ApexJobStatusResponse *self);
void                   apex_job_status_response_deserialize (ApexJobStatusResponse *self,
                                                             const gchar           *data);

ApexJob               *apex_job_status_response_get_job     (ApexJobStatusResponse *self);
void                   apex_job_status_response_set_job     (ApexJobStatusResponse *self,
                                                             ApexJob               *job);

G_END_DECLS
