/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

#include "apex-response.h"

G_BEGIN_DECLS

#define APEX_TYPE_PROPERTY_RESPONSE apex_property_response_get_type ()
G_DECLARE_FINAL_TYPE (ApexPropertyResponse, apex_property_response, APEX, PROPERTY_RESPONSE, ApexResponse)

ApexPropertyResponse *apex_property_response_new         (void);

gchar                *apex_property_response_serialize   (ApexPropertyResponse *self);
void                  apex_property_response_deserialize (ApexPropertyResponse *self,
                                                          const gchar          *data);

ApexProperty         *apex_property_response_get         (ApexPropertyResponse *self);
ApexProperty         *apex_property_response_ref         (ApexPropertyResponse *self);
void                  apex_property_response_set         (ApexPropertyResponse *self,
                                                          ApexProperty         *property);

G_END_DECLS
