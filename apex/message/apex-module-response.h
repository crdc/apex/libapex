/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_MODULE_RESPONSE apex_module_response_get_type ()
G_DECLARE_FINAL_TYPE (ApexModuleResponse, apex_module_response, APEX, MODULE_RESPONSE, GObject)

ApexModuleResponse *apex_module_response_new         (void);

gchar              *apex_module_response_serialize   (ApexModuleResponse *self);
void                apex_module_response_deserialize (ApexModuleResponse *self,
                                                      const gchar        *data);

ApexModule         *apex_module_response_get_module  (ApexModuleResponse *self);
void                apex_module_response_set_module  (ApexModuleResponse *self,
                                                      ApexModule         *module);

G_END_DECLS
