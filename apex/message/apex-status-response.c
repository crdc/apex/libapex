#include "apex-status-response.h"

#include "proto/v1/core.pb-c.h"

/**
 * ApexStatusResponse:
 *
 * Represents a status response protobuf message.
 */
struct _ApexStatusResponse
{
	GObject parent;

  Apex__StatusResponse *pb;
};

G_DEFINE_TYPE (ApexStatusResponse, apex_status_response, G_TYPE_OBJECT)

static void
apex_status_response_finalize (GObject *object)
{
  ApexStatusResponse *self = (ApexStatusResponse *)object;

  apex__status_response__free_unpacked (self->pb, NULL);

  G_OBJECT_CLASS (apex_status_response_parent_class)->finalize (object);
}

static void
apex_status_response_class_init (ApexStatusResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_status_response_finalize;
}

static void
apex_status_response_init (ApexStatusResponse *self)
{
}

ApexStatusResponse *
apex_status_response_new (void)
{
  ApexStatusResponse *response = g_object_new (APEX_TYPE_STATUS_RESPONSE, NULL);

  response->pb = g_malloc (sizeof (Apex__StatusResponse));
  // or ???
  apex__status_response__init (response->pb);

  return response;
}

ApexStatusResponse *
apex_status_response_new_from_data (const guint8 *data)
{
  ApexStatusResponse *response = g_object_new (APEX_TYPE_STATUS_RESPONSE, NULL);

  gsize len = sizeof (data) / sizeof (guint8);

  response->pb = apex__status_response__unpack (NULL, len, data);
  if (response->pb == NULL)
    {
      g_error ("error unpacking status response message");
    }

  return response;
}

guint8 *
apex_status_response_to_data (ApexStatusResponse *self)
{
  guint8 *out = NULL;

  g_return_val_if_fail (APEX_IS_STATUS_RESPONSE (self), NULL);

  // TODO: not sure what to do with this yet, return_val_if ?
  if (apex__status_response__pack (self->pb, out) == 0)
    {
      g_error ("error packing status response message");
    }

  return out;
}
