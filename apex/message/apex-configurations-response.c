/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-configurations-response.h"
#include "core/apex-configuration.h"

/*
 * ApexConfigurationsResponse:
 *
 * Represents a ...
 */
struct _ApexConfigurationsResponse
{
  GObject    parent;
  GPtrArray *configurations;
};

enum {
  PROP_0,
  PROP_CONFIGURATIONS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (ApexConfigurationsResponse, apex_configurations_response, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
apex_configurations_response_serialize_property (JsonSerializable *serializable,
                                                 const gchar      *name,
                                                 const GValue     *value,
                                                 GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "configurations") == 0)
    {
      g_autoptr (GPtrArray) configurations = NULL;
      JsonArray *arr = NULL;

      retval = json_node_new (JSON_NODE_ARRAY);
      arr = json_array_new ();

      configurations = g_value_get_boxed (value);

      for (int i = 0; i < configurations->len; i++)
        {
          JsonNode *node = NULL;
          JsonObject *obj = NULL;

          node = json_gobject_serialize (configurations->pdata[i]);

          if (JSON_NODE_HOLDS_OBJECT (node))
            {
              obj = json_node_get_object (node);
              json_array_add_object_element (arr, obj);
            }
        }

      json_node_take_array (retval, arr);
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
apex_configurations_response_deserialize_property (JsonSerializable *serializable,
                                                   const gchar      *name,
                                                   GValue           *value,
                                                   GParamSpec       *pspec,
                                                   JsonNode         *property_node)
{
  gboolean retval = FALSE;
  JsonArray *arr = NULL;

  if (g_strcmp0 (name, "configurations") == 0)
    {
      g_autoptr (GPtrArray) configurations = NULL;
      arr = json_node_get_array (property_node);
      configurations = g_ptr_array_new ();

      for (int i = 0; i < json_array_get_length (arr); i++)
        {
          g_autoptr (ApexConfiguration) configuration = NULL;
          JsonNode *node = NULL;

          node = json_array_get_element (arr, i);

          configuration =
            APEX_CONFIGURATION (json_gobject_deserialize (
                  APEX_TYPE_CONFIGURATION,
                  node));

          g_return_val_if_fail (APEX_IS_CONFIGURATION (configuration), FALSE);
          g_ptr_array_add (configurations, configuration);
        }

      g_value_set_boxed (value, configurations);

      retval = TRUE;
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = apex_configurations_response_serialize_property;
  iface->deserialize_property = apex_configurations_response_deserialize_property;
}

static void
apex_configurations_response_finalize (GObject *object)
{
  ApexConfigurationsResponse *self = (ApexConfigurationsResponse *)object;

  g_ptr_array_unref (self->configurations);

  G_OBJECT_CLASS (apex_configurations_response_parent_class)->finalize (object);
}

static void
apex_configurations_response_get_property (GObject *object,
                                           guint       prop_id,
                                           GValue     *value,
                                           GParamSpec *pspec)
{
  ApexConfigurationsResponse *self = APEX_CONFIGURATIONS_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_CONFIGURATIONS:
      g_value_set_boxed (value, apex_configurations_response_get_configurations (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_configurations_response_set_property (GObject      *object,
    guint         prop_id,
    const GValue *value,
    GParamSpec   *pspec)
{
  ApexConfigurationsResponse *self = APEX_CONFIGURATIONS_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_CONFIGURATIONS:
      apex_configurations_response_set_configurations (self, g_value_get_boxed (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_configurations_response_class_init (ApexConfigurationsResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_configurations_response_finalize;
  object_class->get_property = apex_configurations_response_get_property;
  object_class->set_property = apex_configurations_response_set_property;

  properties [PROP_CONFIGURATIONS] =
    g_param_spec_boxed ("configurations",
                        "Configurations",
                        "The list of configurations to provide in the response.",
                        G_TYPE_PTR_ARRAY,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_configurations_response_init (ApexConfigurationsResponse *self)
{
}

ApexConfigurationsResponse *
apex_configurations_response_new (void)
{
  // TODO: move into init?
  g_autoptr (GPtrArray) configurations = NULL;

  configurations = g_ptr_array_new ();

  return g_object_new (APEX_TYPE_CONFIGURATIONS_RESPONSE,
                       "configurations", configurations,
                       NULL);
}

/**
 * apex_configurations_response_serialize:
 * @self: an #ApexConfigurationsResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_configurations_response_serialize (ApexConfigurationsResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_CONFIGURATIONS_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
apex_configurations_response_deserialize (ApexConfigurationsResponse *self,
                                          const gchar                *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_CONFIGURATIONS_RESPONSE,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
  {
    g_critical ("%s", err->message);
    g_error_free (err);
  }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_CONFIGURATIONS_RESPONSE (object));

  apex_configurations_response_set_configurations (self,
      apex_configurations_response_get_configurations (
        APEX_CONFIGURATIONS_RESPONSE (object)));

  g_object_unref (object);
}

void
apex_configurations_response_add_configuration (ApexConfigurationsResponse *self,
                                                ApexConfiguration          *configuration)
{
  g_return_if_fail (APEX_IS_CONFIGURATIONS_RESPONSE (self));
  g_return_if_fail (APEX_IS_CONFIGURATION (configuration));

  g_ptr_array_add (self->configurations, configuration);
}

void
apex_configurations_response_remove_configuration (ApexConfigurationsResponse *self,
                                                   ApexConfiguration          *configuration)
{
  g_return_if_fail (APEX_IS_CONFIGURATIONS_RESPONSE (self));
  g_return_if_fail (APEX_IS_CONFIGURATION (configuration));

  for (int i = 0; i < self->configurations->len; i++)
    {
      // TODO: add ID to configuration and compare here, endpoint is wrong
      if (g_strcmp0 (apex_configuration_get_id (configuration),
                     apex_configuration_get_id (self->configurations->pdata[i])) == 0)
        self->configurations = g_ptr_array_remove_index (self->configurations, i);
    }
}

/**
 * apex_configurations_response_get_configurations:
 * @self: a #ApexConfigurationsResponse
 *
 * Returns: (element-type Apex.Configuration) (transfer full): an array of
 *          #ApexConfiguration objects, free the array with g_ptr_array_free
 *          when done.
 */
GPtrArray *
apex_configurations_response_get_configurations (ApexConfigurationsResponse *self)
{
  g_return_val_if_fail (APEX_IS_CONFIGURATIONS_RESPONSE (self), NULL);

  return self->configurations;
}

/**
 * apex_configurations_response_set_configurations:
 * @self: a #ApexConfigurationsResponse
 * @configurations: (element-type Apex.Configuration): an array of
 *                  #ApexConfiguration objects to set.
 */
void
apex_configurations_response_set_configurations (ApexConfigurationsResponse *self,
                                                 GPtrArray                  *configurations)
{
  g_return_if_fail (APEX_IS_CONFIGURATIONS_RESPONSE (self));

  if (self->configurations == configurations)
    return;

  if (self->configurations != NULL)
    g_ptr_array_unref (self->configurations);

  self->configurations = configurations;

  if (self->configurations != NULL)
    g_ptr_array_ref (self->configurations);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CONFIGURATIONS]);
}
