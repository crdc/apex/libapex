/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_SETTINGS_RESPONSE apex_settings_response_get_type ()
G_DECLARE_FINAL_TYPE (ApexSettingsResponse, apex_settings_response, APEX, SETTINGS_RESPONSE, GObject)

ApexSettingsResponse *apex_settings_response_new            (void);

gchar                *apex_settings_response_serialize      (ApexSettingsResponse *self);
void                  apex_settings_response_deserialize    (ApexSettingsResponse *self,
                                                             const gchar          *data);

// XXX: why is there an ID in this? doesn't make sense
const gchar          *apex_settings_response_get_id         (ApexSettingsResponse *self);
gchar                *apex_settings_response_dup_id         (ApexSettingsResponse *self);
void                  apex_settings_response_set_id         (ApexSettingsResponse *self,
                                                             const gchar          *id);

GHashTable           *apex_settings_response_get_settings   (ApexSettingsResponse *self);
void                  apex_settings_response_set_settings   (ApexSettingsResponse *self,
                                                             GHashTable           *settings);

void                  apex_settings_response_add_setting    (ApexSettingsResponse *self,
                                                             const gchar          *key,
                                                             const gchar          *value);
void                  apex_settings_response_remove_setting (ApexSettingsResponse *self,
                                                             const gchar          *key);
const gchar          *apex_settings_response_get_setting    (ApexSettingsResponse *self,
                                                             const gchar          *key);

G_END_DECLS
