/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_RESPONSE apex_response_get_type ()
G_DECLARE_DERIVABLE_TYPE (ApexResponse, apex_response, APEX, RESPONSE, GObject)

struct _ApexResponseClass
{
  /*< private >*/
  GObjectClass parent_class;

  /*< private >*/
  gpointer padding[12];
};

ApexResponse     *apex_response_new         (void);

gchar            *apex_response_serialize   (ApexResponse     *self);
void              apex_response_deserialize (ApexResponse     *self,
                                             const gchar      *data);

ApexMessageError *apex_response_get_error   (ApexResponse     *self);
ApexMessageError *apex_response_ref_error   (ApexResponse     *self);
void              apex_response_set_error   (ApexResponse     *self,
                                             ApexMessageError *error);

G_END_DECLS
