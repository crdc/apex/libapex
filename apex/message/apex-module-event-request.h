/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_MODULE_EVENT_REQUEST apex_module_event_request_get_type ()
G_DECLARE_FINAL_TYPE (ApexModuleEventRequest, apex_module_event_request, APEX, MODULE_EVENT_REQUEST, GObject)

ApexModuleEventRequest *apex_module_event_request_new          (void);

gchar                  *apex_module_event_request_serialize    (ApexModuleEventRequest *self);
void                    apex_module_event_request_deserialize  (ApexModuleEventRequest *self,
                                                                const gchar            *data);

const gchar            *apex_module_event_request_get_id       (ApexModuleEventRequest *self);
void                    apex_module_event_request_set_id       (ApexModuleEventRequest *self,
                                                                const gchar            *id);

gint                    apex_module_event_request_get_event_id (ApexModuleEventRequest *self);
void                    apex_module_event_request_set_event_id (ApexModuleEventRequest *self,
                                                                gint                    event_id);

G_END_DECLS
