/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_CHANNEL_RESPONSE apex_channel_response_get_type ()
G_DECLARE_FINAL_TYPE (ApexChannelResponse, apex_channel_response, APEX, CHANNEL_RESPONSE, GObject)

ApexChannelResponse *apex_channel_response_new         (void);

gchar               *apex_channel_response_serialize   (ApexChannelResponse *self);
void                 apex_channel_response_deserialize (ApexChannelResponse *self,
                                                        const gchar         *data);

ApexChannel         *apex_channel_response_get_channel (ApexChannelResponse *self);
void                 apex_channel_response_set_channel (ApexChannelResponse *self,
                                                        ApexChannel         *channel);

G_END_DECLS
