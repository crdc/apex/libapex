/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-channel-request.h"

struct _ApexChannelRequest
{
  GObject parent;
  gchar   *id;
};

enum {
  PROP_0,
  PROP_ID,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexChannelRequest, apex_channel_request, G_TYPE_OBJECT)

static void
apex_channel_request_finalize (GObject *object)
{
  ApexChannelRequest *self = (ApexChannelRequest *)object;

  g_clear_pointer (&self->id, g_free);

  G_OBJECT_CLASS (apex_channel_request_parent_class)->finalize (object);
}

static void
apex_channel_request_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  ApexChannelRequest *self = APEX_CHANNEL_REQUEST (object);

  switch (prop_id)
  {
    case PROP_ID:
      g_value_set_string (value, apex_channel_request_get_id (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_channel_request_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  ApexChannelRequest *self = APEX_CHANNEL_REQUEST (object);

  switch (prop_id)
  {
    case PROP_ID:
      apex_channel_request_set_id (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_channel_request_class_init (ApexChannelRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_channel_request_finalize;
  object_class->get_property = apex_channel_request_get_property;
  object_class->set_property = apex_channel_request_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The ID of the channel being requested.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_channel_request_init (ApexChannelRequest *self)
{
}

ApexChannelRequest *
apex_channel_request_new (void)
{
  return g_object_new (APEX_TYPE_CHANNEL_REQUEST, NULL);
}

/**
 * apex_channel_request_serialize:
 * @self: an #ApexChannelRequest
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_channel_request_serialize (ApexChannelRequest *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_CHANNEL_REQUEST (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
apex_channel_request_deserialize (ApexChannelRequest *self,
                                  const gchar *data)
{
  g_autoptr (GObject) object = NULL;
  g_autofree gchar *id = NULL;
  GError *err = NULL;

  object = json_gobject_from_data (APEX_TYPE_CHANNEL_REQUEST,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
  {
    g_critical ("%s", err->message);
    g_error_free (err);
  }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_CHANNEL_REQUEST (object));

  id = g_strdup (apex_channel_request_get_id (APEX_CHANNEL_REQUEST (object)));

  apex_channel_request_set_id (self, id);

  g_clear_object (&object);
}

const gchar *
apex_channel_request_get_id (ApexChannelRequest *self)
{
  g_return_val_if_fail (APEX_IS_CHANNEL_REQUEST (self), NULL);

  return self->id;
}

void
apex_channel_request_set_id (ApexChannelRequest *self,
                             const gchar        *id)
{
  g_return_if_fail (APEX_IS_CHANNEL_REQUEST (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}
