/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_MODULE_REQUEST apex_module_request_get_type ()
G_DECLARE_FINAL_TYPE (ApexModuleRequest, apex_module_request, APEX, MODULE_REQUEST, GObject)

ApexModuleRequest *apex_module_request_new         (void);

gchar             *apex_module_request_serialize   (ApexModuleRequest *self);
void               apex_module_request_deserialize (ApexModuleRequest *self,
                                                    const gchar       *data);

const gchar       *apex_module_request_get_id  (ApexModuleRequest *self);
void               apex_module_request_set_id  (ApexModuleRequest *self,
                                                const gchar       *id);

G_END_DECLS
