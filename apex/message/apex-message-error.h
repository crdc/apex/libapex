/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_MESSAGE_ERROR apex_message_error_get_type ()
G_DECLARE_FINAL_TYPE (ApexMessageError, apex_message_error, APEX, MESSAGE_ERROR, GObject)

ApexMessageError *apex_message_error_new         (void);

gchar            *apex_message_error_serialize   (ApexMessageError *self);
void              apex_message_error_deserialize (ApexMessageError *self,
                                                   const gchar      *data);

const gchar      *apex_message_error_get_message   (ApexMessageError *self);
void              apex_message_error_set_message   (ApexMessageError *self,
                                                    const gchar      *message);

gint              apex_message_error_get_code      (ApexMessageError *self);
void              apex_message_error_set_code      (ApexMessageError *self,
                                                   gint              code);

G_END_DECLS
