/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_JOBS_RESPONSE apex_jobs_response_get_type ()
G_DECLARE_FINAL_TYPE (ApexJobsResponse, apex_jobs_response, APEX, JOBS_RESPONSE, GObject)

ApexJobsResponse *apex_jobs_response_new            (void);

gchar            *apex_jobs_response_serialize      (ApexJobsResponse *self);
void              apex_jobs_response_deserialize    (ApexJobsResponse *self,
                                                     const gchar      *data);

void              apex_jobs_response_add_job        (ApexJobsResponse *self,
                                                     ApexJob          *job);
void              apex_jobs_response_remove_job     (ApexJobsResponse *self,
                                                     ApexJob          *job);

GPtrArray        *apex_jobs_response_get_jobs       (ApexJobsResponse *self);
void              apex_jobs_response_set_jobs       (ApexJobsResponse *self,
                                                     GPtrArray        *jobs);

G_END_DECLS
