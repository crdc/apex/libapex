/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_MODULES_RESPONSE apex_modules_response_get_type ()
G_DECLARE_FINAL_TYPE (ApexModulesResponse, apex_modules_response, APEX, MODULES_RESPONSE, GObject)

ApexModulesResponse *apex_modules_response_new         (void);

gchar               *apex_modules_response_serialize   (ApexModulesResponse *self);
void                 apex_modules_response_deserialize (ApexModulesResponse *self,
                                                        const gchar         *data);

GPtrArray           *apex_modules_response_get_modules (ApexModulesResponse *self);
void                 apex_modules_response_set_modules (ApexModulesResponse *self,
                                                        GPtrArray           *modules);

G_END_DECLS
