/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-channels-response.h"
#include "core/apex-channel.h"

/*
 * ApexChannelsResponse:
 *
 * Represents a ...
 */
struct _ApexChannelsResponse
{
  GObject    parent;
  GPtrArray *channels;
};

enum {
  PROP_0,
  PROP_CHANNELS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (ApexChannelsResponse, apex_channels_response, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
apex_channels_response_serialize_property (JsonSerializable *serializable,
                                           const gchar      *name,
                                           const GValue     *value,
                                           GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "channels") == 0)
    {
      g_autoptr (GPtrArray) channels = NULL;
      JsonArray *arr = NULL;

      retval = json_node_new (JSON_NODE_ARRAY);
      arr = json_array_new ();

      channels = g_value_get_boxed (value);

      for (int i = 0; i < channels->len; i++)
        {
          JsonNode *node = NULL;
          JsonObject *obj = NULL;

          node = json_gobject_serialize (channels->pdata[i]);

          if (JSON_NODE_HOLDS_OBJECT (node))
            {
              obj = json_node_get_object (node);
              json_array_add_object_element (arr, obj);
            }
        }

      json_node_take_array (retval, arr);
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
apex_channels_response_deserialize_property (JsonSerializable *serializable,
                                             const gchar      *name,
                                             GValue           *value,
                                             GParamSpec       *pspec,
                                             JsonNode         *property_node)
{
  gboolean retval = FALSE;
  JsonArray *arr = NULL;

  if (g_strcmp0 (name, "channels") == 0)
    {
      g_autoptr (GPtrArray) channels = NULL;
      arr = json_node_get_array (property_node);
      channels = g_ptr_array_new ();

      for (gint i = 0; i < json_array_get_length (arr); i++)
        {
          g_autoptr (ApexChannel) channel = NULL;
          JsonNode *node = NULL;

          node = json_array_get_element (arr, i);

          channel = APEX_CHANNEL (json_gobject_deserialize (APEX_TYPE_CHANNEL, node));
          g_return_val_if_fail (APEX_IS_CHANNEL (channel), FALSE);
          g_ptr_array_add (channels, channel);
        }

      g_value_set_boxed (value, channels);

      retval = TRUE;
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = apex_channels_response_serialize_property;
  iface->deserialize_property = apex_channels_response_deserialize_property;
}

static void
apex_channels_response_finalize (GObject *object)
{
  ApexChannelsResponse *self = (ApexChannelsResponse *)object;

  g_ptr_array_unref (self->channels);

  G_OBJECT_CLASS (apex_channels_response_parent_class)->finalize (object);
}

static void
apex_channels_response_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
  ApexChannelsResponse *self = APEX_CHANNELS_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_CHANNELS:
      g_value_set_boxed (value, apex_channels_response_get_channels (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_channels_response_set_property (GObject      *object,
                                     guint         prop_id,
                                     const GValue *value,
                                     GParamSpec   *pspec)
{
  ApexChannelsResponse *self = APEX_CHANNELS_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_CHANNELS:
      apex_channels_response_set_channels (self, g_value_get_boxed (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_channels_response_class_init (ApexChannelsResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_channels_response_finalize;
  object_class->get_property = apex_channels_response_get_property;
  object_class->set_property = apex_channels_response_set_property;

  properties [PROP_CHANNELS] =
    g_param_spec_boxed ("channels",
                        "Channels",
                        "The list of channels to provide in the response.",
                        G_TYPE_PTR_ARRAY,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_channels_response_init (ApexChannelsResponse *self)
{
}

ApexChannelsResponse *
apex_channels_response_new (void)
{
  // TODO: move this into init?
  g_autoptr (GPtrArray) channels = NULL;

  channels = g_ptr_array_new ();

  return g_object_new (APEX_TYPE_CHANNELS_RESPONSE,
                       "channels", channels,
                       NULL);
}

/**
 * apex_channels_response_serialize:
 * @self: an #ApexChannelsResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_channels_response_serialize (ApexChannelsResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_CHANNELS_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
apex_channels_response_deserialize (ApexChannelsResponse *self,
                                    const gchar          *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_CHANNELS_RESPONSE,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_CHANNELS_RESPONSE (object));

  apex_channels_response_set_channels (self,
                                       apex_channels_response_get_channels (
                                         APEX_CHANNELS_RESPONSE (object)));

  g_object_unref (object);
}

void
apex_channels_response_add_channel (ApexChannelsResponse *self,
                                    ApexChannel          *channel)
{
  g_return_if_fail (APEX_IS_CHANNELS_RESPONSE (self));
  g_return_if_fail (APEX_IS_CHANNEL (channel));

  g_ptr_array_add (self->channels, channel);
}

void
apex_channels_response_remove_channel (ApexChannelsResponse *self,
                                       ApexChannel          *channel)
{
  g_return_if_fail (APEX_IS_CHANNELS_RESPONSE (self));
  g_return_if_fail (APEX_IS_CHANNEL (channel));

  for (int i = 0; i < self->channels->len; i++)
    {
      // TODO: add ID to channel and compare here, endpoint is wrong
      if (g_strcmp0 (apex_channel_get_endpoint (channel),
                     apex_channel_get_endpoint (self->channels->pdata[i])) == 0)
        self->channels = g_ptr_array_remove_index (self->channels, i);
    }
}

/**
 * apex_channels_response_get_channels:
 * @self: a #ApexChannelsResponse
 *
 * Returns: (element-type Apex.Channel) (transfer full): an array of
 *          #ApexChannel objects, free the array with g_ptr_array_free when
 *          done.
 */
GPtrArray *
apex_channels_response_get_channels (ApexChannelsResponse *self)
{
  g_return_val_if_fail (APEX_IS_CHANNELS_RESPONSE (self), NULL);

  return self->channels;
}

/**
 * apex_channels_response_set_channels:
 * @self: a #ApexChannelsResponse
 * @channels: (element-type Apex.Channel): an array of #ApexChannel objects
 *            to set.
 */
void
apex_channels_response_set_channels (ApexChannelsResponse *self,
                                     GPtrArray            *channels)
{
  g_return_if_fail (APEX_IS_CHANNELS_RESPONSE (self));

  if (self->channels == channels)
    return;

  if (self->channels != NULL)
    g_ptr_array_unref (self->channels);

  self->channels = channels;

  if (self->channels != NULL)
    g_ptr_array_ref (self->channels);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CHANNELS]);
}
