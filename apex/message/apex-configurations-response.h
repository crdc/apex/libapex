/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_CONFIGURATIONS_RESPONSE apex_configurations_response_get_type ()
G_DECLARE_FINAL_TYPE (ApexConfigurationsResponse, apex_configurations_response, APEX, CONFIGURATIONS_RESPONSE, GObject)

ApexConfigurationsResponse *apex_configurations_response_new                  (void);

gchar                      *apex_configurations_response_serialize            (ApexConfigurationsResponse *self);
void                        apex_configurations_response_deserialize          (ApexConfigurationsResponse *self,
                                                                               const gchar                *data);

void                        apex_configurations_response_add_configuration    (ApexConfigurationsResponse *self,
                                                                               ApexConfiguration          *configuration);
void                        apex_configurations_response_remove_configuration (ApexConfigurationsResponse *self,
                                                                               ApexConfiguration          *configuration);

GPtrArray                  *apex_configurations_response_get_configurations   (ApexConfigurationsResponse *self);
void                        apex_configurations_response_set_configurations   (ApexConfigurationsResponse *self,
                                                                               GPtrArray                  *configurations);

G_END_DECLS
