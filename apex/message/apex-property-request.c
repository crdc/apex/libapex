/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-property-request.h"
#include "core/apex-property.h"

/*
 * ApexPropertyRequest:
 *
 * Represents a ...
 */
struct _ApexPropertyRequest
{
  GObject  parent;
  gchar   *id;
  gchar   *key;
  gchar   *value;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_KEY,
  PROP_VALUE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexPropertyRequest, apex_property_request, G_TYPE_OBJECT)

static void
apex_property_request_finalize (GObject *object)
{
  ApexPropertyRequest *self = (ApexPropertyRequest *)object;

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->key, g_free);
  g_clear_pointer (&self->value, g_free);

  G_OBJECT_CLASS (apex_property_request_parent_class)->finalize (object);
}

static void
apex_property_request_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  ApexPropertyRequest *self = APEX_PROPERTY_REQUEST (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, apex_property_request_get_id (self));
      break;

    case PROP_KEY:
      g_value_set_string (value, apex_property_request_get_key (self));
      break;

    case PROP_VALUE:
      g_value_set_string (value, apex_property_request_get_value (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_property_request_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  ApexPropertyRequest *self = APEX_PROPERTY_REQUEST (object);

  switch (prop_id)
    {
    case PROP_ID:
      apex_property_request_set_id (self, g_value_get_string (value));
      break;

    case PROP_KEY:
      apex_property_request_set_key (self, g_value_get_string (value));
      break;

    case PROP_VALUE:
      apex_property_request_set_value (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_property_request_class_init (ApexPropertyRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_property_request_finalize;
  object_class->get_property = apex_property_request_get_property;
  object_class->set_property = apex_property_request_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The id of the service the request is meant for",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_KEY] =
    g_param_spec_string ("key",
                         "Key",
                         "The property key being requested",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_VALUE] =
    g_param_spec_string ("value",
                         "Value",
                         "The property value being requested",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_property_request_init (ApexPropertyRequest *self)
{
}

ApexPropertyRequest *
apex_property_request_new (const gchar *id,
                           const gchar *key,
                           const gchar *value)
{
  return g_object_new (APEX_TYPE_PROPERTY_REQUEST,
                       "id", id,
                       "key", key,
                       "value", value,
                       NULL);
}

/**
 * apex_property_request_serialize:
 * @self: an #ApexPropertyRequest
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_property_request_serialize (ApexPropertyRequest *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_PROPERTY_REQUEST (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
apex_property_request_deserialize (ApexPropertyRequest *self,
                                   const gchar         *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_PROPERTY_REQUEST,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_PROPERTY_REQUEST (object));

  apex_property_request_set_id (self,
      apex_property_request_get_id (APEX_PROPERTY_REQUEST (object)));

  apex_property_request_set_key (self,
      apex_property_request_get_key (APEX_PROPERTY_REQUEST (object)));

  apex_property_request_set_value (self,
      apex_property_request_get_value (APEX_PROPERTY_REQUEST (object)));

  g_object_unref (object);
}

const gchar *
apex_property_request_get_id (ApexPropertyRequest *self)
{
  g_return_val_if_fail (APEX_IS_PROPERTY_REQUEST (self), NULL);

  return self->id;
}

void
apex_property_request_set_id (ApexPropertyRequest *self,
                              const gchar         *id)
{
  g_return_if_fail (APEX_IS_PROPERTY_REQUEST (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}

const gchar *
apex_property_request_get_key (ApexPropertyRequest *self)
{
  g_return_val_if_fail (APEX_IS_PROPERTY_REQUEST (self), NULL);

  return self->key;
}

void
apex_property_request_set_key (ApexPropertyRequest *self,
                               const gchar         *key)
{
  g_return_if_fail (APEX_IS_PROPERTY_REQUEST (self));

  if (g_strcmp0 (key, self->key) != 0)
    {
      g_free (self->key);
      self->key = g_strdup (key);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_KEY]);
    }
}

const gchar *
apex_property_request_get_value (ApexPropertyRequest *self)
{
  g_return_val_if_fail (APEX_IS_PROPERTY_REQUEST (self), NULL);

  return self->value;
}

void
apex_property_request_set_value (ApexPropertyRequest *self,
                                 const gchar         *value)
{
  g_return_if_fail (APEX_IS_PROPERTY_REQUEST (self));

  if (g_strcmp0 (value, self->value) != 0)
    {
      g_free (self->value);
      self->value = g_strdup (value);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_VALUE]);
    }
}
