/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include "apex-empty.h"

struct _ApexEmpty
{
  GObject parent;
};

G_DEFINE_TYPE (ApexEmpty, apex_empty, G_TYPE_OBJECT)

static void
apex_empty_finalize (GObject *object)
{
  G_GNUC_UNUSED ApexEmpty *self = (ApexEmpty *)object;

  G_OBJECT_CLASS (apex_empty_parent_class)->finalize (object);
}

static void
apex_empty_class_init (ApexEmptyClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_empty_finalize;
}

static void
apex_empty_init (ApexEmpty *self)
{
}

ApexEmpty *
apex_empty_new (void)
{
  return g_object_new (APEX_TYPE_EMPTY, NULL);
}
