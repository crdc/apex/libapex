/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

#include "apex-response.h"

G_BEGIN_DECLS

#define APEX_TYPE_PROPERTIES_RESPONSE apex_properties_response_get_type ()
G_DECLARE_FINAL_TYPE (ApexPropertiesResponse, apex_properties_response, APEX, PROPERTIES_RESPONSE, ApexResponse)

ApexPropertiesResponse *apex_properties_response_new         (void);

gchar                  *apex_properties_response_serialize   (ApexPropertiesResponse *self);
void                    apex_properties_response_deserialize (ApexPropertiesResponse *self,
                                                              const gchar            *data);

void                    apex_properties_response_add         (ApexPropertiesResponse *self,
                                                              ApexProperty           *property);
void                    apex_properties_response_remove      (ApexPropertiesResponse *self,
                                                              const gchar            *key);
ApexProperty           *apex_properties_response_get         (ApexPropertiesResponse *self,
                                                              const gchar            *key);
gboolean                apex_properties_response_contains    (ApexPropertiesResponse *self,
                                                              const gchar            *key);
GHashTable             *apex_properties_response_get_list    (ApexPropertiesResponse *self);
void                    apex_properties_response_set_list    (ApexPropertiesResponse *self,
                                                              GHashTable             *properties);

G_END_DECLS
