/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_REQUEST apex_request_get_type ()
G_DECLARE_DERIVABLE_TYPE (ApexRequest, apex_request, APEX, REQUEST, GObject)

struct _ApexRequestClass
{
  /*< private >*/
  GObjectClass parent_class;

  /*< private >*/
  gpointer padding[12];
};

ApexRequest *apex_request_new         (void);

gchar       *apex_request_serialize   (ApexRequest *self);
void         apex_request_deserialize (ApexRequest *self,
                                       const gchar *data);

G_END_DECLS
