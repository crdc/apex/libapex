/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_CONFIGURATION_RESPONSE apex_configuration_response_get_type ()
G_DECLARE_FINAL_TYPE (ApexConfigurationResponse, apex_configuration_response, APEX, CONFIGURATION_RESPONSE, GObject)

ApexConfigurationResponse *apex_configuration_response_new               (void);

gchar                     *apex_configuration_response_serialize         (ApexConfigurationResponse *self);
void                       apex_configuration_response_deserialize       (ApexConfigurationResponse *self,
                                                                          const gchar               *data);

ApexConfiguration         *apex_configuration_response_get_configuration (ApexConfigurationResponse *self);
ApexConfiguration         *apex_configuration_response_ref_configuration (ApexConfigurationResponse *self);
void                       apex_configuration_response_set_configuration (ApexConfigurationResponse *self,
                                                                          ApexConfiguration         *configuration);

G_END_DECLS
