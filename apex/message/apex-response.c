/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-response.h"
#include "apex-message-error.h"

/*
 * SECTION:apex-response
 * @short_description: Response base class
 *
 * An #ApexResponse is a structure for all response message types to derive.
 */
typedef struct
{
  ApexMessageError *error;
} ApexResponsePrivate;

enum {
  PROP_0,
  PROP_ERROR,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE_WITH_PRIVATE (ApexResponse, apex_response, G_TYPE_OBJECT)

static void
apex_response_finalize (GObject *object)
{
  ApexResponse *self = (ApexResponse *)object;
  ApexResponsePrivate *priv = apex_response_get_instance_private (self);

  g_clear_object (&priv->error);

  G_OBJECT_CLASS (apex_response_parent_class)->finalize (object);
}

static void
apex_response_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  ApexResponse *self = APEX_RESPONSE (object);
  G_GNUC_UNUSED ApexResponsePrivate *priv = apex_response_get_instance_private (self);

  switch (prop_id)
  {
    case PROP_ERROR:
      g_value_take_object (value, apex_response_ref_error (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_response_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  ApexResponse *self = APEX_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_ERROR:
      apex_response_set_error (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_response_class_init (ApexResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_response_finalize;
  object_class->get_property = apex_response_get_property;
  object_class->set_property = apex_response_set_property;

  properties [PROP_ERROR] =
    g_param_spec_object ("error",
                         "Error",
                         "The error body of the response",
                         APEX_TYPE_MESSAGE_ERROR,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_response_init (ApexResponse *self)
{
}

ApexResponse *
apex_response_new (void)
{
  return g_object_new (APEX_TYPE_RESPONSE, NULL);
}

/**
 * apex_response_serialize:
 * @self: an #ApexResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_response_serialize (ApexResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void apex_response_deserialize (ApexResponse *self,
                                const gchar  *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (ApexMessageError) error = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (APEX_TYPE_RESPONSE,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_RESPONSE (object));

  error = apex_response_ref_error (APEX_RESPONSE (object));
  apex_response_set_error (self, error);

  g_clear_object (&object);
}

/**
 * apex_response_get_error:
 * @self: an #ApexResponse
 *
 * Retrieve the #ApexMessageError contained by the response message.
 *
 * Returns: (transfer none): an #ApexMessageError if one is set.
 */
ApexMessageError *
apex_response_get_error (ApexResponse *self)
{
  ApexMessageError *error;

  g_return_val_if_fail (APEX_IS_RESPONSE (self), NULL);

  g_object_get (self, "error", &error, NULL);

  return error;
}

/**
 * apex_response_ref_error:
 * @self: an #ApexResponse
 *
 * Gets the error for the response message, and returns a new reference
 * to the #ApexMessageError.
 *
 * Returns: (transfer full) (nullable): a #ApexMessageError or %NULL
 */
ApexMessageError *
apex_response_ref_error (ApexResponse *self)
{
  ApexResponsePrivate *priv;
  ApexMessageError *ret = NULL;

  g_return_val_if_fail (APEX_IS_RESPONSE (self), NULL);

  priv = apex_response_get_instance_private (self);

  g_set_object (&ret, priv->error);

  return g_steal_pointer (&ret);
}

void
apex_response_set_error (ApexResponse     *self,
                         ApexMessageError *error)
{
  ApexResponsePrivate *priv;

  g_return_if_fail (APEX_IS_RESPONSE (self));
  g_return_if_fail (!error || APEX_IS_MESSAGE_ERROR (error));

  priv = apex_response_get_instance_private (self);

  if (g_set_object (&priv->error, error))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ERROR]);
}
