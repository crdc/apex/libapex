/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_JOB_REQUEST apex_job_request_get_type ()
G_DECLARE_FINAL_TYPE (ApexJobRequest, apex_job_request, APEX, JOB_REQUEST, GObject)

ApexJobRequest *apex_job_request_new           (const gchar    *id);

gchar          *apex_job_request_serialize     (ApexJobRequest *self);
void            apex_job_request_deserialize   (ApexJobRequest *self,
                                                const gchar    *data);

void            apex_job_request_add           (ApexJobRequest *self,
                                                ApexProperty   *property);
void            apex_job_request_remove        (ApexJobRequest *self,
                                                const gchar    *key);
ApexProperty   *apex_job_request_get           (ApexJobRequest *self,
                                                const gchar    *key);
gboolean        apex_job_request_contains      (ApexJobRequest *self,
                                                const gchar    *key);
GHashTable     *apex_job_request_get_list      (ApexJobRequest *self);
void            apex_job_request_set_list      (ApexJobRequest *self,
                                                GHashTable     *job_properties);

const gchar    *apex_job_request_get_id        (ApexJobRequest *self);
void            apex_job_request_set_id        (ApexJobRequest *self,
                                                const gchar    *id);

const gchar    *apex_job_request_get_job_id    (ApexJobRequest *self);
void            apex_job_request_set_job_id    (ApexJobRequest *self,
                                                const gchar    *job_id);

const gchar    *apex_job_request_get_job_value (ApexJobRequest *self);
void            apex_job_request_set_job_value (ApexJobRequest *self,
                                                const gchar    *job_value);

G_END_DECLS
