/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-event-request.h"
#include "core/apex-event.h"

/*
 * ApexEventRequest:
 *
 * Represents a ...
 */
struct _ApexEventRequest
{
  GObject    parent;
  ApexEvent *event;
};

enum {
  PROP_0,
  PROP_EVENT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexEventRequest, apex_event_request, G_TYPE_OBJECT)

static void
apex_event_request_finalize (GObject *object)
{
  ApexEventRequest *self = (ApexEventRequest *)object;

  g_clear_object (&self->event);

  G_OBJECT_CLASS (apex_event_request_parent_class)->finalize (object);
}

static void
apex_event_request_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  ApexEventRequest *self = APEX_EVENT_REQUEST (object);

  switch (prop_id)
    {
    case PROP_EVENT:
      /*g_value_set_object (value, apex_event_request_get_event (self));*/
      g_value_set_object (value, self->event);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_event_request_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  ApexEventRequest *self = APEX_EVENT_REQUEST (object);

  switch (prop_id)
    {
    case PROP_EVENT:
      apex_event_request_set_event (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_event_request_class_init (ApexEventRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_event_request_finalize;
  object_class->get_property = apex_event_request_get_property;
  object_class->set_property = apex_event_request_set_property;

  properties [PROP_EVENT] =
    g_param_spec_object ("event",
                         "Event",
                         "The event to use in the request.",
                         APEX_TYPE_EVENT,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_event_request_init (ApexEventRequest *self)
{
}

ApexEventRequest *
apex_event_request_new (void)
{
  return g_object_new (APEX_TYPE_EVENT_REQUEST, NULL);
}

/**
 * apex_event_request_serialize:
 * @self: an #ApexEventRequest
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_event_request_serialize (ApexEventRequest *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_EVENT_REQUEST (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
apex_event_request_deserialize (ApexEventRequest *self,
                                const gchar      *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_EVENT_REQUEST,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_EVENT_REQUEST (object));

  apex_event_request_set_event (self,
      apex_event_request_get_event (APEX_EVENT_REQUEST (object)));

  g_object_unref (object);
}

/**
 * apex_event_requst_get_event:
 * @self: #ApexEventRequest instance
 *
 * Returns: (transfer full): The #ApexEvent
 */
ApexEvent *
apex_event_request_get_event (ApexEventRequest *self)
{
  ApexEvent *event;

  g_return_val_if_fail (APEX_IS_EVENT_REQUEST (self), NULL);

  g_object_get (self, "event", &event, NULL);

  return event;
}

void
apex_event_request_set_event (ApexEventRequest *self,
                              ApexEvent        *event)
{
  g_return_if_fail (APEX_IS_EVENT_REQUEST (self));
  g_return_if_fail (APEX_IS_EVENT (event));

  if (self->event)
    g_object_unref (self->event);

  if (event)
    g_object_ref (event);

  self->event = event;

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_EVENT]);
}
