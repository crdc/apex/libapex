/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-request.h"

/*
 * SECTION:apex-request
 * @short_description: Request base class
 *
 * An #ApexRequest is a structure for all request message types to derive.
 */
typedef struct
{
  GObject parent;
} ApexRequestPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (ApexRequest, apex_request, G_TYPE_OBJECT)

static void
apex_request_finalize (GObject *object)
{
  G_GNUC_UNUSED ApexRequest *self = (ApexRequest *)object;

  G_OBJECT_CLASS (apex_request_parent_class)->finalize (object);
}

static void
apex_request_class_init (ApexRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_request_finalize;
}

static void
apex_request_init (ApexRequest *self)
{
}

ApexRequest *
apex_request_new (void)
{
  return g_object_new (APEX_TYPE_REQUEST, NULL);
}

/**
 * apex_request_serialize:
 * @self: an #ApexRequest
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_request_serialize (ApexRequest *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_REQUEST (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
apex_request_deserialize (ApexRequest *self,
                          const gchar *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_REQUEST,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_REQUEST (object));

  g_object_unref (object);
}
