/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-message-error.h"

/*
 * ApexMessageError:
 *
 * Represents a ...
 */
struct _ApexMessageError
{
  GObject  parent;
  gchar   *message;
  gint     code;
};

enum {
  PROP_0,
  PROP_MESSAGE,
  PROP_CODE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexMessageError, apex_message_error, G_TYPE_OBJECT)

static void
apex_message_error_finalize (GObject *object)
{
  ApexMessageError *self = (ApexMessageError *)object;

  g_clear_pointer (&self->message, g_free);

  G_OBJECT_CLASS (apex_message_error_parent_class)->finalize (object);
}

static void
apex_message_error_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  ApexMessageError *self = APEX_MESSAGE_ERROR (object);

  switch (prop_id)
  {
    case PROP_MESSAGE:
      g_value_set_string (value, apex_message_error_get_message (self));
      break;

    case PROP_CODE:
      g_value_set_int (value, apex_message_error_get_code (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_message_error_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  ApexMessageError *self = APEX_MESSAGE_ERROR (object);

  switch (prop_id)
  {
    case PROP_MESSAGE:
      apex_message_error_set_message (self, g_value_get_string (value));
      break;

    case PROP_CODE:
      apex_message_error_set_code (self, g_value_get_int (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_message_error_class_init (ApexMessageErrorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_message_error_finalize;
  object_class->get_property = apex_message_error_get_property;
  object_class->set_property = apex_message_error_set_property;

  properties [PROP_MESSAGE] =
    g_param_spec_string ("message",
                         "Message",
                         "The error message string.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_CODE] =
    g_param_spec_int ("code",
                      "Code",
                      "The error code.",
                      G_MININT,
                      G_MAXINT,
                      0,
                      (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_message_error_init (ApexMessageError *self)
{
}

ApexMessageError *
apex_message_error_new (void)
{
  return g_object_new (APEX_TYPE_MESSAGE_ERROR, NULL);
}

/**
 * apex_message_error_serialize:
 * @self: an #ApexMessageError
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_message_error_serialize (ApexMessageError *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_MESSAGE_ERROR (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
apex_message_error_deserialize (ApexMessageError *self,
                                const gchar      *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_MESSAGE_ERROR,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_MESSAGE_ERROR (object));

  apex_message_error_set_message (self,
      apex_message_error_get_message (APEX_MESSAGE_ERROR (object)));

  apex_message_error_set_code (self,
      apex_message_error_get_code (APEX_MESSAGE_ERROR (object)));

  g_object_unref (object);
}

const gchar *
apex_message_error_get_message (ApexMessageError *self)
{
  g_return_val_if_fail (APEX_IS_MESSAGE_ERROR (self), NULL);

  return self->message;
}

void
apex_message_error_set_message (ApexMessageError *self,
                                const gchar      *message)
{
  g_return_if_fail (APEX_IS_MESSAGE_ERROR (self));

  if (g_strcmp0 (message, self->message) != 0)
    {
      g_free (self->message);
      self->message = g_strdup (message);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_MESSAGE]);
    }
}

gint
apex_message_error_get_code (ApexMessageError *self)
{
  g_return_val_if_fail (APEX_IS_MESSAGE_ERROR (self), -1);

  return self->code;
}

void
apex_message_error_set_code (ApexMessageError *self,
                             gint              code)
{
  g_return_if_fail (APEX_IS_MESSAGE_ERROR (self));

  self->code = code;
}
