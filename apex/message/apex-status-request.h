/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_STATUS_REQUEST apex_status_request_get_type ()
G_DECLARE_FINAL_TYPE (ApexStatusRequest, apex_status_request, APEX, STATUS_REQUEST, GObject)

ApexStatusRequest *apex_status_request_new           (void);
ApexStatusRequest *apex_status_request_new_from_data (const guint8      *data);

guint8            *apex_status_request_to_data       (ApexStatusRequest *self);

gchar             *apex_status_request_serialize     (ApexStatusRequest *self);
void               apex_status_request_deserialize   (ApexStatusRequest *self,
                                                      const gchar       *data);

G_END_DECLS
