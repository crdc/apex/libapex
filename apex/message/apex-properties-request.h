/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_PROPERTIES_REQUEST apex_properties_request_get_type ()
G_DECLARE_FINAL_TYPE (ApexPropertiesRequest, apex_properties_request, APEX, PROPERTIES_REQUEST, GObject)

ApexPropertiesRequest *apex_properties_request_new         (const gchar           *id);

gchar                 *apex_properties_request_serialize   (ApexPropertiesRequest *self);
void                   apex_properties_request_deserialize (ApexPropertiesRequest *self,
                                                            const gchar           *data);

void                   apex_properties_request_add         (ApexPropertiesRequest *self,
                                                            ApexProperty          *property);
void                   apex_properties_request_remove      (ApexPropertiesRequest *self,
                                                            const gchar           *key);
ApexProperty          *apex_properties_request_get         (ApexPropertiesRequest *self,
                                                            const gchar           *key);
gboolean               apex_properties_request_contains    (ApexPropertiesRequest *self,
                                                            const gchar           *key);
GHashTable            *apex_properties_request_get_list    (ApexPropertiesRequest *self);
void                   apex_properties_request_set_list    (ApexPropertiesRequest *self,
                                                            GHashTable            *properties);

const gchar           *apex_properties_request_get_id      (ApexPropertiesRequest *self);
void                   apex_properties_request_set_id      (ApexPropertiesRequest *self,
                                                            const gchar           *id);

G_END_DECLS
