/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_PROPERTY_REQUEST apex_property_request_get_type ()
G_DECLARE_FINAL_TYPE (ApexPropertyRequest, apex_property_request, APEX, PROPERTY_REQUEST, GObject)

ApexPropertyRequest *apex_property_request_new         (const gchar         *id,
                                                        const gchar         *key,
                                                        const gchar         *value);

gchar               *apex_property_request_serialize   (ApexPropertyRequest *self);
void                 apex_property_request_deserialize (ApexPropertyRequest *self,
                                                        const gchar         *data);

const gchar         *apex_property_request_get_id      (ApexPropertyRequest *self);
void                 apex_property_request_set_id      (ApexPropertyRequest *self,
                                                        const gchar         *id);

const gchar         *apex_property_request_get_key     (ApexPropertyRequest *self);
void                 apex_property_request_set_key     (ApexPropertyRequest *self,
                                                        const gchar         *key);

const gchar         *apex_property_request_get_value   (ApexPropertyRequest *self);
void                 apex_property_request_set_value   (ApexPropertyRequest *self,
                                                        const gchar         *value);

G_END_DECLS
