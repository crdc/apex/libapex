/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_CHANNELS_RESPONSE apex_channels_response_get_type ()
G_DECLARE_FINAL_TYPE (ApexChannelsResponse, apex_channels_response, APEX, CHANNELS_RESPONSE, GObject)

ApexChannelsResponse *apex_channels_response_new            (void);

gchar                *apex_channels_response_serialize      (ApexChannelsResponse *self);
void                  apex_channels_response_deserialize    (ApexChannelsResponse *self,
                                                             const gchar          *data);

void                  apex_channels_response_add_channel    (ApexChannelsResponse *self,
                                                             ApexChannel          *channel);
void                  apex_channels_response_remove_channel (ApexChannelsResponse *self,
                                                             ApexChannel          *channel);

GPtrArray            *apex_channels_response_get_channels   (ApexChannelsResponse *self);
void                  apex_channels_response_set_channels   (ApexChannelsResponse *self,
                                                             GPtrArray            *channels);

G_END_DECLS
