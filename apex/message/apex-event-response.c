/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include "apex-event-response.h"

/*
 * ApexEventResponse:
 *
 * Represents a ...
 */
struct _ApexEventResponse
{
  GObject parent;
};

G_DEFINE_TYPE (ApexEventResponse, apex_event_response, G_TYPE_OBJECT)

static void
apex_event_response_finalize (GObject *object)
{
  G_GNUC_UNUSED ApexEventResponse *self = (ApexEventResponse *)object;

  G_OBJECT_CLASS (apex_event_response_parent_class)->finalize (object);
}

static void
apex_event_response_class_init (ApexEventResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_event_response_finalize;
}

static void
apex_event_response_init (ApexEventResponse *self)
{
}

ApexEventResponse *
apex_event_response_new (void)
{
  ApexEventResponse *object = g_object_new (APEX_TYPE_EVENT_RESPONSE, NULL);

  // TODO: add setup

  return object;
}
