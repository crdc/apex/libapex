/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-channel-response.h"
#include "core/apex-channel.h"

/*
 * ApexChannelResponse:
 *
 * Represents a ...
 */
struct _ApexChannelResponse
{
  GObject      parent;
  ApexChannel *channel;
};

enum {
  PROP_0,
  PROP_CHANNEL,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexChannelResponse, apex_channel_response, G_TYPE_OBJECT)

static void
apex_channel_response_finalize (GObject *object)
{
  ApexChannelResponse *self = (ApexChannelResponse *)object;

  // FIXME: this should probably just unref
  g_clear_object (&self->channel);

  G_OBJECT_CLASS (apex_channel_response_parent_class)->finalize (object);
}

static void
apex_channel_response_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  ApexChannelResponse *self = APEX_CHANNEL_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_CHANNEL:
      /*g_value_set_object (value, apex_channel_response_get_channel (self));*/
      g_value_set_object (value, self->channel);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_channel_response_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  ApexChannelResponse *self = APEX_CHANNEL_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_CHANNEL:
      apex_channel_response_set_channel (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_channel_response_class_init (ApexChannelResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_channel_response_finalize;
  object_class->get_property = apex_channel_response_get_property;
  object_class->set_property = apex_channel_response_set_property;

  properties [PROP_CHANNEL] =
    g_param_spec_object ("channel",
                         "Channel",
                         "The channel to response with.",
                         APEX_TYPE_CHANNEL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_channel_response_init (ApexChannelResponse *self)
{
  self->channel = apex_channel_new ();
}

ApexChannelResponse *
apex_channel_response_new (void)
{
  return g_object_new (APEX_TYPE_CHANNEL_RESPONSE, NULL);
}

/**
 * apex_channel_response_serialize:
 * @self: an #ApexChannelResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_channel_response_serialize (ApexChannelResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_CHANNEL_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
apex_channel_response_deserialize (ApexChannelResponse *self,
                                   const gchar         *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_CHANNEL_RESPONSE,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
  {
    g_critical ("%s", err->message);
    g_error_free (err);
  }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_CHANNEL_RESPONSE (object));

  apex_channel_response_set_channel (self,
      apex_channel_response_get_channel (APEX_CHANNEL_RESPONSE (object)));

  g_object_unref (object);
}

/**
 * apex_channel_response_get_channel:
 * @self: #ApexChannelResponse instance
 *
 * Returns: (transfer full): The #ApexChannel
 */
ApexChannel *
apex_channel_response_get_channel (ApexChannelResponse *self)
{
  ApexChannel *channel;

  g_return_val_if_fail (APEX_IS_CHANNEL_RESPONSE (self), NULL);

  g_object_get (self, "channel", &channel, NULL);

  return channel;
}

void
apex_channel_response_set_channel (ApexChannelResponse *self,
                                   ApexChannel         *channel)
{
  g_return_if_fail (APEX_IS_CHANNEL_RESPONSE (self));
  g_return_if_fail (APEX_IS_CHANNEL (channel));

  if (self->channel)
    g_object_unref (self->channel);

  if (channel)
    g_object_ref (channel);

  self->channel = channel;

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CHANNEL]);
}
