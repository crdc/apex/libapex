/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include "apex-settings-request.h"

/*
 * ApexSettingsRequest:
 *
 * Represents a ...
 */
struct _ApexSettingsRequest
{
  GObject parent;
};

G_DEFINE_TYPE (ApexSettingsRequest, apex_settings_request, G_TYPE_OBJECT)

static void
apex_settings_request_finalize (GObject *object)
{
  G_GNUC_UNUSED ApexSettingsRequest *self = (ApexSettingsRequest *)object;

  G_OBJECT_CLASS (apex_settings_request_parent_class)->finalize (object);
}

static void
apex_settings_request_class_init (ApexSettingsRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_settings_request_finalize;
}

static void
apex_settings_request_init (ApexSettingsRequest *self)
{
}

ApexSettingsRequest *
apex_settings_request_new (void)
{
  return g_object_new (APEX_TYPE_SETTINGS_REQUEST, NULL);
}
