/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#include <glib.h>

#define APEX_INSIDE

G_BEGIN_DECLS

// Messages
#include <apex/message/apex-channel-request.h>
#include <apex/message/apex-channel-response.h>
#include <apex/message/apex-channels-response.h>
#include <apex/message/apex-configuration-request.h>
#include <apex/message/apex-configuration-response.h>
#include <apex/message/apex-configurations-response.h>
#include <apex/message/apex-empty.h>
#include <apex/message/apex-event-request.h>
#include <apex/message/apex-event-response.h>
#include <apex/message/apex-job-request.h>
#include <apex/message/apex-job-response.h>
#include <apex/message/apex-jobs-response.h>
#include <apex/message/apex-job-status-response.h>
#include <apex/message/apex-message-error.h>
#include <apex/message/apex-module-event-request.h>
#include <apex/message/apex-module-job-request.h>
#include <apex/message/apex-module-request.h>
#include <apex/message/apex-module-response.h>
#include <apex/message/apex-modules-response.h>
#include <apex/message/apex-property-request.h>
#include <apex/message/apex-property-response.h>
#include <apex/message/apex-properties-request.h>
#include <apex/message/apex-properties-response.h>
#include <apex/message/apex-request.h>
#include <apex/message/apex-response.h>
#include <apex/message/apex-settings-request.h>
#include <apex/message/apex-settings-response.h>
#include <apex/message/apex-status-request.h>
#include <apex/message/apex-status-response.h>

G_END_DECLS

#undef APEX_INSIDE
