/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_CONFIGURATION_REQUEST apex_configuration_request_get_type ()
G_DECLARE_FINAL_TYPE (ApexConfigurationRequest, apex_configuration_request, APEX, CONFIGURATION_REQUEST, GObject)

ApexConfigurationRequest  *apex_configuration_request_new           (void);

gchar                     *apex_configuration_request_serialize     (ApexConfigurationRequest   *self);
void                       apex_configuration_request_deserialize   (ApexConfigurationRequest   *self,
                                                                     const gchar                *data);

const gchar               *apex_configuration_request_get_id        (ApexConfigurationRequest   *self);
void                       apex_configuration_request_set_id        (ApexConfigurationRequest   *self,
                                                                     const gchar                *id);

ApexConfigurationNamespace apex_configuration_request_get_namespace (ApexConfigurationRequest   *self);
void                       apex_configuration_request_set_namespace (ApexConfigurationRequest   *self,
                                                                     ApexConfigurationNamespace  namespace);

G_END_DECLS
