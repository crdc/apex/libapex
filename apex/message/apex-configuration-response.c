/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-configuration-response.h"
#include "core/apex-configuration.h"

/*
 * ApexConfigurationResponse:
 *
 * Represents a ...
 */
struct _ApexConfigurationResponse
{
  GObject            parent;
  ApexConfiguration *configuration;
};

enum {
  PROP_0,
  PROP_CONFIGURATION,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexConfigurationResponse, apex_configuration_response, G_TYPE_OBJECT)

static void
apex_configuration_response_finalize (GObject *object)
{
  ApexConfigurationResponse *self = (ApexConfigurationResponse *)object;

  g_clear_object (&self->configuration);

  G_OBJECT_CLASS (apex_configuration_response_parent_class)->finalize (object);
}

static void
apex_configuration_response_get_property (GObject    *object,
                                          guint       prop_id,
                                          GValue     *value,
                                          GParamSpec *pspec)
{
  ApexConfigurationResponse *self = APEX_CONFIGURATION_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_CONFIGURATION:
      g_value_take_object (value, apex_configuration_response_ref_configuration (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_configuration_response_set_property (GObject      *object,
                                          guint         prop_id,
                                          const GValue *value,
                                          GParamSpec   *pspec)
{
  ApexConfigurationResponse *self = APEX_CONFIGURATION_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_CONFIGURATION:
      apex_configuration_response_set_configuration (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_configuration_response_class_init (ApexConfigurationResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_configuration_response_finalize;
  object_class->get_property = apex_configuration_response_get_property;
  object_class->set_property = apex_configuration_response_set_property;

  properties [PROP_CONFIGURATION] =
    g_param_spec_object ("configuration",
                         "Configuration",
                         "The configuration in the response.",
                         APEX_TYPE_CONFIGURATION,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_configuration_response_init (ApexConfigurationResponse *self)
{
  self->configuration = apex_configuration_new ();
}

ApexConfigurationResponse *
apex_configuration_response_new (void)
{
  return g_object_new (APEX_TYPE_CONFIGURATION_RESPONSE, NULL);
}

/**
 * apex_configuration_response_serialize:
 * @self: an #ApexConfigurationResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_configuration_response_serialize (ApexConfigurationResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_CONFIGURATION_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
apex_configuration_response_deserialize (ApexConfigurationResponse *self,
                                         const gchar               *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (ApexConfiguration) configuration = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (APEX_TYPE_CONFIGURATION_RESPONSE,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_CONFIGURATION_RESPONSE (object));

  configuration = apex_configuration_response_ref_configuration (
      APEX_CONFIGURATION_RESPONSE (object));

  apex_configuration_response_set_configuration (self, configuration);

  g_clear_object (&object);
}

/**
 * apex_configuration_response_get_configuration:
 * @self: #ApexConfigurationResponse instance
 *
 * Returns: (transfer full): The #ApexConfiguration
 */
ApexConfiguration *
apex_configuration_response_get_configuration (ApexConfigurationResponse *self)
{
  ApexConfiguration *configuration;

  g_return_val_if_fail (APEX_IS_CONFIGURATION_RESPONSE (self), NULL);

  g_object_get (self, "configuration", &configuration, NULL);

  return configuration;
}

/**
 * apex_configuration_response_ref_configuration:
 * @self: an #ApexConfigurationResponse
 *
 * Gets the configuration data for the response, and returns a new reference
 *
 * Returns: (transfer full) (nullable): a #ApexConfiguration or %NULL
 */
ApexConfiguration *
apex_configuration_response_ref_configuration (ApexConfigurationResponse *self)
{
  ApexConfiguration *ret = NULL;

  g_return_val_if_fail (APEX_IS_CONFIGURATION_RESPONSE (self), NULL);

  g_set_object (&ret, self->configuration);

  return g_steal_pointer (&ret);
}

void
apex_configuration_response_set_configuration (ApexConfigurationResponse *self,
                                               ApexConfiguration         *configuration)
{
  g_return_if_fail (APEX_IS_CONFIGURATION_RESPONSE (self));
  g_return_if_fail (APEX_IS_CONFIGURATION (configuration));

  if (g_set_object (&self->configuration, configuration))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CONFIGURATION]);
}
