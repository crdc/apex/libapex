/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_EVENT_RESPONSE apex_event_response_get_type ()
G_DECLARE_FINAL_TYPE (ApexEventResponse, apex_event_response, APEX, EVENT_RESPONSE, GObject)

// TODO: change this to ApexEventsResponse

ApexEventResponse *apex_event_response_new         (void);

gchar             *apex_event_response_serialize   (ApexEventResponse *self);
void               apex_event_response_deserialize (ApexEventResponse *self,
                                                    const gchar       *data);

GPtrArray         *apex_event_response_get_events  (ApexEventResponse *self);
void               apex_event_response_set_events  (ApexEventResponse *self,
                                                    GPtrArray         *events);

G_END_DECLS
