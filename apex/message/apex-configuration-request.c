/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include <apex/apex-enum-types.h>

#include "apex-configuration-request.h"
#include "core/apex-configuration.h"

/*
 * ApexConfigurationRequest:
 *
 * Represents a ...
 */
struct _ApexConfigurationRequest
{
  GObject                     parent;
  gchar                      *id;
  ApexConfigurationNamespace  namespace;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_NAMESPACE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexConfigurationRequest, apex_configuration_request, G_TYPE_OBJECT)

static void
apex_configuration_request_finalize (GObject *object)
{
  ApexConfigurationRequest *self = (ApexConfigurationRequest *)object;

  g_clear_pointer (&self->id, g_free);

  G_OBJECT_CLASS (apex_configuration_request_parent_class)->finalize (object);
}

static void
apex_configuration_request_get_property (GObject    *object,
                                         guint       prop_id,
                                         GValue     *value,
                                         GParamSpec *pspec)
{
  ApexConfigurationRequest *self = APEX_CONFIGURATION_REQUEST (object);

  switch (prop_id)
  {
    case PROP_ID:
      g_value_set_string (value, apex_configuration_request_get_id (self));
      break;

    case PROP_NAMESPACE:
      g_value_set_enum (value, apex_configuration_request_get_namespace (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_configuration_request_set_property (GObject      *object,
                                         guint         prop_id,
                                         const GValue *value,
                                         GParamSpec   *pspec)
{
  ApexConfigurationRequest *self = APEX_CONFIGURATION_REQUEST (object);

  switch (prop_id)
  {
    case PROP_ID:
      apex_configuration_request_set_id (self, g_value_get_string (value));
      break;

    case PROP_NAMESPACE:
      apex_configuration_request_set_namespace (self, g_value_get_enum (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_configuration_request_class_init (ApexConfigurationRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_configuration_request_finalize;
  object_class->get_property = apex_configuration_request_get_property;
  object_class->set_property = apex_configuration_request_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The ID of the configuration being requested.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_NAMESPACE] =
    g_param_spec_enum ("namespace",
                       "Namespace",
                       "The namespace of the configuration being requested.",
                       APEX_TYPE_CONFIGURATION_NAMESPACE,
                       APEX_CONFIGURATION_NAMESPACE_ACQUIRE,
                       (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_configuration_request_init (ApexConfigurationRequest *self)
{
}

ApexConfigurationRequest *
apex_configuration_request_new (void)
{
  return g_object_new (APEX_TYPE_CONFIGURATION_REQUEST, NULL);
}

/**
 * apex_configuration_request_serialize:
 * @self: an #ApexConfigurationRequest
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_configuration_request_serialize (ApexConfigurationRequest *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_CONFIGURATION_REQUEST (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

  void
apex_configuration_request_deserialize (ApexConfigurationRequest *self,
                                        const gchar              *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_CONFIGURATION_REQUEST,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
  {
    g_critical ("%s", err->message);
    g_error_free (err);
  }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_CONFIGURATION_REQUEST (object));

  apex_configuration_request_set_id (
      self,
      apex_configuration_request_get_id (APEX_CONFIGURATION_REQUEST (object)));

  apex_configuration_request_set_namespace (
      self,
      apex_configuration_request_get_namespace (APEX_CONFIGURATION_REQUEST (object)));

  g_clear_object (&object);
}

const gchar *
apex_configuration_request_get_id (ApexConfigurationRequest *self)
{
  g_return_val_if_fail (APEX_IS_CONFIGURATION_REQUEST (self), NULL);

  return self->id;
}

void
apex_configuration_request_set_id (ApexConfigurationRequest *self,
                                   const gchar              *id)
{
  g_return_if_fail (APEX_IS_CONFIGURATION_REQUEST (self));

  if (g_strcmp0 (id, self->id) != 0)
  {
    g_free (self->id);
    self->id = g_strdup (id);
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
  }
}

ApexConfigurationNamespace
apex_configuration_request_get_namespace (ApexConfigurationRequest *self)
{
  g_return_val_if_fail (APEX_IS_CONFIGURATION_REQUEST (self),
                        APEX_CONFIGURATION_NAMESPACE_ACQUIRE);

  return self->namespace;
}

void
apex_configuration_request_set_namespace (ApexConfigurationRequest *self,
                                          ApexConfigurationNamespace namespace)
{
  g_return_if_fail (APEX_IS_CONFIGURATION_REQUEST (self));

  self->namespace = namespace;
}
