/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_CHANNEL_REQUEST apex_channel_request_get_type ()
G_DECLARE_FINAL_TYPE (ApexChannelRequest, apex_channel_request, APEX, CHANNEL_REQUEST, GObject)

ApexChannelRequest *apex_channel_request_new  (void);

gchar       *apex_channel_request_serialize   (ApexChannelRequest *self);
void         apex_channel_request_deserialize (ApexChannelRequest *self,
                                               const gchar *data);

const gchar *apex_channel_request_get_id      (ApexChannelRequest *self);
void         apex_channel_request_set_id      (ApexChannelRequest *self,
                                               const gchar        *id);

G_END_DECLS
