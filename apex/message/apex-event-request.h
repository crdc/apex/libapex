/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_EVENT_REQUEST apex_event_request_get_type ()
G_DECLARE_FINAL_TYPE (ApexEventRequest, apex_event_request, APEX, EVENT_REQUEST, GObject)

ApexEventRequest *apex_event_request_new         (void);

gchar            *apex_event_request_serialize   (ApexEventRequest *self);
void              apex_event_request_deserialize (ApexEventRequest *self,
                                                  const gchar      *data);

ApexEvent        *apex_event_request_get_event   (ApexEventRequest *self);
void              apex_event_request_set_event   (ApexEventRequest *self,
                                                  ApexEvent        *event);

G_END_DECLS
