/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_SETTINGS_REQUEST apex_settings_request_get_type ()
G_DECLARE_FINAL_TYPE (ApexSettingsRequest, apex_settings_request, APEX, SETTINGS_REQUEST, GObject)

ApexSettingsRequest *apex_settings_request_new         (void);

gchar               *apex_settings_request_serialize   (ApexSettingsRequest *self);
void                 apex_settings_request_deserialize (ApexSettingsRequest *self,
                                                        const gchar         *data);

G_END_DECLS
