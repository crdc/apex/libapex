/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include <apex/apex.h>

/*
 * ApexModuleResponse:
 *
 * Represents a ...
 */
struct _ApexModuleResponse
{
  GObject     parent;
  ApexModule *module;
};

enum {
  PROP_0,
  PROP_MODULE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexModuleResponse, apex_module_response, G_TYPE_OBJECT)

static void
apex_module_response_finalize (GObject *object)
{
  ApexModuleResponse *self = (ApexModuleResponse *)object;

  // FIXME: this should probably just unref
  g_clear_object (&self->module);

  G_OBJECT_CLASS (apex_module_response_parent_class)->finalize (object);
}

static void
apex_module_response_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  ApexModuleResponse *self = APEX_MODULE_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_MODULE:
      /*g_value_set_object (value, apex_module_response_get_module (self));*/
      g_value_set_object (value, self->module);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_module_response_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  ApexModuleResponse *self = APEX_MODULE_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_MODULE:
      apex_module_response_set_module (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_module_response_class_init (ApexModuleResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_module_response_finalize;
  object_class->get_property = apex_module_response_get_property;
  object_class->set_property = apex_module_response_set_property;

  properties [PROP_MODULE] =
    g_param_spec_object ("module",
                         "Module",
                         "The module to response with.",
                         APEX_TYPE_MODULE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_module_response_init (ApexModuleResponse *self)
{
  // FIXME: this:
  // self->module = apex_module_new ();
  // Should look like this:
  // self->module = apex_module_new (const gchar *id,
  //                                 const gchar *endpoint,
  //                                 const gchar *service)
}

ApexModuleResponse *
apex_module_response_new (void)
{
  return g_object_new (APEX_TYPE_MODULE_RESPONSE, NULL);
}

/**
 * apex_module_response_serialize:
 * @self: an #ApexModuleResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
apex_module_response_serialize (ApexModuleResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_MODULE_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
apex_module_response_deserialize (ApexModuleResponse *self,
                                   const gchar         *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_MODULE_RESPONSE,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
  {
    g_critical ("%s", err->message);
    g_error_free (err);
  }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_MODULE_RESPONSE (object));

  apex_module_response_set_module (self,
      apex_module_response_get_module (APEX_MODULE_RESPONSE (object)));

  g_object_unref (object);
}

/**
 * apex_module_response_get_module:
 * @self: #ApexModuleResponse instance
 *
 * Returns: (transfer full): The #ApexModule
 */
ApexModule *
apex_module_response_get_module (ApexModuleResponse *self)
{
  ApexModule *module;

  g_return_val_if_fail (APEX_IS_MODULE_RESPONSE (self), NULL);

  g_object_get (self, "module", &module, NULL);

  return module;
}

void
apex_module_response_set_module (ApexModuleResponse *self,
                                   ApexModule         *module)
{
  g_return_if_fail (APEX_IS_MODULE_RESPONSE (self));
  g_return_if_fail (APEX_IS_MODULE (module));

  if (self->module)
    g_object_unref (self->module);

  if (module)
    g_object_ref (module);

  self->module = module;

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_MODULE]);
}
