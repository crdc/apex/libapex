#include "apex-status-request.h"

#include "proto/v1/core.pb-c.h"

/**
 * ApexStatusRequest:
 *
 * Represents a status request protobuf message.
 */
struct _ApexStatusRequest
{
	GObject parent;

  Apex__StatusRequest *pb;
};

G_DEFINE_TYPE (ApexStatusRequest, apex_status_request, G_TYPE_OBJECT)

static void
apex_status_request_finalize (GObject *object)
{
  ApexStatusRequest *self = (ApexStatusRequest *)object;

  apex__status_request__free_unpacked (self->pb, NULL);

  G_OBJECT_CLASS (apex_status_request_parent_class)->finalize (object);
}

static void
apex_status_request_class_init (ApexStatusRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_status_request_finalize;
}

static void
apex_status_request_init (ApexStatusRequest *self)
{
}

ApexStatusRequest *
apex_status_request_new (void)
{
  ApexStatusRequest *request;

  request = g_object_new (APEX_TYPE_STATUS_REQUEST, NULL);

  request->pb = g_malloc (sizeof (Apex__StatusRequest));
  // or ???
  apex__status_request__init (request->pb);
  //request->pb = &APEX__STATUS_REQUEST__INIT;

  return request;
}

ApexStatusRequest *
apex_status_request_new_from_data (const guint8 *data)
{
  ApexStatusRequest *request;
  gsize len;

  request = g_object_new (APEX_TYPE_STATUS_REQUEST, NULL);
  // XXX: this may only be correct because /8, don't really need the div?
  len = sizeof (data) / sizeof (guint8);

  request->pb = apex__status_request__unpack (NULL, len, data);
  if (request->pb == NULL)
    {
      g_error ("error unpacking status request message");
    }

  return request;
}

guint8 *
apex_status_request_to_data (ApexStatusRequest *self)
{
  gsize len;
  guint8 *out;

  g_return_val_if_fail (APEX_IS_STATUS_REQUEST (self), NULL);

  len = apex__status_request__get_packed_size (self->pb);
  out = g_malloc (len);

  // TODO: not sure what to do with this yet, return_val_if ?
  // XXX: is this supposed to be == 0 because there's no fields in the message?
  if (apex__status_request__pack (self->pb, out) == 0)
    {
      g_error ("error packing status request message");
    }

  return out;
}
