/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_EMPTY apex_empty_get_type ()
G_DECLARE_FINAL_TYPE (ApexEmpty, apex_empty, APEX, EMPTY, GObject)

ApexEmpty *apex_empty_new (void);

G_END_DECLS
