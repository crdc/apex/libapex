/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_MODULE_JOB_REQUEST apex_module_job_request_get_type ()
G_DECLARE_FINAL_TYPE (ApexModuleJobRequest, apex_module_job_request, APEX, MODULE_JOB_REQUEST, GObject)

ApexModuleJobRequest *apex_module_job_request_new           (void);

gchar                *apex_module_job_request_serialize     (ApexModuleJobRequest *self);
void                  apex_module_job_request_deserialize   (ApexModuleJobRequest *self,
                                                             const gchar          *data);

void                  apex_module_job_request_add           (ApexModuleJobRequest *self,
                                                             ApexProperty         *property);
void                  apex_module_job_request_remove        (ApexModuleJobRequest *self,
                                                             const gchar          *key);
ApexProperty         *apex_module_job_request_get           (ApexModuleJobRequest *self,
                                                             const gchar          *key);
gboolean              apex_module_job_request_contains      (ApexModuleJobRequest *self,
                                                             const gchar          *key);
GHashTable           *apex_module_job_request_get_list      (ApexModuleJobRequest *self);
void                  apex_module_job_request_set_list      (ApexModuleJobRequest *self,
                                                             GHashTable           *job_properties);

const gchar          *apex_module_job_request_get_id        (ApexModuleJobRequest *self);
void                  apex_module_job_request_set_id        (ApexModuleJobRequest *self,
                                                             const gchar          *id);

const gchar          *apex_module_job_request_get_job_id    (ApexModuleJobRequest *self);
void                  apex_module_job_request_set_job_id    (ApexModuleJobRequest *self,
                                                             const gchar          *job_id);

const gchar          *apex_module_job_request_get_job_value (ApexModuleJobRequest *self);
void                  apex_module_job_request_set_job_value (ApexModuleJobRequest *self,
                                                             const gchar          *job_value);

G_END_DECLS
