#define G_LOG_DOMAIN "apex-client"

#include "apex-mdp.h"
#include "apex-client.h"

// TODO: need to clean up data, add dispose?

struct _ApexClient
{
  GObject parent;
  gchar *broker;
  zsock_t *client;             // Socket to broker
  gint timeout;                // Request timeout
};

G_DEFINE_TYPE (ApexClient, apex_client, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_BROKER,
  PROP_TIMEOUT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
apex_client_finalize (GObject *object)
{
  ApexClient *self = (ApexClient *)object;

  zsock_destroy (&self->client);
  g_clear_pointer (&self->broker, g_free);

  G_OBJECT_CLASS (apex_client_parent_class)->finalize (object);
}

static void
apex_client_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  ApexClient *self = APEX_CLIENT (object);

  switch (prop_id)
    {
    case PROP_BROKER:
      g_value_set_string (value, apex_client_get_broker (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_client_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  ApexClient *self = APEX_CLIENT (object);

  switch (prop_id)
    {
    case PROP_BROKER:
      apex_client_set_broker (self, g_value_get_string (value));
      break;

    case PROP_TIMEOUT:
      apex_client_set_timeout (self, g_value_get_int (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_client_class_init (ApexClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_client_finalize;
  object_class->get_property = apex_client_get_property;
  object_class->set_property = apex_client_set_property;

  properties [PROP_BROKER] =
    g_param_spec_string ("broker",
                         "Broker",
                         "The broker endpoint to connect to.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_TIMEOUT] =
    g_param_spec_string ("timeout",
                         "Timeout",
                         "The connection timeout",
                         NULL,
                         (G_PARAM_WRITABLE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

/*
 * Connect or reconnect to broker. In this asynchronous class we use a
 * DEALER socket instead of a REQ socket; this lets us send any number
 * of requests without waiting for a reply.
 */
static void
apex_client_connect_to_broker (ApexClient *self)
{
  if (self->client)
    zsock_destroy (&self->client);

  self->client = zsock_new (ZMQ_DEALER);
  zsock_connect (self->client, "%s", self->broker);

  g_debug ("connecting to broker at %s...", self->broker);

  zsock_set_rcvtimeo(self->client, self->timeout);
}

static void
apex_client_init (ApexClient *self)
{
  self->timeout = 2500;     // msecs
}

// TODO: require broker endpoint

ApexClient *
apex_client_new (const gchar *broker)
{
  ApexClient *client = g_object_new (APEX_TYPE_CLIENT, "broker", broker, NULL);

  // FIXME: this should either be in init or a separate function, eg. worker
  apex_client_connect_to_broker (client);

  return client;
}

/**
 * apex_client_send:
 * @self: an #ApexClient
 * @service: the name of the service the request is sent to
 * @request_p: pointer to a multi-frame message.
 *
 * Send a message to an broker service using the MDP protocol.
 *
 * Returns: number of bytes sent.
 */
gint
apex_client_send (ApexClient   *self,
                  const gchar  *service,
                  zmsg_t      **request_p)
{
  zmsg_t *request;

  g_return_val_if_fail (APEX_IS_CLIENT (self), 0);

  request = *request_p;

  /*
   * Prefix request with protocol frames
   * Frame 0: empty (REQ emulation)
   * Frame 1: "MDPCxy" (six bytes, MDP/Client x.y)
   * Frame 2: Service name (printable string)
   */
  zmsg_pushstr (request, service);
  zmsg_pushstr (request, APEX_MDP_CLIENT);
  zmsg_pushstr (request, "");

  APEX_TRACE_MSG ("send request to '%s' service:", service);
  apex_mdp_dump (request);

  zmsg_send (request_p, self->client);

  return 0;
}

/**
 * apex_client_send_request:
 * @self: an #ApexClient
 * @service: the name of the service the request is sent to
 * @rpc: the call to make through the service
 * @request: a serialized request message
 *
 * This is really just a hack to send an Apex specific request message until
 * something better exists.
 *
 * Returns: number of bytes sent.
 */
gint
apex_client_send_request (ApexClient  *self,
                          const gchar *service,
                          const gchar *rpc,
                          const gchar *request)
{
  gint n;
  zmsg_t *msg;

  g_return_val_if_fail (APEX_IS_CLIENT (self), 0);

  msg = zmsg_new ();
  zmsg_pushstr (msg, request);
  zmsg_pushstr (msg, rpc);
  n = apex_client_send (self, service, &msg);
  zmsg_destroy (&msg);

  return n;
}

zmsg_t *
apex_client_recv (ApexClient *self, char **service_p)
{
  zmsg_t *msg;
  zframe_t *empty;
  zframe_t *header;
  zframe_t *service;

  g_return_val_if_fail (APEX_IS_CLIENT (self), NULL);

  msg = zmsg_recv (self->client);

  if (msg == NULL)
    return NULL;

  apex_mdp_dump (msg);

  // Don't try to handle errors, just assert noisily
  assert (zmsg_size (msg) >= 4);

  empty = zmsg_pop (msg);
  assert (zframe_streq (empty, ""));
  zframe_destroy (&empty);

  header = zmsg_pop (msg);
  assert (zframe_streq (header, APEX_MDP_CLIENT));
  zframe_destroy (&header);

  service = zmsg_pop (msg);
  if (service_p)
    *service_p = zframe_strdup (service);
  zframe_destroy (&service);

  return msg;       // Success
}

/**
 * apex_client_recv_response:
 * @self: an #ApexClient
 *
 * This is the corresponding hack for apex_client_send_request. It only
 * returns the last frame of a multi-part message so this is not an ideal
 * long term solution.
 *
 * Returns: (transfer full): the contents of the response as a string
 */
gchar *
apex_client_recv_response (ApexClient *self)
{
  g_autoptr (GString) response = NULL;
  gchar *ret;
  zmsg_t *msg;
  zframe_t *last;

  g_return_val_if_fail (APEX_IS_CLIENT (self), NULL);

  msg = apex_client_recv (self, NULL);
  if (msg == NULL)
    return NULL;

  response = g_string_new (NULL);
  last = zmsg_last (msg);
  if (last)
    {
      g_autofree gchar *data = NULL;

      data = zframe_strdup (last);
      response = g_string_append (response, data);
    }
  zmsg_destroy (&msg);

  ret = g_strdup (response->str);

  return g_steal_pointer (&ret);
}

const gchar *
apex_client_get_broker (ApexClient *self)
{
  g_return_val_if_fail (APEX_IS_CLIENT (self), NULL);

  return self->broker;
}

void
apex_client_set_broker (ApexClient  *self,
                        const gchar *broker)
{
  g_return_if_fail (APEX_IS_CLIENT (self));

  if (g_strcmp0 (broker, self->broker) != 0)
    {
      g_free (self->broker);
      self->broker = g_strdup (broker);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_BROKER]);
    }
}

void
apex_client_set_timeout (ApexClient *self,
                         gint        timeout)
{
  g_return_if_fail (APEX_IS_CLIENT (self));

	self->timeout = timeout;
}
