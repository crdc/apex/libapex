#define G_LOG_DOMAIN "apex-worker"

#include <glib-unix.h>
#include <apex/apex.h>

#include "apex-worker.h"
#include "apex-mdp.h"

// TODO: need to clean up data, add dispose?

// Reliability parameters
#define HEARTBEAT_LIVENESS  3  // 3-5 is reasonable

struct _ApexWorker
{
  GObject  parent;
  gchar   *broker;
  gchar   *service;
  zsock_t *worker;             // Socket to broker

  // Heartbeat management
  guint heartbeat_at;          // When to send HEARTBEAT
  gsize liveness;              // How many attempts left
  gint  heartbeat;             // Heartbeat delay, msecs
  gint  reconnect;             // Reconnect delay, msecs

  // Signal FD
  gint pipefd[2];
};

static void s_send_to_broker (ApexWorker  *self,
                              const gchar *command,
                              const gchar *option,
                              zmsg_t      *msg);

static void s_connect_to_broker (ApexWorker *self);

G_DEFINE_TYPE (ApexWorker, apex_worker, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_BROKER,
  PROP_SERVICE,
  PROP_LIVENESS,
  PROP_HEARTBEAT,
  PROP_RECONNECT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
apex_worker_dispose (GObject *object)
{
  ApexWorker *self = (ApexWorker *)object;

  s_send_to_broker (self, APEX_MDP_WORKER_DISCONNECT, NULL, NULL);

  zsock_destroy (&self->worker);

  G_OBJECT_CLASS (apex_worker_parent_class)->dispose (object);
}

static void
apex_worker_finalize (GObject *object)
{
  ApexWorker *self = (ApexWorker *)object;

  g_clear_pointer (&self->broker, g_free);
  g_clear_pointer (&self->service, g_free);

  close (self->pipefd[0]);
  close (self->pipefd[1]);

  G_OBJECT_CLASS (apex_worker_parent_class)->finalize (object);
}

static void
apex_worker_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  ApexWorker *self = APEX_WORKER (object);

  switch (prop_id)
    {
    case PROP_BROKER:
      g_value_set_string (value, apex_worker_get_broker (self));
      break;

    case PROP_SERVICE:
      g_value_set_string (value, apex_worker_get_service (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_worker_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  ApexWorker *self = APEX_WORKER (object);

  switch (prop_id)
    {
    case PROP_BROKER:
      apex_worker_set_broker (self, g_value_get_string (value));
      break;

    case PROP_SERVICE:
      apex_worker_set_service (self, g_value_get_string (value));
      break;

    case PROP_LIVENESS:
      apex_worker_set_liveness (self, g_value_get_int (value));
      break;

    case PROP_HEARTBEAT:
      apex_worker_set_heartbeat (self, g_value_get_int (value));
      break;

    case PROP_RECONNECT:
      apex_worker_set_reconnect (self, g_value_get_int (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_worker_class_init (ApexWorkerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = apex_worker_dispose;
  object_class->finalize = apex_worker_finalize;
  object_class->get_property = apex_worker_get_property;
  object_class->set_property = apex_worker_set_property;

  properties [PROP_BROKER] =
    g_param_spec_string ("broker",
                         "Broker",
                         "The broker endpoint to connect to.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_SERVICE] =
    g_param_spec_string ("service",
                         "Service",
                         "The broker service name.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_LIVENESS] =
    g_param_spec_int ("liveness",
                      "Liveness",
                      "The liveness parameter for the heartbeat.",
                      0,
                      G_MAXINT,
                      0,
                      (G_PARAM_WRITABLE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_HEARTBEAT] =
    g_param_spec_int ("heartbeat",
                      "Heartbeat",
                      "The heartbeat timeout value.",
                      0,
                      G_MAXINT,
                      0,
                      (G_PARAM_WRITABLE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_RECONNECT] =
    g_param_spec_int ("reconnect",
                      "Reconnect",
                      "The reconnection time for the heartbeat.",
                      0,
                      G_MAXINT,
                      0,
                      (G_PARAM_WRITABLE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

/*
 * Send message to broker
 * If no msg is provided, creates one internally
 */
static void
s_send_to_broker (ApexWorker  *self,
                  const gchar *command,
                  const gchar *option,
                  zmsg_t      *msg)
{
  msg = msg ? zmsg_dup (msg) : zmsg_new ();

  // Stack protocol envelope to start of message
  if (option)
    zmsg_pushstr (msg, option);

  zmsg_pushstr (msg, command);
  zmsg_pushstr (msg, APEX_MDP_WORKER);
  zmsg_pushstr (msg, "");

  APEX_TRACE_MSG ("sending %s to broker", mdpw_commands [(int) *command]);
  apex_mdp_dump (msg);

  zmsg_send (&msg, zsock_resolve (self->worker));
}

/*
 * Connect or reconnect to broker
 */
static void
s_connect_to_broker (ApexWorker *self)
{
  if (self->worker)
    zsock_destroy (&self->worker);

  self->worker = zsock_new (ZMQ_DEALER);

  /*
   * A non-zero linger value is required for DISCONNECT to be sent
   * when the worker is destroyed.  100 is arbitrary but chosen to be
   * sufficient for common cases without significant delay in broken ones.
   */
  zsock_set_linger (self->worker, 100);

  zsock_connect (self->worker, "%s", self->broker);

  g_debug ("connecting to broker at %s...", self->broker);

  // Register service with broker
  s_send_to_broker (self, APEX_MDP_WORKER_READY, self->service, NULL);

  // If liveness hits zero, queue is considered disconnected
  self->liveness = HEARTBEAT_LIVENESS;
  self->heartbeat_at = zclock_time () + self->heartbeat;
}

/*
 *static int s_fd;
 *static void
 *_signal_handler ()
 *{
 *  int rc = write (s_fd, " ", sizeof (" "));
 *  if (rc != sizeof(" "))
 *    {
 *      write (STDOUT_FILENO, "error", sizeof ("error") - 1);
 *      exit(1);
 *    }
 *}
 */

/*
 *static void
 *_catch_signals (gint fd)
 *{
 *  s_fd = fd;
 *
 *  struct sigaction action;
 *  action.sa_handler = _signal_handler;
 *  // Doesn't matter if SA_RESTART set because self-pipe will wake up zmq_poll
 *  // But setting to 0 will allow zmq_read to be interrupted.
 *  action.sa_flags = 0;
 *  sigemptyset (&action.sa_mask);
 *  sigaction (SIGINT, &action, NULL);
 *  sigaction (SIGTERM, &action, NULL);
 *}
 */

static void
apex_worker_init (ApexWorker *self)
{
  g_autoptr (GError) error = NULL;
  gboolean res;
  gint flags;

  self->heartbeat = 2500;
  self->reconnect = 2500;

  res = g_unix_open_pipe (self->pipefd, FD_CLOEXEC, &error);
  g_assert_no_error (error);
  g_assert_cmpint (res, ==, TRUE);

  res = g_unix_set_fd_nonblocking (self->pipefd[0], TRUE, &error);
  g_assert (res);
  g_assert_no_error (error);

  flags = fcntl (self->pipefd[0], F_GETFL);
  g_assert_cmpint (flags, !=, -1);
  g_assert (flags & O_NONBLOCK);

  /*_catch_signals (self->pipefd[1]);*/
}

// TODO: require broker endpoint

ApexWorker *
apex_worker_new (const gchar *broker,
                 const gchar *service)
{
  return g_object_new (APEX_TYPE_WORKER,
                       "broker", broker,
                       "service", service,
                       NULL);
}

void
apex_worker_connect (ApexWorker *self)
{
  g_return_if_fail (APEX_IS_WORKER (self));

  s_connect_to_broker (self);
}

void
apex_worker_disconnect (ApexWorker *self)
{
  g_return_if_fail (APEX_IS_WORKER (self));

  s_send_to_broker (self, APEX_MDP_WORKER_DISCONNECT, NULL, NULL);

  zsock_destroy (&self->worker);
}

const gchar *
apex_worker_get_broker (ApexWorker *self)
{
  g_return_val_if_fail (APEX_IS_WORKER (self), NULL);

  return self->broker;
}

void
apex_worker_set_broker (ApexWorker  *self,
                        const gchar *broker)
{
  g_return_if_fail (APEX_IS_WORKER (self));

  if (g_strcmp0 (broker, self->broker) != 0)
    {
      g_free (self->broker);
      self->broker = g_strdup (broker);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_BROKER]);
    }
}

const gchar *
apex_worker_get_service (ApexWorker *self)
{
  g_return_val_if_fail (APEX_IS_WORKER (self), NULL);

  return self->service;
}

void
apex_worker_set_service (ApexWorker  *self,
                         const gchar *service)
{
  g_return_if_fail (APEX_IS_WORKER (self));

  if (g_strcmp0 (service, self->service) != 0)
    {
      g_free (self->service);
      self->service = g_strdup (service);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SERVICE]);
    }
}

void
apex_worker_set_liveness (ApexWorker *self,
                          gint        liveness)
{
  g_return_if_fail (APEX_IS_WORKER (self));

	self->liveness = liveness;
}

void
apex_worker_set_heartbeat (ApexWorker *self,
                           gint        heartbeat)
{
  g_return_if_fail (APEX_IS_WORKER (self));

	self->heartbeat = heartbeat;
}

void
apex_worker_set_reconnect (ApexWorker *self,
                           gint        reconnect)
{
  g_return_if_fail (APEX_IS_WORKER (self));

	self->reconnect = reconnect;
}

zmsg_t *
apex_worker_recv (ApexWorker  *self,
                  zframe_t   **reply_to_p)
{
  g_return_val_if_fail (APEX_IS_WORKER (self), NULL);

  while (true)
    {
      zmq_pollitem_t items [] = {
        { 0, self->pipefd[0], ZMQ_POLLIN, 0 },
        { zsock_resolve (self->worker),  0, ZMQ_POLLIN, 0 }
      };

      int rc = zmq_poll (items, 2, self->heartbeat * ZMQ_POLL_MSEC);
      if (rc == 0)
        {
          continue;
        }
      else if (rc < 0)
        {
          if (errno == EINTR)
            continue;

          g_critical ("worker polling");
          break;
        }

      // Signal pipe FD
      if (items [0].revents & ZMQ_POLLIN)
        {
          gchar buffer [1];
          read (self->pipefd[0], buffer, 1);
          g_warning ("interrupt received");
          break;
        }

      // Read socket
      if (items [1].revents & ZMQ_POLLIN)
        {
          zmsg_t *msg;
          zframe_t *empty;
          zframe_t *header;
          zframe_t *command;

          msg = zmsg_recv (zsock_resolve (self->worker));

          if (!msg)
              break;          // Interrupted

          APEX_TRACE_MSG ("received message from broker:");
          apex_mdp_dump (msg);

          self->liveness = HEARTBEAT_LIVENESS;

          // Don't try to handle errors, just assert noisily
          g_assert (zmsg_size (msg) >= 3);

          empty = zmsg_pop (msg);
          g_assert (zframe_streq (empty, ""));
          zframe_destroy (&empty);

          header = zmsg_pop (msg);
          g_assert (zframe_streq (header, APEX_MDP_WORKER));
          zframe_destroy (&header);

          command = zmsg_pop (msg);

          if (zframe_streq (command, APEX_MDP_WORKER_REQUEST))
            {
              // We should pop and save as many addresses as there are
              // up to a null part, but for now, just save one...
              zframe_t *reply_to;

              reply_to = zmsg_unwrap (msg);
              if (reply_to_p)
                *reply_to_p = reply_to;
              else
                zframe_destroy (&reply_to);

              zframe_destroy (&command);

              // There's a message to process; return it to the caller
              return msg;
            }
          else if (zframe_streq (command, APEX_MDP_WORKER_HEARTBEAT))
            {
              ;               // Do nothing for heartbeats
            }
          else if (zframe_streq (command, APEX_MDP_WORKER_DISCONNECT))
            {
              s_connect_to_broker (self);
            }
          else
            {
              g_warning ("invalid input message");
              apex_mdp_dump (msg);
            }

          zframe_destroy (&command);
          zmsg_destroy (&msg);
        }
      else if (--self->liveness == 0)
        {
          g_warning ("disconnected from broker - retrying...");
          zclock_sleep (self->reconnect);
          s_connect_to_broker (self);
        }

      // Send HEARTBEAT if it's time
      if (zclock_time () > self->heartbeat_at)
        {
          s_send_to_broker (self, APEX_MDP_WORKER_HEARTBEAT, NULL, NULL);
          self->heartbeat_at = zclock_time () + self->heartbeat;
        }
    }

  if (zctx_interrupted)
    g_info ("interrupt received, killing worker...\n");

  return NULL;
}

void
apex_worker_send (ApexWorker *self, zmsg_t **report_p, zframe_t *reply_to)
{
  zmsg_t *report = *report_p;
  // Add client address
  zmsg_wrap (report, zframe_dup (reply_to));
  s_send_to_broker (self, APEX_MDP_WORKER_REPLY, NULL, report);
  zmsg_destroy (report_p);
}
