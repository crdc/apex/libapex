/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>
#include <czmq.h>

G_BEGIN_DECLS

/*
 * Type declaration.
 */
#define APEX_TYPE_WORKER apex_worker_get_type ()
G_DECLARE_FINAL_TYPE (ApexWorker, apex_worker, APEX, WORKER, GObject)

/*
 * Method definitions.
 */
ApexWorker  *apex_worker_new           (const gchar  *broker,
                                        const gchar  *service);

void         apex_worker_connect       (ApexWorker   *self);
void         apex_worker_disconnect    (ApexWorker   *self);

const gchar *apex_worker_get_broker    (ApexWorker   *self);
void         apex_worker_set_broker    (ApexWorker   *self,
                                        const gchar  *broker);

const gchar *apex_worker_get_service   (ApexWorker   *self);
void         apex_worker_set_service   (ApexWorker   *self,
                                        const gchar  *service);

void         apex_worker_set_liveness  (ApexWorker   *self,
                                        gint          liveness);

void         apex_worker_set_heartbeat (ApexWorker   *self,
                                        gint          heartbeat);

void         apex_worker_set_reconnect (ApexWorker   *self,
                                        gint          reconnect);

zmsg_t      *apex_worker_recv          (ApexWorker   *self,
                                        zframe_t    **reply_p);

void         apex_worker_send          (ApexWorker   *self,
                                        zmsg_t      **report_p,
                                        zframe_t     *reply_to);

G_END_DECLS
