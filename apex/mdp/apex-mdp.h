/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <glib-unix.h>
#include <czmq.h>

#include "apex-debug.h"

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

// This is the version of MDP/Client we implement
#define APEX_MDP_CLIENT             "MDPC01"

// MDP/Client commands, as strings
#define APEX_MDP_CLIENT_REQUEST     "\001"
#define APEX_MDP_CLIENT_REPLY       "\002"
#define APEX_MDP_CLIENT_NAK         "\003"

G_GNUC_UNUSED static const gchar *mdpc_commands [] = {
    NULL,
    "REQUEST",
    "REPLY",
    "NAK"
};

// This is the version of MDP/Worker we implement
#define APEX_MDP_WORKER             "MDPW01"

// MDP/Server commands, as strings
#define APEX_MDP_WORKER_READY       "\001"
#define APEX_MDP_WORKER_REQUEST     "\002"
#define APEX_MDP_WORKER_REPLY       "\003"
#define APEX_MDP_WORKER_HEARTBEAT   "\004"
#define APEX_MDP_WORKER_DISCONNECT  "\005"

static const gchar *mdpw_commands [] = {
    NULL,
    "READY",
    "REQUEST",
    "REPLY",
    "HEARTBEAT",
    "DISCONNECT"
};

static void
apex_mdp_dump (zmsg_t *msg)
{
  for (zframe_t *iter = zmsg_first (msg);
       iter != NULL;
       iter = zmsg_next (msg))
    {
      APEX_TRACE_MSG ("[%03d] %s",
                      (gint)zframe_size (iter),
                      zframe_strdup (iter));
    }
}
