/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "apex-mdp-service"

#include <czmq.h>
#include <apex/apex.h>

#include "apex-mdp-service.h"
/*#include "apex-mdp-worker.h"*/

struct _ApexMdpService
{
  GObject     parent;
  gchar      *name;
  gsize       workers;
  zlist_t    *requests;
  zlist_t    *waiting;
  zlist_t    *blacklist;
  ApexBroker *broker;
};

G_DEFINE_TYPE (ApexMdpService, apex_mdp_service, G_TYPE_OBJECT)

static void
apex_mdp_service_finalize (GObject *object)
{
  ApexMdpService *self;
  g_autofree gchar *command = NULL;

  self = (ApexMdpService *)object;

  while (zlist_size (self->requests))
    {
      zmsg_t *msg = (zmsg_t *) zlist_pop (self->requests);
      zmsg_destroy (&msg);
    }

  /* Free memory keeping blacklisted commands */
  command = (gchar *) zlist_first (self->blacklist);
  while (command)
    zlist_remove (self->blacklist, command);

  zlist_destroy (&self->requests);
  zlist_destroy (&self->waiting);
  zlist_destroy (&self->blacklist);

  g_clear_pointer (&self->name, g_free);

  G_OBJECT_CLASS (apex_mdp_service_parent_class)->finalize (object);
}

static void
apex_mdp_service_class_init (ApexMdpServiceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_mdp_service_finalize;
}

static void
apex_mdp_service_init (ApexMdpService *self)
{
  /* TODO: switch to GList or GPtrArray */
  self->requests = zlist_new ();
  self->waiting = zlist_new ();
  self->blacklist = zlist_new ();
}

ApexMdpService *
apex_mdp_service_new (void)
{
  return g_object_new (APEX_TYPE_MDP_SERVICE, NULL);
}

void
apex_mdp_service_dispatch (ApexMdpService *self)
{
}

void
apex_mdp_service_enable_command (ApexMdpService *self,
                                 const gchar    *name)
{
}

void
apex_mdp_service_disable_command (ApexMdpService *self,
                                  const gchar    *name)
{
}

gboolean
apex_mdp_service_command_enabled (ApexMdpService *self,
                                  const gchar    *name)
{
  return FALSE;
}

void
apex_mdp_service_push_request (ApexMdpService *self,
                               gpointer        request)
{
}

gpointer
apex_mdp_service_pop_request (ApexMdpService *self)
{
  return NULL;
}

gpointer
apex_mdp_service_peek_request (ApexMdpService *self)
{
  return NULL;
}

gint
apex_mdp_service_requests_length (ApexMdpService *self)
{
  return 0;
}

gint
apex_mdp_service_worker_count (ApexMdpService *self)
{
  return 0;
}

void
apex_mdp_service_add_waiting (ApexMdpService *self,
                              gpointer        waiting)
{
}

void
apex_mdp_service_remove_waiting (ApexMdpService *self,
                                 gpointer        waiting)
{
}

gchar *
apex_mdp_service_dup_name (ApexMdpService *self)
{
  return NULL;
}

void
apex_mdp_service_set_name (ApexMdpService *self,
                           const gchar    *name)
{
}

ApexBroker *
apex_mdp_service_ref_broker (ApexMdpService *self)
{
  return NULL;
}

void
apex_mdp_service_set_broker (ApexMdpService *self,
                             ApexBroker     *broker)
{
}
