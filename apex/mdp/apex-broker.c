/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "apex-broker"

#include <glib-unix.h>
#include <gio/gio.h>
#include <czmq.h>
#include <apex/apex.h>

#include "apex-broker.h"
#include "apex-mdp.h"
#include "apex-mdp-service.h"

// TODO: these should come from configuration

#define HEARTBEAT_LIVENESS  3     // 3-5 is reasonable
#define HEARTBEAT_INTERVAL  2500  // msecs
#define HEARTBEAT_EXPIRY    HEARTBEAT_INTERVAL * HEARTBEAT_LIVENESS

struct _ApexBroker
{
  GObject  parent;
  zsock_t *socket;
  gchar   *endpoint;
  zhash_t *services;
  zhash_t *workers;
  zlist_t *waiting;

  guint    heartbeat_at;
  gsize    liveness;
  gint     interval;
  gint     expiry;
  gboolean running;
};

G_DEFINE_TYPE (ApexBroker, apex_broker, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_ENDPOINT,
  PROP_LIVENESS,
  PROP_INTERVAL,
  PROP_EXPIRY,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/* Internal service type */
typedef struct {
  ApexBroker *broker;
  gchar      *name;
  zlist_t    *requests;
  zlist_t    *waiting;
  gsize       workers;
  zlist_t    *blacklist;
} Service;

/* Internal broker functions */
static void s_broker_worker_msg (ApexBroker *self, zframe_t *sender, zmsg_t *msg);
static void s_broker_client_msg (ApexBroker *self, zframe_t *sender, zmsg_t *msg);
static void s_broker_purge (ApexBroker *self);
static Service *s_service_require (ApexBroker *self, zframe_t *service_frame);

/* Internal service functions */
static void s_service_destroy (void *argument);
static void s_service_dispatch (Service *self);
static void s_service_enable_command (Service *self, const gchar *command);
static void s_service_disable_command (Service *self, const gchar *command);
static gint s_service_is_command_enabled (Service *self, const gchar *command);

/* Internal worker type */
typedef struct {
  ApexBroker *broker;
  gchar      *identity;
  zframe_t   *address;
  Service    *service;
  gint64      expiry;
} Worker;

static Worker *s_worker_require (ApexBroker *self, zframe_t *address);
static void s_worker_delete (Worker *self, gint disconnect);
static void s_worker_destroy (void *argument);
static void s_worker_send (Worker *self, const gchar *command, gchar *option, zmsg_t *msg);

__attribute__ ((__noreturn__))
static void
s_run_cb (GObject      *source_object,
          GAsyncResult *result,
          gpointer      user_data)
{
  // g_task_run_in_thread_sync shouldn't get there
  g_assert_not_reached ();
}

static void
s_run_thread (GTask        *task,
              gpointer      source_object,
              gpointer      task_data,
              GCancellable *cancellable)
{
  ApexBroker *self;

  g_assert (source_object == g_task_get_source_object (task));
  g_assert (task_data == g_task_get_task_data (task));
  g_assert (cancellable == g_task_get_cancellable (task));

  self = (ApexBroker *)source_object;

  self->running = TRUE;

  while (self->running)
    {
      gint rc;
      guint time;

      zmq_pollitem_t items [] = {
        { zsock_resolve(self->socket),  0, ZMQ_POLLIN, 0 }
      };

      if (g_cancellable_is_cancelled (cancellable))
        {
          g_task_return_new_error (task,
                                   G_IO_ERROR, G_IO_ERROR_CANCELLED,
                                   "Task cancelled");
          return;
        }

      // check polling
      rc = zmq_poll (items, 1, HEARTBEAT_INTERVAL * ZMQ_POLL_MSEC);

      // Check for interruption
      if (rc == -1)
        break;

      // Process next input message, if any
      if (items [0].revents & ZMQ_POLLIN)
        {
          zmsg_t *msg;
          zframe_t *sender;
          zframe_t *empty;
          zframe_t *header;

          msg = zmsg_recv (self->socket);
          if (!msg)
            break; // Interrupted

          APEX_TRACE_MSG ("received message:");
          apex_mdp_dump (msg);

          sender = zmsg_pop (msg);
          empty  = zmsg_pop (msg);
          header = zmsg_pop (msg);

          if (zframe_streq (header, APEX_MDP_CLIENT))
            {
              s_broker_client_msg (self, sender, msg);
            }
          else if (zframe_streq (header, APEX_MDP_WORKER))
            {
              s_broker_worker_msg (self, sender, msg);
            }
          else
            {
              g_critical ("invalid message:");
              apex_mdp_dump (msg);
              zmsg_destroy (&msg);
            }

          zframe_destroy (&sender);
          zframe_destroy (&empty);
          zframe_destroy (&header);
        }

      // Comparison fails if done against call directly
      time = zclock_time ();

      // Disconnect and delete any expired workers
      // Send heartbeats to idle workers if needed
      if (time > self->heartbeat_at)
        {
          Worker *worker;

          APEX_TRACE_MSG ("sending heartbeat at: %ld", zclock_time ());

          s_broker_purge (self);
          worker = (Worker *) zlist_first (self->waiting);
          while (worker)
            {
              s_worker_send (worker, APEX_MDP_WORKER_HEARTBEAT, NULL, NULL);
              worker = (Worker *) zlist_next (self->waiting);
            }

          APEX_TRACE_MSG ("sent heartbeat, incrementing heartbeat at check");
          self->heartbeat_at = zclock_time () + HEARTBEAT_INTERVAL;
        }
    }

  if (zctx_interrupted)
    g_warning ("interrupt received, shutting down...");

  g_task_return_boolean (task, TRUE);
}

static void
s_run_async (ApexBroker *self)
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (GCancellable) cancellable = NULL;

  cancellable = g_cancellable_new ();
  task = g_task_new (self, cancellable, s_run_cb, NULL);

  g_task_run_in_thread (task, s_run_thread);
}

static void
s_run_cancel (ApexBroker *self,
              gpointer    data)
{
  GCancellable *cancellable;

  cancellable = G_CANCELLABLE (data);
  g_cancellable_cancel (cancellable);

  self->running = FALSE;

  g_debug ("run cancelled");
}

static void
apex_broker_dispose (GObject *object)
{
  ApexBroker *self = (ApexBroker *)object;

  if (self->running)
    s_run_cancel (self, NULL);

  zsock_destroy (&self->socket);
  zhash_destroy (&self->services);
  zhash_destroy (&self->workers);
  zlist_destroy (&self->waiting);

  G_OBJECT_CLASS (apex_broker_parent_class)->dispose (object);
}

static void
apex_broker_finalize (GObject *object)
{
  ApexBroker *self = (ApexBroker *)object;

  g_clear_pointer (&self->endpoint, g_free);

  G_OBJECT_CLASS (apex_broker_parent_class)->finalize (object);
}

static void
apex_broker_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  ApexBroker *self = APEX_BROKER (object);

  switch (prop_id)
    {
    case PROP_ENDPOINT:
      g_value_set_string (value, apex_broker_get_endpoint (self));
      break;

    case PROP_LIVENESS:
      g_value_set_int (value, apex_broker_get_liveness (self));
      break;

    case PROP_INTERVAL:
      g_value_set_int (value, apex_broker_get_interval (self));
      break;

    case PROP_EXPIRY:
      g_value_set_int (value, apex_broker_get_expiry (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_broker_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  ApexBroker *self = APEX_BROKER (object);

  switch (prop_id)
    {
    case PROP_ENDPOINT:
      apex_broker_set_endpoint (self, g_value_get_string (value));
      break;

    case PROP_LIVENESS:
      apex_broker_set_liveness (self, g_value_get_int (value));
      break;

    case PROP_INTERVAL:
      apex_broker_set_interval (self, g_value_get_int (value));
      break;

    case PROP_EXPIRY:
      apex_broker_set_expiry (self, g_value_get_int (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_broker_class_init (ApexBrokerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = apex_broker_dispose;
  object_class->finalize = apex_broker_finalize;
  object_class->get_property = apex_broker_get_property;
  object_class->set_property = apex_broker_set_property;

  properties [PROP_ENDPOINT] =
    g_param_spec_string ("endpoint",
                         "Endpoint",
                         "The broker endpoint.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_LIVENESS] =
    g_param_spec_int ("liveness",
                      "Liveness",
                      "The liveness parameter for the heartbeat.",
                      0,
                      G_MAXINT,
                      0,
                      (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_INTERVAL] =
    g_param_spec_int ("interval",
                      "Interval",
                      "The interval for the heartbeat check.",
                      0,
                      G_MAXINT,
                      0,
                      (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_EXPIRY] =
    g_param_spec_int ("expiry",
                      "Expiry",
                      "The heartbeat expiry.",
                      0,
                      G_MAXINT,
                      0,
                      (G_PARAM_WRITABLE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_broker_init (ApexBroker *self)
{
  self->socket = zsock_new (ZMQ_ROUTER);
  self->services = zhash_new ();
  self->workers = zhash_new ();
  self->waiting = zlist_new ();
  self->heartbeat_at = zclock_time () + HEARTBEAT_INTERVAL;
  self->liveness = HEARTBEAT_LIVENESS;
  self->interval = HEARTBEAT_INTERVAL;
  self->expiry = HEARTBEAT_EXPIRY;

  zsock_set_rcvhwm (self->socket, 500000);
}

ApexBroker *
apex_broker_new (const gchar *endpoint)
{
  return g_object_new (APEX_TYPE_BROKER,
                       "endpoint", endpoint,
                       NULL);
}

/**
 * apex_broker_bind:
 * @self: an #ApexBroker instance
 *
 * The bind method binds the broker instance to an endpoint. We can call
 * this multiple times. Note that MDP uses a single socket for both clients
 * and workers.
 */
void
apex_broker_bind (ApexBroker *self)
{
  zsock_bind (self->socket, "%s", self->endpoint);
  g_info ("MDP broker/0.2.0 is active at %s", self->endpoint);
}

void
apex_broker_run (ApexBroker *self)
{
  s_run_async (self);
}

static void
s_broker_worker_msg (ApexBroker *self,
                     zframe_t   *sender,
                     zmsg_t     *msg)
{
  g_autofree gchar *identity = NULL;
  Worker *worker;
  zframe_t *command;
  gint worker_ready;

  g_return_if_fail (APEX_IS_BROKER (self));
  g_return_if_fail (zmsg_size (msg) >= 1);    // needs to be a command

  command = zmsg_pop (msg);
  identity = zframe_strhex (sender);
  worker_ready = (zhash_lookup (self->workers, identity) != NULL);
  worker = s_worker_require (self, sender);

  if (zframe_streq (command, APEX_MDP_WORKER_READY))
    {
      if (worker_ready)               // Not first command in session
        {
          s_worker_delete (worker, 1);
        }
      else if (zframe_size (sender) >= 4  // Reserved service name
               && memcmp (zframe_data (sender), "mmi.", 4) == 0)
        {
          s_worker_delete (worker, 1);
        }
      else
        {
          // Attach worker to service and mark as idle
          zframe_t *service_frame = zmsg_pop (msg);
          worker->service = s_service_require (self, service_frame);
          zlist_append (self->waiting, worker);
          zlist_append (worker->service->waiting, worker);
          worker->service->workers++;
          worker->expiry = zclock_time () + HEARTBEAT_EXPIRY;
          s_service_dispatch (worker->service);
          zframe_destroy (&service_frame);
          g_info ("worker created");
        }
    }
  else if (zframe_streq (command, APEX_MDP_WORKER_REPLY))
    {
      if (worker_ready)
        {
          zframe_t *client;

          // Remove & save client return envelope and insert the
          // protocol header and service name, then rewrap envelope.
          client = zmsg_unwrap (msg);
          zmsg_pushstr (msg, (const gchar *) (worker->service->name));
          zmsg_pushstr (msg, APEX_MDP_CLIENT_REPLY);
          zmsg_pushstr (msg, APEX_MDP_CLIENT);
          zmsg_wrap (msg, client);
          zmsg_send (&msg, self->socket);
        }
      else
        {
          s_worker_delete (worker, 1);
        }
    }
  else if (zframe_streq (command, APEX_MDP_WORKER_HEARTBEAT))
    {
      if (worker_ready)
        {
          if (zlist_size (self->waiting) > 1)
            {
              // Move worker to the end of the waiting queue,
              // so s_broker_purge will only check old worker(s)
              zlist_remove (self->waiting, worker);
              zlist_append (self->waiting, worker);
            }
          worker->expiry = zclock_time () + HEARTBEAT_EXPIRY;
        }
      else
        {
          s_worker_delete (worker, 1);
        }
    }
  else if (zframe_streq (command, APEX_MDP_WORKER_DISCONNECT))
    {
      s_worker_delete (worker, 0);
    }
  else
    {
      zclock_log ("E: invalid input message");
      apex_mdp_dump (msg);
    }

  zframe_destroy (&command);
  zmsg_destroy (&msg);
}

static void
s_broker_client_msg (ApexBroker *self,
                     zframe_t   *sender,
                     zmsg_t     *msg)

{
  zframe_t *service_frame;

  g_return_if_fail (APEX_IS_BROKER (self));
  g_return_if_fail (zmsg_size (msg) >= 2);    // needs to be a command

  service_frame = zmsg_pop (msg);

  // If we got a MMI service request, process that internally
  if (zframe_size (service_frame) >= 4 &&
      memcmp (zframe_data (service_frame), "mmi.", 4) == 0)
    {
      g_autofree gchar *return_code = NULL;

      if (zframe_streq (service_frame, "mmi.service"))
        {
          g_autofree gchar *name = NULL;
          Service *service;

          name = zframe_strdup (zmsg_last (msg));
          service = (Service *) zhash_lookup (self->services, name);
          return_code = service && service->workers
            ? g_strdup ("200")
            : g_strdup ("404");
        }
      else
        {
          // The filter service that can be used to manipulate
          // the command filter table.
          if (zframe_streq (service_frame, "mmi.filter") &&
              zmsg_size (msg) == 3)
            {
              g_autofree gchar *command_str = NULL;
              zframe_t *operation;
              zframe_t *filter_service_frame;
              zframe_t *command_frame;

              operation = zmsg_pop (msg);
              filter_service_frame = zmsg_pop (msg);
              command_frame = zmsg_pop (msg);
              command_str = zframe_strdup (command_frame);

              if (zframe_streq (operation, "enable"))
                {
                  Service *service = s_service_require (self, filter_service_frame);
                  s_service_enable_command (service, command_str);
                  return_code = g_strdup ("200");
                }
              else
                {
                  if (zframe_streq (operation, "disable"))
                    {
                      Service *service = s_service_require (self, filter_service_frame);
                      s_service_disable_command (service, command_str);
                      return_code = g_strdup ("200");
                    }
                  else
                    {
                      return_code = g_strdup ("400");
                    }
                }

              zframe_destroy (&operation);
              zframe_destroy (&filter_service_frame);
              zframe_destroy (&command_frame);

              // Add an empty frame; it will be replaced by the return code.
              zmsg_pushstr (msg, "");
            }
          else
            {
              return_code = g_strdup ("501");
            }
        }

      zframe_reset (zmsg_last (msg), return_code, strlen (return_code));

      // Insert the protocol header and service name, then rewrap envelope.
      zmsg_push (msg, zframe_dup (service_frame));
      zmsg_pushstr (msg, APEX_MDP_CLIENT_REPLY);
      zmsg_pushstr (msg, APEX_MDP_CLIENT);
      zmsg_wrap (msg, zframe_dup (sender));
      zmsg_send (&msg, self->socket);
    }
  else
    {
      Service *service;
      gint enabled = 1;

      service = s_service_require (self, service_frame);

      if (zmsg_size (msg) >= 1)
        {
          g_autofree gchar *cmd = NULL;
          zframe_t *cmd_frame = zmsg_first (msg);
          cmd = zframe_strdup (cmd_frame);
          enabled = s_service_is_command_enabled (service, cmd);
        }

      // Forward the message to the worker.
      if (enabled)
        {
          zmsg_wrap (msg, zframe_dup (sender));
          zlist_append (service->requests, msg);
          s_service_dispatch (service);
        }
      // Send a NAK message back to the client.
      else
        {
          zmsg_push (msg, zframe_dup (service_frame));
          zmsg_pushstr (msg, APEX_MDP_CLIENT_NAK);
          zmsg_pushstr (msg, APEX_MDP_CLIENT);
          zmsg_wrap (msg, zframe_dup (sender));
          zmsg_send (&msg, self->socket);
        }
    }

  zframe_destroy (&service_frame);
}

static void
s_broker_purge (ApexBroker *self)
{
  Worker *worker;

  worker = (Worker *) zlist_first (self->waiting);
  while (worker)
    {
      // check if worker is alive
      if (zclock_time () < worker->expiry)
        break;

      g_info ("deleting expired worker: %s", worker->identity);
      s_worker_delete (worker, 0);
      worker = (Worker *) zlist_first (self->waiting);
    }
}

/* Implementation of internal service methods */

static Service *
s_service_require (ApexBroker *self,
                   zframe_t   *service_frame)
{
  Service *service;
  g_autofree gchar *name = NULL;

  name = zframe_strdup (service_frame);
  service = (Service *) zhash_lookup (self->services, name);

  if (service == NULL)
    {
      service = (Service *) zmalloc (sizeof (Service));

      service->broker = self;
      service->name = name;
      service->requests = zlist_new ();
      service->waiting = zlist_new ();
      service->blacklist = zlist_new ();

      zhash_insert (self->services, name, service);
      zhash_freefn (self->services, name, s_service_destroy);

      g_info ("added service: %s", name);
    }

  return service;
}

// \Check
static void
s_service_destroy (void *argument)
{
  Service *service;
  g_autofree gchar *command = NULL;

  service = (Service *) argument;
  while (zlist_size (service->requests))
    {
      zmsg_t *msg = (zmsg_t *) zlist_pop (service->requests);
      zmsg_destroy (&msg);
    }

  // Free memory keeping  blacklisted commands.
  command = (gchar *) zlist_first (service->blacklist);
  while (command)
    zlist_remove (service->blacklist, command);

  zlist_destroy (&service->requests);
  zlist_destroy (&service->waiting);
  zlist_destroy (&service->blacklist);

  g_free (service->name);
  g_free (service);
}

static void
s_service_dispatch (Service *self)
{
  s_broker_purge (self->broker);

  if (zlist_size (self->waiting) == 0)
    return;

  while (zlist_size (self->requests) > 0)
    {
      Worker *worker;
      zmsg_t *msg;

      worker = (Worker*) zlist_pop (self->waiting);
      zlist_remove (self->waiting, worker);
      msg = (zmsg_t*) zlist_pop (self->requests);
      s_worker_send (worker, APEX_MDP_WORKER_REQUEST, NULL, msg);

      // Workers are scheduled in the round-robin fashion
      zlist_append (self->waiting, worker);
      zmsg_destroy (&msg);
    }
}

static void
s_service_enable_command (Service     *self,
                          const gchar *command)
{
  gchar *item;

  item = (gchar *) zlist_first (self->blacklist);
  while (item && g_strcmp0 (item, command) != 0)
    item = (gchar *) zlist_next (self->blacklist);

  if (item)
    {
      zlist_remove (self->blacklist, item);
      g_free (item);
    }
}

static void
s_service_disable_command (Service     *self,
                           const gchar *command)
{
  gchar *item;

  item = (gchar *) zlist_first (self->blacklist);
  while (item && g_strcmp0 (item, command) != 0)
    item = (gchar *) zlist_next (self->blacklist);

  if (!item)
    zlist_push (self->blacklist, strdup (command));
}

static gint
s_service_is_command_enabled (Service     *self,
                              const gchar *command)
{
  gchar *item;

  item = (gchar *) zlist_first (self->blacklist);
  while (item && g_strcmp0 (item, command) != 0)
    item = (gchar *) zlist_next (self->blacklist);

  return item ? 0 : 1;
}

/* Implementation of internal worker methods */

static Worker *
s_worker_require (ApexBroker *self,
                  zframe_t   *address)
{
  Worker *worker;
  g_autofree gchar *identity = NULL;

  // self->workers is keyed off worker identity
  identity = zframe_strhex (address);
  worker = (Worker *) zhash_lookup (self->workers, identity);

  if (worker == NULL)
    {
      worker = (Worker *) zmalloc (sizeof (Worker));

      worker->broker = self;
      worker->identity = identity;
      worker->address = zframe_dup (address);

      zhash_insert (self->workers, identity, worker);
      zhash_freefn (self->workers, identity, s_worker_destroy);

      g_info ("registering new worker: %s", identity);
    }

  return worker;
}

static void
s_worker_delete (Worker *self,
                 gint    disconnect)
{
  if (disconnect)
    s_worker_send (self, APEX_MDP_WORKER_DISCONNECT, NULL, NULL);

  if (self->service)
    {
      zlist_remove (self->service->waiting, self);
      self->service->workers--;
    }

  zlist_remove (self->broker->waiting, self);
  // This implicitly calls s_worker_destroy
  zhash_delete (self->broker->workers, self->identity);
}

static void
s_worker_destroy (void *argument)
{
  Worker *self;

  self = (Worker *) argument;

  zframe_destroy (&self->address);
  g_free (self->identity);
  g_free (self);
}

static void
s_worker_send (Worker      *self,
               const gchar *command,
               gchar       *option,
               zmsg_t      *msg)
{
  msg = msg ? zmsg_dup (msg) : zmsg_new ();

  // Stack protocol envelope to start of message
  if (option)
    zmsg_pushstr (msg, option);

  zmsg_pushstr (msg, command);
  zmsg_pushstr (msg, APEX_MDP_WORKER);

  // Stack routing envelope to start of message
  zmsg_wrap (msg, zframe_dup (self->address));

  APEX_TRACE_MSG ("sending %s to worker", mdpw_commands [(int) *command]);

  zmsg_send (&msg, self->broker->socket);
}

/* Broker getters and setters */

/**
 * apex_broker_get_endpoint:
 */
const gchar *
apex_broker_get_endpoint (ApexBroker *self)
{
  g_return_val_if_fail (APEX_IS_BROKER (self), NULL);

  return self->endpoint;
}

void
apex_broker_set_endpoint (ApexBroker  *self,
                          const gchar *endpoint)
{
  g_return_if_fail (APEX_IS_BROKER (self));

  if (g_strcmp0 (endpoint, self->endpoint) != 0)
    {
      g_free (self->endpoint);
      self->endpoint = g_strdup (endpoint);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENDPOINT]);
    }
}

gint
apex_broker_get_liveness (ApexBroker *self)
{
  g_return_val_if_fail (APEX_IS_BROKER (self), 0);

  return self->liveness;
}

void
apex_broker_set_liveness (ApexBroker *self,
                          gint        liveness)
{
  g_return_if_fail (APEX_IS_BROKER (self));

  self->liveness = liveness;
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_LIVENESS]);
}

gint
apex_broker_get_interval (ApexBroker *self)
{
  g_return_val_if_fail (APEX_IS_BROKER (self), 0);

  return self->interval;
}

void
apex_broker_set_interval (ApexBroker *self,
                          gint        interval)
{
  g_return_if_fail (APEX_IS_BROKER (self));

  self->interval = interval;
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_INTERVAL]);
}

gint
apex_broker_get_expiry (ApexBroker *self)
{
  g_return_val_if_fail (APEX_IS_BROKER (self), 0);

  return self->expiry;
}

void
apex_broker_set_expiry (ApexBroker *self,
                        gint        expiry)
{
  g_return_if_fail (APEX_IS_BROKER (self));

  self->expiry = expiry;
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_EXPIRY]);
}
