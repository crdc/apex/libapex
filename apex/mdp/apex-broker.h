/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

/*
 * Type declaration.
 */
#define APEX_TYPE_BROKER apex_broker_get_type ()
G_DECLARE_FINAL_TYPE (ApexBroker, apex_broker, APEX, BROKER, GObject)

/*
 * Method definitions.
 */
ApexBroker  *apex_broker_new           (const gchar *endpoint);

void         apex_broker_bind          (ApexBroker  *self);
void         apex_broker_run           (ApexBroker  *self);

const gchar *apex_broker_get_endpoint  (ApexBroker  *self);
void         apex_broker_set_endpoint  (ApexBroker  *self,
                                        const gchar *endpoint);

gint         apex_broker_get_liveness  (ApexBroker  *self);
void         apex_broker_set_liveness  (ApexBroker  *self,
                                        gint         liveness);

gint         apex_broker_get_interval  (ApexBroker  *self);
void         apex_broker_set_interval  (ApexBroker  *self,
                                        gint         interval);

gint         apex_broker_get_expiry    (ApexBroker  *self);
void         apex_broker_set_expiry    (ApexBroker  *self,
                                        gint         expiry);

G_END_DECLS
