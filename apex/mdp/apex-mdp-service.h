/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_MDP_SERVICE apex_mdp_service_get_type ()
G_DECLARE_FINAL_TYPE (ApexMdpService, apex_mdp_service, APEX, MDP_SERVICE, GObject)

ApexMdpService *apex_mdp_service_new             (void);

void            apex_mdp_service_dispatch        (ApexMdpService *self);
void            apex_mdp_service_enable_command  (ApexMdpService *self,
                                                  const gchar    *name);
void            apex_mdp_service_disable_command (ApexMdpService *self,
                                                  const gchar    *name);
gboolean        apex_mdp_service_command_enabled (ApexMdpService *self,
                                                  const gchar    *name);

void            apex_mdp_service_push_request    (ApexMdpService *self,
                                                  gpointer        request);
gpointer        apex_mdp_service_pop_request     (ApexMdpService *self);
gpointer        apex_mdp_service_peek_request    (ApexMdpService *self);
gint            apex_mdp_service_requests_length (ApexMdpService *self);
gint            apex_mdp_service_worker_count    (ApexMdpService *self);
void            apex_mdp_service_add_waiting     (ApexMdpService *self,
                                                  gpointer        waiting);
void            apex_mdp_service_remove_waiting  (ApexMdpService *self,
                                                  gpointer        waiting);

gchar          *apex_mdp_service_dup_name        (ApexMdpService *self);
void            apex_mdp_service_set_name        (ApexMdpService *self,
                                                  const gchar    *name);

ApexBroker     *apex_mdp_service_ref_broker      (ApexMdpService *self);
void            apex_mdp_service_set_broker      (ApexMdpService *self,
                                                  ApexBroker     *broker);

G_END_DECLS
