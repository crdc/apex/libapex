/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>
#include <czmq.h>

G_BEGIN_DECLS

/*
 * Type declaration.
 */
#define APEX_TYPE_CLIENT apex_client_get_type ()
G_DECLARE_FINAL_TYPE (ApexClient, apex_client, APEX, CLIENT, GObject)

/*
 * Method definitions.
 */
ApexClient  *apex_client_new           (const gchar  *broker);

gint         apex_client_send          (ApexClient   *self,
                                        const gchar  *service,
                                        zmsg_t      **request_p);
gint         apex_client_send_request  (ApexClient   *self,
                                        const gchar  *service,
                                        const gchar  *rpc,
                                        const gchar  *request);
zmsg_t      *apex_client_recv          (ApexClient   *self,
                                        char        **service);
gchar       *apex_client_recv_response (ApexClient *self);

const gchar *apex_client_get_broker    (ApexClient   *self);
void         apex_client_set_broker    (ApexClient   *self,
                                        const gchar  *broker);

void         apex_client_set_timeout   (ApexClient   *self,
                                        gint          timeout);

G_END_DECLS
