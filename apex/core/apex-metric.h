/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_METRIC apex_metric_get_type ()
G_DECLARE_FINAL_TYPE (ApexMetric, apex_metric, APEX, METRIC, GObject)

ApexMetric      *apex_metric_new          (void);
ApexMetric      *apex_metric_new_full     (const gchar     *id,
                                           const gchar     *name,
                                           ApexMetricTable *data);

gchar           *apex_metric_serialize    (ApexMetric      *self);
void             apex_metric_deserialize  (ApexMetric      *self,
                                           const gchar     *data);

const gchar     *apex_metric_get_id       (ApexMetric      *self);
void             apex_metric_set_id       (ApexMetric      *self,
                                           const gchar     *id);
gchar           *apex_metric_dup_id       (ApexMetric      *self);

const gchar     *apex_metric_get_name     (ApexMetric      *self);
void             apex_metric_set_name     (ApexMetric      *self,
                                           const gchar     *name);
gchar           *apex_metric_dup_name     (ApexMetric      *self);

ApexMetricTable *apex_metric_get_data     (ApexMetric      *self);
ApexMetricTable *apex_metric_ref_data     (ApexMetric      *self);
void             apex_metric_set_data     (ApexMetric      *self,
                                           ApexMetricTable *data);

G_END_DECLS
