/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib.h>

G_BEGIN_DECLS

void apex_log_init               (gboolean     _stdout,
                                  const gchar *filename);
void apex_log_shutdown           (void);
void apex_log_increase_verbosity (void);
void apex_log_decrease_verbosity (void);
void apex_log_enable_json        (void);
void apex_log_disable_json       (void);

G_END_DECLS
