/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "apex-application"

#include <glib-unix.h>
#include <czmq.h>
#include <apex-debug.h>

#include "apex-application.h"
#include "apex-error.h"
#include "apex-configuration.h"
#include "apex-event.h"
#include "apex-handler.h"
#include "apex-job.h"
#include "apex-job-queue.h"
#include "apex-log.h"
#include "apex-mdp-handler.h"
#include "apex-metric.h"
#include "apex-sink.h"
#include "apex-source.h"
#include "apex-property.h"
#include "mdp/apex-broker.h"
#include "message/apex-message.h"

/**
 * SECTION:apex-application
 * @short_description: Application base class
 *
 * An #ApexApplication is used by applications that should support message handling.
 */

/**
 * ApexApplication:
 *
 * #ApexApplication is meant to be derived by services that integrate with a larger
 * Apex network, and are accessed using the functions defined here.
 */

/**
 * ApexApplicationClass:
 * @property_changed: invoked after a stored property has been changed
 * @get_configuration: invoked by the message handler when the
 *    'get-configuration' message has been received.
 * @get_status: invoked by the message handler when the
 *    'get-status' message has been received.
 * @get_settings: invoked by the message handler when the
 *    'get-settings' message has been received.
 * @get_job: invoked by the message handler when the
 *    'get-job' message has been received.
 * @get_jobs: invoked by the message handler when the
 *    'get-jobs' message has been received.
 * @get_active_job: invoked by the message handler when the
 *    'get-active-job' message has been received.
 * @submit_job: invoked by the message handler when the
 *    'submit-job' message has been received.
 * @cancel_job: invoked by the message handler when the
 *    'cancel-job' message has been received.
 * @submit_event: invoked by the message handler when the
 *    'submit-event' message has been received.
 * @available_events: invoked by the message handler when the
 *    'available-events' message has been received.
 *
 * Virtual function table for #ApexApplication.
 */

typedef struct
{
  /*< public >*/
  gchar             *id;
  gchar             *endpoint;
  gchar             *service;
  ApexConfiguration *configuration;

  /*< private >*/
  gboolean           enable_handler;
  gboolean           enable_job_queue;
  gboolean           enable_standalone;
  GAction           *shutdown_action;
  GHashTable        *property_table;
  GHashTable        *sink_table;
  GHashTable        *source_table;
  gchar             *broker_endpoint;
  ApexBroker        *broker;
  ApexHandler       *handler;
  ApexJobQueue      *job_queue;
} ApexApplicationPrivate;

enum {
  PROP_0,
  PROP_ID,
  PROP_ENDPOINT,
  PROP_SERVICE,
  PROP_CONFIG,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

enum {
  SIGNAL_PROPERTY_CHANGED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

G_DEFINE_TYPE_WITH_PRIVATE (ApexApplication, apex_application, G_TYPE_APPLICATION)

/* TODO: do real signals that are useful */
static void
s_shutdown_action_cb (GAction  *action,
                      GVariant *parameter,
                      gpointer  data)
{
  GApplication *app = NULL;

  app = G_APPLICATION (data);

  g_application_hold (app);
  g_info ("Action %s activated\n", g_action_get_name (action));
  g_application_release (app);
}

static void
s_add_actions (ApexApplication *self)
{
  ApexApplicationPrivate *priv;
  /*GAction *action = NULL;*/

  priv = apex_application_get_instance_private (self);

  priv->shutdown_action = G_ACTION (g_simple_action_new ("shutdown-action", NULL));
  g_signal_connect (priv->shutdown_action, "activate", G_CALLBACK (s_shutdown_action_cb), self);
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (priv->shutdown_action));
  /*g_object_unref (action);*/
}

static gboolean
s_signal_handler (gpointer user_data)
{
  GApplication *app = NULL;

  app = G_APPLICATION (user_data);

  g_info ("%s\n", "Stop signal received, exiting");
  g_application_quit (app);

  return G_SOURCE_REMOVE;
}

static void
s_add_signals (ApexApplication *self)
{
  g_debug ("add signal interrupts");

  g_source_set_name_by_id (g_unix_signal_add (SIGUSR1,
                                              s_signal_handler,
                                              self),
                           "[ApexApplication] SIGUSR1 listener");
  g_source_set_name_by_id (g_unix_signal_add (SIGHUP,
                                              s_signal_handler,
                                              self),
                           "[ApexApplication] SIGHUP listener");
  g_source_set_name_by_id (g_unix_signal_add (SIGTERM,
                                              s_signal_handler,
                                              self),
                           "[ApexApplication] SIGTERM listener");
  g_source_set_name_by_id (g_unix_signal_add (SIGINT,
                                              s_signal_handler,
                                              self),
                           "[ApexApplication] SIGINT listener");
}

static void
s_process_env (ApexApplication *self)
{
  ApexApplicationPrivate *priv;

  priv = apex_application_get_instance_private (self);

  /* Setup the defaults */
  priv->enable_handler = TRUE;
  priv->enable_job_queue = TRUE;
  priv->enable_standalone = FALSE;

  if (g_getenv ("PLANTD_MODULE_HANDLER") != NULL)
    {
      g_autofree gchar *env = NULL;
      env = g_utf8_strdown (g_getenv ("PLANTD_MODULE_HANDLER"), -1);
      g_debug ("[PLANTD_MODULE_HANDLER]: %s", env);
      if (g_strcmp0 (env, "false") == 0 ||
          g_strcmp0 (env, "no") == 0 ||
          g_strcmp0 (env, "0") == 0)
        priv->enable_handler = FALSE;
    }

  if (g_getenv ("PLANTD_MODULE_JOB_QUEUE") != NULL)
    {
      g_autofree gchar *env = NULL;
      env = g_utf8_strdown (g_getenv ("PLANTD_MODULE_JOB_QUEUE"), -1);
      g_debug ("[PLANTD_MODULE_JOB_QUEUE]: %s", env);
      if (g_strcmp0 (env, "false") == 0 ||
          g_strcmp0 (env, "no") == 0 ||
          g_strcmp0 (env, "0") == 0)
        priv->enable_job_queue = FALSE;
    }

  if (g_getenv ("PLANTD_MODULE_STANDALONE") != NULL)
    {
      g_autofree gchar *env = NULL;
      env = g_utf8_strdown (g_getenv ("PLANTD_MODULE_STANDALONE"), -1);
      g_debug ("[PLANTD_MODULE_STANDALONE]: %s", env);
      if (g_strcmp0 (env, "true") == 0 ||
          g_strcmp0 (env, "yes") == 0 ||
          g_strcmp0 (env, "1") == 0)
        priv->enable_standalone = TRUE;
    }

  /* XXX: these may need to be handled after application run otherwise the
   * environment will be lower priority than values set by the application */

  if (g_getenv ("PLANTD_MODULE_ENDPOINT") != NULL)
    {
      const gchar *env;
      env = g_getenv ("PLANTD_MODULE_ENDPOINT");
      apex_application_set_endpoint (self, env);
      g_debug ("[PLANTD_MODULE_ENDPOINT]: %s", env);
    }

  if (g_getenv ("PLANTD_MODULE_BROKER") != NULL)
    {
      const gchar *env;
      env = g_getenv ("PLANTD_MODULE_BROKER");
      priv->broker_endpoint = g_strdup (env);
      g_debug ("[PLANTD_MODULE_BROKER]: %s", env);
    }
  else
    {
      priv->broker_endpoint - g_strdup ("tcp://*:5555");
    }
}

static void
s_run_standalone (ApexApplication *self)
{
  ApexApplicationPrivate *priv;

  g_debug ("run in standalone mode");

  priv = apex_application_get_instance_private (self);

  priv->broker = apex_broker_new (priv->broker_endpoint);
  apex_broker_bind (priv->broker);
  apex_broker_run (priv->broker);
}

/* vfunc defaults {{{1 */
static ApexConfigurationResponse *
apex_application_real_get_configuration (ApexApplication  *self,
                                         GError          **error)
{
  ApexApplicationPrivate *priv;
  ApexConfigurationResponse *response;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);

  priv = apex_application_get_instance_private (self);

  response = apex_configuration_response_new ();
  apex_configuration_response_set_configuration (response, priv->configuration);

  return response;
}

static ApexStatusResponse *
apex_application_real_get_status (ApexApplication  *self,
                                  GError          **error)
{
  ApexStatusResponse *response;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);

  response = apex_status_response_new ();

  return response;
}

static ApexSettingsResponse *
apex_application_real_get_settings (ApexApplication  *self,
                                    GError          **error)
{
  ApexSettingsResponse *response;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);

  response = apex_settings_response_new ();

  return response;
}

static ApexJobResponse *
apex_application_real_get_job (ApexApplication  *self,
                               const gchar      *id,
                               GError          **error)
{
  ApexJobResponse *response;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);

  response = apex_job_response_new ();

  return response;
}

static ApexJobsResponse *
apex_application_real_get_jobs (ApexApplication  *self,
                                GError          **error)
{
  ApexJobsResponse *response;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);

  response = apex_jobs_response_new ();

  return response;
}

static ApexJobResponse *
apex_application_real_get_active_job (ApexApplication  *self,
                                      GError          **error)
{
  ApexJobResponse *response;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);

  response = apex_job_response_new ();

  return response;
}

static void
apex_application_startup (ApexApplication *self)
{
  GApplication *app;

  g_return_if_fail (APEX_IS_APPLICATION (self));

  app = G_APPLICATION (self);

  g_application_hold (app);
  g_debug ("startup");
  g_application_release (app);
}

static void
apex_application_activate (ApexApplication *self)
{
  ApexApplicationPrivate *priv;
  GApplication *app;

  g_return_if_fail (APEX_IS_APPLICATION (self));

  priv = apex_application_get_instance_private (self);

  app = G_APPLICATION (self);

  g_application_hold (app);
  g_debug ("activate");

  /* Setup */
  s_add_actions (self);
  s_add_signals (self);

  /* Apply configuration from environment */
  s_process_env (self);

  /* Launch async tasks for message handling and job queue */
  if (priv->enable_handler)
    apex_handler_run (priv->handler, self, NULL);

  if (priv->enable_job_queue)
    apex_job_queue_run (priv->job_queue, NULL);

  if (priv->enable_standalone)
    s_run_standalone (self);

  g_debug ("finished activation");
  g_application_release (app);
}

static void
apex_application_shutdown (ApexApplication *self)
{
  ApexApplicationPrivate *priv;
  GApplication *app;
  GHashTableIter source_iter;
  GHashTableIter sink_iter;
  gpointer key, val;

  g_return_if_fail (APEX_IS_APPLICATION (self));

  priv = apex_application_get_instance_private (self);

  app = G_APPLICATION (self);

  g_debug ("shutdown");

  /* Clean up source sockets */
  if (priv->source_table)
    {
      g_debug ("cleaning up source sockets");
      g_hash_table_iter_init (&source_iter, priv->source_table);
      while (g_hash_table_iter_next (&source_iter, &key, &val))
        {
          g_debug ("stopping source: %s", (gchar *)key);
          apex_source_stop (APEX_SOURCE (val));
        }
    }

  /* Clean up sink sockets */
  if (priv->sink_table)
    {
      g_debug ("cleaning up sink sockets");
      g_hash_table_iter_init (&sink_iter, priv->sink_table);
      while (g_hash_table_iter_next (&sink_iter, &key, &val))
        {
          g_debug ("stopping sink: %s", (gchar *)key);
          apex_sink_stop (APEX_SINK (val));
        }
    }

  apex_job_queue_cancel (priv->job_queue);
  apex_handler_cancel (priv->handler);

  /* Wait for other things to finish, probably not necessary */
  g_usleep (250000);

  g_application_quit (app);
}

static void
s_handle_shutdown (ApexHandler     *handler,
                   ApexApplication *app)
{
  g_debug ("received shutdown event from handler");

  apex_application_shutdown (app);
}

/* early param check test start */

static gboolean
s_verbose_cb (const gchar  *option_name,
              const gchar  *value,
              gpointer      data,
              GError      **error)
{
  apex_log_increase_verbosity ();
  return TRUE;
}

static void
param_check (gint    *argc,
             gchar ***argv)
{
  g_autoptr(GOptionContext) context = NULL;
  GOptionEntry entries[] = {
    { "verbose", 'v', G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_CALLBACK, s_verbose_cb },
    { NULL }
  };

  context = g_option_context_new (NULL);
  g_option_context_set_ignore_unknown_options (context, TRUE);
  g_option_context_set_help_enabled (context, FALSE);
  g_option_context_add_main_entries (context, entries, NULL);
  g_option_context_parse (context, argc, argv, NULL);
}

/* early param check test stop */

/* TODO: remove this? */
static gboolean
apex_application_local_command_line (GApplication   *self,
                                     gchar        ***arguments,
                                     gint           *exit_status)
{
  gchar **argv;
  /*gint i, j;*/
  gint argc;

  argv = *arguments;

  /*i = 1;*/
  argc = 0;
  while (argv[argc])
  /*while (argv[i])*/
    {
/*
 *      if (g_str_has_prefix (argv[i], "--local-"))
 *        {
 *          g_debug ("handling argument %s locally", argv[i]);
 *
 *          if (g_str_has_prefix (argv[i], "--local-config"))
 *            {
 *              // TODO: check for --local-config= as well
 *              // TODO: check for null i+1
 *              g_debug ("Load config: %s", argv[i+1]);
 *            }
 *
 *          g_free (argv[i]);
 *          for (j = i; argv[j]; j++)
 *            argv[j] = argv[j + 1];
 *        }
 *      else
 *        {
 *          g_debug ("not handling argument %s locally", argv[i]);
 *          i++;
 *        }
 */

      argc++;
    }

  param_check (&argc, &argv);

  *exit_status = 0;

  return G_APPLICATION_CLASS (
      apex_application_parent_class)->local_command_line
        (G_APPLICATION (self), arguments, exit_status);
}

/* GObject setup {{{1 */
static void
apex_application_finalize (GObject *object)
{
  ApexApplication *self = (ApexApplication *)object;
  ApexApplicationPrivate *priv = apex_application_get_instance_private (self);

  g_debug ("application finalize");

  g_clear_pointer (&priv->id, g_free);
  g_clear_pointer (&priv->endpoint, g_free);
  g_clear_pointer (&priv->service, g_free);
  g_clear_pointer (&priv->broker_endpoint, g_free);
  g_clear_object (&priv->broker);
  g_clear_object (&priv->handler);
  g_clear_object (&priv->job_queue);

  if (priv->configuration)
    g_object_unref (priv->configuration);

  if (priv->property_table)
    g_hash_table_unref (priv->property_table);

  if (priv->sink_table)
    g_hash_table_unref (priv->sink_table);

  if (priv->source_table)
    g_hash_table_unref (priv->source_table);

  if (priv->shutdown_action)
    g_object_unref (priv->shutdown_action);

  G_OBJECT_CLASS (apex_application_parent_class)->finalize (object);
}

static void
apex_application_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  ApexApplicationPrivate *priv;
  ApexApplication *self = APEX_APPLICATION (object);

  priv = apex_application_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, apex_application_get_id (self));
      break;

    case PROP_ENDPOINT:
      g_value_set_string (value, apex_application_get_endpoint (self));
      break;

    case PROP_SERVICE:
      g_value_set_string (value, apex_application_get_service (self));
      break;

    case PROP_CONFIG:
      g_value_set_object (value, priv->configuration);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_application_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  ApexApplication *self = APEX_APPLICATION (object);

  switch (prop_id)
    {
    case PROP_ID:
      apex_application_set_id (self, g_value_get_string (value));
      break;

    case PROP_ENDPOINT:
      apex_application_set_endpoint (self, g_value_get_string (value));
      break;

    case PROP_SERVICE:
      apex_application_set_service (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_application_class_init (ApexApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_application_finalize;
  object_class->get_property = apex_application_get_property;
  object_class->set_property = apex_application_set_property;

  /*
   *G_APPLICATION_CLASS (klass)->activate = apex_application_activate;
   *G_APPLICATION_CLASS (klass)->startup = apex_application_startup;
   *G_APPLICATION_CLASS (klass)->shutdown = apex_application_shutdown;
   */
  G_APPLICATION_CLASS (klass)->local_command_line = apex_application_local_command_line;

  klass->get_configuration = apex_application_real_get_configuration;
  klass->get_status = apex_application_real_get_status;
  klass->get_settings = apex_application_real_get_settings;
  klass->get_job = apex_application_real_get_job;
  klass->get_jobs = apex_application_real_get_jobs;
  klass->get_active_job = apex_application_real_get_active_job;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Id",
                         "The id of the application.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_ENDPOINT] =
    g_param_spec_string ("endpoint",
                         "Endpoint",
                         "The message queue endpoint.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_SERVICE] =
    g_param_spec_string ("service",
                         "Service",
                         "The message queue service name.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_CONFIG] =
    g_param_spec_object ("config",
                         "Config",
                         "The configuration in the response.",
                         APEX_TYPE_CONFIGURATION,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  /**
   * ApexApplication::property_changed:
   * @self: the application
   *
   * The ::property_changed signal is emitted on the primary instance
   * after a stored property has been updated.
   */
  signals [SIGNAL_PROPERTY_CHANGED] =
    g_signal_new ("property-changed", APEX_TYPE_APPLICATION, G_SIGNAL_RUN_FIRST,
                  G_STRUCT_OFFSET (ApexApplicationClass, property_changed),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 1, APEX_TYPE_PROPERTY);
}

static void
apex_application_init (ApexApplication *self)
{
  ApexApplicationPrivate *priv;

  priv = apex_application_get_instance_private (self);

  priv->id = g_strdup ("org.apex.Module");
  priv->endpoint = g_strdup ("tcp://localhost:5555");
  priv->service = g_strdup ("module");

  priv->property_table = g_hash_table_new_full (g_str_hash,
                                                g_str_equal,
                                                g_free,
                                                NULL);

  priv->sink_table = g_hash_table_new_full (g_str_hash,
                                            g_str_equal,
                                            g_free,
                                            NULL);

  priv->source_table = g_hash_table_new_full (g_str_hash,
                                              g_str_equal,
                                              g_free,
                                              NULL);

  priv->job_queue = apex_job_queue_new ();
  priv->handler = APEX_HANDLER (apex_mdp_handler_new ());

  g_signal_connect (self, "activate", G_CALLBACK (apex_application_activate), NULL);
  g_signal_connect (self, "startup", G_CALLBACK (apex_application_startup), NULL);
  g_signal_connect (self, "shutdown", G_CALLBACK (apex_application_shutdown), NULL);

  g_signal_connect (APEX_HANDLER (priv->handler),
                    "shutdown-requested",
                    G_CALLBACK (s_handle_shutdown),
                    self);
}

/**
 * apex_application_new:
 * @application_id: (allow-none): The application ID.
 * @flags: the application flags
 *
 * Creates a new #ApexApplication instance.
 *
 * If non-%NULL, the application ID must be valid.  See
 * g_application_id_is_valid().
 *
 * If no application ID is given then some features (most notably application
 * uniqueness) will be disabled.
 *
 * Returns: a new #ApexApplication instance
 */
ApexApplication *
apex_application_new (const gchar       *application_id,
                      GApplicationFlags  flags)
{
  g_return_val_if_fail (application_id == NULL ||
                        g_application_id_is_valid (application_id), NULL);

  return g_object_new (APEX_TYPE_APPLICATION,
                       "application-id", application_id,
                       "flags", flags | G_APPLICATION_HANDLES_COMMAND_LINE,
                       NULL);
}

/**
 * apex_application_get_configuration:
 * @self: an #ApexApplication
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
ApexConfigurationResponse *
apex_application_get_configuration (ApexApplication  *self,
                                    GError          **error)
{
  ApexApplicationClass *klass;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = APEX_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->get_configuration != NULL, NULL);

  return klass->get_configuration (self, error);
}

/**
 * apex_application_get_status:
 * @self: an #ApexApplication
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
ApexStatusResponse *
apex_application_get_status (ApexApplication  *self,
                             GError          **error)
{
  ApexApplicationClass *klass;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = APEX_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->get_status != NULL, NULL);

  return klass->get_status (self, error);
}

/**
 * apex_application_get_settings:
 * @self: an #ApexApplication
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
ApexSettingsResponse *
apex_application_get_settings (ApexApplication  *self,
                               GError          **error)
{
  ApexApplicationClass *klass;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = APEX_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->get_settings != NULL, NULL);

  return klass->get_settings (self, error);
}

/**
 * apex_application_get_job:
 * @self: an #ApexApplication
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
ApexJobResponse *
apex_application_get_job (ApexApplication  *self,
                          const gchar      *job_id,
                          GError          **error)
{
  ApexApplicationClass *klass;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = APEX_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->get_job != NULL, NULL);

  return klass->get_job (self, job_id, error);
}

/**
 * apex_application_get_jobs:
 * @self: an #ApexApplication
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
ApexJobsResponse *
apex_application_get_jobs (ApexApplication  *self,
                           GError          **error)
{
  ApexApplicationClass *klass;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = APEX_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->get_jobs != NULL, NULL);

  return klass->get_jobs (self, error);
}

/**
 * apex_application_get_active_job:
 * @self: an #ApexApplication
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
ApexJobResponse *
apex_application_get_active_job (ApexApplication  *self,
                                 GError          **error)
{
  ApexApplicationClass *klass;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = APEX_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->get_active_job != NULL, NULL);

  return klass->get_active_job (self, error);
}

/**
 * apex_application_cancel_job:
 * @self: an #ApexApplication
 * @job_id: ID of the job to cancel
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
ApexJobResponse *
apex_application_cancel_job (ApexApplication   *self,
                             const gchar       *job_id,
                             GError           **error)
{
  ApexApplicationClass *klass;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = APEX_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->cancel_job != NULL, NULL);

  return klass->cancel_job (self, job_id, error);
}

/**
 * apex_application_submit_job:
 * @self: an #ApexApplication
 * @job_id: incorrectly named param for the job to execute
 * @job_value: additional data to go with the job
 * @job_properties: (element-type utf8 utf8) (transfer none): A #GHashTable of properties
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
ApexJobResponse *
apex_application_submit_job (ApexApplication  *self,
                             const gchar      *job_id,
                             const gchar      *job_value,
                             GHashTable       *job_properties,
                             GError          **error)
{
  ApexApplicationClass *klass;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = APEX_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->submit_job != NULL, NULL);

  return klass->submit_job (self, job_id, job_value, job_properties, error);
}

/**
 * apex_application_submit_event:
 * @self: an #ApexApplication
 * @event_id: ID of the event to submit
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
ApexJobResponse *
apex_application_submit_event (ApexApplication  *self,
                               const gint        event_id,
                               GError          **error)
{
  ApexApplicationClass *klass;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = APEX_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->submit_event != NULL, NULL);

  return klass->submit_event (self, event_id, error);
}

/**
 * apex_application_available_events:
 * @self: an #ApexApplication
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
ApexEventResponse *
apex_application_available_events (ApexApplication  *self,
                                   GError          **error)
{
  ApexApplicationClass *klass;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = APEX_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->available_events != NULL, NULL);

  return klass->available_events (self, error);
}

void apex_application_load_config (ApexApplication  *self,
                                   const gchar      *config,
                                   GError          **error)
{
  ApexApplicationPrivate *priv;

  g_return_if_fail (APEX_IS_APPLICATION (self));

  priv = apex_application_get_instance_private (self);

  if (priv->configuration)
    g_object_unref (priv->configuration);

  priv->configuration = apex_configuration_new ();
  apex_configuration_load (priv->configuration, config, error);
}

/**
 * apex_application_get_loaded_config:
 * @self: #ApexApplication instance
 *
 * Returns: (transfer none): The #ApexConfiguration
 */
ApexConfiguration *
apex_application_get_loaded_config (ApexApplication *self)
{
  ApexConfiguration *configuration;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);

  g_object_get (self, "config", &configuration, NULL);

  return configuration;
}

/**
 * apex_application_add_property:
 * @self: an #ApexApplication
 * @property: and #ApexProperty
 *
 * Add or replace a property of @self.
 */
void
apex_application_add_property (ApexApplication *self,
                               ApexProperty    *property)
{
  ApexApplicationPrivate *priv;
  const gchar *name;

  g_return_if_fail (APEX_IS_APPLICATION (self));
  g_return_if_fail (APEX_IS_PROPERTY (property));

  priv = apex_application_get_instance_private (self);

  if (priv->property_table == NULL)
    priv->property_table = g_hash_table_new_full (g_str_hash,
                                                  g_str_equal,
                                                  g_free,
                                                  NULL);

  name = apex_property_get_key (property);

  if (g_hash_table_contains (priv->property_table, name))
    g_hash_table_remove (priv->property_table, name);

  g_hash_table_insert (priv->property_table, g_strdup (name), property);
}

void
apex_application_remove_property (ApexApplication *self,
                                  const gchar     *key)
{
  ApexApplicationPrivate *priv;

  g_return_if_fail (APEX_IS_APPLICATION (self));

  priv = apex_application_get_instance_private (self);

  g_return_if_fail (priv->property_table != NULL);

  if (g_hash_table_contains (priv->property_table, key))
    g_hash_table_remove (priv->property_table, key);
}

/**
 * apex_application_lookup_property:
 * @self: an #ApexApplication
 * @key: key of the property to lookup
 *
 * Retrieve an #ApexProperty by key.
 *
 * Returns: (transfer none): an #ApexProperty if one is available with @key.
 */
ApexProperty *
apex_application_lookup_property (ApexApplication *self,
                                  const gchar     *key)
{
  ApexApplicationPrivate *priv;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);

  priv = apex_application_get_instance_private (self);

  g_return_val_if_fail (priv->property_table != NULL, NULL);

  if (g_hash_table_contains (priv->property_table, key))
    return g_hash_table_lookup (priv->property_table, key);

  return NULL;
}

gboolean
apex_application_update_property (ApexApplication *self,
                                  const gchar     *key,
                                  const gchar     *value)
{
  ApexApplicationPrivate *priv;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), FALSE);

  priv = apex_application_get_instance_private (self);

  g_return_val_if_fail (priv->property_table != NULL, FALSE);

  if (g_hash_table_contains (priv->property_table, key))
    {
      ApexProperty *prop;

      prop = g_hash_table_lookup (priv->property_table, key);
      apex_property_set_value (prop, value);

      g_signal_emit (self, signals [SIGNAL_PROPERTY_CHANGED], 0, prop);

      return TRUE;
    }

  return FALSE;
}

gboolean
apex_application_has_property (ApexApplication *self,
                               const gchar     *key)
{
  ApexApplicationPrivate *priv;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), FALSE);

  priv = apex_application_get_instance_private (self);

  g_return_val_if_fail (priv->property_table != NULL, FALSE);

  return g_hash_table_contains (priv->property_table, key);
}

/**
 * apex_application_add_sink:
 * @self: an #ApexApplication
 * @name: the name of the sink
 * @sink: and #ApexSink
 *
 * Add a message sink (subscriber) to @self.
 */
void
apex_application_add_sink (ApexApplication *self,
                           const gchar     *name,
                           ApexSink        *sink)
{
  ApexApplicationPrivate *priv;

  g_return_if_fail (APEX_IS_APPLICATION (self));

  priv = apex_application_get_instance_private (self);

  if (priv->sink_table == NULL)
    priv->sink_table = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);

  // TODO: review this, seems a little unnecessary
  if (g_hash_table_contains (priv->sink_table, name))
    {
      g_autoptr (ApexSink) s = NULL;
      s = g_hash_table_lookup (priv->sink_table, name);
      apex_sink_stop (s);
      g_hash_table_remove (priv->sink_table, name);
    }

  g_hash_table_insert (priv->sink_table, g_strdup (name), sink);
}

/**
 * apex_application_remove_sink:
 * @self: an #ApexApplication
 * @name: the name of the sink
 *
 * Remove a message sink (subscriber) from @self.
 */
void
apex_application_remove_sink (ApexApplication *self,
                              const gchar     *name)
{
  ApexApplicationPrivate *priv;

  g_return_if_fail (APEX_IS_APPLICATION (self));

  priv = apex_application_get_instance_private (self);

  g_return_if_fail (priv->sink_table != NULL);

  if (g_hash_table_contains (priv->sink_table, name))
    {
      g_autoptr (ApexSink) sink = NULL;
      sink = g_hash_table_lookup (priv->sink_table, name);
      apex_sink_stop (sink);
      g_hash_table_remove (priv->sink_table, name);
    }
}

/**
 * apex_application_get_sink:
 * @self: an #ApexApplication
 * @name: name of the sink
 *
 * Retrieve an #ApexSink by name.
 *
 * Returns: (transfer none): an #ApexSink if one is available with @name.
 */
ApexSink *
apex_application_get_sink (ApexApplication *self,
                           const gchar     *name)
{
  ApexApplicationPrivate *priv;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);

  priv = apex_application_get_instance_private (self);

  g_return_val_if_fail (priv->sink_table != NULL, NULL);

  if (g_hash_table_contains (priv->sink_table, name))
    return g_hash_table_lookup (priv->sink_table, name);

  return NULL;
}

gboolean
apex_application_has_sink (ApexApplication *self,
                           const gchar     *name)
{
  ApexApplicationPrivate *priv;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), FALSE);

  priv = apex_application_get_instance_private (self);

  g_return_val_if_fail (priv->sink_table != NULL, FALSE);

  return g_hash_table_contains (priv->sink_table, name);
}

/**
 * apex_application_add_source:
 * @self: an #ApexApplication
 * @name: the name of the source
 * @source: and #ApexSource
 *
 * Add a message source (publisher) to @self.
 */
void
apex_application_add_source (ApexApplication *self,
                             const gchar     *name,
                             ApexSource      *source)
{
  ApexApplicationPrivate *priv;

  g_return_if_fail (APEX_IS_APPLICATION (self));

  priv = apex_application_get_instance_private (self);

  if (priv->source_table == NULL)
    priv->source_table = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);

  // TODO: review this, seems a little unnecessary
  if (g_hash_table_contains (priv->source_table, name))
    {
      g_autoptr (ApexSource) s = NULL;
      s = g_hash_table_lookup (priv->source_table, name);
      apex_source_stop (s);
      g_hash_table_remove (priv->source_table, name);
    }

  g_hash_table_insert (priv->source_table, g_strdup (name), source);
}

/**
 * apex_application_remove_source:
 * @self: an #ApexApplication
 * @name: the name of the source
 *
 * Remove a message source (publisher) from @self.
 */
void
apex_application_remove_source (ApexApplication *self,
                                const gchar     *name)
{
  ApexApplicationPrivate *priv;

  g_return_if_fail (APEX_IS_APPLICATION (self));

  priv = apex_application_get_instance_private (self);

  g_return_if_fail (priv->source_table != NULL);

  if (g_hash_table_contains (priv->source_table, name))
    {
      g_autoptr (ApexSource) source = NULL;
      source = g_hash_table_lookup (priv->source_table, name);
      apex_source_stop (source);
      g_hash_table_remove (priv->source_table, name);
    }
}

/**
 * apex_application_get_source:
 * @self: an #ApexApplication
 * @name: name of the source
 *
 * Retrieve an #ApexSource by name.
 *
 * Returns: (transfer none): an #ApexSource if one is available with @name.
 */
ApexSource *
apex_application_get_source (ApexApplication *self,
                             const gchar     *name)
{
  ApexApplicationPrivate *priv;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);

  priv = apex_application_get_instance_private (self);

  g_return_val_if_fail (priv->source_table != NULL, NULL);

  if (g_hash_table_contains (priv->source_table, name))
    return g_hash_table_lookup (priv->source_table, name);

  return NULL;
}

gboolean
apex_application_has_source (ApexApplication *self,
                             const gchar     *name)
{
  ApexApplicationPrivate *priv;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), FALSE);

  priv = apex_application_get_instance_private (self);

  g_return_val_if_fail (priv->source_table != NULL, FALSE);

  return g_hash_table_contains (priv->source_table, name);
}

/**
 * apex_application_send_event:
 * @self: an #ApexApplication
 * @event: an #ApexEvent
 * @error: return location for a GError, or NULL
 *
 * Send an event message, assumes an existing #ApexSource named 'event'.
 */
void
apex_application_send_event (ApexApplication  *self,
                             ApexEvent        *event,
                             GError          **error)
{
  ApexApplicationPrivate *priv;
  ApexSource *source;
  g_autofree gchar *msg = NULL;

  g_return_if_fail (APEX_IS_APPLICATION (self));
  g_return_if_fail (APEX_IS_EVENT (event));
  g_return_if_fail (error == NULL || *error == NULL);

  priv = apex_application_get_instance_private (self);

  if (priv->source_table == NULL)
    {
      g_set_error (error,
                   APEX_ERROR,
                   APEX_ERROR_NOT_CONNECTED,
                   "The application does not contain any producer connections.");
      return;
    }
  else if (!apex_application_has_source (self, "event"))
    {
      g_set_error (error,
                   APEX_ERROR,
                   APEX_ERROR_NOT_CONNECTED,
                   "The application does not contain an event bus connection.");
      return;
    }

  source = g_hash_table_lookup (priv->source_table, "event");
  msg = apex_event_serialize (event);
  apex_source_queue_message (source, msg);
}

/**
 * apex_application_send_metric:
 * @self: an #ApexApplication
 * @metric: an #ApexMetric
 * @error: return location for a GError, or NULL
 *
 * Send a metric message, assumes an existing #ApexSource named 'metric'.
 */
void
apex_application_send_metric (ApexApplication  *self,
                              ApexMetric       *metric,
                              GError          **error)
{
  ApexApplicationPrivate *priv;
  ApexSource *source;
  g_autofree gchar *msg = NULL;

  g_return_if_fail (APEX_IS_APPLICATION (self));
  g_return_if_fail (APEX_IS_METRIC (metric));
  g_return_if_fail (error == NULL || *error == NULL);

  priv = apex_application_get_instance_private (self);

  if (priv->source_table == NULL)
    {
      g_set_error (error,
                   APEX_ERROR,
                   APEX_ERROR_NOT_CONNECTED,
                   "The application does not contain any producer connections.");
      return;
    }
  else if (!apex_application_has_source (self, "metric"))
    {
      g_set_error (error,
                   APEX_ERROR,
                   APEX_ERROR_NOT_CONNECTED,
                   "The application does not contain a metric bus connection.");
      return;
    }

  source = g_hash_table_lookup (priv->source_table, "metric");
  msg = apex_metric_serialize (metric);
  apex_source_queue_message (source, msg);
}

/**
 * apex_application_enqueue_job:
 * @self: an #ApexApplication
 * @job: an #ApexJob
 *
 * Add a job to the internal job queue.
 */
void
apex_application_enqueue_job (ApexApplication *self,
                              ApexJob         *job)
{
  ApexApplicationPrivate *priv;

  g_return_if_fail (APEX_IS_APPLICATION (self));

  priv = apex_application_get_instance_private (self);

  apex_job_queue_enqueue (priv->job_queue, job);
}

/**
 * apex_application_register_job:
 * @self: an #ApexApplication
 * @job: an #ApexJob
 *
 * Register a job with the application enabling it to be launched by name.
 * TODO: don't use this
 */
void
apex_application_register_job (ApexApplication *self,
                               ApexJob         *job)
{
  ApexApplicationPrivate *priv;

  g_return_if_fail (APEX_IS_APPLICATION (self));

  priv = apex_application_get_instance_private (self);

  // TODO: why are you even putting this here already?

  //g_hash_table_insert (priv->registered_jobs, name, job);
}

/**
 * apex_application_get_id:
 * @self: an #ApexApplication
 *
 * Gets the application id of @self.
 *
 * Returns: (nullable): the application id, if one is set.
 */
const gchar *
apex_application_get_id (ApexApplication *self)
{
  ApexApplicationPrivate *priv;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);

  priv = apex_application_get_instance_private (self);
  return priv->id;
}

/**
 * apex_application_set_id:
 * @self: an #ApexApplication
 * @id: (nullable): the application id to use
 *
 * Sets (or unsets) the application id of @self.
 */
void
apex_application_set_id (ApexApplication *self,
                         const gchar     *id)
{
  ApexApplicationPrivate *priv;

  g_return_if_fail (APEX_IS_APPLICATION (self));
  g_return_if_fail (id != NULL);

  priv = apex_application_get_instance_private (self);

  if (g_strcmp0 (id, priv->id) != 0)
    {
      g_free (priv->id);
      priv->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}

/**
 * apex_application_get_endpoint:
 * @self: an #ApexApplication
 *
 * Gets the application endpoint of @self.
 *
 * Returns: (nullable): the application endpoint, if one is set.
 */
const gchar *
apex_application_get_endpoint (ApexApplication *self)
{
  ApexApplicationPrivate *priv;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);

  priv = apex_application_get_instance_private (self);
  return priv->endpoint;
}

/**
 * apex_application_set_endpoint:
 * @self: an #ApexApplication
 * @endpoint: (nullable): the broker endpoint to connect to
 *
 * Sets (or unsets) the endpoint of @self.
 */
void
apex_application_set_endpoint (ApexApplication *self,
                               const gchar     *endpoint)
{
  ApexApplicationPrivate *priv;

  g_return_if_fail (APEX_IS_APPLICATION (self));
  g_return_if_fail (endpoint != NULL);

  priv = apex_application_get_instance_private (self);

  if (g_strcmp0 (endpoint, priv->endpoint) != 0)
    {
      g_free (priv->endpoint);
      priv->endpoint = g_strdup (endpoint);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENDPOINT]);
    }
}

/**
 * apex_application_get_service:
 * @self: an #ApexApplication
 *
 * Gets the application service of @self.
 *
 * Returns: (nullable): the application service, if one is set.
 */
const gchar *
apex_application_get_service (ApexApplication *self)
{
  ApexApplicationPrivate *priv;

  g_return_val_if_fail (APEX_IS_APPLICATION (self), NULL);

  priv = apex_application_get_instance_private (self);
  return priv->service;
}

/**
 * apex_application_set_service:
 * @self: an #ApexApplication
 * @service: (nullable): the application service to use
 *
 * Sets (or unsets) the application service to @self to register with the broker.
 */
void
apex_application_set_service (ApexApplication  *self,
                              const gchar      *service)
{
  ApexApplicationPrivate *priv;

  g_return_if_fail (APEX_IS_APPLICATION (self));
  g_return_if_fail (service != NULL);

  priv = apex_application_get_instance_private (self);

  if (g_strcmp0 (service, priv->service) != 0)
    {
      g_free (priv->service);
      priv->service = g_strdup (service);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SERVICE]);
    }
}
