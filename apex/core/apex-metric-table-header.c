/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-metric-table-header.h"
#include "apex-table.h"

/*
 * ApexMetricTableHeader:
 *
 * Represents a ...
 */
struct _ApexMetricTableHeader
{
  GObject     parent;
  gchar      *name;
  ApexTable  *columns;
};

enum {
  PROP_0,
  PROP_NAME,
  PROP_COLUMNS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (ApexMetricTableHeader, apex_metric_table_header, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
apex_metric_table_header_serialize_property (JsonSerializable *serializable,
                                             const gchar      *name,
                                             const GValue     *value,
                                             GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "columns") == 0)
    {
      g_return_val_if_fail (value != NULL, retval);
      g_return_val_if_fail (G_VALUE_HOLDS_OBJECT (value), retval);

      retval = apex_table_serialize (APEX_TABLE (g_value_get_object (value)), JSON_TYPE_OBJECT);
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
apex_metric_table_header_deserialize_property (JsonSerializable *serializable,
                                               const gchar      *name,
                                               GValue           *value,
                                               GParamSpec       *pspec,
                                               JsonNode         *property_node)
{
  gboolean retval = FALSE;

  if (g_strcmp0 (name, "columns") == 0)
    {
      /*g_autoptr (ApexTable) columns = NULL;*/
      ApexTable *columns;

      columns = apex_table_new (G_TYPE_DOUBLE);
      apex_table_deserialize (columns, property_node);

      /*g_value_take_object (value, g_object_ref (columns));*/
      g_value_take_object (value, columns);

      retval = TRUE;

      /*g_clear_object (&columns);*/
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = apex_metric_table_header_serialize_property;
  iface->deserialize_property = apex_metric_table_header_deserialize_property;
}

static void
apex_metric_table_header_finalize (GObject *object)
{
  ApexMetricTableHeader *self = (ApexMetricTableHeader *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_object (&self->columns);

  G_OBJECT_CLASS (apex_metric_table_header_parent_class)->finalize (object);
}

static void
apex_metric_table_header_get_property (GObject    *object,
                                       guint       prop_id,
                                       GValue     *value,
                                       GParamSpec *pspec)
{
  ApexMetricTableHeader *self = APEX_METRIC_TABLE_HEADER (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_take_string (value, apex_metric_table_header_dup_name (self));
      break;

    case PROP_COLUMNS:
      g_value_take_object (value, apex_metric_table_header_ref_columns (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_metric_table_header_set_property (GObject      *object,
                                       guint         prop_id,
                                       const GValue *value,
                                       GParamSpec   *pspec)
{
  ApexMetricTableHeader *self = APEX_METRIC_TABLE_HEADER (object);

  switch (prop_id)
    {
    case PROP_NAME:
      apex_metric_table_header_set_name (self, g_value_get_string (value));
      break;

    case PROP_COLUMNS:
      apex_metric_table_header_set_columns (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_metric_table_header_class_init (ApexMetricTableHeaderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_metric_table_header_finalize;
  object_class->get_property = apex_metric_table_header_get_property;
  object_class->set_property = apex_metric_table_header_set_property;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the table header",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_COLUMNS] =
    g_param_spec_object ("columns",
                         "Columns",
                         "List of metric table header columns",
                         APEX_TYPE_TABLE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_metric_table_header_init (ApexMetricTableHeader *self)
{
  self->columns = apex_table_new (G_TYPE_DOUBLE);
}

ApexMetricTableHeader *
apex_metric_table_header_new (const gchar *name)
{
  return g_object_new (APEX_TYPE_METRIC_TABLE_HEADER,
                       "name", name,
                       NULL);
}

const gchar *
apex_metric_table_header_serialize (ApexMetricTableHeader *self)
{
  g_return_val_if_fail (APEX_IS_METRIC_TABLE_HEADER (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
apex_metric_table_header_deserialize (ApexMetricTableHeader *self,
                                      const gchar           *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (ApexTable) columns = NULL;
  g_autofree gchar *name = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (APEX_TYPE_METRIC_TABLE_HEADER,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_METRIC_TABLE_HEADER (object));

  name = apex_metric_table_header_dup_name (APEX_METRIC_TABLE_HEADER (object));
  columns = apex_metric_table_header_ref_columns (APEX_METRIC_TABLE_HEADER (object));

  apex_metric_table_header_set_name (self, name);
  apex_metric_table_header_set_columns (self, columns);

  g_clear_object (&object);
}

const gchar *
apex_metric_table_header_get_name (ApexMetricTableHeader *self)
{
  g_return_val_if_fail (APEX_IS_METRIC_TABLE_HEADER (self), NULL);

  return self->name;
}

/**
 * apex_metric_table_header_dup_name:
 *
 * Copies the name of the metric table header and returns it to the caller
 * (after locking the object). A copy is used to avoid thread-races.
 */
gchar *
apex_metric_table_header_dup_name (ApexMetricTableHeader *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_METRIC_TABLE_HEADER (self), NULL);

  /*apex_object_lock (APEX_OBJECT (self));*/
  ret = g_strdup (self->name);
  /*apex_object_unlock (APEX_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
apex_metric_table_header_set_name (ApexMetricTableHeader *self,
                                   const gchar           *name)
{
  g_return_if_fail (APEX_IS_METRIC_TABLE_HEADER (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

/**
 * apex_metric_table_header_get_columns:
 * @self: an #ApexMetricTableHeader
 *
 * Retrieve the #ApexTable containing header calibration columns.
 *
 * Returns: (transfer none): a #ApexTable of calibration columns if one is set.
 */
ApexTable *
apex_metric_table_header_get_columns (ApexMetricTableHeader *self)
{
  ApexTable *columns;

  g_return_val_if_fail (APEX_IS_METRIC_TABLE_HEADER (self), NULL);

  g_object_get (self, "columns", &columns, NULL);

  return columns;
}

/**
 * apex_metric_table_header_ref_columns:
 *
 * Gets the header columns, and returns a new reference
 * to the #ApexTable.
 *
 * Returns: (transfer full) (nullable): a #ApexTable or %NULL
 */
ApexTable *
apex_metric_table_header_ref_columns (ApexMetricTableHeader *self)
{
  ApexTable *ret = NULL;

  g_return_val_if_fail (APEX_IS_METRIC_TABLE_HEADER (self), NULL);

  g_set_object (&ret, self->columns);

  return g_steal_pointer (&ret);
}

void
apex_metric_table_header_set_columns (ApexMetricTableHeader *self,
                                      ApexTable             *columns)
{
  g_return_if_fail (APEX_IS_METRIC_TABLE_HEADER (self));
  g_return_if_fail (APEX_IS_TABLE (self->columns));

  if (g_set_object (&self->columns, columns))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_COLUMNS]);
}

gboolean
apex_metric_table_header_add_column (ApexMetricTableHeader *self,
                                     const gchar           *key,
                                     gdouble                value)
{
  GValue val = G_VALUE_INIT;

  g_return_val_if_fail (APEX_IS_METRIC_TABLE_HEADER (self), FALSE);

  g_value_init (&val, G_TYPE_DOUBLE);
  g_value_set_double (&val, value);

  apex_table_add (self->columns, key, &val);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_COLUMNS]);

  return TRUE;
}

gboolean
apex_metric_table_header_remove_column (ApexMetricTableHeader *self,
                                        const gchar           *key)
{
  g_return_val_if_fail (APEX_IS_METRIC_TABLE_HEADER (self), FALSE);
  g_return_val_if_fail (self->columns != NULL, FALSE);

  apex_table_remove (self->columns, key);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_COLUMNS]);

  return TRUE;
}

gdouble
apex_metric_table_header_get_column (ApexMetricTableHeader *self,
                                     const gchar           *key)
{
  g_return_val_if_fail (APEX_IS_METRIC_TABLE_HEADER (self), G_MINDOUBLE);
  g_return_val_if_fail (self->columns != NULL, G_MINDOUBLE);

  return g_value_get_double (apex_table_get (self->columns, key));
}

gboolean
apex_metric_table_header_has_column (ApexMetricTableHeader *self,
                                     const gchar           *key)
{
  gboolean ret;

  g_return_val_if_fail (APEX_IS_METRIC_TABLE_HEADER (self), FALSE);
  g_return_val_if_fail (self->columns != NULL, FALSE);
  g_return_val_if_fail (key != NULL, FALSE);

  ret = apex_table_has (self->columns, key);

  return ret;
}
