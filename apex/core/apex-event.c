/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-event.h"

struct _ApexEvent
{
  GObject  parent;
  gint     id;
  gchar   *name;
  gchar   *description;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_NAME,
  PROP_DESCRIPTION,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexEvent, apex_event, G_TYPE_OBJECT)

static void
apex_event_finalize (GObject *object)
{
  ApexEvent *self = (ApexEvent *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->description, g_free);

  G_OBJECT_CLASS (apex_event_parent_class)->finalize (object);
}

static void
apex_event_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  ApexEvent *self = APEX_EVENT (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_int (value, apex_event_get_id (self));
      break;

    case PROP_NAME:
      g_value_take_string (value, apex_event_dup_name (self));
      break;

    case PROP_DESCRIPTION:
      g_value_take_string (value, apex_event_dup_description (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_event_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  ApexEvent *self = APEX_EVENT (object);

  switch (prop_id)
    {
    case PROP_ID:
      apex_event_set_id (self, g_value_get_int (value));
      break;

    case PROP_NAME:
      apex_event_set_name (self, g_value_get_string (value));
      break;

    case PROP_DESCRIPTION:
      apex_event_set_description (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_event_class_init (ApexEventClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_event_finalize;
  object_class->get_property = apex_event_get_property;
  object_class->set_property = apex_event_set_property;

  properties [PROP_ID] =
    g_param_spec_int ("id",
                      "ID",
                      "The ID of the event.",
                      G_MININT,
                      G_MAXINT,
                      0,
                      (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the event.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_DESCRIPTION] =
    g_param_spec_string ("description",
                         "Description",
                         "The event description",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_event_init (ApexEvent *self)
{
}

ApexEvent *
apex_event_new (void)
{
  return g_object_new (APEX_TYPE_EVENT, NULL);
}

ApexEvent *
apex_event_new_full (gint         id,
                     const gchar *name,
                     const gchar *description)
{
  return g_object_new (APEX_TYPE_EVENT,
                       "id", id,
                       "name", name,
                       "description", description,
                       NULL);
}

gchar *
apex_event_serialize (ApexEvent *self)
{
  g_return_val_if_fail (APEX_IS_EVENT (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
apex_event_deserialize (ApexEvent   *self,
                        const gchar *data)
{
  g_autoptr (GObject) object = NULL;
  g_autofree gchar *name = NULL;
  g_autofree gchar *description = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (APEX_TYPE_EVENT,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_EVENT (object));

  name = apex_event_dup_name (APEX_EVENT (object));
  description = apex_event_dup_description (APEX_EVENT (object));

  apex_event_set_id (self, apex_event_get_id (APEX_EVENT (object)));
  apex_event_set_name (self, name);
  apex_event_set_description (self, description);

  g_clear_object (&object);
}

gint
apex_event_get_id (ApexEvent *self)
{
  // TODO: define a no-event event for this case
  g_return_val_if_fail (APEX_IS_EVENT (self), -1);

  return self->id;
}

void
apex_event_set_id (ApexEvent *self,
                   gint       id)
{
  g_return_if_fail (APEX_IS_EVENT (self));

  self->id = id;
}

const gchar *
apex_event_get_name (ApexEvent *self)
{
  g_return_val_if_fail (APEX_IS_EVENT (self), NULL);

  return self->name;
}

/**
 * apex_event_dup_name:
 *
 * Copies the name of the event and returns it to the caller (after
 * locking the object). A copy is used to avoid thread-races.
 */
gchar *
apex_event_dup_name (ApexEvent *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_EVENT (self), NULL);

  /*apex_object_lock (APEX_OBJECT (self));*/
  ret = g_strdup (self->name);
  /*apex_object_unlock (APEX_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
apex_event_set_name (ApexEvent   *self,
                     const gchar *name)
{
  g_return_if_fail (APEX_IS_EVENT (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

const gchar *
apex_event_get_description (ApexEvent *self)
{
  g_return_val_if_fail (APEX_IS_EVENT (self), NULL);

  return self->description;
}

/**
 * apex_event_dup_description:
 *
 * Copies the description of the event and returns it to the caller (after
 * locking the object). A copy is used to avoid thread-races.
 */
gchar *
apex_event_dup_description (ApexEvent *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_EVENT (self), NULL);

  /*apex_object_lock (APEX_OBJECT (self));*/
  ret = g_strdup (self->description);
  /*apex_object_unlock (APEX_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
apex_event_set_description (ApexEvent   *self,
                            const gchar *description)
{
  g_return_if_fail (APEX_IS_EVENT (self));

  if (g_strcmp0 (description, self->description) != 0)
    {
      g_free (self->description);
      self->description = g_strdup (description);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DESCRIPTION]);
    }
}
