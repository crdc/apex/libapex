/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_MODEL apex_model_get_type ()
G_DECLARE_DERIVABLE_TYPE (ApexModel, apex_model, APEX, MODEL, GObject)

struct _ApexModelClass
{
  /*< private >*/
  GObjectClass parent_class;

  /*< public >*/
  /* signals */
  void (*property_changed) (ApexModel   *self,
                            const gchar *key,
                            GValue      *value);

  /*< private >*/
  gpointer padding[12];
};

ApexModel   *apex_model_new             (void);

gchar       *apex_model_serialize       (ApexModel    *self);
void         apex_model_deserialize     (ApexModel    *self,
                                         const gchar  *data);

void         apex_model_add             (ApexModel    *self,
                                         gchar        *key,
                                         GValue       *value);
void         apex_model_remove          (ApexModel    *self,
                                         const gchar  *key);

// XXX: add properties?
void         apex_model_add_property    (ApexModel    *self,
                                         ApexProperty *property);
void         apex_model_remove_property (ApexModel    *self,
                                         ApexProperty *property);

GValue       apex_model_get             (ApexModel    *self,
                                         const gchar  *key);
void         apex_model_set             (ApexModel    *self,
                                         const gchar  *key,
                                         GValue       *value);
gint         apex_model_get_int         (ApexModel    *self,
                                         const gchar  *key);
void         apex_model_set_int         (ApexModel    *self,
                                         const gchar  *key,
                                         gint         value);
gdouble      apex_model_get_double      (ApexModel    *self,
                                         const gchar  *key);
void         apex_model_set_double      (ApexModel    *self,
                                         const gchar  *key,
                                         gdouble       value);
const gchar *apex_model_get_string      (ApexModel    *self,
                                         const gchar  *key);
void         apex_model_set_string      (ApexModel    *self,
                                         const gchar  *key,
                                         const gchar  *value);

G_END_DECLS
