/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_OBJECT apex_object_get_type ()
G_DECLARE_FINAL_TYPE (ApexObject, apex_object, APEX, OBJECT, GObject)

ApexObject   *apex_object_new             (const gchar *id);

void          apex_object_dump            (ApexObject  *self);

gchar        *apex_object_serialize       (ApexObject  *self);
void          apex_object_deserialize     (ApexObject  *self,
                                           const gchar *data);

const gchar  *apex_object_get_id          (ApexObject  *self);
void          apex_object_set_id          (ApexObject  *self,
                                           const gchar *id);

const gchar  *apex_object_get_name        (ApexObject  *self);
void          apex_object_set_name        (ApexObject  *self,
                                           const gchar *name);

void          apex_object_add_object      (ApexObject   *self,
                                           ApexObject   *object);
void          apex_object_remove_object   (ApexObject   *self,
                                           const gchar  *id);
ApexObject   *apex_object_lookup_object   (ApexObject   *self,
                                           const gchar  *id);
gboolean      apex_object_has_object      (ApexObject   *self,
                                           const gchar  *id);

void          apex_object_add_property    (ApexObject   *self,
                                           ApexProperty *property);
void          apex_object_remove_property (ApexObject   *self,
                                           const gchar  *key);
ApexProperty *apex_object_lookup_property (ApexObject   *self,
                                           const gchar  *key);
gboolean      apex_object_has_property    (ApexObject   *self,
                                           const gchar  *key);

ApexTable    *apex_object_get_objects     (ApexObject   *self);
ApexTable    *apex_object_ref_objects     (ApexObject   *self);
void          apex_object_set_objects     (ApexObject   *self,
                                           ApexTable    *objects);

ApexTable    *apex_object_get_properties  (ApexObject   *self);
ApexTable    *apex_object_ref_properties  (ApexObject   *self);
void          apex_object_set_properties  (ApexObject   *self,
                                           ApexTable    *properties);

GObject      *apex_gobject_from_data      (const gchar *data);

G_END_DECLS
