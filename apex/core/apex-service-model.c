/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-service.h"

struct _ApexService
{
  GObject          parent;
  gchar           *name;
  gchar           *description;
  gchar           *configuration_id;
  ApexStatus      *status;
  ApexState       *state;
};

enum {
  PROP_0,
  PROP_NAME,
  PROP_DESCRIPTION,
  PROP_CONFIGURATION_ID,
  /*PROP_STATUS,*/
  /*PROP_STATE,*/
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexService, apex_service, G_TYPE_OBJECT)

static void
apex_service_finalize (GObject *object)
{
  ApexService *self = (ApexService *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->description, g_free);
  g_clear_pointer (&self->configuration_id, g_free);

  G_OBJECT_CLASS (apex_service_parent_class)->finalize (object);
}

static void
apex_service_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  ApexService *self = APEX_SERVICE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, apex_service_get_name (self));
      break;

    case PROP_DESCRIPTION:
      g_value_set_string (value, apex_service_get_description (self));
      break;

    case PROP_CONFIGURATION_ID:
      g_value_set_string (value, apex_service_get_configuration_id (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_service_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  ApexService *self = APEX_SERVICE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      apex_service_set_name (self, g_value_get_string (value));
      break;

    case PROP_DESCRIPTION:
      apex_service_set_description (self, g_value_get_string (value));
      break;

    case PROP_CONFIGURATION_ID:
      apex_service_set_configuration_id (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_service_class_init (ApexServiceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_service_finalize;
  object_class->get_property = apex_service_get_property;
  object_class->set_property = apex_service_set_property;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the service.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_DESCRIPTION] =
    g_param_spec_string ("description",
                         "Description",
                         "The service description.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_CONFIGURATION_ID] =
    g_param_spec_string ("configuration-id",
                         "Configuration ID",
                         "The service configuration ID.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_service_init (ApexService *self)
{
}

ApexService *
apex_service_new (void)
{
  ApexService *object = g_object_new (APEX_TYPE_SERVICE, NULL);

  // TODO: add setup

  return object;
}

gchar *
apex_service_serialize (ApexService *self)
{
  g_return_val_if_fail (APEX_IS_SERVICE (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
apex_service_deserialize (ApexService *self,
                          const gchar *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_SERVICE,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
  {
    g_critical ("%s", err->message);
    g_error_free (err);
  }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_SERVICE (object));

  apex_service_set_name (self, apex_service_get_name (APEX_SERVICE (object)));
  apex_service_set_description (self, apex_service_get_description (APEX_SERVICE (object)));
  apex_service_set_configuration_id (self, apex_service_get_configuration_id (APEX_SERVICE (object)));
  // TODO: add boxed types

  g_object_unref (object);
}

const gchar *
apex_service_get_name (ApexService *self)
{
  g_return_val_if_fail (APEX_IS_SERVICE (self), NULL);

  return self->name;
}

void
apex_service_set_name (ApexService *self,
                       const gchar *name)
{
  g_return_if_fail (APEX_IS_SERVICE (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

const gchar *
apex_service_get_description (ApexService *self)
{
  g_return_val_if_fail (APEX_IS_SERVICE (self), NULL);

  return self->description;
}

void
apex_service_set_description (ApexService *self,
                              const gchar *description)
{
  g_return_if_fail (APEX_IS_SERVICE (self));

  if (g_strcmp0 (description, self->description) != 0)
    {
      g_free (self->description);
      self->description = g_strdup (description);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DESCRIPTION]);
    }
}

/**
 * apex_service_get_configuration_id:
 * @self: a #ApexService
 *
 * Gets the application configuration_id of @self.
 *
 * Returns: (nullable): the application configuration_id, if one is set.
 */
const gchar *
apex_service_get_configuration_id (ApexService *self)
{
  g_return_val_if_fail (APEX_IS_SERVICE (self), NULL);

  return self->configuration_id;
}

/**
 * apex_service_set_configuration_id:
 * @self: a #ApexService
 * @configuration_id: (nullable): the configuration ID the service uses
 *
 * Sets (or unsets) the configuration ID of @self.
 */
void
apex_service_set_configuration_id (ApexService *self,
                                   const gchar *configuration_id)
{
  g_return_if_fail (APEX_IS_SERVICE (self));

  if (g_strcmp0 (configuration_id, self->configuration_id) != 0)
    {
      g_free (self->configuration_id);
      self->configuration_id = g_strdup (configuration_id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CONFIGURATION_ID]);
    }
}

// XXX: see ApexModule for correct way to handle

/**
 * apex_service_get_status:
 * @self: a #ApexService
 *
 * Gets the application status of @self.
 *
 * Returns: (transfer none): (nullable): an #ApexStatus, if one is set.
 */
ApexStatus *
apex_service_get_status (ApexService *self)
{
  g_return_val_if_fail (APEX_IS_SERVICE (self), NULL);

  return self->status;
}

/**
 * apex_service_set_status:
 * @self: a #ApexService
 * @status: (nullable): the module status as an #ApexStatus
 *
 * Sets (or unsets) the status of @self.
 */
void
apex_service_set_status (ApexService *self,
                         ApexStatus  *status)
{
  g_return_if_fail (APEX_IS_SERVICE (self));

  self->status = status;
}

/**
 * apex_service_get_state:
 * @self: a #ApexService
 *
 * Gets the application state of @self.
 *
 * Returns: (transfer none): (nullable): an #ApexState, if one is set.
 */
ApexState *
apex_service_get_state (ApexService *self)
{
  g_return_val_if_fail (APEX_IS_SERVICE (self), NULL);

  return self->state;
}

/**
 * apex_service_set_state:
 * @self: a #ApexService
 * @state: (nullable): the service state as an #ApexState
 *
 * Sets (or unsets) the state of @self.
 */
void
apex_service_set_state (ApexService *self,
                        ApexState   *state)
{
  g_return_if_fail (APEX_IS_SERVICE (self));

  self->state = state;
}
