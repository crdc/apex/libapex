/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <apex-debug.h>

#include "apex-model.h"

/**
 * SECTION:apex-model
 * @short_description: Data model base class
 *
 * An #ApexModel is used as a base for containing property information.
 */

/**
 * ApexModel:
 *
 * #ApexModel is an opaque data structure and can only be accessed using the
 * following functions.
 */

/**
 * ApexModelClass:
 * @property_changed: invoked after a stored property has been changed
 *
 * Virtual function table for #ApexModel.
 */

// TODO: somehow need to maintain a data type table to allow mixed types

typedef struct
{
  /*< public >*/

  /*< private >*/
} ApexModelPrivate;

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

enum
{
  SIGNAL_PROPERTY_CHANGED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

G_DEFINE_TYPE_WITH_PRIVATE (ApexModel, apex_model, G_TYPE_OBJECT)

static void
apex_model_finalize (GObject *object)
{
  ApexModel *self = (ApexModel *)object;
  ApexModelPrivate *priv = apex_model_get_instance_private (self);

  // TODO: cleanup...

  G_OBJECT_CLASS (apex_model_parent_class)->finalize (object);
}

static void
apex_model_class_init (ApexModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_model_finalize;

  /**
   * ApexModel::property_changed:
   * @self: the application
   *
   * The ::property_changed signal is emitted on the primary instance
   * after a stored property has been updated.
   */
  signals [SIGNAL_PROPERTY_CHANGED] =
    g_signal_new ("property-changed",
                  APEX_TYPE_MODEL,
                  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                  G_STRUCT_OFFSET (ApexModelClass, property_changed),
                  NULL,
                  NULL,
                  NULL,
                  G_TYPE_NONE,
                  2,
                  G_TYPE_STRING,
                  G_TYPE_STRING);
}

static void
apex_model_init (ApexModel *self)
{
  ApexModelPrivate *priv;

  priv = apex_model_get_instance_private (self);
}

/**
 * apex_model_new:
 *
 * Creates a new #ApexModel instance.
 *
 * Returns: a new #ApexModel instance
 */
ApexModel *
apex_model_new ()
{
  return g_object_new (APEX_TYPE_MODEL, NULL);
}
