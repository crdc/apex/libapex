/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>
#include <gio/gio.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_JOB_QUEUE apex_job_queue_get_type ()
G_DECLARE_FINAL_TYPE (ApexJobQueue, apex_job_queue, APEX, JOB_QUEUE, GObject)

ApexJobQueue *apex_job_queue_new     (void);

void          apex_job_queue_enqueue (ApexJobQueue  *self,
                                      ApexJob       *job);
void          apex_job_queue_dequeue (ApexJobQueue  *self,
                                      ApexJob       *job);
void          apex_job_queue_run     (ApexJobQueue  *self,
                                      GError       **error);
void          apex_job_queue_cancel  (ApexJobQueue  *self);

G_END_DECLS
