/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "apex-table"

#include <json-glib/json-glib.h>

#include "apex-table.h"
#include "apex-object.h"
#include "apex-property.h"

/*
 * ApexTable:
 *
 * Represents a ...
 */
typedef struct
{
  /*< public >*/
  GType key_type;   // TODO: add this? currently only string supported
  GType value_type;

  /* <private> */
  GHashTable *internal;
} ApexTablePrivate;

enum {
  PROP_0,
  PROP_VALUE_TYPE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE_WITH_PRIVATE (ApexTable, apex_table, G_TYPE_OBJECT);

G_LOCK_DEFINE (table_lock);

static void s_table_value_free (gpointer data);

static void
apex_table_finalize (GObject *object)
{
  ApexTable *self = (ApexTable *)object;
  ApexTablePrivate *priv;
  /*GHashTableIter iter;*/
  /*gpointer key, val;*/

  priv = apex_table_get_instance_private (self);

  g_debug (">>>> Table finalize");

  g_hash_table_destroy (priv->internal);

  /*
   *g_hash_table_iter_init (&iter, priv->internal);
   *while (g_hash_table_iter_next (&iter, &key, &val))
   *  {
   *    g_debug ("there's a thing");
   *    g_free (key);
   *    s_table_value_free ((gpointer) val);
   *  }
   */

  /*g_clear_pointer (&priv->internal, g_hash_table_unref);*/

  G_OBJECT_CLASS (apex_table_parent_class)->finalize (object);

  g_debug ("<<<< Table finalize finish");
}

static void
apex_table_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  ApexTablePrivate *priv;
  ApexTable *self = APEX_TABLE (object);

  priv = apex_table_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_VALUE_TYPE:
      g_value_set_gtype (value, priv->value_type);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_table_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  ApexTablePrivate *priv;
  ApexTable *self = APEX_TABLE (object);

  priv = apex_table_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_VALUE_TYPE:
      priv->value_type = g_value_get_gtype (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
s_table_value_free (gpointer data)
{
  GValue *value = (GValue *) data;

  if (G_VALUE_TYPE (value) == G_TYPE_OBJECT ||
      G_VALUE_TYPE (value) == APEX_TYPE_OBJECT ||
      G_VALUE_TYPE (value) == APEX_TYPE_PROPERTY)
    {
      GObject *object;

      if (G_VALUE_HOLDS_OBJECT (value))
        g_debug ("wooooot");

      object = g_value_get_object (value);
      g_debug ("---- clear: %s", G_VALUE_TYPE_NAME (value));
      /*g_clear_object (&object);*/
      /*g_value_unset (value);*/
      // do release some memory
      /*g_free (object);*/
      /*g_clear_pointer (&object, g_free);*/
      /*g_clear_pointer (&object, g_object_unref);*/

      // WHAAAAAAT THAAAAA FUUUUUCK, double ref in configuration somehow but not in object
      /*
       *if (G_VALUE_TYPE (value) == APEX_TYPE_OBJECT)
       *  {
       *    g_debug ("holy balls");
       *    g_object_unref (object);
       *    g_object_unref (object);
       *  }
       *else
       *  g_clear_pointer (&object, g_object_unref);
       */
      g_clear_pointer (&object, g_object_unref);
    }
  else
    {
      g_value_unset (value);
    }

  g_free (value);
}

static void
s_table_dump (ApexTable *self)
{
  ApexTablePrivate *priv;
  g_autofree gchar *bar = NULL;
  GHashTableIter iter;
  gpointer key, val;

  g_return_if_fail (APEX_IS_TABLE (self));

  priv = apex_table_get_instance_private (self);

  // I know this is dumb, you're dumb
  bar = g_strdup_printf ("%s%s%s%s%s%s",
                         "----------",
                         "----------",
                         "----------",
                         "----------",
                         "----------",
                         "----------");

  g_debug ("%s", bar);
  g_debug ("%-20s%-20s%-20s", "key", "value", "type");
  g_debug ("%s", bar);
  g_hash_table_iter_init (&iter, priv->internal);
  while (g_hash_table_iter_next (&iter, &key, &val))
    {
      g_autofree gchar *value = NULL;
      if (priv->value_type == G_TYPE_DOUBLE)
        value = g_strdup_printf ("%.2f", g_value_get_double (val));
      else if (priv->value_type == G_TYPE_STRING)
        value = g_strdup (g_value_get_string (val));
      g_debug ("%-20s%-20s%-20s", (gchar *) key, value, G_VALUE_TYPE_NAME (val));
    }
  g_debug ("%s", bar);
}

static void
apex_table_class_init (ApexTableClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_table_finalize;
  object_class->get_property = apex_table_get_property;
  object_class->set_property = apex_table_set_property;

  properties [PROP_VALUE_TYPE] =
    g_param_spec_gtype ("value-type",
                        "Value Type",
                        "Type of the table entries value",
                        G_TYPE_NONE,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_table_init (ApexTable *self)
{
  ApexTablePrivate *priv;

  priv = apex_table_get_instance_private (self);
  priv->internal = g_hash_table_new_full (g_str_hash,
                                          g_str_equal,
                                          g_free,
                                          s_table_value_free);
}

ApexTable *
apex_table_new (GType value_type)
{
  return g_object_new (APEX_TYPE_TABLE,
                       "value-type", value_type,
                       NULL);
}

void
apex_table_flush (ApexTable *self)
{
  ApexTablePrivate *priv;

  GHashTableIter iter;
  gpointer key, val;

  g_return_if_fail (APEX_IS_TABLE (self));

  priv = apex_table_get_instance_private (self);

  if (priv->internal == NULL)
    return;

  G_LOCK (table_lock);

  g_hash_table_iter_init (&iter, priv->internal);
  while (g_hash_table_iter_next (&iter, &key, &val))
    {
      g_debug ("clearing: %s", (const gchar *) key);
      /*g_free (key);*/
      /*g_clear_pointer ();*/
    }

  G_UNLOCK (table_lock);
}

JsonNode *
apex_table_serialize (ApexTable *self,
                      GType      json_type)
{
  ApexTablePrivate *priv;

  g_autoptr (JsonNode) node = NULL;
  GHashTableIter iter;
  gpointer key, val;

  g_return_val_if_fail (APEX_IS_TABLE (self), NULL);
  g_return_val_if_fail (json_type == JSON_TYPE_ARRAY || json_type == JSON_TYPE_OBJECT, NULL);

  priv = apex_table_get_instance_private (self);

  if (priv->internal == NULL)
    return NULL;

  g_hash_table_iter_init (&iter, priv->internal);

  if (json_type == JSON_TYPE_ARRAY)
    {
      g_autoptr (JsonArray) arr = NULL;

      node = json_node_new (JSON_NODE_ARRAY);
      arr = json_array_new ();

      while (g_hash_table_iter_next (&iter, &key, &val))
        {
          if (priv->value_type == G_TYPE_DOUBLE)
            {
              json_array_add_double_element (arr, g_value_get_double (val));
            }
          else if (priv->value_type == G_TYPE_STRING)
            {
              json_array_add_string_element (arr, g_value_get_string (val));
            }
          else if (priv->value_type == G_TYPE_OBJECT ||
                   priv->value_type == APEX_TYPE_OBJECT ||
                   priv->value_type == APEX_TYPE_PROPERTY)
            {
              g_autoptr (JsonNode) data = NULL;
              g_autoptr (JsonObject) obj = NULL;
              data = json_gobject_serialize (g_value_get_object (val));
              obj = json_node_dup_object (data);
              json_array_add_object_element (arr, json_object_ref (obj));
            }
        }

      json_node_take_array (node, json_array_ref (arr));
    }
  else if (json_type == JSON_TYPE_OBJECT)
    {
      g_autoptr (JsonObject) obj = NULL;

      node = json_node_new (JSON_NODE_OBJECT);
      obj = json_object_new ();

      while (g_hash_table_iter_next (&iter, &key, &val))
        {
          if (priv->value_type == G_TYPE_DOUBLE)
            {
              json_object_set_double_member (obj,
                                             (const gchar *) key,
                                             g_value_get_double (val));
            }
          else if (priv->value_type == G_TYPE_STRING)
            {
              json_object_set_string_member (obj,
                                             (const gchar *) key,
                                             g_value_get_string (val));
            }
          else if (priv->value_type == G_TYPE_OBJECT ||
                   priv->value_type == APEX_TYPE_OBJECT ||
                   priv->value_type == APEX_TYPE_PROPERTY)
            {
              g_autoptr (JsonNode) data = NULL;
              data = json_gobject_serialize (g_value_get_object (val));
              json_object_set_object_member (obj,
                                             (const gchar *) key,
                                             json_object_ref (json_node_get_object (data)));
            }

          // TODO: handle other cases?
        }

      json_node_take_object (node, json_object_ref (obj));
    }

  return json_node_ref (node);
}

void
apex_table_deserialize (ApexTable *self,
                        JsonNode  *node)
{
  ApexTablePrivate *priv;

  g_return_if_fail (APEX_IS_TABLE (self));
  g_return_if_fail (JSON_NODE_HOLDS_ARRAY (node) || JSON_NODE_HOLDS_OBJECT (node));

  priv = apex_table_get_instance_private (self);

  if (JSON_NODE_HOLDS_ARRAY (node))
    {
      g_autoptr (JsonArray) arr = NULL;
      gchar *key = NULL;
      gint len;

      arr = json_node_get_array (node);
      len = (arr == NULL) ? 0 : json_array_get_length (arr);
      json_array_ref (arr);

      g_debug ("/table/deserialize/array/length: %d", len);

      for (gint i = 0; i < len; i++)
        {
          //GValue *val;
          GValue val = G_VALUE_INIT;

          g_debug ("/table/deserialize/array/type: %s", g_type_name (priv->value_type));

          //val = g_new0 (GValue, 1);
          g_value_init (&val, priv->value_type);

          if (priv->value_type == G_TYPE_DOUBLE)
            {
              key = g_strdup_printf ("key%d", i);
              g_value_set_double (&val, json_array_get_double_element (arr, i));
            }
          else if (priv->value_type == G_TYPE_STRING)
            {
              key = g_strdup_printf ("key%d", i);
              g_value_set_static_string (&val, json_array_get_string_element (arr, i));
            }
          else if (priv->value_type == G_TYPE_OBJECT ||
                   priv->value_type == APEX_TYPE_OBJECT ||
                   priv->value_type == APEX_TYPE_PROPERTY)
            {
              /*g_autoptr (GObject) object = NULL;*/
              GObject *object;
              JsonNode *data;

              data = json_array_dup_element (arr, i);
              object = json_gobject_deserialize (priv->value_type, data);

              if (APEX_IS_OBJECT (object))
                {
                  key = g_strdup (apex_object_get_id (APEX_OBJECT (object)));
                  /*g_value_take_object (val, APEX_OBJECT (g_steal_pointer (&object)));*/
                }
              else if (APEX_IS_PROPERTY (object))
                {
                  key = g_strdup (apex_property_get_key (APEX_PROPERTY (object)));
                  /*g_value_take_object (val, APEX_PROPERTY (g_steal_pointer (&object)));*/
                }
              else
                {
                  key = g_strdup_printf ("obj%d", i);
                  /*g_value_take_object (val, g_steal_pointer (&object));*/
                }

              /*if (object != NULL)*/
                /*{*/
                  /*g_value_take_object (&val, g_steal_pointer (&object));*/
                  /*g_value_take_object (val, g_object_ref (object));*/
                  g_value_take_object (&val, object); // segv
                  /*g_value_set_object (val, g_steal_pointer (&object));*/ // more loss
                  /*g_value_set_object (val, g_object_ref (object));*/ // more loss
                  /*g_value_set_object (val, object);*/
                /*}*/

              json_node_free (data);
            }

          apex_table_add (self, key, &val);

          g_clear_pointer (&key, g_free);
          g_value_unset (&val);
          //g_free (val);
        }
    }
  else if (JSON_NODE_HOLDS_OBJECT (node))
    {
      g_autoptr (JsonObject) obj = NULL;
      JsonObjectIter iter;
      const gchar *key;
      JsonNode *data;

      obj = json_node_get_object (node);
      json_object_ref (obj);

      json_object_iter_init (&iter, obj);
      while (json_object_iter_next (&iter, &key, &data))
        {
          //GValue *val;
          GValue val = G_VALUE_INIT;

          g_debug ("/table/deserialize/object/type: %s", g_type_name (priv->value_type));

          //val = g_new0 (GValue, 1);
          g_value_init (&val, priv->value_type);

          if (priv->value_type == G_TYPE_DOUBLE)
            {
              g_value_set_double (&val, json_node_get_double (data));
            }
          else if (priv->value_type == G_TYPE_STRING)
            {
              g_value_set_static_string (&val, json_node_get_string (data));
            }
          else if (priv->value_type == G_TYPE_OBJECT ||
                   priv->value_type == APEX_TYPE_OBJECT ||
                   priv->value_type == APEX_TYPE_PROPERTY)
            {
              /*g_autoptr (GObject) object = NULL;*/
              GObject *object;

              object = json_gobject_deserialize (priv->value_type, data);
              /*g_value_take_object (val, g_object_ref (object));*/
              g_value_take_object (&val, object);
              /*g_value_set_object (val, g_object_ref (object));*/
              /*g_value_set_object (val, object);*/

              json_node_free (data);
            }

          apex_table_add (self, key, &val);

          g_value_unset (&val);
          //g_free (val);
        }
    }

  s_table_dump (self);
}

void
apex_table_add (ApexTable    *self,
                const gchar  *key,
                const GValue *value)
{
  ApexTablePrivate *priv;
  GValue *copy = g_new0 (GValue, 1);

  g_return_if_fail (APEX_IS_TABLE (self));

  priv = apex_table_get_instance_private (self);

  if (priv->internal == NULL)
    {
      priv->internal = g_hash_table_new_full (g_str_hash,
                                              g_str_equal,
                                              g_free,
                                              s_table_value_free);
    }

  g_value_init (copy, G_VALUE_TYPE (value));
  g_value_copy (value, copy);

  if (g_hash_table_contains (priv->internal, key))
    g_hash_table_replace (priv->internal, g_strdup (key), copy);
  else
    g_hash_table_insert (priv->internal, g_strdup (key), copy);
}

void
apex_table_remove (ApexTable   *self,
                   const gchar *key)
{
  ApexTablePrivate *priv;

  g_return_if_fail (APEX_IS_TABLE (self));

  priv = apex_table_get_instance_private (self);

  g_return_if_fail (priv->internal != NULL);

  if (g_hash_table_contains (priv->internal, key))
    g_hash_table_remove (priv->internal, key);
}

GValue *
apex_table_get (ApexTable   *self,
                const gchar *key)
{
  ApexTablePrivate *priv;

  g_return_val_if_fail (APEX_IS_TABLE (self), NULL);

  priv = apex_table_get_instance_private (self);

  g_return_val_if_fail (priv->internal != NULL, NULL);

  if (g_hash_table_contains (priv->internal, key))
    return g_hash_table_lookup (priv->internal, key);

  return NULL;
}

gboolean
apex_table_has (ApexTable   *self,
                const gchar *key)
{
  ApexTablePrivate *priv;
  gboolean ret;

  g_return_val_if_fail (APEX_IS_TABLE (self), FALSE);

  priv = apex_table_get_instance_private (self);

  g_return_val_if_fail (priv->internal != NULL, FALSE);
  g_return_val_if_fail (key != NULL, FALSE);

  ret = g_hash_table_contains (priv->internal, key);

  return ret;
}


/**
 * apex_table_get_keys:
 * @self: a #ApexTable
 *
 * Retrieves every key inside the internal hash table. The returned data is
 * valid until changes to the hash release those keys.
 *
 * Returns: (transfer container): a #GList containing all the keys
 *     inside the internal hash table. The content of the list is owned by the
 *     hash table and should not be modified or freed. Use g_list_free()
 *     when done using the list.
 */
GList *
apex_table_get_keys (ApexTable *self)
{
  ApexTablePrivate *priv;

  g_return_val_if_fail (APEX_IS_TABLE (self), NULL);

  priv = apex_table_get_instance_private (self);

  g_return_val_if_fail (priv->internal != NULL, NULL);

  return g_hash_table_get_keys (priv->internal);
}
