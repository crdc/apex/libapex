/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "apex-job-queue"

#include "apex-job-queue.h"
#include "apex-job.h"

struct _ApexJobQueue
{
  GObject      parent;
  gboolean     running;
  GAsyncQueue *queue;
  GThreadPool *pool;
  //GHashTable  *table;
  gint         max_jobs;
  gchar       *active_job_id;
};

static void s_queue_async  (ApexJobQueue  *self,
                            GError       **error);
static void s_queue_cancel (ApexJobQueue *self,
                            gpointer      user_data);

G_DEFINE_TYPE (ApexJobQueue, apex_job_queue, G_TYPE_OBJECT)

static void
apex_job_queue_finalize (GObject *object)
{
  ApexJobQueue *self = (ApexJobQueue *)object;

  g_async_queue_unref (self->queue);
  g_clear_pointer (&self->active_job_id, g_free);
  if (self->pool)
    g_thread_pool_free (self->pool, TRUE, FALSE);

  G_OBJECT_CLASS (apex_job_queue_parent_class)->finalize (object);
}

static void
apex_job_queue_class_init (ApexJobQueueClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_job_queue_finalize;
}

static void
apex_job_queue_init (ApexJobQueue *self)
{
  /* Job queue initialization */
  self->running = FALSE;
  self->queue = g_async_queue_new ();
  self->max_jobs = 1;
}

ApexJobQueue *
apex_job_queue_new (void)
{
  return g_object_new (APEX_TYPE_JOB_QUEUE, NULL);
}

/**
 * apex_job_queue_run:
 * @self: an #ApexJobQueue
 * @error: return location for a GError, or NULL
 *
 * Starts the thread that monitors the messages received from the parent
 * service.
 */
void
apex_job_queue_run (ApexJobQueue  *self,
                    GError       **error)
{
  g_return_if_fail (APEX_IS_JOB_QUEUE (self));

  s_queue_async (self, error);
}

/**
 * apex_job_queue_cancel:
 * @self: an #ApexJobQueue
 *
 * Stops the thread that monitors the messages received from the parent
 * service.
 */
void
apex_job_queue_cancel (ApexJobQueue *self)
{
  g_return_if_fail (APEX_IS_JOB_QUEUE (self));

  s_queue_cancel (self, NULL);
}

/**
 * apex_job_queue_enqueue:
 * @self: an #ApexJobQueue
 * @job: an #ApexJob
 *
 * Add a job to the internal job queue.
 */
void
apex_job_queue_enqueue (ApexJobQueue *self,
                        ApexJob      *job)
{
  g_return_if_fail (APEX_IS_JOB_QUEUE (self));

  g_async_queue_lock (self->queue);
  g_async_queue_push_unlocked (self->queue, job);
  g_async_queue_unlock (self->queue);
}

/**
 * apex_job_queue_dequeue:
 * @self: an #ApexJobQueue
 * @job: an #ApexJob
 *
 * Remove a job from the internal job queue.
 * TODO: don't use this
 */
void
apex_job_queue_dequeue (ApexJobQueue *self,
                        ApexJob       *job)
{
  g_return_if_fail (APEX_IS_JOB_QUEUE (self));

  // TODO: implement this
}

static void
s_job_func (gpointer data,
            gpointer user_data)
{
  g_return_if_fail (APEX_IS_JOB_QUEUE (user_data));
  g_return_if_fail (APEX_IS_JOB (data));

  apex_job_task (APEX_JOB (data));
}

static void
s_queue_cb (GObject      *source_object,
            GAsyncResult *result,
            gpointer      user_data)
{
  g_debug ("job monitor finished");
}

static void
s_queue_cancel (ApexJobQueue *self,
                gpointer      data)
{
  GCancellable *cancellable;

  cancellable = G_CANCELLABLE (data);
  g_cancellable_cancel (cancellable);

  self->running = FALSE;

  g_debug ("job monitor cancelled");
}

static void
s_queue_thread (GTask        *task,
                gpointer      source_object,
                gpointer      task_data,
                GCancellable *cancellable)
{
  g_assert (source_object == g_task_get_source_object (task));
  g_assert (cancellable == g_task_get_cancellable (task));

  APEX_JOB_QUEUE(source_object)->running = TRUE;

  while (APEX_JOB_QUEUE(source_object)->running)
    {
      ApexJob *job;

      if (g_cancellable_is_cancelled (cancellable))
        {
          g_task_return_new_error (task,
                                   G_IO_ERROR, G_IO_ERROR_CANCELLED,
                                   "Task cancelled");
          return;
        }

      g_async_queue_lock (APEX_JOB_QUEUE(source_object)->queue);
      job = APEX_JOB (g_async_queue_pop_unlocked (APEX_JOB_QUEUE(source_object)->queue));
      g_async_queue_unlock (APEX_JOB_QUEUE(source_object)->queue);

      g_debug ("Monitor received a new job: %s", apex_job_get_id (job));

      // TODO: add job to thread pool
      g_thread_pool_push (APEX_JOB_QUEUE(source_object)->pool, job, NULL);
    }

  g_task_return_boolean (task, TRUE);
}

/**
 * s_queue_async:
 * @self: an #ApexApplication
 *
 * Starts the thread that monitors the job queue.
 */
static void
s_queue_async (ApexJobQueue  *self,
               GError       **error)
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (GCancellable) cancellable = NULL;

  g_return_if_fail (APEX_IS_JOB_QUEUE (self));

  self->pool = g_thread_pool_new (s_job_func, self, self->max_jobs, FALSE, NULL);

  g_return_if_fail (self->pool != NULL);

  g_thread_pool_set_max_unused_threads (1000);

  cancellable = g_cancellable_new ();
  task = g_task_new (self, cancellable, s_queue_cb, NULL);
  /*
   *if (self->max_jobs < 1)
   *  {
   *    g_task_return_new_error (task, JOB_QUEUE_ERROR,
   *                             JOB_QUEUE_ERROR_INVALID_JOB_QUEUE_SIZE,
   *                             "%d is not a valid job queue size",
   *                             self->max_jobs);
   *    g_object_unref (task);
   *    return;
   *  }
   */

  g_task_run_in_thread (task, s_queue_thread);
}
