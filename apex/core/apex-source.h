/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_SOURCE apex_source_get_type ()
G_DECLARE_FINAL_TYPE (ApexSource, apex_source, APEX, SOURCE, GObject)

ApexSource  *apex_source_new           (const gchar *endpoint,
                                        const gchar *envelope);

gchar       *apex_source_serialize     (ApexSource  *self);
void         apex_source_deserialize   (ApexSource  *self,
                                        const gchar *data);

void         apex_source_start         (ApexSource  *self);
void         apex_source_stop          (ApexSource  *self);

void         apex_source_queue_message (ApexSource  *self,
                                        const gchar *data);

gboolean     apex_source_running       (ApexSource  *self);

const gchar *apex_source_get_endpoint  (ApexSource  *self);
void         apex_source_set_endpoint  (ApexSource  *self,
                                        const gchar *endpoint);

const gchar *apex_source_get_envelope  (ApexSource  *self);
void         apex_source_set_envelope  (ApexSource  *self,
                                        const gchar *envelope);

G_END_DECLS
