/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_HANDLER apex_handler_get_type ()
G_DECLARE_INTERFACE (ApexHandler, apex_handler, APEX, HANDLER, GObject)

struct _ApexHandlerInterface
{
  GTypeInterface parent_iface;

  /*< public >*/
  /* signals */
  void (*shutdown_requested) (ApexHandler *self);

  /* vfuncs */
  void (*run)    (ApexHandler  *self,
                  gpointer      data,
                  GError      **error);
  void (*cancel) (ApexHandler  *self);
};

void
apex_handler_run    (ApexHandler  *self,
                     gpointer      data,
                     GError      **error);
void
apex_handler_cancel (ApexHandler  *self);

G_END_DECLS
