/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_TABLE apex_table_get_type ()
G_DECLARE_DERIVABLE_TYPE (ApexTable, apex_table, APEX, TABLE, GObject)

struct _ApexTableClass
{
  /*< private >*/
  GObjectClass parent_class;

  /*< private >*/
  gpointer padding[12];
};

ApexTable *apex_table_new         (GType         value_type);
void       apex_table_flush       (ApexTable    *self);

JsonNode  *apex_table_serialize   (ApexTable    *self,
                                   GType         json_type);
void       apex_table_deserialize (ApexTable    *self,
                                   JsonNode     *node);

void       apex_table_add         (ApexTable    *self,
                                   const gchar  *key,
                                   const GValue *value);
void       apex_table_remove      (ApexTable    *self,
                                   const gchar  *key);
GValue    *apex_table_get         (ApexTable    *self,
                                   const gchar  *key);
gboolean   apex_table_has         (ApexTable    *self,
                                   const gchar  *key);
GList     *apex_table_get_keys    (ApexTable    *self);

G_END_DECLS
