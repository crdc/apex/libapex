/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-metric-table.h"
#include "apex-metric-table-header.h"
#include "apex-table.h"

/*
 * ApexMetricTable:
 *
 * Represents a ...
 */
struct _ApexMetricTable
{
  GObject                parent;
  gchar                 *name;
  gchar                 *timestamp;
  ApexMetricTableHeader *header;
  ApexTable             *entries;
};

enum {
  PROP_0,
  PROP_NAME,
  PROP_TIMESTAMP,
  PROP_HEADER,
  PROP_ENTRIES,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (ApexMetricTable, apex_metric_table, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
apex_metric_table_serialize_property (JsonSerializable *serializable,
                                      const gchar      *name,
                                      const GValue     *value,
                                      GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "entries") == 0)
    {
      if (value != NULL && G_VALUE_HOLDS_OBJECT (value))
        retval = apex_table_serialize (APEX_TABLE (g_value_get_object (value)), JSON_TYPE_OBJECT);
    }
  else if (g_strcmp0 (name, "header") == 0)
    {
      ApexMetricTableHeader *header;

      if (value != NULL && G_VALUE_HOLDS_OBJECT (value))
        {
          header = g_value_get_object (value);
          retval = json_gobject_serialize (G_OBJECT (header));
        }
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
apex_metric_table_deserialize_property (JsonSerializable *serializable,
                                        const gchar      *name,
                                        GValue           *value,
                                        GParamSpec       *pspec,
                                        JsonNode         *property_node)
{
  gboolean retval = FALSE;

  if (g_strcmp0 (name, "entries") == 0)
    {
      /*g_autoptr (ApexTable) entries = NULL;*/
      ApexTable *entries;

      entries = apex_table_new (G_TYPE_DOUBLE);
      apex_table_deserialize (entries, property_node);

      /*g_value_take_object (value, g_object_ref (entries));*/
      g_value_take_object (value, entries);

      retval = TRUE;

      /*g_clear_object (&entries);*/
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = apex_metric_table_serialize_property;
  iface->deserialize_property = apex_metric_table_deserialize_property;
}

static void
apex_metric_table_finalize (GObject *object)
{
  ApexMetricTable *self = (ApexMetricTable *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->timestamp, g_free);
  g_clear_object (&self->header);
  g_clear_object (&self->entries);

  G_OBJECT_CLASS (apex_metric_table_parent_class)->finalize (object);
}

static void
apex_metric_table_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  ApexMetricTable *self = APEX_METRIC_TABLE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_take_string (value, apex_metric_table_dup_name (self));
      break;

    case PROP_TIMESTAMP:
      g_value_take_string (value, apex_metric_table_dup_timestamp (self));
      break;

    case PROP_HEADER:
      g_value_take_object (value, apex_metric_table_ref_header (self));
      break;

    case PROP_ENTRIES:
      g_value_take_object (value, apex_metric_table_ref_entries (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_metric_table_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  ApexMetricTable *self = APEX_METRIC_TABLE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      apex_metric_table_set_name (self, g_value_get_string (value));
      break;

    case PROP_TIMESTAMP:
      apex_metric_table_set_timestamp (self, g_value_get_string (value));
      break;

    case PROP_HEADER:
      apex_metric_table_set_header (self, g_value_get_object (value));
      break;

    case PROP_ENTRIES:
      apex_metric_table_set_entries (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_metric_table_class_init (ApexMetricTableClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_metric_table_finalize;
  object_class->get_property = apex_metric_table_get_property;
  object_class->set_property = apex_metric_table_set_property;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the table",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_TIMESTAMP] =
    g_param_spec_string ("timestamp",
                         "Timestamp",
                         "Time stamp for the recorded metric values",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_HEADER] =
    g_param_spec_object ("header",
                         "Header",
                         "Table header data",
                         APEX_TYPE_METRIC_TABLE_HEADER,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_ENTRIES] =
    g_param_spec_object ("entries",
                         "Entries",
                         "List of metric value entries",
                         APEX_TYPE_TABLE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_metric_table_init (ApexMetricTable *self)
{
  self->entries = apex_table_new (G_TYPE_DOUBLE);
}

ApexMetricTable *
apex_metric_table_new (void)
{
  return g_object_new (APEX_TYPE_METRIC_TABLE, NULL);
}

gchar *
apex_metric_table_serialize (ApexMetricTable *self)
{
  g_return_val_if_fail (APEX_IS_METRIC_TABLE (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
apex_metric_table_deserialize (ApexMetricTable *self,
                               const gchar     *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (ApexMetricTableHeader) header = NULL;
  g_autoptr (ApexTable) entries = NULL;
  g_autofree gchar *name = NULL;
  g_autofree gchar *timestamp = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (APEX_TYPE_METRIC_TABLE,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_METRIC_TABLE (object));

  name = apex_metric_table_dup_name (APEX_METRIC_TABLE (object));
  timestamp = apex_metric_table_dup_timestamp (APEX_METRIC_TABLE (object));
  header = apex_metric_table_ref_header (APEX_METRIC_TABLE (object));
  entries = apex_metric_table_ref_entries (APEX_METRIC_TABLE (object));

  apex_metric_table_set_name (self, name);
  apex_metric_table_set_timestamp (self, timestamp);
  apex_metric_table_set_header (self, header);
  apex_metric_table_set_entries (self, entries);

  g_clear_object (&object);
}

/**
 * apex_metric_table_dup_name:
 *
 * Copies the name of the metric table and returns it to the caller (after
 * locking the object). A copy is used to avoid thread-races.
 */
gchar *
apex_metric_table_dup_name (ApexMetricTable *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_METRIC_TABLE (self), NULL);

  /*apex_object_lock (APEX_OBJECT (self));*/
  ret = g_strdup (self->name);
  /*apex_object_unlock (APEX_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

const gchar *
apex_metric_table_get_name (ApexMetricTable *self)
{
  g_return_val_if_fail (APEX_IS_METRIC_TABLE (self), NULL);

  return self->name;
}

void
apex_metric_table_set_name (ApexMetricTable *self,
                            const gchar     *name)
{
  g_return_if_fail (APEX_IS_METRIC_TABLE (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

/**
 * apex_metric_table_dup_timestamp:
 *
 * Copies the timestamp of the metric table and returns it to the caller (after
 * locking the object). A copy is used to avoid thread-races.
 */
gchar *
apex_metric_table_dup_timestamp (ApexMetricTable *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_METRIC_TABLE (self), NULL);

  /*apex_object_lock (APEX_OBJECT (self));*/
  ret = g_strdup (self->timestamp);
  /*apex_object_unlock (APEX_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

const gchar *
apex_metric_table_get_timestamp (ApexMetricTable *self)
{
  g_return_val_if_fail (APEX_IS_METRIC_TABLE (self), NULL);

  return self->timestamp;
}

void
apex_metric_table_set_timestamp (ApexMetricTable *self,
                                 const gchar     *timestamp)
{
  g_return_if_fail (APEX_IS_METRIC_TABLE (self));

  if (g_strcmp0 (timestamp, self->timestamp) != 0)
    {
      g_free (self->timestamp);
      self->timestamp = g_strdup (timestamp);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TIMESTAMP]);
    }
}

/**
 * apex_metric_table_get_header:
 * @self: an #ApexMetricTable
 *
 * Retrieve the #ApexMetricTableHeader containing the table header information.
 *
 * Returns: (transfer full): an #ApexMetricTableHeader if one is set.
 */
ApexMetricTableHeader *
apex_metric_table_get_header (ApexMetricTable *self)
{
  ApexMetricTableHeader *header;

  g_return_val_if_fail (APEX_IS_METRIC_TABLE (self), NULL);

  g_object_get (self, "header", &header, NULL);

  return header;
}

/**
 * apex_metric_table_ref_header:
 * @self: an #ApexMetricTable
 *
 * Gets the header for the metric table, and returns a new reference
 * to the #ApexMetricTableHeader.
 *
 * Returns: (transfer full) (nullable): a #ApexMetricTableHeader or %NULL
 */
ApexMetricTableHeader *
apex_metric_table_ref_header (ApexMetricTable *self)
{
  ApexMetricTableHeader *ret = NULL;

  g_return_val_if_fail (APEX_IS_METRIC_TABLE (self), NULL);

  /*apex_object_lock (G_OBJECT (self));*/
  g_set_object (&ret, self->header);
  /*apex_object_unlock (G_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
apex_metric_table_set_header (ApexMetricTable       *self,
                              ApexMetricTableHeader *header)
{
  g_return_if_fail (APEX_IS_METRIC_TABLE (self));
  g_return_if_fail (APEX_IS_METRIC_TABLE_HEADER (header));

  if (g_set_object (&self->header, header))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_HEADER]);
}

/**
 * apex_metric_table_get_entries:
 * @self: an #ApexMetricTable
 *
 * Retrieve the #ApexTable containing table entries.
 *
 * Returns: (transfer full): a #ApexTable of entries if one is set.
 */
ApexTable *
apex_metric_table_get_entries (ApexMetricTable *self)
{
  ApexTable *entries;

  g_return_val_if_fail (APEX_IS_METRIC_TABLE (self), NULL);

  g_object_get (self, "entries", &entries, NULL);

  return entries;
}

/**
 * apex_metric_table_ref_entries:
 *
 * Gets the table entries, and returns a new reference
 * to the #ApexTable.
 *
 * Returns: (transfer full) (nullable): a #ApexTable or %NULL
 */
ApexTable *
apex_metric_table_ref_entries (ApexMetricTable *self)
{
  ApexTable *ret = NULL;

  g_return_val_if_fail (APEX_IS_METRIC_TABLE (self), NULL);

  g_set_object (&ret, self->entries);

  return g_steal_pointer (&ret);
}

void
apex_metric_table_set_entries (ApexMetricTable *self,
                               ApexTable       *entries)
{
  g_return_if_fail (APEX_IS_METRIC_TABLE (self));
  g_return_if_fail (APEX_IS_TABLE (self->entries));

  if (g_set_object (&self->entries, entries))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENTRIES]);
}

void
apex_metric_table_add_entry (ApexMetricTable *self,
                             const gchar     *key,
                             gdouble          value)
{
  GValue val = G_VALUE_INIT;

  g_return_if_fail (APEX_IS_METRIC_TABLE (self));

  g_value_init (&val, G_TYPE_DOUBLE);
  g_value_set_double (&val, value);

  apex_table_add (self->entries, key, &val);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENTRIES]);

  g_value_unset (&val);
}

void
apex_metric_table_remove_entry (ApexMetricTable *self,
                                const gchar     *key)
{
  g_return_if_fail (APEX_IS_METRIC_TABLE (self));
  g_return_if_fail (self->entries != NULL);

  apex_table_remove (self->entries, key);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENTRIES]);
}

gdouble
apex_metric_table_get_entry (ApexMetricTable *self,
                             const gchar     *key)
{
  g_return_val_if_fail (APEX_IS_METRIC_TABLE (self), G_MINDOUBLE);
  g_return_val_if_fail (self->entries != NULL, G_MINDOUBLE);

  return g_value_get_double (apex_table_get (self->entries, key));
}

gboolean
apex_metric_table_has_entry (ApexMetricTable *self,
                             const gchar     *key)
{
  gboolean ret;

  g_return_val_if_fail (APEX_IS_METRIC_TABLE (self), FALSE);
  g_return_val_if_fail (self->entries != NULL, FALSE);
  g_return_val_if_fail (key != NULL, FALSE);

  ret = apex_table_has (self->entries, key);

  return ret;
}
