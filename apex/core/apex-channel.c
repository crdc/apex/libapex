/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-channel.h"

/* TODO: add prop things for type */
struct _ApexChannel
{
  GObject          parent;
  /*ApexChannelType  channel_type;*/
  gchar           *endpoint;
  gchar           *envelope;
};

enum {
  PROP_0,
  /*PROP_CHANNEL_TYPE,*/
  PROP_ENDPOINT,
  PROP_ENVELOPE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexChannel, apex_channel, G_TYPE_OBJECT)

static void
apex_channel_finalize (GObject *object)
{
  ApexChannel *self = (ApexChannel *)object;

  g_clear_pointer (&self->endpoint, g_free);
  g_clear_pointer (&self->envelope, g_free);

  G_OBJECT_CLASS (apex_channel_parent_class)->finalize (object);
}

static void
apex_channel_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  ApexChannel *self = APEX_CHANNEL (object);

  switch (prop_id)
  {
    /*case PROP_CHANNEL_TYPE:*/
      /*g_value_set_enum (value, apex_channel_get_channel_type (self));*/
      /*break;*/

    case PROP_ENDPOINT:
      g_value_set_string (value, apex_channel_get_endpoint (self));
      break;

    case PROP_ENVELOPE:
      g_value_set_string (value, apex_channel_get_envelope (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_channel_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  ApexChannel *self = APEX_CHANNEL (object);

  switch (prop_id)
  {
    /*case PROP_CHANNEL_TYPE:*/
      /*apex_channel_set_channel_type (self, g_value_get_enum (value));*/
      /*break;*/

    case PROP_ENDPOINT:
      apex_channel_set_endpoint (self, g_value_get_string (value));
      break;

    case PROP_ENVELOPE:
      apex_channel_set_envelope (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_channel_class_init (ApexChannelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_channel_finalize;
  object_class->get_property = apex_channel_get_property;
  object_class->set_property = apex_channel_set_property;

  properties [PROP_ENDPOINT] =
    g_param_spec_string ("endpoint",
                         "Endpoint",
                         "The channel endpoint.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_ENVELOPE] =
    g_param_spec_string ("envelope",
                         "Envelope",
                         "The channel envelope.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_channel_init (ApexChannel *self)
{
}

ApexChannel *
apex_channel_new (void)
{
  ApexChannel *object = g_object_new (APEX_TYPE_CHANNEL, NULL);

  // TODO: add setup

  return object;
}

gchar *
apex_channel_serialize (ApexChannel *self)
{
  g_return_val_if_fail (APEX_IS_CHANNEL (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
apex_channel_deserialize (ApexChannel *self,
                          const gchar *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_CHANNEL,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
  {
    g_critical ("%s", err->message);
    g_error_free (err);
  }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_CHANNEL (object));

  apex_channel_set_endpoint (self, apex_channel_get_endpoint (APEX_CHANNEL (object)));
  apex_channel_set_envelope (self, apex_channel_get_envelope (APEX_CHANNEL (object)));

  g_clear_pointer (&APEX_CHANNEL (object)->endpoint, g_free);
  g_clear_pointer (&APEX_CHANNEL (object)->envelope, g_free);
  g_object_unref (object);
}

const gchar *
apex_channel_get_endpoint (ApexChannel *self)
{
  g_return_val_if_fail (APEX_IS_CHANNEL (self), NULL);

  return self->endpoint;
}

void
apex_channel_set_endpoint (ApexChannel *self,
                           const gchar *endpoint)
{
  g_return_if_fail (APEX_IS_CHANNEL (self));

  if (g_strcmp0 (endpoint, self->endpoint) != 0)
    {
      g_free (self->endpoint);
      self->endpoint = g_strdup (endpoint);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENDPOINT]);
    }
}

const gchar *
apex_channel_get_envelope (ApexChannel *self)
{
  g_return_val_if_fail (APEX_IS_CHANNEL (self), NULL);

  return self->envelope;
}

void
apex_channel_set_envelope (ApexChannel *self,
                           const gchar *envelope)
{
  g_return_if_fail (APEX_IS_CHANNEL (self));

  if (g_strcmp0 (envelope, self->envelope) != 0)
    {
      g_free (self->envelope);
      self->envelope = g_strdup (envelope);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENVELOPE]);
    }
}
