/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_PROPERTY apex_property_get_type ()
G_DECLARE_FINAL_TYPE (ApexProperty, apex_property, APEX, PROPERTY, GObject)

ApexProperty *apex_property_new         (const gchar  *key,
                                         const gchar  *value);

gchar        *apex_property_serialize   (ApexProperty *self);
void          apex_property_deserialize (ApexProperty *self,
                                         const gchar  *data);

const gchar  *apex_property_get_key     (ApexProperty *self);
void          apex_property_set_key     (ApexProperty *self,
                                         const gchar  *key);

const gchar  *apex_property_get_value   (ApexProperty *self);
void          apex_property_set_value   (ApexProperty *self,
                                         const gchar  *value);

G_END_DECLS
