/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_CHANNEL apex_channel_get_type ()
G_DECLARE_FINAL_TYPE (ApexChannel, apex_channel, APEX, CHANNEL, GObject)

ApexChannel *apex_channel_new          (void);

gchar       *apex_channel_serialize    (ApexChannel *self);
void         apex_channel_deserialize  (ApexChannel *self,
                                        const gchar *data);

const gchar *apex_channel_get_endpoint (ApexChannel *self);
void         apex_channel_set_endpoint (ApexChannel *self,
                                        const gchar *endpoint);

const gchar *apex_channel_get_envelope (ApexChannel *self);
void         apex_channel_set_envelope (ApexChannel *self,
                                        const gchar *envelope);

G_END_DECLS
