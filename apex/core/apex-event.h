/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_EVENT apex_event_get_type ()
G_DECLARE_FINAL_TYPE (ApexEvent, apex_event, APEX, EVENT, GObject)

ApexEvent   *apex_event_new             (void);
ApexEvent   *apex_event_new_full        (gint         id,
                                         const gchar *name,
                                         const gchar *description);

gchar       *apex_event_serialize       (ApexEvent   *self);
void         apex_event_deserialize     (ApexEvent   *self,
                                         const gchar *data);

gint         apex_event_get_id          (ApexEvent   *self);
void         apex_event_set_id          (ApexEvent   *self,
                                         gint         id);

const gchar *apex_event_get_name        (ApexEvent   *self);
gchar       *apex_event_dup_name        (ApexEvent   *self);
void         apex_event_set_name        (ApexEvent   *self,
                                         const gchar *name);

const gchar *apex_event_get_description (ApexEvent   *self);
gchar       *apex_event_dup_description (ApexEvent   *self);
void         apex_event_set_description (ApexEvent   *self,
                                         const gchar *description);

G_END_DECLS
