/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "apex-object"

#include <json-glib/json-glib.h>

#include "apex-object.h"
#include "apex-property.h"
#include "apex-table.h"

struct _ApexObject
{
  GObject     parent;
  gchar      *id;
  gchar      *name;
  ApexTable  *properties;
  ApexTable  *objects;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_NAME,
  PROP_PROPERTIES,
  PROP_OBJECTS,
  N_PROPS
};

static GParamSpec *class_properties [N_PROPS];

static JsonSerializableIface *serializable_iface = NULL;

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (ApexObject, apex_object, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
apex_object_serialize_property (JsonSerializable *serializable,
                                const gchar      *name,
                                const GValue     *value,
                                GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "properties") == 0)
    {
      if (value != NULL && G_VALUE_HOLDS_OBJECT (value))
        retval = apex_table_serialize (APEX_TABLE (g_value_get_object (value)), JSON_TYPE_ARRAY);
    }
  else if (g_strcmp0 (name, "objects") == 0)
    {
      if (value != NULL && G_VALUE_HOLDS_OBJECT (value))
        retval = apex_table_serialize (APEX_TABLE (g_value_get_object (value)), JSON_TYPE_ARRAY);
    }
  else
    {
      retval = serializable_iface->serialize_property (serializable,
                                                       name,
                                                       value,
                                                       pspec);

      /*GValue copy = { 0, };*/

      /*retval = json_node_new (JSON_NODE_VALUE);*/

      /*g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));*/
      /*g_value_copy (value, &copy);*/
      /*json_node_set_value (retval, &copy);*/
      /*g_value_unset (&copy);*/
    }

  return retval;
}

static gboolean
apex_object_deserialize_property (JsonSerializable *serializable,
                                  const gchar      *name,
                                  GValue           *value,
                                  GParamSpec       *pspec,
                                  JsonNode         *property_node)
{
  gboolean retval = FALSE;

  if (g_strcmp0 (name, "properties") == 0)
    {
      g_autoptr (ApexTable) properties = NULL;

      properties = apex_table_new (APEX_TYPE_PROPERTY);
      apex_table_deserialize (properties, property_node);

      g_value_take_object (value, g_object_ref (properties));

      retval = TRUE;
    }
  else if (g_strcmp0 (name, "objects") == 0)
    {
      g_autoptr (ApexTable) objects = NULL;

      objects = apex_table_new (APEX_TYPE_OBJECT);
      apex_table_deserialize (objects, property_node);

      g_value_take_object (value, g_object_ref (objects));

      retval = TRUE;
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  serializable_iface = g_type_default_interface_peek (JSON_TYPE_SERIALIZABLE);

  iface->serialize_property = apex_object_serialize_property;
  iface->deserialize_property = apex_object_deserialize_property;
}

static void
apex_object_finalize (GObject *object)
{
  ApexObject *self = (ApexObject *)object;

  g_debug ("Finaliiiiiiiize!!!!: %s (%s)", self->id, self->name);

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->name, g_free);
  g_clear_object (&self->objects);
  g_clear_object (&self->properties);

  G_OBJECT_CLASS (apex_object_parent_class)->finalize (object);
}

static void
apex_object_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  ApexObject *self = APEX_OBJECT (object);

  switch (prop_id)
	  {
    case PROP_ID:
      g_value_set_string (value, apex_object_get_id (self));
      break;

    case PROP_NAME:
      g_value_set_string (value, apex_object_get_name (self));
      break;

    case PROP_PROPERTIES:
      g_value_take_object (value, apex_object_ref_properties (self));
			break;

    case PROP_OBJECTS:
      g_value_take_object (value, apex_object_ref_objects (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  	}
}

static void
apex_object_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  ApexObject *self = APEX_OBJECT (object);

  switch (prop_id)
  	{
    case PROP_ID:
      apex_object_set_id (self, g_value_get_string (value));
      break;

    case PROP_NAME:
      apex_object_set_name (self, g_value_get_string (value));
      break;

		case PROP_PROPERTIES:
      apex_object_set_properties (self, g_value_get_object (value));
      break;

    case PROP_OBJECTS:
      apex_object_set_objects (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  	}
}

static void
apex_object_class_init (ApexObjectClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_object_finalize;
  object_class->get_property = apex_object_get_property;
  object_class->set_property = apex_object_set_property;

  class_properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The object ID.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  class_properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The object name.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

	class_properties [PROP_PROPERTIES] =
	  g_param_spec_object ("properties",
                         "Properties",
                         "List of properties.",
                         APEX_TYPE_TABLE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  class_properties [PROP_OBJECTS] =
    g_param_spec_object ("objects",
                         "Objects",
                         "List of objects.",
                         APEX_TYPE_TABLE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, class_properties);
}

static void
apex_object_init (ApexObject *self)
{
  self->properties = apex_table_new (APEX_TYPE_PROPERTY);
  self->objects = apex_table_new (APEX_TYPE_OBJECT);
}

ApexObject *
apex_object_new (const gchar *id)
{
  return g_object_new (APEX_TYPE_OBJECT,
                       "id", id,
                       "name", "Object",
                       NULL);
}

// TODO: get rid of this
void
apex_object_dump (ApexObject *self)
{
  g_return_if_fail (APEX_IS_OBJECT (self));

  g_print ("id:   %s\n", apex_object_get_id (self));
  g_print ("name: %s\n", apex_object_get_name (self));

  if (self->properties != NULL)
    {
      g_print ("properties:\n");
      g_print (" - TODO: removed hash table iteration\n");
    }

  // TODO: implement a depth argument to the call to dump
  if (self->objects != NULL)
    {
      g_print ("objects:\n");
      g_print (" - TODO: removed hash table iteration\n");
    }
}

gchar *
apex_object_serialize (ApexObject *self)
{
  g_return_val_if_fail (APEX_IS_OBJECT (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
apex_object_deserialize (ApexObject  *self,
                         const gchar *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (ApexTable) objects = NULL;
  g_autoptr (ApexTable) properties = NULL;
  g_autofree gchar *id = NULL;
  g_autofree gchar *name = NULL;
  GError *err = NULL;

  object = json_gobject_from_data (APEX_TYPE_OBJECT,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_OBJECT (object));

  id = g_strdup (apex_object_get_id (APEX_OBJECT (object)));
  name = g_strdup (apex_object_get_name (APEX_OBJECT (object)));
  properties = apex_object_ref_properties (APEX_OBJECT (object));
  objects = apex_object_ref_objects (APEX_OBJECT (object));

  apex_object_set_id (self, id);
  apex_object_set_name (self, name);
  apex_object_set_properties (self, properties);
  apex_object_set_objects (self, objects);

  g_clear_object (&object);
}

GObject *
apex_gobject_from_data (const gchar *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_OBJECT,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_val_if_fail (object != NULL, NULL);
  g_return_val_if_fail (APEX_IS_OBJECT (object), NULL);

  return object;
}

/**
 * apex_object_get_id:
 * @self: an #ApexObject
 *
 * Returns: (transfer none): The object id, or %NULL
 */
const gchar *
apex_object_get_id (ApexObject *self)
{
  g_return_val_if_fail (APEX_IS_OBJECT (self), NULL);

  return self->id;
}

/**
 * apex_object_set_id:
 * @self: an #ApexObject
 * @id: (nullable): a string containing the id, or %NULL
 *
 * Sets the #ApexObject:id property.
 */
void
apex_object_set_id (ApexObject  *self,
                    const gchar *id)
{
  g_return_if_fail (APEX_IS_OBJECT (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_ID]);
    }
}

/**
 * apex_object_get_name:
 * @self: an #ApexObject
 *
 * Returns: (transfer none): The object name, or %NULL
 */
const gchar *
apex_object_get_name (ApexObject *self)
{
  g_return_val_if_fail (APEX_IS_OBJECT (self), NULL);

  return self->name;
}

/**
 * apex_object_set_name:
 * @self: an #ApexObject
 * @name: (nullable): a string containing the name, or %NULL
 *
 * Sets the #ApexObject:name property.
 */
void
apex_object_set_name (ApexObject  *self,
                      const gchar *name)
{
  g_return_if_fail (APEX_IS_OBJECT (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_NAME]);
    }
}

/**
 * apex_object_add_object:
 * @self: an #ApexObject
 * @object: (transfer full): object to add
 */
void
apex_object_add_object (ApexObject *self,
                        ApexObject *object)
{
  GValue val = G_VALUE_INIT;
  gchar *id;

  g_return_if_fail (APEX_IS_OBJECT (self));
  g_return_if_fail (APEX_IS_OBJECT (object));

  if (self->objects == NULL)
    self->objects = apex_table_new (APEX_TYPE_OBJECT);

  g_value_init (&val, APEX_TYPE_OBJECT);
  /*g_value_set_object (&val, object);*/
  g_value_take_object (&val, object);
  id = g_strdup (apex_object_get_id (object));

  apex_table_add (self->objects, id, &val);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_OBJECTS]);

  g_value_unset (&val);
  g_free (id);
}

void
apex_object_remove_object (ApexObject  *self,
                           const gchar *id)
{
  g_return_if_fail (APEX_IS_OBJECT (self));
  g_return_if_fail (self->objects != NULL);

  apex_table_remove (self->objects, id);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_OBJECTS]);
}

/**
 * apex_object_lookup_object:
 * @self: an #ApexObject
 * @id: the ID of the object to retrieve
 *
 * Returns: (transfer none): The object if found, NULL otherwise.
 */
ApexObject *
apex_object_lookup_object (ApexObject  *self,
                           const gchar *id)
{
  g_return_val_if_fail (APEX_IS_OBJECT (self), NULL);
  g_return_val_if_fail (self->objects != NULL, NULL);

  return g_value_get_object (apex_table_get (self->objects, id));
}

gboolean
apex_object_has_object (ApexObject  *self,
                        const gchar *id)
{
  gboolean ret;

  g_return_val_if_fail (APEX_IS_OBJECT (self), FALSE);
  g_return_val_if_fail (self->objects != NULL, FALSE);
  g_return_val_if_fail (id != NULL, FALSE);

  ret = apex_table_has (self->objects, id);

  return ret;
}

/**
 * apex_object_add_property:
 * @self: an #ApexObject
 * @property: (transfer full): property to add
 */
void
apex_object_add_property (ApexObject   *self,
                          ApexProperty *property)
{
  GValue val = G_VALUE_INIT;
  gchar *key;

  g_return_if_fail (APEX_IS_OBJECT (self));
  g_return_if_fail (APEX_IS_PROPERTY (property));

  if (self->properties == NULL)
    self->properties = apex_table_new (APEX_TYPE_PROPERTY);

  g_value_init (&val, APEX_TYPE_PROPERTY);
  g_value_take_object (&val, property);
  key = g_strdup (apex_property_get_key (property));

  apex_table_add (self->properties, key, &val);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_PROPERTIES]);

  g_value_unset (&val);
  g_free (key);
}

void
apex_object_remove_property (ApexObject  *self,
                             const gchar *key)
{
  g_return_if_fail (APEX_IS_OBJECT (self));
  g_return_if_fail (self->properties != NULL);

  apex_table_remove (self->properties, key);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_PROPERTIES]);
}

/**
 * apex_object_lookup_property:
 * @self: an #ApexObject
 * @key: the key of the property to retrieve
 *
 * Returns: (transfer none): The property if found, NULL otherwise.
 */
ApexProperty *
apex_object_lookup_property (ApexObject  *self,
                             const gchar *key)
{
  g_return_val_if_fail (APEX_IS_OBJECT (self), NULL);
  g_return_val_if_fail (self->properties != NULL, NULL);

  return g_value_get_object (apex_table_get (self->properties, key));
}

gboolean
apex_object_has_property (ApexObject  *self,
                          const gchar *key)
{
  gboolean ret;

  g_return_val_if_fail (APEX_IS_OBJECT (self), FALSE);
  g_return_val_if_fail (self->properties != NULL, FALSE);
  g_return_val_if_fail (key != NULL, FALSE);

  ret = apex_table_has (self->properties, key);

  return ret;
}

/**
 * apex_object_get_objects:
 * @self: a #ApexObject
 *
 * Retrieve the #ApexTable containing an object list.
 *
 * Returns: (transfer none): a #ApexTable of objects if one is set.
 */
ApexTable *
apex_object_get_objects (ApexObject *self)
{
  ApexTable *objects;

  g_return_val_if_fail (APEX_IS_OBJECT (self), NULL);

  g_object_get (self, "objects", &objects, NULL);

  return objects;
}

/**
 * apex_object_ref_objects:
 *
 * Gets the object list, and returns a new reference
 * to the #ApexTable.
 *
 * Returns: (transfer full) (nullable): a #ApexTable or %NULL
 */
ApexTable *
apex_object_ref_objects (ApexObject *self)
{
  ApexTable *ret = NULL;

  g_return_val_if_fail (APEX_IS_OBJECT (self), NULL);

  g_set_object (&ret, self->objects);

  return g_steal_pointer (&ret);
}

/**
 * apex_object_set_objects:
 * @self: a #ApexObject
 * @objects: An #ApexTable of #ApexObject objects to set.
 */
void
apex_object_set_objects (ApexObject *self,
                         ApexTable  *objects)
{
  g_return_if_fail (APEX_IS_OBJECT (self));
  g_return_if_fail (APEX_IS_TABLE (self->objects));

  if (g_set_object (&self->objects, objects))
    g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_OBJECTS]);
}

/**
 * apex_object_get_properties:
 * @self: an #ApexProperty
 *
 * Retrieve the #ApexTable containing a property list.
 *
 * Returns: (transfer none): a #ApexTable of objects if one is set.
 */
ApexTable *
apex_object_get_properties (ApexObject *self)
{
  ApexTable *properties;

  g_return_val_if_fail (APEX_IS_OBJECT (self), NULL);

  g_object_get (self, "properties", &properties, NULL);

  return properties;
}

/**
 * apex_object_ref_properties:
 *
 * Gets the object list, and returns a new reference
 * to the #ApexTable.
 *
 * Returns: (transfer full) (nullable): a #ApexTable or %NULL
 */
ApexTable *
apex_object_ref_properties (ApexObject *self)
{
  ApexTable *ret = NULL;

  g_return_val_if_fail (APEX_IS_OBJECT (self), NULL);

  g_set_object (&ret, self->properties);

  return g_steal_pointer (&ret);
}

/**
 * apex_object_set_properties:
 * @self: an #ApexObject
 * @objects: An #ApexTable of #ApexProperty objects to set.
 */
void
apex_object_set_properties (ApexObject *self,
                            ApexTable  *properties)
{
  g_return_if_fail (APEX_IS_OBJECT (self));
  g_return_if_fail (APEX_IS_TABLE (self->properties));

  if (g_set_object (&self->properties, properties))
    g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_PROPERTIES]);
}
