/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libdcs, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-status.h"

/*#include "proto/v1/core.pb-c.h"*/

/*
 *message Status {
 *  bool enabled = 1;
 *  bool loaded = 2;
 *  bool active = 3;
 *  map<string, string> details = 4;
 *}
 */

struct _ApexStatus
{
  GObject     parent;

  gboolean    enabled;
  gboolean    loaded;
  gboolean    active;
  GHashTable *details;

  // XXX: not sure this is necessary anymore
  /*Apex__Status *pb;*/
};

enum {
  PROP_0,
  PROP_ENABLED,
  PROP_LOADED,
  PROP_ACTIVE,
  PROP_DETAILS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (ApexStatus, apex_status, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
apex_status_serialize_property (JsonSerializable *serializable,
                                const gchar      *name,
                                const GValue     *value,
                                GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "details") == 0)
    {
      GHashTable *details = NULL;
      GHashTableIter iter;
      JsonObject *obj = NULL;
      gpointer key, val;

      retval = json_node_new (JSON_NODE_OBJECT);

      g_return_val_if_fail (value != NULL, retval);
      g_return_val_if_fail (G_VALUE_HOLDS_POINTER (value), retval);

      details = g_value_get_pointer (value);

      g_return_val_if_fail (details != NULL, retval);

      obj = json_object_new ();

      if (details != NULL)
        {
          g_hash_table_iter_init (&iter, details);
          while(g_hash_table_iter_next (&iter, &key, &val))
            {
              json_object_set_string_member (obj,
                                             (const gchar *) key,
                                             (const gchar *) val);
            }
        }

      json_node_take_object (retval, obj);
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
apex_status_deserialize_property (JsonSerializable *serializable,
                                  const gchar      *name,
                                  GValue           *value,
                                  GParamSpec       *pspec,
                                  JsonNode         *property_node)
{
  gboolean retval = FALSE;

  if (g_strcmp0 (name, "details") == 0)
    {
      g_autoptr (GHashTable) details = NULL;
      g_autoptr (JsonObject) obj = NULL;
      JsonObjectIter iter;
      const gchar *key;
      JsonNode *val;

      obj = json_node_get_object (property_node);
      details = g_hash_table_new_full (g_str_hash,
                                       g_str_equal,
                                       g_free,
                                       g_free);

      json_object_ref (obj);

      json_object_iter_init (&iter, obj);
      while (json_object_iter_next (&iter, &key, &val))
        {
          g_hash_table_insert (details,
                               g_strdup (key),
                               g_strdup (json_node_get_string (val)));
        }

      g_value_set_pointer (value, g_hash_table_ref (details));

      retval = TRUE;
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = apex_status_serialize_property;
  iface->deserialize_property = apex_status_deserialize_property;
}

static void
apex_status_finalize (GObject *object)
{
  ApexStatus *self = (ApexStatus *)object;

  /*apex__status__free_unpacked (self->pb, NULL);*/

  g_clear_pointer (&self->details, g_hash_table_unref);

  G_OBJECT_CLASS (apex_status_parent_class)->finalize (object);
}

static void
apex_status_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  ApexStatus *self = APEX_STATUS (object);

  switch (prop_id)
    {
    case PROP_ENABLED:
      g_value_set_boolean (value, apex_status_get_enabled (self));
      break;

    case PROP_LOADED:
      g_value_set_boolean (value, apex_status_get_loaded (self));
      break;

    case PROP_ACTIVE:
      g_value_set_boolean (value, apex_status_get_active (self));
      break;

    case PROP_DETAILS:
      /*g_value_set_object (value, apex_status_get_details (self));*/
      g_value_set_pointer (value, self->details);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_status_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  ApexStatus *self = APEX_STATUS (object);

  switch (prop_id)
    {
    case PROP_ENABLED:
      apex_status_set_enabled (self, g_value_get_boolean (value));
      break;

    case PROP_LOADED:
      apex_status_set_loaded (self, g_value_get_boolean (value));
      break;

    case PROP_ACTIVE:
      apex_status_set_active (self, g_value_get_boolean (value));
      break;

    case PROP_DETAILS:
      /*apex_status_set_details (self, g_value_get_boxed (value));*/
      /* TODO: Free GHashTable memory with g_hash_table_free */
      apex_status_set_details (self, g_value_get_pointer (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_status_class_init (ApexStatusClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_status_finalize;
  object_class->get_property = apex_status_get_property;
  object_class->set_property = apex_status_set_property;

  properties [PROP_ENABLED] =
    g_param_spec_boolean ("enabled",
                          "Enabled",
                          "",
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_LOADED] =
    g_param_spec_boolean ("loaded",
                          "Loaded",
                          "",
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_ACTIVE] =
    g_param_spec_boolean ("active",
                          "Active",
                          "",
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_DETAILS] =
    g_param_spec_pointer ("details",
                          "Details",
                          "",
                          G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_status_init (ApexStatus *self)
{
  /*
   *self->pb = g_malloc (sizeof (Apex__Status));
   *apex__status__init (self->pb);
   */
}

ApexStatus *
apex_status_new (void)
{
  return g_object_new (APEX_TYPE_STATUS, NULL);
}

/*
 *ApexStatus *
 *apex_status_new_from_data (const guint8 *data)
 *{
 *  ApexStatus *object = g_object_new (APEX_TYPE_STATUS, NULL);
 *
 *  gsize len = sizeof (data);
 *  g_message ("unpacking %d bytes", len);
 *
 *  object->pb = apex__status__unpack (NULL, len, data);
 *  if (object->pb == NULL)
 *    {
 *      g_error ("error unpacking message data");
 *    }
 *
 *  return object;
 *}
 */

/*
 *guint8 *
 *apex_status_to_data (ApexStatus *self)
 *{
 *  g_return_val_if_fail (APEX_IS_STATUS (self), NULL);
 *
 *  gsize len = apex__status__get_packed_size (self->pb);
 *  guint8 *out = g_malloc (sizeof (guint8) * len);
 *
 *  if (apex__status__pack (self->pb, out) == 0)
 *    {
 *      g_warning ("message pack returned zero bytes");
 *    }
 *
 *  return out;
 *}
 */

gchar *
apex_status_serialize (ApexStatus *self)
{
  g_return_val_if_fail (APEX_IS_STATUS (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
apex_status_deserialize (ApexStatus  *self,
                         const gchar *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (GHashTable) details = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (APEX_TYPE_STATUS,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_STATUS (object));

  details = apex_status_get_details (APEX_STATUS (object));

  apex_status_set_enabled (self, apex_status_get_enabled (APEX_STATUS (object)));
  apex_status_set_loaded (self, apex_status_get_loaded (APEX_STATUS (object)));
  apex_status_set_active (self, apex_status_get_active (APEX_STATUS (object)));
  apex_status_set_details (self, details);

  g_clear_object (&object);
}

gboolean
apex_status_get_enabled (ApexStatus *self)
{
  g_return_val_if_fail (APEX_IS_STATUS (self), FALSE);

  return self->enabled;
}

void
apex_status_set_enabled (ApexStatus *self,
                        gboolean     enabled)
{
  g_return_if_fail (APEX_IS_STATUS (self));

  self->enabled = enabled;
}

gboolean
apex_status_get_loaded (ApexStatus *self)
{
  g_return_val_if_fail (APEX_IS_STATUS (self), FALSE);

  return self->loaded;
}

void
apex_status_set_loaded (ApexStatus *self,
                        gboolean    loaded)
{
  g_return_if_fail (APEX_IS_STATUS (self));

  self->loaded = loaded;
}

gboolean
apex_status_get_active (ApexStatus *self)
{
  g_return_val_if_fail (APEX_IS_STATUS (self), FALSE);

  return self->active;
}

void
apex_status_set_active (ApexStatus *self,
                        gboolean    active)
{
  g_return_if_fail (APEX_IS_STATUS (self));

  self->active = active;
}

GHashTable *
apex_status_get_details (ApexStatus *self)
{
  GHashTable *details;

  g_return_val_if_fail (APEX_IS_STATUS (self), NULL);

  g_object_get (self, "details", &details, NULL);

  return details;
}

void
apex_status_set_details (ApexStatus *self,
                         GHashTable *details)
{
  g_return_if_fail (APEX_IS_STATUS (self));

  if (self->details == details)
    return;

  if (details)
    g_hash_table_ref (details);

  if (self->details)
    g_hash_table_unref (self->details);

  self->details = details;

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DETAILS]);
}

void
apex_status_add_detail (ApexStatus  *self,
                        const gchar *key,
                        const gchar *value)
{
  g_return_if_fail (APEX_IS_STATUS (self));

  if (self->details == NULL)
    self->details = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);

  if (g_hash_table_contains (self->details, key))
    g_hash_table_replace (self->details, g_strdup (key), g_strdup (value));
  else
    g_hash_table_insert (self->details, g_strdup (key), g_strdup (value));
}

void
apex_status_remove_detail (ApexStatus  *self,
                           const gchar *key)
{
  g_return_if_fail (APEX_IS_STATUS (self));
  g_return_if_fail (self->details != NULL);

  if (g_hash_table_contains (self->details, key))
    g_hash_table_remove (self->details, key);
}

const gchar *
apex_status_get_detail (ApexStatus  *self,
                        const gchar *key)
{
  g_return_val_if_fail (APEX_IS_STATUS (self), NULL);
  g_return_val_if_fail (self->details != NULL, NULL);

  if (g_hash_table_contains (self->details, key))
    return g_hash_table_lookup (self->details, key);

  return NULL;
}
