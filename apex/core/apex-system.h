/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_SYSTEM apex_system_get_type ()
G_DECLARE_FINAL_TYPE (ApexSystem, apex_system, APEX, SYSTEM, GObject)

ApexSystem  *apex_system_new          (void);

gchar       *apex_system_serialize    (ApexSystem  *self);
void         apex_system_deserialize  (ApexSystem  *self,
                                       const gchar *data);

/*
 *ApexService *apex_system_get_master   (ApexSystem  *self);
 *void         apex_system_set_master   (ApexSystem  *self,
 *                                       ApexService *master);
 *
 *ApexService *apex_system_get_broker   (ApexSystem  *self);
 *void         apex_system_set_broker   (ApexSystem  *self,
 *                                       ApexService *broker);
 *
 *GHashTable  *apex_system_get_services (ApexSystem  *self);
 *void         apex_system_set_services (ApexSystem  *self,
 *                                       GHashTable  *services);
 *
 *GHashTable  *apex_system_get_modules  (ApexSystem  *self);
 *void         apex_system_set_modules  (ApexSystem  *self,
 *                                       GHashTable  *modules);
 */

G_END_DECLS
