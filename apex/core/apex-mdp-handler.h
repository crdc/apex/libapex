/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_MDP_HANDLER apex_mdp_handler_get_type ()
G_DECLARE_FINAL_TYPE (ApexMdpHandler, apex_mdp_handler, APEX, MDP_HANDLER, GObject)

ApexMdpHandler *apex_mdp_handler_new (void);

G_END_DECLS
