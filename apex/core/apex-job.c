/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-job.h"

/**
 * SECTION:apex-job
 * @short_description: Job base class
 *
 * An #ApexJob is a structure for the Job message type, and also as a runnable
 * object that provides a means for launching asynchronous jobs by an
 * #ApexApplication.
 */

/**
 * ApexJob:
 *
 * #ApexJob is an opaque data structure and can only be accessed using the
 * following functions.
 */

/**
 * ApexJobClass:
 * @task: invoked by an #ApexApplication as a thread callback.
 *
 * Virtual function table for #ApexJob.
 */

typedef struct
{
  GObject            parent;
  gchar             *id;
  // TODO: resolve this, should probably be an actual status type, but it's
  // currently an enum in the API
  /*ApexStatus        *status;*/
  /*ApexJobStatusType  status;*/
  gint               priority;
} ApexJobPrivate;

enum {
  PROP_0,
  PROP_ID,
  /*PROP_STATUS,*/
  PROP_PRIORITY,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE_WITH_PRIVATE (ApexJob, apex_job, G_TYPE_OBJECT)

static void
apex_job_finalize (GObject *object)
{
  ApexJob *self = (ApexJob *)object;
  ApexJobPrivate *priv = apex_job_get_instance_private (self);

  g_clear_pointer (&priv->id, g_free);

  G_OBJECT_CLASS (apex_job_parent_class)->finalize (object);
}

static void
apex_job_get_property (GObject    *object,
                       guint       prop_id,
                       GValue     *value,
                       GParamSpec *pspec)
{
  ApexJob *self = APEX_JOB (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, apex_job_get_id (self));
      break;

    /*
     *case PROP_STATUS:
     *  g_value_set_enum (value, apex_job_get_status (self));
     *  break;
     */

    case PROP_PRIORITY:
      g_value_set_int (value, apex_job_get_priority (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_job_set_property (GObject      *object,
                       guint         prop_id,
                       const GValue *value,
                       GParamSpec   *pspec)
{
  ApexJob *self = APEX_JOB (object);

  switch (prop_id)
    {
    case PROP_ID:
      apex_job_set_id (self, g_value_get_string (value));
      break;

    /*
     *case PROP_STATUS:
     *  apex_job_set_status (self, g_value_get_boxed (value));
     *  break;
     */

    case PROP_PRIORITY:
      apex_job_set_priority(self, g_value_get_int (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_job_class_init (ApexJobClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_job_finalize;
  object_class->get_property = apex_job_get_property;
  object_class->set_property = apex_job_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The job ID.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_PRIORITY] =
    g_param_spec_int ("priority",
                      "Priority",
                      "The job priority.",
                      G_MININT,
                      G_MAXINT,
                      100,
                      (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_job_init (ApexJob *self)
{
  ApexJobPrivate *priv;

  priv = apex_job_get_instance_private (self);

  priv->id = g_uuid_string_random ();
}

ApexJob *
apex_job_new (void)
{
  return g_object_new (APEX_TYPE_JOB, NULL);
}

gchar *
apex_job_serialize (ApexJob *self)
{
  g_return_val_if_fail (APEX_IS_JOB (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
apex_job_deserialize (ApexJob     *self,
                      const gchar *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_JOB,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_JOB (object));

  apex_job_set_id (self, apex_job_get_id (APEX_JOB (object)));
  apex_job_set_priority (self, apex_job_get_priority (APEX_JOB (object)));

  g_object_unref (object);
}

/**
 * apex_job_task:
 * @self: an #ApexJob
 */
void
apex_job_task (ApexJob *self)
{
  ApexJobClass *klass;

  g_return_if_fail (APEX_IS_JOB (self));

  klass = APEX_JOB_GET_CLASS (self);
  g_return_if_fail (klass->task != NULL);

  klass->task (self);
}

const gchar *
apex_job_get_id (ApexJob *self)
{
  ApexJobPrivate *priv;

  g_return_val_if_fail (APEX_IS_JOB (self), NULL);

  priv = apex_job_get_instance_private (self);
  return priv->id;
}

void
apex_job_set_id (ApexJob     *self,
                 const gchar *id)
{
  ApexJobPrivate *priv;

  g_return_if_fail (APEX_IS_JOB (self));

  priv = apex_job_get_instance_private (self);

  if (g_strcmp0 (id, priv->id) != 0)
    {
      g_free (priv->id);
      priv->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}

gint
apex_job_get_priority (ApexJob *self)
{
  ApexJobPrivate *priv;

  g_return_val_if_fail (APEX_IS_JOB (self), -1);

  priv = apex_job_get_instance_private (self);

  return priv->priority;
}

void
apex_job_set_priority (ApexJob *self,
                       gint     priority)
{
  ApexJobPrivate *priv;

  g_return_if_fail (APEX_IS_JOB (self));

  priv = apex_job_get_instance_private (self);

  priv->priority = priority;
}
