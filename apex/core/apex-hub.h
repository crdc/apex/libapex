/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_HUB apex_hub_get_type ()
G_DECLARE_FINAL_TYPE (ApexHub, apex_hub, APEX, HUB, GObject)

ApexHub *apex_hub_new         (const gchar *frontend,
                               const gchar *backend);

void    apex_hub_start        (ApexHub     *self);
void    apex_hub_stop         (ApexHub     *self);

gchar  *apex_hub_dup_frontend (ApexHub     *self);
void    apex_hub_set_frontend (ApexHub     *self,
                               const gchar *frontend);

gchar  *apex_hub_dup_backend  (ApexHub     *self);
void    apex_hub_set_backend  (ApexHub     *self,
                               const gchar *backend);

G_END_DECLS
