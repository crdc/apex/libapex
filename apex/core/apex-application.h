/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib.h>
#include <gio/gio.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_APPLICATION apex_application_get_type ()
G_DECLARE_DERIVABLE_TYPE (ApexApplication, apex_application, APEX, APPLICATION, GApplication)

struct _ApexApplicationClass
{
  /*< private >*/
  GApplicationClass parent_class;

  /*< public >*/
  /* signals */
  void                       (*property_changed)  (ApexApplication  *self,
                                                   ApexProperty     *property);

  /* vfuncs */
  ApexConfigurationResponse *(*get_configuration) (ApexApplication  *self,
                                                   GError          **error);

  ApexStatusResponse        *(*get_status)        (ApexApplication  *self,
                                                   GError          **error);

  ApexSettingsResponse      *(*get_settings)      (ApexApplication  *self,
                                                   GError          **error);

  ApexJobResponse           *(*get_job)           (ApexApplication  *self,
                                                   const gchar      *job_id,
                                                   GError          **error);

  ApexJobsResponse          *(*get_jobs)          (ApexApplication  *self,
                                                   GError          **error);

  ApexJobResponse           *(*get_active_job)    (ApexApplication  *self,
                                                   GError          **error);

  ApexJobResponse           *(*cancel_job)        (ApexApplication  *self,
                                                   const gchar      *job_id,
                                                   GError          **error);

  ApexJobResponse           *(*submit_job)        (ApexApplication  *self,
                                                   const gchar      *job_id,
                                                   const gchar      *job_value,
                                                   GHashTable       *job_properties,
                                                   GError          **error);

  ApexJobResponse           *(*submit_event)      (ApexApplication  *self,
                                                   const gint        event_id,
                                                   GError          **error);

  ApexEventResponse         *(*available_events)  (ApexApplication  *self,
                                                   GError          **error);

  /*< private >*/
  gpointer padding[12];
};

ApexApplication           *apex_application_new               (const gchar       *application_id,
                                                               GApplicationFlags  flags);

ApexConfigurationResponse *apex_application_get_configuration (ApexApplication    *self,
                                                               GError            **error);

ApexStatusResponse        *apex_application_get_status        (ApexApplication    *self,
                                                               GError            **error);

ApexSettingsResponse      *apex_application_get_settings      (ApexApplication    *self,
                                                               GError            **error);

ApexJobResponse           *apex_application_get_job           (ApexApplication    *self,
                                                               const gchar        *job_id,
                                                               GError            **error);

ApexJobsResponse          *apex_application_get_jobs          (ApexApplication    *self,
                                                               GError            **error);

ApexJobResponse           *apex_application_get_active_job    (ApexApplication    *self,
                                                               GError            **error);

ApexJobResponse           *apex_application_cancel_job        (ApexApplication    *self,
                                                               const gchar        *job_id,
                                                               GError            **error);

ApexJobResponse           *apex_application_submit_job        (ApexApplication    *self,
                                                               const gchar        *job_id,
                                                               const gchar        *job_value,
                                                               GHashTable         *job_properties,
                                                               GError            **error);

ApexJobResponse           *apex_application_submit_event      (ApexApplication    *self,
                                                               const gint          event_id,
                                                               GError            **error);

ApexEventResponse         *apex_application_available_events  (ApexApplication    *self,
                                                               GError            **error);

void                       apex_application_load_config       (ApexApplication    *self,
                                                               const gchar        *config,
                                                               GError            **error);

ApexConfiguration         *apex_application_get_loaded_config (ApexApplication    *self);

void                       apex_application_add_property      (ApexApplication    *self,
                                                               ApexProperty       *property);
void                       apex_application_remove_property   (ApexApplication    *self,
                                                               const gchar        *key);
ApexProperty              *apex_application_lookup_property   (ApexApplication    *self,
                                                               const gchar        *key);
gboolean                   apex_application_update_property   (ApexApplication    *self,
                                                               const gchar        *key,
                                                               const gchar        *value);
gboolean                   apex_application_has_property      (ApexApplication    *self,
                                                               const gchar        *key);

void                       apex_application_add_sink          (ApexApplication    *self,
                                                               const gchar        *name,
                                                               ApexSink           *sink);
void                       apex_application_remove_sink       (ApexApplication    *self,
                                                               const gchar        *name);
ApexSink                  *apex_application_get_sink          (ApexApplication    *self,
                                                               const gchar        *name);
gboolean                   apex_application_has_sink          (ApexApplication    *self,
                                                               const gchar        *name);

void                       apex_application_add_source        (ApexApplication    *self,
                                                               const gchar        *name,
                                                               ApexSource         *source);
void                       apex_application_remove_source     (ApexApplication    *self,
                                                               const gchar        *name);
ApexSource                *apex_application_get_source        (ApexApplication    *self,
                                                               const gchar        *name);
gboolean                   apex_application_has_source        (ApexApplication    *self,
                                                               const gchar        *name);

void                       apex_application_send_event        (ApexApplication    *self,
                                                               ApexEvent          *event,
                                                               GError            **error);
void                       apex_application_send_metric       (ApexApplication    *self,
                                                               ApexMetric         *metric,
                                                               GError            **error);

void                       apex_application_enqueue_job       (ApexApplication    *self,
                                                               ApexJob            *job);
void                       apex_application_register_job      (ApexApplication    *self,
                                                               ApexJob            *job);

const gchar               *apex_application_get_id            (ApexApplication    *self);
void                       apex_application_set_id            (ApexApplication    *self,
                                                               const gchar        *id);

const gchar               *apex_application_get_endpoint      (ApexApplication    *self);
void                       apex_application_set_endpoint      (ApexApplication    *self,
                                                               const gchar        *endpoint);

const gchar               *apex_application_get_service       (ApexApplication    *self);
void                       apex_application_set_service       (ApexApplication    *self,
                                                               const gchar        *service);

G_END_DECLS
