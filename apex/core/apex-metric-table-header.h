/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_METRIC_TABLE_HEADER apex_metric_table_header_get_type ()
G_DECLARE_FINAL_TYPE (ApexMetricTableHeader, apex_metric_table_header, APEX, METRIC_TABLE_HEADER, GObject)

ApexMetricTableHeader *apex_metric_table_header_new           (const gchar           *name);

const gchar           *apex_metric_table_header_serialize     (ApexMetricTableHeader *self);
void                   apex_metric_table_header_deserialize   (ApexMetricTableHeader *self,
                                                               const gchar           *data);

const gchar           *apex_metric_table_header_get_name      (ApexMetricTableHeader *self);
gchar                 *apex_metric_table_header_dup_name      (ApexMetricTableHeader *self);
void                   apex_metric_table_header_set_name      (ApexMetricTableHeader *self,
                                                               const gchar           *name);

ApexTable             *apex_metric_table_header_get_columns   (ApexMetricTableHeader *self);
ApexTable             *apex_metric_table_header_ref_columns   (ApexMetricTableHeader *self);
void                   apex_metric_table_header_set_columns   (ApexMetricTableHeader *self,
                                                               ApexTable             *columns);

gboolean               apex_metric_table_header_add_column    (ApexMetricTableHeader *self,
                                                               const gchar           *key,
                                                               gdouble                value);
gboolean               apex_metric_table_header_remove_column (ApexMetricTableHeader *self,
                                                               const gchar           *key);
gdouble                apex_metric_table_header_get_column    (ApexMetricTableHeader *self,
                                                               const gchar           *key);
gboolean               apex_metric_table_header_has_column    (ApexMetricTableHeader *self,
                                                               const gchar           *key);

G_END_DECLS
