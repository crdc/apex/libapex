/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_CONFIGURATION apex_configuration_get_type ()
G_DECLARE_FINAL_TYPE (ApexConfiguration, apex_configuration, APEX, CONFIGURATION, GObject)

ApexConfiguration          *apex_configuration_new             (void);

gchar                      *apex_configuration_serialize       (ApexConfiguration          *self);
void                        apex_configuration_deserialize     (ApexConfiguration          *self,
                                                                const gchar                *data);

void                        apex_configuration_load            (ApexConfiguration          *self,
                                                                const gchar                *filename,
                                                                GError                    **error);
void                        apex_configuration_save            (ApexConfiguration          *self,
                                                                const gchar                *filename,
                                                                GError                    **error);

const gchar                *apex_configuration_get_id          (ApexConfiguration          *self);
void                        apex_configuration_set_id          (ApexConfiguration          *self,
                                                                const gchar                *id);

ApexConfigurationNamespace  apex_configuration_get_namespace   (ApexConfiguration          *self);
void                        apex_configuration_set_namespace   (ApexConfiguration          *self,
                                                                ApexConfigurationNamespace  namespace);

void                        apex_configuration_add_object      (ApexConfiguration          *self,
                                                                ApexObject                 *object);
void                        apex_configuration_remove_object   (ApexConfiguration          *self,
                                                                const gchar                *id);

ApexObject                 *apex_configuration_lookup_object   (ApexConfiguration          *self,
                                                                const gchar                *id);
gboolean                    apex_configuration_has_object      (ApexConfiguration          *self,
                                                                const gchar                *id);

void                        apex_configuration_add_property    (ApexConfiguration          *self,
                                                                ApexProperty               *property);
void                        apex_configuration_remove_property (ApexConfiguration          *self,
                                                                const gchar                *key);
ApexProperty               *apex_configuration_lookup_property (ApexConfiguration          *self,
                                                                const gchar                *key);
gboolean                    apex_configuration_has_property    (ApexConfiguration          *self,
                                                                const gchar                *key);

GHashTable                 *apex_configuration_get_properties  (ApexConfiguration          *self);
ApexTable                  *apex_configuration_ref_properties  (ApexConfiguration          *self);
void                        apex_configuration_set_properties  (ApexConfiguration          *self,
                                                                ApexTable                  *properties);

GHashTable                 *apex_configuration_get_objects     (ApexConfiguration          *self);
ApexTable                  *apex_configuration_ref_objects     (ApexConfiguration          *self);
void                        apex_configuration_set_objects     (ApexConfiguration          *self,
                                                                ApexTable                  *objects);

G_END_DECLS
