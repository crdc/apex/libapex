/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_JOB apex_job_get_type ()
G_DECLARE_DERIVABLE_TYPE (ApexJob, apex_job, APEX, JOB, GObject)

struct _ApexJobClass
{
  /*< private >*/
  GObjectClass parent_class;

  /* vfuncs */

  void (*task) (ApexJob *self);

  /*< private >*/
  gpointer padding[12];
};

ApexJob     *apex_job_new          (void);

gchar       *apex_job_serialize    (ApexJob     *self);
void         apex_job_deserialize  (ApexJob     *self,
                                    const gchar *data);

void         apex_job_task         (ApexJob     *self);

const gchar *apex_job_get_id       (ApexJob     *self);
void         apex_job_set_id       (ApexJob     *self,
                                    const gchar *id);

gint         apex_job_get_priority (ApexJob     *self);
void         apex_job_set_priority (ApexJob     *self,
                                    gint         priority);

G_END_DECLS
