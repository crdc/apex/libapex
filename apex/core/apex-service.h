/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_SERVICE apex_service_get_type ()
G_DECLARE_DERIVABLE_TYPE (ApexService, apex_service, APEX, SERVICE, GObject)

struct _ApexServiceClass
{
  /*< private >*/
  GObjectClass parent_class;

  /*< public >*/

  /*< private >*/
  gpointer padding[12];
};

ApexService *apex_service_new            (const gchar *name);
ApexService *apex_service_new_with_model (const gchar *name,
                                          ApexModel   *model);

gchar       *apex_service_dup_name       (ApexService *self);
void         apex_service_set_name       (ApexService *self,
                                          const gchar *name);

ApexStatus  *apex_service_ref_status     (ApexService *self);
void         apex_service_set_status     (ApexService *self,
                                          ApexStatus  *status);

/*
 *ApexState   *apex_service_ref_state      (ApexService *self);
 *void         apex_service_set_state      (ApexService *self,
 *                                          ApexState   *state);
 */

G_END_DECLS
