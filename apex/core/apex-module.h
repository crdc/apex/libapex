/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib.h>

#include <apex/apex-types.h>

/*
 *message Module {
 *  string id = 1;
 *  string name = 2;
 *  string service_name = 3;
 *  string configuration_id = 4;
 *  Status status = 5;
 *  State state = 6;
 *  repeated string services = 7;
 *}
 */

G_BEGIN_DECLS

#define APEX_TYPE_MODULE apex_module_get_type ()
G_DECLARE_FINAL_TYPE (ApexModule, apex_module, APEX, MODULE, GObject)

ApexModule  *apex_module_new                  (void);

const gchar *apex_module_get_id               (ApexModule  *self);
void         apex_module_set_id               (ApexModule  *self,
                                               const gchar *id);

const gchar *apex_module_get_name             (ApexModule  *self);
void         apex_module_set_name             (ApexModule  *self,
                                               const gchar *name);

const gchar *apex_module_get_service_name     (ApexModule  *self);
void         apex_module_set_service_name     (ApexModule  *self,
                                               const gchar *service_name);

const gchar *apex_module_get_configuration_id (ApexModule  *self);
void         apex_module_set_configuration_id (ApexModule  *self,
                                               const gchar *configuration_id);

ApexStatus  *apex_module_get_status           (ApexModule  *self);
void         apex_module_set_status           (ApexModule  *self,
                                               ApexStatus  *status);

ApexState   *apex_module_get_state            (ApexModule  *self);
void         apex_module_set_state            (ApexModule  *self,
                                               ApexState   *state);

/*
 *GPtrArray   *apex_module_get_services         (ApexModule  *self);
 *void         apex_module_set_services         (ApexModule  *self,
 *                                               GPtrArray    services);
 */

G_END_DECLS
