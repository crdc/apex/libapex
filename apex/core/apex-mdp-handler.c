/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "apex-mdp-handler"

#include <apex-debug.h>

#include "apex-mdp-handler.h"
#include "apex-handler.h"
#include "apex-application.h"
#include "mdp/apex-worker.h"
#include "message/apex-message.h"

struct _ApexMdpHandler
{
  GObject          parent;
  gboolean         running;
  gboolean         cancelled;
  ApexApplication *app;
};

static void apex_handler_interface_init (gpointer *iface);

G_DEFINE_TYPE_WITH_CODE (ApexMdpHandler, apex_mdp_handler, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (APEX_TYPE_HANDLER,
                                                apex_handler_interface_init));

static void     s_handler_async  (ApexMdpHandler      *self,
                                  GCancellable        *cancellable,
                                  GAsyncReadyCallback  callback,
                                  gpointer             user_data);
static void     s_handler_cancel (ApexMdpHandler *self,
                                  gpointer        user_data);
static gboolean s_handler_finish (ApexMdpHandler  *self,
                                  GAsyncResult    *result,
                                  GError         **error);

/**
 * apex_mdp_handler_run:
 * @self: an #ApexMdpHandler
 * @user_data: a #gpointer that expects an #ApexApplication
 * @error: return location for a GError, or NULL
 *
 * Starts the thread that monitors the messages received from the parent
 * service.
 */
static void
apex_mdp_handler_run (ApexHandler  *self,
                      gpointer      user_data,
                      GError      **error)
{
  APEX_MDP_HANDLER (self)->app = APEX_APPLICATION (user_data);
  s_handler_async (APEX_MDP_HANDLER (self), NULL, (GAsyncReadyCallback) s_handler_finish, NULL);
}

/**
 * apex_mdp_handler_cancel:
 * @self: an #ApexMdpHandler
 *
 * Stops the thread that monitors the messages received from the parent
 * service.
 */
static void
apex_mdp_handler_cancel (ApexHandler *self)
{
  s_handler_cancel (APEX_MDP_HANDLER (self), NULL);
}

static void
apex_handler_interface_init (gpointer *g_iface)
{
  ApexHandlerInterface *iface = (ApexHandlerInterface *) g_iface;

  iface->run = apex_mdp_handler_run;
  iface->cancel = apex_mdp_handler_cancel;
}

static void
apex_mdp_handler_finalize (GObject *object)
{
  G_GNUC_UNUSED ApexMdpHandler *self = (ApexMdpHandler *)object;

  G_OBJECT_CLASS (apex_mdp_handler_parent_class)->finalize (object);
}

static void
apex_mdp_handler_class_init (ApexMdpHandlerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_mdp_handler_finalize;
}

static void
apex_mdp_handler_init (ApexMdpHandler *self)
{
}

ApexMdpHandler *
apex_mdp_handler_new (void)
{
  return g_object_new (APEX_TYPE_MDP_HANDLER, NULL);
}

// ---

static gchar *
apex_mdp_handler_get_configuration (ApexMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (ApexConfigurationResponse) response = NULL;
  gchar *data;

  response = apex_application_get_configuration (self->app, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `get-configuration': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      g_assert (APEX_IS_CONFIGURATION_RESPONSE (response));
      data = apex_configuration_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
apex_mdp_handler_get_property (ApexMdpHandler      *self,
                               ApexPropertyRequest *request)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (ApexPropertyResponse) response = NULL;
  ApexProperty *property = NULL;
  const gchar *key;
  gchar *data;

  response = apex_property_response_new ();
  key = apex_property_request_get_key (request);
  property = apex_application_lookup_property (self->app, key);
  apex_property_response_set (response, property);

  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `get-property': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = apex_property_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
apex_mdp_handler_set_property (ApexMdpHandler      *self,
                               ApexPropertyRequest *request)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (ApexPropertyResponse) response = NULL;
  ApexProperty *property;
  const gchar *key;
  const gchar *value;
  gchar *data;

  response = apex_property_response_new ();
  key = apex_property_request_get_key (request);
  value = apex_property_request_get_value (request);

  property = apex_application_lookup_property (self->app, key);
  apex_application_update_property (self->app, key, value);
  apex_property_response_set (response, property);

  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `set-property': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = apex_property_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
apex_mdp_handler_get_properties (ApexMdpHandler        *self,
                                 ApexPropertiesRequest *request)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (ApexPropertiesResponse) response = NULL;
  g_autoptr (ApexMessageError) err = NULL;
  GHashTable *props = NULL;
  GHashTableIter iter;
  gpointer key, val;
  gchar *data;

  response = apex_properties_response_new ();
  err = apex_message_error_new ();
  props = apex_properties_request_get_list (request);

  g_hash_table_iter_init (&iter, props);
  while (g_hash_table_iter_next (&iter, &key, &val))
    {
      if (apex_application_has_property (self->app, key))
        {
          apex_properties_response_add (response,
              apex_application_lookup_property (self->app, key));
        }
    }

  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      //g_assert (response == NULL);
      apex_message_error_set_code (err, 500);
      g_warning ("Error during `get-properties': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      apex_message_error_set_code (err, 200);
    }

  // there should always be a response
  apex_response_set_error (APEX_RESPONSE (response), err);
  data = apex_properties_response_serialize (response);

  return g_steal_pointer (&data);
}

static gchar *
apex_mdp_handler_set_properties (ApexMdpHandler        *self,
                                 ApexPropertiesRequest *request)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (ApexPropertiesResponse) response = NULL;
  GHashTable *props = NULL;
  GHashTableIter iter;
  gpointer key, val;
  gchar *data;

  response = apex_properties_response_new ();
  props = apex_properties_request_get_list (request);

  g_hash_table_iter_init (&iter, props);
  while(g_hash_table_iter_next (&iter, &key, &val))
    {
      if (apex_application_has_property (self->app, key))
        {
          apex_application_update_property (self->app, key, val);
          apex_properties_response_add (response,
              apex_application_lookup_property (self->app, key));
        }
    }

  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `set-properties': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = apex_properties_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
apex_mdp_handler_get_status (ApexMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (ApexStatusResponse) response = NULL;
  gchar *data;

  response = apex_application_get_status (self->app, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `get-status': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      /*data = apex_status_response_serialize (response);*/
    }

  return g_steal_pointer (&data);
}

static gchar *
apex_mdp_handler_get_settings (ApexMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (ApexSettingsResponse) response = NULL;
  gchar *data;

  response = apex_application_get_settings (self->app, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `get-settings': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = apex_settings_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
apex_mdp_handler_get_job (ApexMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  // TODO: deserialize request message
  // TODO: get job_id from request message
  const gchar *job_id = "job0";
  g_autoptr (ApexJobResponse) response = NULL;
  gchar *data;

  response = apex_application_get_job (self->app, job_id, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `get-job': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = apex_job_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
apex_mdp_handler_get_jobs (ApexMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (ApexJobsResponse) response = NULL;
  gchar *data;

  response = apex_application_get_jobs (self->app, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `get-jobs': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = apex_jobs_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
apex_mdp_handler_get_active_job (ApexMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (ApexJobResponse) response = NULL;
  gchar *data;

  response = apex_application_get_active_job (self->app, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `get-active-job': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = apex_job_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
apex_mdp_handler_cancel_job (ApexMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  // TODO: deserialize request message
  // TODO: get job_id from request message
  const gchar *job_id = "job0";
  g_autoptr (ApexJobResponse) response = NULL;
  gchar *data;

  response = apex_application_cancel_job (self->app, job_id, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `cancel-job': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = apex_job_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
apex_mdp_handler_submit_job (ApexMdpHandler       *self,
                             ApexModuleJobRequest *request)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (ApexJobResponse) response = NULL;
  const gchar *job_id;
  const gchar *job_value;
  GHashTable *job_properties;
  gchar *data;

  job_id = apex_module_job_request_get_job_id (request);
  job_value = apex_module_job_request_get_job_value (request);
  job_properties = apex_module_job_request_get_list (request);

  response = apex_application_submit_job (self->app,
                                          job_id,
                                          job_value,
                                          job_properties,
                                          &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `submit-job': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      // TODO: check that response->job != NULL
      apex_application_enqueue_job (self->app,
                                    apex_job_response_get_job (response));
      data = apex_job_response_serialize (response);
      APEX_TRACE_MSG ("data: %s", data);
    }

  return g_steal_pointer (&data);
}

static gchar *
apex_mdp_handler_submit_event (ApexMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  // TODO: deserialize request message
  // TODO: get job_id from request message
  const gint event_id = 0;
  g_autoptr (ApexJobResponse) response = NULL;
  gchar *data;

  response = apex_application_submit_event (self->app, event_id, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `submit-event': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = apex_job_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
apex_mdp_handler_available_events (ApexMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (ApexEventResponse) response = NULL;
  gchar *data;

  response = apex_application_available_events (self->app, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `available-events': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      /*data = apex_event_response_serialize (response);*/
    }

  return g_steal_pointer (&data);
}

// ---

__attribute__ ((__noreturn__))
static void
s_handler_cb (GObject      *source_object,
              GAsyncResult *result,
              gpointer      user_data)
{
  // g_task_run_in_thread_sync shouldn't get there
  /*g_assert_not_reached ();*/

  g_debug ("EHRMAHGHERD");
}

static void
s_handler_cancel (ApexMdpHandler *self,
                  gpointer        data)
{
  GCancellable *cancellable;

  if (self->cancelled)
    return;

  cancellable = G_CANCELLABLE (data);
  g_cancellable_cancel (cancellable);

  self->cancelled = TRUE;

  // TODO: something more robust may be in order here
  self->running = FALSE;

  g_debug ("message handler cancelled");
}

// TODO: consider moving this into interface and just provide handlers in implementation
static void
s_handler_thread (GTask        *task,
                  gpointer      source_object,
                  gpointer      task_data,
                  GCancellable *cancellable)
{
  g_autoptr (ApexWorker) worker = NULL;
  /*gboolean shutdown_requested = FALSE;*/

  // XXX: should these just be checks that fail gracefully?
  g_assert (source_object == g_task_get_source_object (task));
  g_assert (task_data == g_task_get_task_data (task));
  g_assert (cancellable == g_task_get_cancellable (task));

  g_application_hold (G_APPLICATION (APEX_MDP_HANDLER (source_object)->app));

  APEX_MDP_HANDLER (source_object)->cancelled = FALSE;
  APEX_MDP_HANDLER (source_object)->running = TRUE;
  worker = apex_worker_new (apex_application_get_endpoint (APEX_MDP_HANDLER (source_object)->app),
                            apex_application_get_service (APEX_MDP_HANDLER (source_object)->app));

  // XXX: shouldn't be necessary, here to test signals
  g_usleep (100000);
  apex_worker_connect (worker);

  while (APEX_MDP_HANDLER (source_object)->running)
    {
      g_autoptr (GError) error = NULL;
      g_autofree gchar *message = NULL;
      g_autofree gchar *body = NULL;
      g_autofree gchar *data = NULL;
      zframe_t *reply;
      zmsg_t *request;

      request = apex_worker_recv (worker, &reply);

      /* Check if worker was interrupted */
      if (request == NULL)
        break;

      message = zframe_strdup (zmsg_first (request));
      body = zframe_strdup (zmsg_next (request));

      APEX_TRACE_MSG ("message handler received message: %s", message);
      APEX_TRACE_MSG ("%s", body);

      if (g_strcmp0 (message, "get-configuration") == 0)
        {
          data = apex_mdp_handler_get_configuration (APEX_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "get-property") == 0)
        {
          g_autoptr (ApexPropertyRequest) req = NULL;

          req = apex_property_request_new (NULL, NULL, NULL);
          apex_property_request_deserialize (req, body);
          data = apex_mdp_handler_get_property (APEX_MDP_HANDLER (source_object), req);
        }
      else if (g_strcmp0 (message, "set-property") == 0)
        {
          g_autoptr (ApexPropertyRequest) req = NULL;

          req = apex_property_request_new (NULL, NULL, NULL);
          apex_property_request_deserialize (req, body);
          data = apex_mdp_handler_set_property (APEX_MDP_HANDLER (source_object), req);
        }
      else if (g_strcmp0 (message, "get-properties") == 0)
        {
          g_autoptr (ApexPropertiesRequest) req = NULL;

          req = apex_properties_request_new (NULL);
          apex_properties_request_deserialize(req, body);
          data = apex_mdp_handler_get_properties (APEX_MDP_HANDLER (source_object), req);
        }
      else if (g_strcmp0 (message, "set-properties") == 0)
        {
          g_autoptr (ApexPropertiesRequest) req = NULL;

          req = apex_properties_request_new (NULL);
          apex_properties_request_deserialize(req, body);
          data = apex_mdp_handler_set_properties (APEX_MDP_HANDLER (source_object), req);
        }
      else if (g_strcmp0 (message, "get-status") == 0)
        {
          data = apex_mdp_handler_get_status (APEX_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "get-settings") == 0)
        {
          data = apex_mdp_handler_get_settings (APEX_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "get-job") == 0)
        {
          data = apex_mdp_handler_get_job (APEX_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "get-jobs") == 0)
        {
          data = apex_mdp_handler_get_jobs (APEX_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "get-active-job") == 0)
        {
          data = apex_mdp_handler_get_active_job (APEX_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "cancel-job") == 0)
        {
          data = apex_mdp_handler_cancel_job (APEX_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "submit-job") == 0)
        {
          g_autoptr (ApexModuleJobRequest) req = NULL;

          req = apex_module_job_request_new ();
          apex_module_job_request_deserialize (req, body);
          data = apex_mdp_handler_submit_job (APEX_MDP_HANDLER (source_object), req);
        }
      else if (g_strcmp0 (message, "submit-event") == 0)
        {
          data = apex_mdp_handler_submit_event (APEX_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "available-events") == 0)
        {
          data = apex_mdp_handler_available_events (APEX_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "shutdown") == 0)
        {
          // XXX: this way could be causing the occasional need for a double call
          /*shutdown_requested = TRUE;*/
          data = g_strdup ("{\"shutdown\":true}");
          g_debug ("received shutdown");
          g_signal_emit_by_name (APEX_MDP_HANDLER (source_object), "shutdown-requested");
          s_handler_cancel (APEX_MDP_HANDLER (source_object), cancellable);
        }
      else
        {
          g_autoptr (ApexResponse) response = NULL;
          g_autoptr (ApexMessageError) err = NULL;
          response = apex_response_new ();
          err = apex_message_error_new ();
          apex_message_error_set_code (err, 501);
          apex_response_set_error (response, err);
          data = apex_response_serialize (response);
        }

      // Add data to response
      if (data)
        {
          zmsg_remove (request, zmsg_last (request));
          zmsg_addstr (request, data);
        }

      // Send response
      apex_worker_send (worker, &request, reply);
      zframe_destroy (&reply);
      /*zmsg_destroy (&request);*/
    }

  g_application_release (G_APPLICATION (APEX_MDP_HANDLER (source_object)->app));

  g_task_return_boolean (task, TRUE);
}

static void
s_handler_async (ApexMdpHandler      *self,
                 GCancellable        *cancellable,
                 GAsyncReadyCallback  callback,
                 gpointer             user_data)
{
  // TODO: receive error as (out) parameter?
  /*GError *error = NULL;*/
  g_autoptr (GTask) task = NULL;
  /*g_autoptr (GCancellable) cancellable = NULL;*/

  /*cancellable = g_cancellable_new ();*/
  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, s_handler_async);
  g_task_set_return_on_cancel (task, TRUE);

  // TODO: find a way to handle cancelling properly, this is carry over from the application
  /*
   *g_signal_connect (G_APPLICATION (self),
   *                  "shutdown",
   *                  G_CALLBACK (s_handler_cancel),
   *                  cancellable);
   */

  g_task_run_in_thread (task, s_handler_thread);

  // TODO: replace assertions with checks/errors

  /*g_assert (thread_ran == TRUE);*/
  /*g_assert (task != NULL);*/
  /*g_assert (!g_task_had_error (task));*/

  /*ret = g_task_propagate_int (task, &error);*/
  /*g_assert_no_error (error);*/
  /*g_assert_cmpint (ret, ==, 0);*/
}

static gboolean
s_handler_finish (ApexMdpHandler  *self,
                  GAsyncResult    *result,
                  GError         **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}
