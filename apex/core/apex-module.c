/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-module.h"

/*
 *message Module {
 *  string id = 1;
 *  string name = 2;
 *  string service_name = 3;
 *  string configuration_id = 4;
 *  Status status = 5;
 *  State state = 6;
 *  repeated string services = 7;
 *}
 */

struct _ApexModule
{
  GObject     parent;
  gchar      *id;
  gchar      *name;
  gchar      *service_name;
  gchar      *configuration_id;
  ApexStatus *status;
  ApexState  *state;
  /*GPtrArray   services;*/
};

enum {
  PROP_0,
  PROP_ID,
  PROP_NAME,
  PROP_SERVICE_NAME,
  PROP_CONFIGURATION_ID,
  PROP_STATUS,
  PROP_STATE,
  /*PROP_SERVICES,*/
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexModule, apex_module, G_TYPE_OBJECT)

static void
apex_module_finalize (GObject *object)
{
  ApexModule *self = (ApexModule *)object;

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->service_name, g_free);
  g_clear_pointer (&self->configuration_id, g_free);

  G_OBJECT_CLASS (apex_module_parent_class)->finalize (object);
}

static void
apex_module_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  ApexModule *self = APEX_MODULE (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, apex_module_get_id (self));
      break;

    case PROP_NAME:
      g_value_set_string (value, apex_module_get_name (self));
      break;

    case PROP_SERVICE_NAME:
      g_value_set_string (value, apex_module_get_service_name (self));
      break;

    case PROP_CONFIGURATION_ID:
      g_value_set_string (value, apex_module_get_configuration_id (self));
      break;

    case PROP_STATUS:
      g_value_set_object (value, apex_module_get_status (self));
      break;

    case PROP_STATE:
      g_value_set_object (value, apex_module_get_state (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_module_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  ApexModule *self = APEX_MODULE (object);

  switch (prop_id)
    {
    case PROP_ID:
      apex_module_set_id (self, g_value_get_string (value));
      break;

    case PROP_NAME:
      apex_module_set_name (self, g_value_get_string (value));
      break;

    case PROP_SERVICE_NAME:
      apex_module_set_service_name (self, g_value_get_string (value));
      break;

    case PROP_CONFIGURATION_ID:
      apex_module_set_configuration_id (self, g_value_get_string (value));
      break;

    case PROP_STATUS:
      apex_module_set_status (self, g_value_get_object (value));
      break;

    case PROP_STATE:
      apex_module_set_state (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_module_class_init (ApexModuleClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_module_finalize;
  object_class->get_property = apex_module_get_property;
  object_class->set_property = apex_module_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Id",
                         "The id of the application.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_module_init (ApexModule *self)
{
}

ApexModule *
apex_module_new (void)
{
  return g_object_new (APEX_TYPE_MODULE,
                       "id", g_uuid_string_random (),
                       NULL);
}

/**
 * apex_module_get_id:
 * @self: a #ApexModule
 *
 * Gets the application id of @self.
 *
 * Returns: (nullable): the application id, if one is set.
 */
const gchar *
apex_module_get_id (ApexModule *self)
{
  g_return_val_if_fail (APEX_IS_MODULE (self), NULL);

  return self->id;
}

/**
 * apex_module_set_id:
 * @self: a #ApexModule
 * @id: (nullable): the application id to use
 *
 * Sets (or unsets) the application id of @self.
 */
void
apex_module_set_id (ApexModule  *self,
                    const gchar *id)
{
  g_return_if_fail (APEX_IS_MODULE (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}

/**
 * apex_module_get_name:
 * @self: a #ApexModule
 *
 * Gets the application service name of @self.
 *
 * Returns: (nullable): the application name, if one is set.
 */
const gchar *
apex_module_get_name (ApexModule *self)
{
  g_return_val_if_fail (APEX_IS_MODULE (self), NULL);

  return self->name;
}

/**
 * apex_module_set_name:
 * @self: a #ApexModule
 * @name: (nullable): the application name to use
 *
 * Sets (or unsets) the module name to @self to register with the broker.
 */
void
apex_module_set_name (ApexModule  *self,
                      const gchar *name)
{
  g_return_if_fail (APEX_IS_MODULE (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

/**
 * apex_module_get_service_name:
 * @self: a #ApexModule
 *
 * Gets the application service name of @self.
 *
 * Returns: (nullable): the application service name, if one is set.
 */
const gchar *
apex_module_get_service_name (ApexModule *self)
{
  g_return_val_if_fail (APEX_IS_MODULE (self), NULL);

  return self->service_name;
}

/**
 * apex_module_set_service_name:
 * @self: a #ApexModule
 * @service_name: (nullable): the service name the module is connected to
 *
 * Sets (or unsets) the module service name @self is connected to.
 */
void
apex_module_set_service_name (ApexModule  *self,
                              const gchar *service_name)
{
  g_return_if_fail (APEX_IS_MODULE (self));

  if (g_strcmp0 (service_name, self->service_name) != 0)
    {
      g_free (self->service_name);
      self->service_name = g_strdup (service_name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SERVICE_NAME]);
    }
}

/**
 * apex_module_get_configuration_id:
 * @self: a #ApexModule
 *
 * Gets the application configuration_id of @self.
 *
 * Returns: (nullable): the application configuration_id, if one is set.
 */
const gchar *
apex_module_get_configuration_id (ApexModule *self)
{
  g_return_val_if_fail (APEX_IS_MODULE (self), NULL);

  return self->configuration_id;
}

/**
 * apex_module_set_configuration_id:
 * @self: a #ApexModule
 * @configuration_id: (nullable): the configuration ID the module uses
 *
 * Sets (or unsets) the configuration ID of @self.
 */
void
apex_module_set_configuration_id (ApexModule  *self,
                                  const gchar *configuration_id)
{
  g_return_if_fail (APEX_IS_MODULE (self));

  if (g_strcmp0 (configuration_id, self->configuration_id) != 0)
    {
      g_free (self->configuration_id);
      self->configuration_id = g_strdup (configuration_id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CONFIGURATION_ID]);
    }
}

/**
 * apex_module_get_status:
 * @self: a #ApexModule
 *
 * Gets the application status of @self.
 *
 * Returns: (transfer none): (nullable): an #ApexStatus, if one is set.
 */
ApexStatus *
apex_module_get_status (ApexModule *self)
{
  ApexStatus *status;

  g_return_val_if_fail (APEX_IS_MODULE (self), NULL);

  g_object_get (self, "status", &status, NULL);

  return status;
}

/**
 * apex_module_set_status:
 * @self: a #ApexModule
 * @status: (nullable): the module status as an #ApexStatus
 *
 * Sets (or unsets) the status of @self.
 */
void
apex_module_set_status (ApexModule *self,
                        ApexStatus *status)
{
  g_return_if_fail (APEX_IS_MODULE (self));

  if (self->status)
    g_object_unref (self->status);

  if (status)
    g_object_ref (status);

  self->status = status;
}

/**
 * apex_module_get_state:
 * @self: a #ApexModule
 *
 * Gets the application state of @self.
 *
 * Returns: (transfer none): (nullable): an #ApexState, if one is set.
 */
ApexState *
apex_module_get_state (ApexModule *self)
{
  ApexState *state;

  g_return_val_if_fail (APEX_IS_MODULE (self), NULL);

  g_object_get (self, "state", &state, NULL);

  return state;
}

/**
 * apex_module_set_state:
 * @self: a #ApexModule
 * @state: (nullable): the module state as an #ApexState
 *
 * Sets (or unsets) the state of @self.
 */
void
apex_module_set_state (ApexModule *self,
                       ApexState  *state)
{
  g_return_if_fail (APEX_IS_MODULE (self));

  if (self->state)
    g_object_unref (self->state);

  if (state)
    g_object_ref (state);

  self->state = state;
}
