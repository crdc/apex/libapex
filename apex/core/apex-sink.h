/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_SINK apex_sink_get_type ()
G_DECLARE_DERIVABLE_TYPE (ApexSink, apex_sink, APEX, SINK, GObject)

struct _ApexSinkClass
{
  /*< private >*/
  GObjectClass parent_class;

  /* vfuncs */

  void (*handle_message) (ApexSink    *self,
                          const gchar *msg);

  /*< private >*/
  gpointer padding[12];
};

ApexSink    *apex_sink_new          (const gchar *endpoint,
                                     const gchar *filter);

void         apex_sink_start        (ApexSink    *self);
void         apex_sink_stop         (ApexSink    *self);

gchar       *apex_sink_serialize    (ApexSink    *self);
void         apex_sink_deserialize  (ApexSink    *self,
                                     const gchar *data);

gboolean     apex_sink_running      (ApexSink    *self);

const gchar *apex_sink_get_endpoint (ApexSink    *self);
void         apex_sink_set_endpoint (ApexSink    *self,
                                     const gchar *endpoint);

const gchar *apex_sink_get_filter   (ApexSink    *self);
void         apex_sink_set_filter   (ApexSink    *self,
                                     const gchar *filter);

G_END_DECLS
