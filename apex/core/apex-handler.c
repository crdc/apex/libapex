/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "apex-handler"

#include "apex-handler.h"

enum {
  SIGNAL_SHUTDOWN_REQUESTED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

G_DEFINE_INTERFACE (ApexHandler, apex_handler, G_TYPE_OBJECT)

static void
apex_handler_default_init (ApexHandlerInterface *iface)
{
  /**
   * ApexHandler::shutdown_requested
   * @self: the message handler
   *
   * The ::shutdown_requested signal is emitted when the message handler
   * implementation receives the `shutdown' message.
   */
  signals [SIGNAL_SHUTDOWN_REQUESTED] =
    g_signal_new ("shutdown-requested", APEX_TYPE_HANDLER, G_SIGNAL_RUN_FIRST,
                  G_STRUCT_OFFSET (ApexHandlerInterface, shutdown_requested),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 0, NULL);
}

void
apex_handler_run (ApexHandler  *self,
                  gpointer      data,
                  GError      **error)
{
  g_return_if_fail (APEX_IS_HANDLER (self));
  g_return_if_fail (error == NULL || *error == NULL);

  if (APEX_HANDLER_GET_IFACE (self)->run)
    APEX_HANDLER_GET_IFACE (self)->run (self, data, error);
}

void
apex_handler_cancel (ApexHandler *self)
{
  g_return_if_fail (APEX_IS_HANDLER (self));

  if (APEX_HANDLER_GET_IFACE (self)->cancel)
    APEX_HANDLER_GET_IFACE (self)->cancel (self);
}
