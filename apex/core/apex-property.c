/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-property.h"

struct _ApexProperty
{
  GObject  parent;
  gchar   *key;
  gchar   *value;
};

G_DEFINE_TYPE (ApexProperty, apex_property, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_KEY,
  PROP_VALUE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
apex_property_finalize (GObject *object)
{
  ApexProperty *self = (ApexProperty *)object;

  g_clear_pointer (&self->key, g_free);
  g_clear_pointer (&self->value, g_free);

  G_OBJECT_CLASS (apex_property_parent_class)->finalize (object);
}

static void
apex_property_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  ApexProperty *self = APEX_PROPERTY (object);

  switch (prop_id)
  {
    case PROP_KEY:
      g_value_set_string (value, apex_property_get_key (self));
      break;

    case PROP_VALUE:
      g_value_set_string (value, apex_property_get_value (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_property_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  ApexProperty *self = APEX_PROPERTY (object);

  switch (prop_id)
  {
    case PROP_KEY:
      apex_property_set_key (self, g_value_get_string (value));
      break;

    case PROP_VALUE:
      apex_property_set_value (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
apex_property_class_init (ApexPropertyClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_property_finalize;
  object_class->get_property = apex_property_get_property;
  object_class->set_property = apex_property_set_property;

  properties [PROP_KEY] =
    g_param_spec_string ("key",
                         "Key",
                         "The property key.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_VALUE] =
    g_param_spec_string ("value",
                         "Value",
                         "The property value.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_property_init (ApexProperty *self)
{
}

ApexProperty *
apex_property_new (const gchar *key,
                   const gchar *value)
{
  return g_object_new (APEX_TYPE_PROPERTY,
                       "key", key,
                       "value", value,
                       NULL);
}

gchar *
apex_property_serialize (ApexProperty *self)
{
  g_return_val_if_fail (APEX_IS_PROPERTY (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
apex_property_deserialize (ApexProperty *self,
                           const gchar  *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (APEX_TYPE_PROPERTY,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  apex_property_set_key (self, apex_property_get_key (APEX_PROPERTY (object)));
  apex_property_set_value (self, apex_property_get_value (APEX_PROPERTY (object)));

  g_object_unref (object);
}

/**
 * apex_property_get_key:
 * @self: an #ApexProperty
 *
 * Returns: (transfer none): The key, or %NULL
 */
const gchar *
apex_property_get_key (ApexProperty *self)
{
  g_return_val_if_fail (APEX_IS_PROPERTY (self), NULL);

  return self->key;
}

/**
 * apex_property_set_key:
 * @self: an #ApexProperty
 * @key: (nullable): a string containing the key, or %NULL
 *
 * Sets the #ApexProperty:key property.
 */
void
apex_property_set_key (ApexProperty *self,
                       const gchar  *key)
{
  g_return_if_fail (APEX_IS_PROPERTY (self));

  if (g_strcmp0 (key, self->key) != 0)
    {
      g_free (self->key);
      self->key = g_strdup (key);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_KEY]);
    }
}

/**
 * apex_property_get_value:
 * @self: an #ApexProperty
 *
 * Returns: (transfer none): The value, or %NULL
 */
const gchar *
apex_property_get_value (ApexProperty *self)
{
  g_return_val_if_fail (APEX_IS_PROPERTY (self), NULL);

  return self->value;
}

/**
 * apex_property_set_value:
 * @self: an #ApexProperty
 * @value: (nullable): a string containing the value, or %NULL
 *
 * Sets the #ApexProperty:value property.
 */
void
apex_property_set_value (ApexProperty *self,
                         const gchar  *value)
{
  g_return_if_fail (APEX_IS_PROPERTY (self));

  if (g_strcmp0 (value, self->value) != 0)
    {
      g_free (self->value);
      self->value = g_strdup (value);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_VALUE]);
    }
}
