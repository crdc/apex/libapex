/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include <apex/apex-enum-types.h>

#include "apex-configuration.h"
#include "apex-object.h"
#include "apex-property.h"
#include "apex-table.h"

/* XXX: this is the manual way of creating an enum type, might be better than the template way */
/*
 *#define APEX_TYPE_CONFIGURATION_NAMESPACE (apex_configuration_namespace_get_type ())
 *static GType
 *apex_configuration_namespace_get_type (void)
 *{
 *  static GType configuration_namespace_type = 0;
 *
 *  if (!configuration_namespace_type)
 *    {
 *      static GEnumValue namespace_types[] = {
 *        { APEX_CONFIGURATION_NAMESPACE_ACQUIRE,    "Acquire",    "acquire" },
 *        { APEX_CONFIGURATION_NAMESPACE_ANALYZE,    "Analyze",    "analyze" },
 *        { APEX_CONFIGURATION_NAMESPACE_CONTROL,    "Control",    "control" },
 *        { APEX_CONFIGURATION_NAMESPACE_EXPERIMENT, "Experiment", "experiment" },
 *        { APEX_CONFIGURATION_NAMESPACE_MONITOR,    "Monitor",    "monitor" },
 *        { APEX_CONFIGURATION_NAMESPACE_PRESENT,    "Present",    "present" },
 *        { APEX_CONFIGURATION_NAMESPACE_RECORD,     "Record",     "record" },
 *        { APEX_CONFIGURATION_NAMESPACE_STATE,      "State",      "state" },
 *        { 0, NULL, NULL }
 *      };
 *
 *      configuration_namespace_type =
 *        g_enum_register_static ("ApexConfigurationNamespaceType", namespace_types);
 *    }
 *
 *  return configuration_namespace_type;
 *}
 */

struct _ApexConfiguration
{
  GObject                     parent;
  gchar                      *id;
  ApexConfigurationNamespace  namespace;
  ApexTable                  *properties;
  ApexTable                  *objects;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_NAMESPACE,
  PROP_PROPERTIES,
  PROP_OBJECTS,
  N_PROPS
};

static GParamSpec *class_properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (ApexConfiguration, apex_configuration, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
apex_configuration_serialize_property (JsonSerializable *serializable,
                                       const gchar      *name,
                                       const GValue     *value,
                                       GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "properties") == 0)
    {
      if (value != NULL && G_VALUE_HOLDS_OBJECT (value))
        retval = apex_table_serialize (APEX_TABLE (g_value_get_object (value)),
                                       JSON_TYPE_ARRAY);
    }
  else if (g_strcmp0 (name, "objects") == 0)
    {
      if (value != NULL && G_VALUE_HOLDS_OBJECT (value)) {
        retval = apex_table_serialize (APEX_TABLE (g_value_get_object (value)),
                                       JSON_TYPE_ARRAY);
      }
    }
  else if (g_strcmp0 (name, "namespace") == 0)
    {
      GValue nsval = { 0, };
      ApexConfigurationNamespace namespace;

      retval = json_node_new (JSON_NODE_VALUE);

      namespace = g_value_get_enum (value);
      g_value_init (&nsval, G_TYPE_STRING);

      switch (namespace)
        {
        case APEX_CONFIGURATION_NAMESPACE_ACQUIRE:
          g_value_set_static_string (&nsval, "ACQUIRE");
          break;
        case APEX_CONFIGURATION_NAMESPACE_ANALYZE:
          g_value_set_static_string (&nsval, "ANALYZE");
          break;
        case APEX_CONFIGURATION_NAMESPACE_CONTROL:
          g_value_set_static_string (&nsval, "CONTROL");
          break;
        case APEX_CONFIGURATION_NAMESPACE_EXPERIMENT:
          g_value_set_static_string (&nsval, "EXPERIMENT");
          break;
        case APEX_CONFIGURATION_NAMESPACE_MONITOR:
          g_value_set_static_string (&nsval, "MONITOR");
          break;
        case APEX_CONFIGURATION_NAMESPACE_PRESENT:
          g_value_set_static_string (&nsval, "PRESENT");
          break;
        case APEX_CONFIGURATION_NAMESPACE_RECORD:
          g_value_set_static_string (&nsval, "RECORD");
          break;
        case APEX_CONFIGURATION_NAMESPACE_STATE:
          g_value_set_static_string (&nsval, "STATE");
          break;
        default:
          g_value_set_static_string (&nsval, "ACQUIRE");
          break;
        }

      json_node_set_value (retval, &nsval);
      g_value_unset (&nsval);
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
apex_configuration_deserialize_property (JsonSerializable *serializable,
                                         const gchar      *name,
                                         GValue           *value,
                                         GParamSpec       *pspec,
                                         JsonNode         *property_node)
{
  gboolean retval = FALSE;

  if (g_strcmp0 (name, "properties") == 0)
    {
      g_autoptr (ApexTable) properties = NULL;

      properties = apex_table_new (APEX_TYPE_PROPERTY);
      apex_table_deserialize (properties, property_node);

      g_value_take_object (value, g_steal_pointer (&properties));

      retval = TRUE;
    }
  else if (g_strcmp0 (name, "objects") == 0)
    {
      g_autoptr (ApexTable) objects = NULL;

      objects = apex_table_new (APEX_TYPE_OBJECT);
      apex_table_deserialize (objects, property_node);

      g_value_take_object (value, g_steal_pointer (&objects));

      retval = TRUE;
    }
  else if (g_strcmp0 (name, "namespace") == 0)
    {
      g_autofree const gchar *str = NULL;
      GValue nsval = { 0, };
      ApexConfigurationNamespace namespace;

      json_node_get_value (property_node, &nsval);
      str = g_value_get_string (&nsval);

      // TODO: really should convert to lower/upper
      if (g_strcmp0 (str, "ACQUIRE") == 0)
        namespace = APEX_CONFIGURATION_NAMESPACE_ACQUIRE;
      else if (g_strcmp0 (str, "ANALYZE") == 0)
        namespace = APEX_CONFIGURATION_NAMESPACE_ANALYZE;
      else if (g_strcmp0 (str, "CONTROL") == 0)
        namespace = APEX_CONFIGURATION_NAMESPACE_CONTROL;
      else if (g_strcmp0 (str, "EXPERIMENT") == 0)
        namespace = APEX_CONFIGURATION_NAMESPACE_EXPERIMENT;
      else if (g_strcmp0 (str, "MONITOR") == 0)
        namespace = APEX_CONFIGURATION_NAMESPACE_MONITOR;
      else if (g_strcmp0 (str, "PRESENT") == 0)
        namespace = APEX_CONFIGURATION_NAMESPACE_PRESENT;
      else if (g_strcmp0 (str, "RECORD") == 0)
        namespace = APEX_CONFIGURATION_NAMESPACE_RECORD;
      else if (g_strcmp0 (str, "STATE") == 0)
        namespace = APEX_CONFIGURATION_NAMESPACE_STATE;
      else
        namespace = APEX_CONFIGURATION_NAMESPACE_ACQUIRE;

      g_value_set_enum (value, namespace);

      retval = TRUE;
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = apex_configuration_serialize_property;
  iface->deserialize_property = apex_configuration_deserialize_property;
}

static void
apex_configuration_finalize (GObject *object)
{
  ApexConfiguration *self = (ApexConfiguration *)object;

  g_debug ("!!!! Configuration finalize");

  /*apex_table_flush (self->objects);*/
  /*apex_table_flush (self->properties);*/

  g_clear_pointer (&self->id, g_free);
  g_clear_object (&self->objects);
  g_clear_object (&self->properties);

  G_OBJECT_CLASS (apex_configuration_parent_class)->finalize (object);

  g_debug ("!!!! Configuration finalize finish");
}

static void
apex_configuration_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  ApexConfiguration *self = APEX_CONFIGURATION (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, apex_configuration_get_id (self));
      break;

    case PROP_NAMESPACE:
      g_value_set_enum (value, apex_configuration_get_namespace (self));
      break;

    case PROP_PROPERTIES:
      g_value_take_object (value, apex_configuration_ref_properties (self));
      break;

    case PROP_OBJECTS:
      g_value_take_object (value, apex_configuration_ref_objects (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_configuration_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  ApexConfiguration *self = APEX_CONFIGURATION (object);

  switch (prop_id)
    {
    case PROP_ID:
      apex_configuration_set_id (self, g_value_get_string (value));
      break;

    case PROP_NAMESPACE:
      apex_configuration_set_namespace (self, g_value_get_enum (value));
      break;

    case PROP_PROPERTIES:
      apex_configuration_set_properties (self, g_value_get_object (value));
      break;

    case PROP_OBJECTS:
      apex_configuration_set_objects (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_configuration_class_init (ApexConfigurationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_configuration_finalize;
  object_class->get_property = apex_configuration_get_property;
  object_class->set_property = apex_configuration_set_property;

  class_properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The configuration ID.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  class_properties [PROP_NAMESPACE] =
    g_param_spec_enum ("namespace",
                       "Namespace",
                       "The configuration namespace.",
                       APEX_TYPE_CONFIGURATION_NAMESPACE,
                       APEX_CONFIGURATION_NAMESPACE_ACQUIRE,
                       (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  class_properties [PROP_PROPERTIES] =
    g_param_spec_object ("properties",
                         "Properties",
                         "List of properties.",
                         APEX_TYPE_TABLE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  class_properties [PROP_OBJECTS] =
    g_param_spec_object ("objects",
                         "Objects",
                         "List of objects.",
                         APEX_TYPE_TABLE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, class_properties);
}

static void
apex_configuration_init (ApexConfiguration *self)
{
  self->properties = apex_table_new (APEX_TYPE_PROPERTY);
  self->objects = apex_table_new (APEX_TYPE_OBJECT);
}

ApexConfiguration *
apex_configuration_new (void)
{
  return g_object_new (APEX_TYPE_CONFIGURATION, NULL);
}

gchar *
apex_configuration_serialize (ApexConfiguration *self)
{
  g_return_val_if_fail (APEX_IS_CONFIGURATION (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
apex_configuration_deserialize (ApexConfiguration *self,
                                const gchar       *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (ApexTable) objects = NULL;
  g_autoptr (ApexTable) properties = NULL;
  g_autofree gchar *id = NULL;
  ApexConfigurationNamespace namespace;
  GError *err = NULL;

  object = json_gobject_from_data (APEX_TYPE_CONFIGURATION,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_CONFIGURATION (object));

  id = g_strdup (apex_configuration_get_id (APEX_CONFIGURATION (object)));
  namespace = apex_configuration_get_namespace (APEX_CONFIGURATION (object));
  properties = apex_configuration_ref_properties (APEX_CONFIGURATION (object));
  objects = apex_configuration_ref_objects (APEX_CONFIGURATION (object));

  apex_configuration_set_id (self, id);
  apex_configuration_set_namespace (self, namespace);
  apex_configuration_set_properties (self, properties);
  apex_configuration_set_objects (self, objects);

  g_clear_object (&object);
}

void
apex_configuration_load (ApexConfiguration  *self,
                         const gchar        *filename,
                         GError            **error)
{
  g_autoptr (GFile) file = NULL;
  g_autoptr (GFileInputStream) in = NULL;
  g_autoptr (GString) contents = NULL;
  g_autofree gchar *buf = NULL;
  gssize read;

  g_return_if_fail (APEX_IS_CONFIGURATION (self));

  file = g_file_new_for_path (filename);
  g_return_if_fail (file != NULL);

  in = g_file_read (file, NULL, error);
  g_return_if_fail (in != NULL);
  g_return_if_fail (error == NULL || *error == NULL);

  contents = g_string_new (NULL);
  buf = g_malloc (1024);

  while (TRUE)
    {
      read = g_input_stream_read (G_INPUT_STREAM(in), buf, 1024 - 1, NULL, NULL);
      if (read > 0)
        {
          buf[read] = '\0';
          contents = g_string_append (contents, buf);
        }
      else if (read < 0)
        {
          // error = ...
          break;
        }
      else
        {
          break;
        }
    }

  apex_configuration_deserialize (self, contents->str);

  g_input_stream_close (G_INPUT_STREAM (in), NULL, error);
}

void
apex_configuration_save (ApexConfiguration  *self,
                         const gchar        *filename,
                         GError            **error)
{
  g_autoptr (GFile) file = NULL;
  g_autoptr (GFileOutputStream) out = NULL;
  g_autofree char *data = NULL;
  gsize wrote;

  g_return_if_fail (APEX_IS_CONFIGURATION (self));

  file = g_file_new_for_path (filename);
  g_return_if_fail (file != NULL);

  out = g_file_replace (file, NULL, FALSE,
                        G_FILE_CREATE_REPLACE_DESTINATION,
                        NULL, error);

  data = apex_configuration_serialize (self);

  g_output_stream_write_all (G_OUTPUT_STREAM (out),
                             data, strlen (data),
                             &wrote, NULL, error);
}

/**
 * apex_configuration_get_id:
 * @self: an #ApexConfiguration
 *
 * Returns: (transfer none): The id, or %NULL
 */
const gchar *
apex_configuration_get_id (ApexConfiguration *self)
{
  g_return_val_if_fail (APEX_IS_CONFIGURATION (self), NULL);

  return self->id;
}

/**
 * apex_configuration_set_id:
 * @self: an #ApexConfiguration
 * @id: (nullable): a string containing the id, or %NULL
 *
 * Sets the #ApexConfiguration:id property.
 */
void
apex_configuration_set_id (ApexConfiguration *self,
                           const gchar       *id)
{
  g_return_if_fail (APEX_IS_CONFIGURATION (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_ID]);
    }
}

ApexConfigurationNamespace
apex_configuration_get_namespace (ApexConfiguration *self)
{
  /* FIXME: this should be a none/unknown type */
  g_return_val_if_fail (APEX_IS_CONFIGURATION (self), APEX_CONFIGURATION_NAMESPACE_ACQUIRE);

  return self->namespace;
}

void
apex_configuration_set_namespace (ApexConfiguration          *self,
                                  ApexConfigurationNamespace  namespace)
{
  g_return_if_fail (APEX_IS_CONFIGURATION (self));

  self->namespace = namespace;
}

/**
 * apex_configuration_add_object:
 * @self: an #ApexConfiguration
 * @object: (transfer full): object to add
 */
void
apex_configuration_add_object (ApexConfiguration *self,
                               ApexObject        *object)
{
  GValue val = G_VALUE_INIT;
  char *id;

  g_return_if_fail (APEX_IS_CONFIGURATION (self));
  g_return_if_fail (APEX_IS_OBJECT (object));

  if (self->objects == NULL)
    self->objects = apex_table_new (APEX_TYPE_OBJECT);

  g_value_init (&val, APEX_TYPE_OBJECT);
  g_value_take_object (&val, object);
  id = g_strdup (apex_object_get_id (object));

  apex_table_add (self->objects, id, &val);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_OBJECTS]);

  g_value_unset (&val);
  g_free (id);
}

void
apex_configuration_remove_object (ApexConfiguration *self,
                                  const gchar       *id)
{
  g_return_if_fail (APEX_IS_CONFIGURATION (self));
  g_return_if_fail (self->objects != NULL);

  apex_table_remove (self->objects, id);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_OBJECTS]);
}

/**
 * apex_configuration_lookup_object:
 * @self: an #ApexConfiguration
 * @id: the ID of the object to retrieve
 *
 * Returns: (transfer none): The object if found, NULL otherwise.
 */
ApexObject *
apex_configuration_lookup_object (ApexConfiguration *self,
                                  const gchar       *id)
{
  g_return_val_if_fail (APEX_IS_CONFIGURATION (self), NULL);
  g_return_val_if_fail (self->objects != NULL, NULL);

  return g_value_get_object (apex_table_get (self->objects, id));
}

gboolean
apex_configuration_has_object (ApexConfiguration *self,
                               const gchar       *id)
{
  gboolean ret;

  g_return_val_if_fail (APEX_IS_CONFIGURATION (self), FALSE);
  g_return_val_if_fail (self->objects != NULL, FALSE);
  g_return_val_if_fail (id != NULL, FALSE);

  ret = apex_table_has (self->objects, id);

  return ret;
}

/**
 * apex_configuration_add_property:
 * @self: an #ApexConfiguration
 * @property: (transfer full): property to add
 */
void
apex_configuration_add_property (ApexConfiguration *self,
                                 ApexProperty      *property)
{
  GValue val = G_VALUE_INIT;
  gchar *key;

  g_return_if_fail (APEX_IS_CONFIGURATION (self));
  g_return_if_fail (APEX_IS_PROPERTY (property));

  if (self->properties == NULL)
    self->properties = apex_table_new (APEX_TYPE_PROPERTY);

  g_value_init (&val, APEX_TYPE_PROPERTY);
  g_value_take_object (&val, property);
  key = g_strdup (apex_property_get_key (property));

  apex_table_add (self->properties, key, &val);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_PROPERTIES]);

  g_value_unset (&val);
  g_free (key);
}

void
apex_configuration_remove_property (ApexConfiguration *self,
                                    const gchar       *key)
{
  g_return_if_fail (APEX_IS_CONFIGURATION (self));
  g_return_if_fail (self->properties != NULL);

  apex_table_remove (self->properties, key);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_PROPERTIES]);
}

/**
 * apex_configuration_lookup_property:
 * @self: an #ApexConfiguration
 * @key: the key of the property to retrieve
 *
 * Returns: (transfer none): The property if found, NULL otherwise.
 */
ApexProperty *
apex_configuration_lookup_property (ApexConfiguration *self,
                                    const gchar       *key)
{
  g_return_val_if_fail (APEX_IS_CONFIGURATION (self), NULL);
  g_return_val_if_fail (self->properties != NULL, NULL);

  return g_value_get_object (apex_table_get (self->properties, key));
}

gboolean
apex_configuration_has_property (ApexConfiguration *self,
                                 const gchar       *key)
{
  gboolean ret;

  g_return_val_if_fail (APEX_IS_CONFIGURATION (self), FALSE);
  g_return_val_if_fail (self->properties != NULL, FALSE);
  g_return_val_if_fail (key != NULL, FALSE);

  ret = apex_table_has (self->properties, key);

  return ret;
}

/**
 * apex_configuration_get_objects:
 * @self: a #ApexConfiguration
 *
 * Returns: (element-type utf8 Apex.Object) (transfer none): an table of
 *          #ApexObject objects, free the table with g_hash_table_destroy
 *          when done.
 */
GHashTable *
apex_configuration_get_objects (ApexConfiguration *self)
{
  g_autoptr (GHashTable) objects = NULL;
  GList *keys;

  g_return_val_if_fail (APEX_IS_CONFIGURATION (self), NULL);

  objects = g_hash_table_new_full (g_str_hash,
                                   g_str_equal,
                                   g_free,
                                   g_object_unref);

  keys = apex_table_get_keys (self->objects);
  for (GList *key = keys; key != NULL; key = key->next)
    {
      GValue *val = g_new0 (GValue, 1);

      val = g_value_init (val, APEX_TYPE_OBJECT);
      g_value_copy (apex_table_get (self->objects, key->data), val);
      g_hash_table_insert (objects,
                           g_strdup (key->data),
                           g_value_get_object (val));

      g_free (val);
    }

  g_list_free (keys);

  return g_hash_table_ref (objects);
}

/**
 * apex_configuration_ref_objects:
 *
 * Gets the object list, and returns a new reference
 * to the #ApexTable.
 *
 * Returns: (transfer full) (nullable): a #ApexTable or %NULL
 */
ApexTable *
apex_configuration_ref_objects (ApexConfiguration *self)
{
  ApexTable *ret = NULL;

  g_return_val_if_fail (APEX_IS_CONFIGURATION (self), NULL);

  g_set_object (&ret, self->objects);

  return g_steal_pointer (&ret);
}

/**
 * apex_configuration_set_objects:
 * @self: a #ApexConfiguration
 * @objects: An #ApexTable of #ApexObject objects to set.
 */
void
apex_configuration_set_objects (ApexConfiguration *self,
                                ApexTable         *objects)
{
  g_return_if_fail (APEX_IS_CONFIGURATION (self));
  g_return_if_fail (APEX_IS_TABLE (self->objects));

  if (g_set_object (&self->objects, objects))
    g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_OBJECTS]);
}

/**
 * apex_configuration_get_properties:
 * @self: an #ApexConfiguration
 *
 * Returns: (element-type utf8 Apex.Property) (transfer none): a hash table of
 *          #ApexProperty objects, free the table with g_hash_table_destroy when
 *          done.
 */
GHashTable *
apex_configuration_get_properties (ApexConfiguration *self)
{
  g_autoptr (GHashTable) properties = NULL;
  GList *keys;

  g_return_val_if_fail (APEX_IS_CONFIGURATION (self), NULL);

  properties = g_hash_table_new_full (g_str_hash,
                                      g_str_equal,
                                      g_free,
                                      g_object_unref);

  keys = apex_table_get_keys (self->properties);
  for (GList *key = keys; key != NULL; key = key->next)
    {
      GValue *val = g_new0 (GValue, 1);

      val = g_value_init (val, APEX_TYPE_PROPERTY);
      g_value_copy (apex_table_get (self->properties, key->data), val);
      g_hash_table_insert (properties,
                           g_strdup (key->data),
                           g_object_ref (g_value_get_object (val)));

      g_free (val);
    }

  g_list_free (keys);

  return g_hash_table_ref (properties);
}

/**
 * apex_configuration_ref_properties:
 *
 * Gets the property list, and returns a new reference
 * to the #ApexTable.
 *
 * Returns: (transfer full) (nullable): a #ApexTable or %NULL
 */
ApexTable *
apex_configuration_ref_properties (ApexConfiguration *self)
{
  ApexTable *ret = NULL;

  g_return_val_if_fail (APEX_IS_CONFIGURATION (self), NULL);

  g_set_object (&ret, self->properties);

  return g_steal_pointer (&ret);
}

/**
 * apex_configuration_set_properties:
 * @self: an #ApexConfiguration
 * @properties: An #ApexTable of #ApexProperty objects to set.
 */
void
apex_configuration_set_properties (ApexConfiguration *self,
                                   ApexTable         *properties)
{
  g_return_if_fail (APEX_IS_CONFIGURATION (self));
  g_return_if_fail (APEX_IS_TABLE (self->properties));

  if (g_set_object (&self->properties, properties))
    g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_PROPERTIES]);
}
