/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "apex-global"

#include "config.h"

#include "apex-debug.h"
#include "apex-global.h"
#include "apex-macros.h"
#include "apex-private.h"

const gchar *
apex_gettext (const gchar *message)
{
  if (message != NULL)
    return g_dgettext (GETTEXT_PACKAGE, message);
  return NULL;
}
