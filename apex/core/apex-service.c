/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-service.h"
#include "apex-status.h"
/*#include "apex-state-machine.h"*/

typedef struct
{
  GObject          parent;
  ApexModel       *model;
  gchar           *name;
  ApexStatus      *status;
  /*ApexState       *state;*/
} ApexServicePrivate;

enum {
  PROP_0,
  /*PROP_MODEL,*/
  PROP_NAME,
  /*PROP_STATUS,*/
  /*PROP_STATE,*/
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE_WITH_PRIVATE (ApexService, apex_service, G_TYPE_OBJECT)

static void
apex_service_finalize (GObject *object)
{
  ApexService *self = (ApexService *)object;
  ApexServicePrivate *priv = apex_service_get_instance_private (self);

  g_clear_pointer (&priv->name, g_free);

  G_OBJECT_CLASS (apex_service_parent_class)->finalize (object);
}

static void
apex_service_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  ApexService *self = APEX_SERVICE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_take_string (value, apex_service_dup_name (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_service_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  ApexService *self = APEX_SERVICE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      apex_service_set_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_service_class_init (ApexServiceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_service_finalize;
  object_class->get_property = apex_service_get_property;
  object_class->set_property = apex_service_set_property;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the service.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_service_init (ApexService *self)
{
}

ApexService *
apex_service_new (const gchar *name)
{
  return g_object_new (APEX_TYPE_SERVICE,
                       "name", name,
                       NULL);
}

/*
 *ApexService *
 *apex_service_new_with_model (const gchar *name,
 *                             ApexModel   *model)
 *{
 *  return g_object_new (APEX_TYPE_SERVICE,
 *                       "name", name,
 *                       "model", model,
 *                       NULL);
 *}
 */

/**
 * apex_service_dup_name:
 *
 * Copies the name of the service and returns it to the caller (after locking
 * the object). A copy is used to avoid thread-races.
 */
gchar *
apex_service_dup_name (ApexService *self)
{
  ApexServicePrivate *priv;
  gchar *ret;

  g_return_val_if_fail (APEX_IS_SERVICE (self), NULL);

  priv = apex_service_get_instance_private (self);

  /*apex_object_lock (APEX_OBJECT (self));*/
  ret = g_strdup (priv->name);
  /*apex_object_unlock (APEX_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
apex_service_set_name (ApexService *self,
                       const gchar *name)
{
  ApexServicePrivate *priv;

  g_return_if_fail (APEX_IS_SERVICE (self));

  priv = apex_service_get_instance_private (self);

  if (g_strcmp0 (name, priv->name) != 0)
    {
      g_free (priv->name);
      priv->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

/**
 * apex_service_ref_status:
 * @self: a #ApexService
 *
 * Gets the application status of @self.
 *
 * Returns: (transfer full): (nullable): an #ApexStatus or %NULL
 */
ApexStatus *
apex_service_ref_status (ApexService *self)
{
  ApexServicePrivate *priv;
  ApexStatus *ret = NULL;

  g_return_val_if_fail (APEX_IS_SERVICE (self), NULL);

  priv = apex_service_get_instance_private (self);

  g_set_object (&ret, priv->status);

  return g_steal_pointer (&ret);
}

/**
 * apex_service_set_status:
 * @self: a #ApexService
 * @status: (nullable): the module status as an #ApexStatus
 *
 * Sets (or unsets) the status of @self.
 */
void
apex_service_set_status (ApexService *self,
                         ApexStatus  *status)
{
  ApexServicePrivate *priv;

  g_return_if_fail (APEX_IS_SERVICE (self));
  g_return_if_fail (APEX_IS_STATUS (status));

  priv = apex_service_get_instance_private (self);

  /*if (g_set_object (&priv->status, status))*/
    /*g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_STATUS]);*/
}

/**
 * apex_service_ref_state:
 * @self: a #ApexService
 *
 * Gets the application state of @self.
 *
 * Returns: (transfer full): (nullable): an #ApexState or %NULL
 */
/*
 *ApexState *
 *apex_service_ref_state (ApexService *self)
 *{
 *  ApexServicePrivate *priv;
 *  ApexState *ret = NULL;
 *
 *  g_return_val_if_fail (APEX_IS_SERVICE (self), NULL);
 *
 *  priv = apex_service_get_instance_private (self);
 *
 *  g_set_object (&ret, priv->state);
 *
 *  return g_steal_pointer (&ret);
 *}
 */

/**
 * apex_service_set_state:
 * @self: a #ApexService
 * @state: (nullable): the service state as an #ApexState
 *
 * Sets (or unsets) the state of @self.
 */
/*
 *void
 *apex_service_set_state (ApexService *self,
 *                        ApexState   *state)
 *{
 *  ApexServicePrivate *priv;
 *
 *  g_return_if_fail (APEX_IS_SERVICE (self));
 *  g_return_if_fail (APEX_IS_STATE (state));
 *
 *  priv = apex_service_get_instance_private (self);
 *
 *  if (g_set_object (&priv->state, state))
 *    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_STATE]);
 *}
 */
