/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_METRIC_TABLE apex_metric_table_get_type ()
G_DECLARE_FINAL_TYPE (ApexMetricTable, apex_metric_table, APEX, METRIC_TABLE, GObject)

ApexMetricTable       *apex_metric_table_new           (void);

gchar                 *apex_metric_table_serialize     (ApexMetricTable       *self);
void                   apex_metric_table_deserialize   (ApexMetricTable       *self,
                                                        const gchar           *data);

gchar                 *apex_metric_table_dup_name      (ApexMetricTable       *self);
const gchar           *apex_metric_table_get_name      (ApexMetricTable       *self);
void                   apex_metric_table_set_name      (ApexMetricTable       *self,
                                                        const gchar           *name);

gchar                 *apex_metric_table_dup_timestamp (ApexMetricTable       *self);
const gchar           *apex_metric_table_get_timestamp (ApexMetricTable       *self);
void                   apex_metric_table_set_timestamp (ApexMetricTable       *self,
                                                        const gchar           *timestamp);

ApexMetricTableHeader *apex_metric_table_get_header    (ApexMetricTable       *self);
ApexMetricTableHeader *apex_metric_table_ref_header    (ApexMetricTable       *self);
void                   apex_metric_table_set_header    (ApexMetricTable       *self,
                                                        ApexMetricTableHeader *header);

ApexTable             *apex_metric_table_get_entries   (ApexMetricTable       *self);
ApexTable             *apex_metric_table_ref_entries   (ApexMetricTable       *self);
void                   apex_metric_table_set_entries   (ApexMetricTable       *self,
                                                        ApexTable             *entries);

void                   apex_metric_table_add_entry     (ApexMetricTable       *self,
                                                        const gchar           *key,
                                                        gdouble                value);
void                   apex_metric_table_remove_entry  (ApexMetricTable       *self,
                                                        const gchar           *key);
gdouble                apex_metric_table_get_entry     (ApexMetricTable       *self,
                                                        const gchar           *key);
gboolean               apex_metric_table_has_entry     (ApexMetricTable       *self,
                                                        const gchar           *key);

G_END_DECLS
