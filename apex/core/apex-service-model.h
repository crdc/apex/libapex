/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

#include <apex/apex-types.h>

G_BEGIN_DECLS

#define APEX_TYPE_SERVICE apex_service_get_type ()
G_DECLARE_FINAL_TYPE (ApexService, apex_service, APEX, SERVICE, GObject)

ApexService     *apex_service_new                  (void);

gchar           *apex_service_serialize            (ApexService     *self);
void             apex_service_deserialize          (ApexService     *self,
                                                    const gchar     *data);

const gchar     *apex_service_get_name             (ApexService     *self);
void             apex_service_set_name             (ApexService     *self,
                                                    const gchar     *name);

const gchar     *apex_service_get_description      (ApexService     *self);
void             apex_service_set_description      (ApexService     *self,
                                                    const gchar     *description);

const gchar     *apex_service_get_configuration_id (ApexService     *self);
void             apex_service_set_configuration_id (ApexService     *self,
                                                    const gchar     *configuration_id);

ApexStatus      *apex_service_get_status           (ApexService     *self);
void             apex_service_set_status           (ApexService     *self,
                                                    ApexStatus      *status);

ApexState       *apex_service_get_state            (ApexService     *self);
void             apex_service_set_state            (ApexService     *self,
                                                    ApexState       *state);

G_END_DECLS
