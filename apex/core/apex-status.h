/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define APEX_TYPE_STATUS apex_status_get_type ()
G_DECLARE_FINAL_TYPE (ApexStatus, apex_status, APEX, STATUS, GObject)

ApexStatus  *apex_status_new            (void);
//ApexStatus  *apex_status_new_from_data  (const guint8 *data);

//guint8      *apex_status_to_data        (ApexStatus   *self);

gchar       *apex_status_serialize      (ApexStatus   *self);
void         apex_status_deserialize    (ApexStatus   *self,
                                         const gchar  *data);

gboolean     apex_status_get_enabled    (ApexStatus   *self);
void         apex_status_set_enabled    (ApexStatus   *self,
                                         gboolean      enabled);

gboolean     apex_status_get_loaded     (ApexStatus   *self);
void         apex_status_set_loaded     (ApexStatus   *self,
                                         gboolean      loaded);

gboolean     apex_status_get_active     (ApexStatus   *self);
void         apex_status_set_active     (ApexStatus   *self,
                                         gboolean      active);

GHashTable  *apex_status_get_details    (ApexStatus   *self);
void         apex_status_set_details    (ApexStatus   *self,
                                         GHashTable   *details);

void         apex_status_add_detail     (ApexStatus   *self,
                                         const gchar  *key,
                                         const gchar  *value);
void         apex_status_remove_detail  (ApexStatus   *self,
                                         const gchar  *key);
const gchar *apex_status_get_detail     (ApexStatus   *self,
                                         const gchar  *key);

G_END_DECLS
