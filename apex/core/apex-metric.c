/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <json-glib/json-glib.h>

#include "apex-metric.h"
#include "apex-metric-table.h"

/*
 * ApexMetric:
 *
 * Represents a ...
 */
struct _ApexMetric
{
  GObject          parent;
  gchar           *id;
  gchar           *name;
  ApexMetricTable *data;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_NAME,
  PROP_DATA,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexMetric, apex_metric, G_TYPE_OBJECT)

static void
apex_metric_finalize (GObject *object)
{
  ApexMetric *self = (ApexMetric *)object;

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->name, g_free);
  g_clear_object (&self->data);

  G_OBJECT_CLASS (apex_metric_parent_class)->finalize (object);
}

static void
apex_metric_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  ApexMetric *self = APEX_METRIC (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_take_string (value, apex_metric_dup_id (self));
      break;

    case PROP_NAME:
      g_value_take_string (value, apex_metric_dup_name (self));
      break;

    case PROP_DATA:
      g_value_take_object (value, apex_metric_ref_data (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_metric_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  ApexMetric *self = APEX_METRIC (object);

  switch (prop_id)
    {
    case PROP_ID:
      apex_metric_set_id (self, g_value_get_string (value));
      break;

    case PROP_NAME:
      apex_metric_set_name (self, g_value_get_string (value));
      break;

    case PROP_DATA:
      apex_metric_set_data (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_metric_class_init (ApexMetricClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_metric_finalize;
  object_class->get_property = apex_metric_get_property;
  object_class->set_property = apex_metric_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The metric message identifier",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "A name that represents the metric",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_DATA] =
    g_param_spec_object ("data",
                         "Data",
                         "Metric data table",
                         APEX_TYPE_METRIC_TABLE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_metric_init (ApexMetric *self)
{
}

ApexMetric *
apex_metric_new (void)
{
  return g_object_new (APEX_TYPE_METRIC, NULL);
}

ApexMetric *
apex_metric_new_full (const gchar     *id,
                      const gchar     *name,
                      ApexMetricTable *data)
{
  return g_object_new (APEX_TYPE_METRIC,
                       "id", id,
                       "name", name,
                       "data", data,
                       NULL);
}

gchar *
apex_metric_serialize (ApexMetric *self)
{
  g_return_val_if_fail (APEX_IS_METRIC (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
apex_metric_deserialize (ApexMetric  *self,
                         const gchar *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (ApexMetricTable) table = NULL;
  g_autofree gchar *id = NULL;
  g_autofree gchar *name = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (APEX_TYPE_METRIC,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (APEX_IS_METRIC (object));

  id = apex_metric_dup_id (APEX_METRIC (object));
  name = apex_metric_dup_name (APEX_METRIC (object));
  table = apex_metric_ref_data (APEX_METRIC (object));

  apex_metric_set_id (self, id);
  apex_metric_set_name (self, name);
  apex_metric_set_data (self, table);

  g_clear_object (&object);
}

const gchar *
apex_metric_get_id (ApexMetric *self)
{
  g_return_val_if_fail (APEX_IS_METRIC (self), NULL);

  return self->id;
}

void
apex_metric_set_id (ApexMetric  *self,
                    const gchar *id)
{
  g_return_if_fail (APEX_IS_METRIC (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}

/**
 * apex_metric_dup_id:
 *
 * Copies the id of the metric and returns it to the caller (after locking
 * the object). A copy is used to avoid thread-races.
 */
gchar *
apex_metric_dup_id (ApexMetric *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_METRIC (self), NULL);

  /*apex_object_lock (APEX_OBJECT (self));*/
  ret = g_strdup (self->id);
  /*apex_object_unlock (APEX_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

const gchar *
apex_metric_get_name (ApexMetric *self)
{
  g_return_val_if_fail (APEX_IS_METRIC (self), NULL);

  return self->name;
}

void
apex_metric_set_name (ApexMetric  *self,
                      const gchar *name)
{
  g_return_if_fail (APEX_IS_METRIC (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

/**
 * apex_metric_dup_name:
 *
 * Copies the name of the metric and returns it to the caller (after locking
 * the object). A copy is used to avoid thread-races.
 */
gchar *
apex_metric_dup_name (ApexMetric *self)
{
  gchar *ret;

  g_return_val_if_fail (APEX_IS_METRIC (self), NULL);

  /*apex_object_lock (APEX_OBJECT (self));*/
  ret = g_strdup (self->name);
  /*apex_object_unlock (APEX_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

/**
 * apex_metric_get_data:
 * @self: an #ApexMetric
 *
 * Retrieve the #ApexMetricTable held as the metric data.
 *
 * Returns: (transfer none): an #ApexMetricTable if one is set.
 */
ApexMetricTable *
apex_metric_get_data (ApexMetric *self)
{
  ApexMetricTable *data;

  g_return_val_if_fail (APEX_IS_METRIC (self), NULL);

  g_object_get (self, "data", &data, NULL);

  return data;
}

/**
 * apex_metric_ref_data:
 * @self: an #ApexMetric
 *
 * Gets the data for the metric, and returns a new reference
 * to the #ApexMetricTable.
 *
 * Returns: (transfer full) (nullable): a #ApexMetricTable or %NULL
 */
ApexMetricTable *
apex_metric_ref_data (ApexMetric *self)
{
  ApexMetricTable *ret = NULL;

  g_return_val_if_fail (APEX_IS_METRIC (self), NULL);

  /*apex_object_lock (G_OBJECT (self));*/
  g_set_object (&ret, self->data);
  /*apex_object_unlock (G_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
apex_metric_set_data (ApexMetric      *self,
                      ApexMetricTable *data)
{
  g_return_if_fail (APEX_IS_METRIC (self));
  g_return_if_fail (APEX_IS_METRIC_TABLE (data));

  if (g_set_object (&self->data, data))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DATA]);
}
