#include "apex-utils.h"

gchar *
remove_whitespace (const gchar *str)
{
  gchar *ret = NULL;
  gchar *nosp;
  gchar **sp_tokens;
  gchar **nl_tokens;

  // Remove spaces
  sp_tokens = g_strsplit (str, " ", -1);
  nosp = g_strjoinv ("", sp_tokens);
  g_strfreev (sp_tokens);

  // Remove newlines
  nl_tokens = g_strsplit (nosp, "\n", -1);
  ret = g_strjoinv ("", nl_tokens);
  g_free (nosp);
  g_strfreev (nl_tokens);

  return ret;
}
