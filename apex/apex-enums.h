/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

/**
 * ApexConfigurationNamespace:
 * @APEX_CONFIGURATION_NAMESPACE_ACQUIRE:
 * @APEX_CONFIGURATION_NAMESPACE_ANALYZE:
 * @APEX_CONFIGURATION_NAMESPACE_CONTROL:
 * @APEX_CONFIGURATION_NAMESPACE_EXPERIMENT:
 * @APEX_CONFIGURATION_NAMESPACE_MONITOR:
 * @APEX_CONFIGURATION_NAMESPACE_PRESENT:
 * @APEX_CONFIGURATION_NAMESPACE_RECORD:
 * @APEX_CONFIGURATION_NAMESPACE_STATE:
 *
 * The configuration namespace type.
 */
typedef enum
{
  APEX_CONFIGURATION_NAMESPACE_ACQUIRE = 0,
  APEX_CONFIGURATION_NAMESPACE_ANALYZE,
  APEX_CONFIGURATION_NAMESPACE_CONTROL,
  APEX_CONFIGURATION_NAMESPACE_EXPERIMENT,
  APEX_CONFIGURATION_NAMESPACE_MONITOR,
  APEX_CONFIGURATION_NAMESPACE_PRESENT,
  APEX_CONFIGURATION_NAMESPACE_RECORD,
  APEX_CONFIGURATION_NAMESPACE_STATE
} ApexConfigurationNamespace;

/**
 * ApexJobStatusType:
 * @APEX_JOB_STATUS_TYPE_SUBMITTED: specifies a submitted job.
 * @APEX_JOB_STATUS_TYPE_READY: specifies a ready job.
 * @APEX_JOB_STATUS_TYPE_STARTED: specifies a started job.
 * @APEX_JOB_STATUS_TYPE_RUNNING: specifies a running job.
 * @APEX_JOB_STATUS_TYPE_PAUSED: specifies a paused job.
 * @APEX_JOB_STATUS_TYPE_RESUMED: specifies a resumed job.
 * @APEX_JOB_STATUS_TYPE_STOPPED: specifies a stopped job.
 * @APEX_JOB_STATUS_TYPE_FAILED: specifies a failed job.
 *
 * The job status value.
 */
typedef enum
{
  APEX_JOB_STATUS_TYPE_SUBMITTED = 0,
  APEX_JOB_STATUS_TYPE_READY,
  APEX_JOB_STATUS_TYPE_STARTED,
  APEX_JOB_STATUS_TYPE_RUNNING,
  APEX_JOB_STATUS_TYPE_PAUSED,
  APEX_JOB_STATUS_TYPE_RESUMED,
  APEX_JOB_STATUS_TYPE_STOPPED,
  APEX_JOB_STATUS_TYPE_FAILED
} ApexJobStatusType;

/**
 * ApexChannelType:
 * @APEX_CHANNEL_TYPE_SINK:
 * @APEX_CHANNEL_TYPE_SOURCE:
 *
 * The type of channel.
 */
typedef enum
{
  APEX_CHANNEL_TYPE_SINK = 0,
  APEX_CHANNEL_TYPE_SOURCE
} ApexChannelType;

/**
 * ApexServiceType:
 * @APEX_SERVICE_TYPE_ACQUIRE:
 * @APEX_SERVICE_TYPE_ANALYZE:
 * @APEX_SERVICE_TYPE_CONTROL:
 * @APEX_SERVICE_TYPE_EXPERIMENT:
 * @APEX_SERVICE_TYPE_MONITOR:
 * @APEX_SERVICE_TYPE_PRESENT:
 * @APEX_SERVICE_TYPE_RECORD:
 * @APEX_SERVICE_TYPE_STATE:
 *
 * The service type.
 */
typedef enum
{
  APEX_SERVICE_TYPE_ACQUIRE = 0,
  APEX_SERVICE_TYPE_ANALYZE,
  APEX_SERVICE_TYPE_CONTROL,
  APEX_SERVICE_TYPE_EXPERIMENT,
  APEX_SERVICE_TYPE_MONITOR,
  APEX_SERVICE_TYPE_PRESENT,
  APEX_SERVICE_TYPE_RECORD,
  APEX_SERVICE_TYPE_STATE
} ApexServiceType;

/**
 * ApexErrorEnum:
 * @APEX_ERROR_FAILED: Generic error condition for when an operation fails.
 * @APEX_ERROR_NOT_CONNECTED: Transport endpoint is not connected.
 *
 * Error codes returned by Apex functions.
 **/
typedef enum {
  APEX_ERROR_FAILED,
  APEX_ERROR_NOT_CONNECTED
} ApexErrorEnum;

G_END_DECLS
