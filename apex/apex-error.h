/*
 * apex-error.h
 * This file is part of libapex
 * Copyright © 2019 - Geoff Johnson
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <glib.h>
#include <apex/apex-enums.h>

G_BEGIN_DECLS

/**
 * APEX_ERROR:
 *
 * Error domain for Apex. Errors in this domain will be from the #ApexErrorEnum enumeration.
 * See #GError for more information on error domains.
 **/
#define APEX_ERROR apex_error_quark()

//APEX_AVAILABLE_IN_ALL
GQuark        apex_error_quark      (void);
//APEX_AVAILABLE_IN_ALL
ApexErrorEnum apex_error_from_errno (gint err_no);

G_END_DECLS
