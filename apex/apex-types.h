/*
 * Copyright © the Apex contributors. All rights reserved.
 *
 * This file is part of libapex, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (APEX_INSIDE) && !defined (APEX_COMPILATION)
# error "Only <apex/apex.h> can be included directly."
#endif

#include <apex/apex-enums.h>

G_BEGIN_DECLS

typedef struct _ApexApplication             ApexApplication;
typedef struct _ApexChannel                 ApexChannel;
typedef struct _ApexConfiguration           ApexConfiguration;
typedef struct _ApexEvent                   ApexEvent;
typedef struct _ApexHandler                 ApexHandler;
typedef struct _ApexHub                     ApexHub;
typedef struct _ApexJob                     ApexJob;
typedef struct _ApexJobQueue                ApexJobQueue;
typedef struct _ApexMdpHandler              ApexMdpHandler;
typedef struct _ApexMetric                  ApexMetric;
typedef struct _ApexMetricTable             ApexMetricTable;
typedef struct _ApexMetricTableHeader       ApexMetricTableHeader;
typedef struct _ApexModel                   ApexModel;
typedef struct _ApexModule                  ApexModule;
typedef struct _ApexObject                  ApexObject;
typedef struct _ApexProperty                ApexProperty;
typedef struct _ApexService                 ApexService;
typedef struct _ApexSink                    ApexSink;
typedef struct _ApexSource                  ApexSource;
typedef struct _ApexStatus                  ApexStatus;
typedef struct _ApexSystem                  ApexSystem;
typedef struct _ApexTable                   ApexTable;

typedef struct _ApexState                   ApexState;
typedef struct _ApexStateMachine            ApexStateMachine;
typedef struct _ApexTransition              ApexTransition;
typedef struct _ApexTransitionTable         ApexTransitionTable;

/**
 * Apex:
 *
 * Represents a ...
 */
typedef struct _ApexClient                  ApexClient;
typedef struct _ApexWorker                  ApexWorker;

typedef struct _ApexChannelRequest          ApexChannelRequest;
typedef struct _ApexChannelResponse         ApexChannelResponse;
typedef struct _ApexChannelsResponse        ApexChannelsResponse;
typedef struct _ApexConfigurationRequest    ApexConfigurationRequest;
typedef struct _ApexConfigurationResponse   ApexConfigurationResponse;
typedef struct _ApexConfigurationsResponse  ApexConfigurationsResponse;
typedef struct _ApexEmpty                   ApexEmpty;
typedef struct _ApexEventRequest            ApexEventRequest;
typedef struct _ApexEventResponse           ApexEventResponse;
typedef struct _ApexJobRequest              ApexJobRequest;
typedef struct _ApexJobResponse             ApexJobResponse;
typedef struct _ApexJobStatusResponse       ApexJobStatusResponse;
typedef struct _ApexJobsResponse            ApexJobsResponse;
typedef struct _ApexMessageError            ApexMessageError;
typedef struct _ApexModuleEventRequest      ApexModuleEventRequest;
typedef struct _ApexModuleJobRequest        ApexModuleJobRequest;
typedef struct _ApexModuleRequest           ApexModuleRequest;
typedef struct _ApexModuleResponse          ApexModuleResponse;
typedef struct _ApexModulesResponse         ApexModulesResponse;
typedef struct _ApexPropertyRequest         ApexPropertyRequest;
typedef struct _ApexPropertyResponse        ApexPropertyResponse;
typedef struct _ApexPropertiesRequest       ApexPropertiesRequest;
typedef struct _ApexPropertiesResponse      ApexPropertiesResponse;
typedef struct _ApexRequest                 ApexRequest;
typedef struct _ApexResponse                ApexResponse;
typedef struct _ApexSettingsRequest         ApexSettingsRequest;
typedef struct _ApexSettingsResponse        ApexSettingsResponse;
typedef struct _ApexStatusRequest           ApexStatusRequest;
typedef struct _ApexStatusResponse          ApexStatusResponse;

/**
 * ApexJobCallback:
 * @job: a #ApexJob
 * @cancellable: optinal #GCancellable object, %NULL to ignore.
 * @user_data: user data passed into the callback.
 *
 * Long-running jobs should occasionally check the @cancellable to see if they
 * have been cancelled.
 */
/*
 *typedef void (*ApexJobFunc) (ApexJob      *job,
 *                             GCancellable *cancellable,
 *                             gpointer      user_data);
 */

G_END_DECLS
