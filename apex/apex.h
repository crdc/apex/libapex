/*
 * apex.h
 * This file is part of libapex
 * Copyright © 2019 - Geoff Johnson
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <glib.h>

#define APEX_INSIDE

G_BEGIN_DECLS

#if !GLIB_CHECK_VERSION(2, 48, 0)
# error "libapex requires glib-2.0 >= 2.48.0"
#endif

#include <apex/apex-types.h>
#include <apex/apex-debug.h>
#include <apex/apex-error.h>
#include <apex/apex-enums.h>
#include <apex/apex-enum-types.h>

// MDP API
#include <apex/mdp/apex-broker.h>
#include <apex/mdp/apex-client.h>
#include <apex/mdp/apex-worker.h>

// Core types
#include <apex/core/apex-application.h>
#include <apex/core/apex-channel.h>
#include <apex/core/apex-configuration.h>
#include <apex/core/apex-event.h>
#include <apex/core/apex-handler.h>
#include <apex/core/apex-hub.h>
#include <apex/core/apex-job.h>
#include <apex/core/apex-log.h>
#include <apex/core/apex-metric.h>
#include <apex/core/apex-metric-table.h>
#include <apex/core/apex-metric-table-header.h>
#include <apex/core/apex-module.h>
#include <apex/core/apex-object.h>
#include <apex/core/apex-property.h>
#include <apex/core/apex-service.h>
#include <apex/core/apex-sink.h>
#include <apex/core/apex-source.h>
#include <apex/core/apex-status.h>
#include <apex/core/apex-system.h>

// State related types
#include <apex/state/apex-state.h>
#include <apex/state/apex-state-machine.h>
#include <apex/state/apex-transition.h>
#include <apex/state/apex-transition-table.h>

// Messages
#include <apex/message/apex-channel-request.h>
#include <apex/message/apex-channel-response.h>
#include <apex/message/apex-channels-response.h>
#include <apex/message/apex-configuration-request.h>
#include <apex/message/apex-configuration-response.h>
#include <apex/message/apex-configurations-response.h>
#include <apex/message/apex-empty.h>
#include <apex/message/apex-event-request.h>
#include <apex/message/apex-event-response.h>
#include <apex/message/apex-job-request.h>
#include <apex/message/apex-job-response.h>
#include <apex/message/apex-jobs-response.h>
#include <apex/message/apex-job-status-response.h>
#include <apex/message/apex-message-error.h>
#include <apex/message/apex-module-event-request.h>
#include <apex/message/apex-module-job-request.h>
#include <apex/message/apex-module-request.h>
#include <apex/message/apex-module-response.h>
#include <apex/message/apex-modules-response.h>
#include <apex/message/apex-property-request.h>
#include <apex/message/apex-property-response.h>
#include <apex/message/apex-properties-request.h>
#include <apex/message/apex-properties-response.h>
#include <apex/message/apex-request.h>
#include <apex/message/apex-response.h>
#include <apex/message/apex-settings-request.h>
#include <apex/message/apex-settings-response.h>
#include <apex/message/apex-status-request.h>
#include <apex/message/apex-status-response.h>

G_END_DECLS

#undef APEX_INSIDE
